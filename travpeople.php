<?php include("header.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div>
</div>
<div class="clear"></div>
<div class="container page_container">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout">
      <div class="main-content with-lmenu sub-page peopleknow-page">
         <div class="combined-column">
            <span class="mob-title">Suggested connections</span>
            <div class="content-box bshadow">
               <div class="cbox-title">						
                  Suggested connections
               </div>
               <div class="cbox-desc">
                  <div class="connections-grid">
                     <div class="row">
                        <div class="grid-box">
                           <input type="hidden" name="to_id" id="to_id">
                           <div class="connect-box">
                              <div class="imgholder online-img">
                                 <img src="images/connections-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                 <span>Bhadresh Ramani</span>
                                 </a>
                                 <span class="designation">Suggested by Bhadresh Ramani</span>
                                 <div class="btn-area travconnections_<?=$frdetails['_id']?>">
                                    <a href="javascript:void(0)" class="gray-text-555">
                                       <i class="mdi mdi-account-plus"></i>
                                       <i class="dis-none mdi mdi-account-minus"></i>
                                    </a>
                                    <a href="javascript:void(0)" class="tb-pyk-remove"></a>
                                 </div>
                                 
                              </div>
                           </div>
                        </div>
                        <div class="grid-box">
                           <input type="hidden" name="to_id" id="to_id">
                           <div class="connect-box">
                              <div class="imgholder online-img">
                                 <img src="images/connections-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                 <span>Bhadresh Ramani</span>
                                 </a>
                                 <span class="designation">Suggested by Bhadresh Ramani</span>
                                 <div class="btn-area travconnections_<?=$frdetails['_id']?>">
                                    <a href="javascript:void(0)" class="gray-text-555">
                                    <i class="dis-none mdi mdi-account-plus"></i>	
                                    <i class="mdi mdi-account-minus"></i>
                                    </a>
                                    <a href="javascript:void(0)" class="tb-pyk-remove"></a>
                                 </div>
                                 
                              </div>
                           </div>
                        </div>
                        <div class="grid-box">
                           <input type="hidden" name="to_id" id="to_id">
                           <div class="connect-box">
                              <div class="imgholder">
                                 <img src="images/connections-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                 <span>Bhadresh Ramani</span>
                                 </a>
                                 <span class="designation">Suggested by Bhadresh Ramani</span>
                                 <div class="btn-area travconnections_<?=$frdetails['_id']?>">
                                    <a href="javascript:void(0)" class="dropdown-button gray-text-555" data-activates='travpeople'>
                                    <i class="dis-none mdi mdi-account-plus"></i>	
                                    <i class="dis-none mdi mdi-account-minus"></i>
                                    <i class="mdi mdi-chevron-down"></i>
                                    </a> 
                                    <!-- Dropdown Structure -->
                                    <ul id='travpeople' class='dropdown-content'>
                                       <li><a href="#!">Unconnect</a></li>
                                       <li><a href="#!">View all</a></li>
                                       <li><a href="#!">Suggest connections</a></li>
                                       <li><a href="#!">Mute Notifications</a></li>
                                       <li><a href="#!">Chat</a></li>
                                       <li><a href="#!">Send gift</a></li>
                                       <li><a href="#!">Block</a></li>
                                    </ul>
                                    <a href="javascript:void(0)" class="tb-pyk-remove"></a>
                                 </div>
                                 
                              </div>
                           </div>
                        </div>
                        <div class="grid-box">
                           <input type="hidden" name="to_id" id="to_id">
                           <div class="connect-box">
                              <div class="imgholder">
                                 <img src="images/connections-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                 <span>Bhadresh Ramani</span>
                                 </a>
                                 <span class="designation">Suggested by Bhadresh Ramani</span>
                                 <div class="btn-area travconnections_<?=$frdetails['_id']?>">
                                    <a href="javascript:void(0)" class="gray-text-555">
                                    <i class="mdi mdi-account-plus"></i>
                                    <i class="dis-none mdi mdi-account-minus"></i>
                                    </a>
                                    <a href="javascript:void(0)" class="tb-pyk-remove"></a>
                                 </div>
                                 
                              </div>
                           </div>
                        </div>
                        <div class="grid-box">
                           <input type="hidden" name="to_id" id="to_id">
                           <div class="connect-box">
                              <div class="imgholder">
                                 <img src="images/connections-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                 <span>Bhadresh Ramani</span>
                                 </a>
                                 <span class="designation">Suggested by Bhadresh Ramani</span>
                                 <div class="btn-area travconnections_<?=$frdetails['_id']?>">
                                    <a href="javascript:void(0)" class="gray-text-555">
                                    <i class="mdi mdi-account-plus"></i>
                                    <i class="dis-none mdi mdi-account-minus"></i>
                                    </a>
                                    <a href="javascript:void(0)" class="tb-pyk-remove"></a>
                                 </div>
                                 
                              </div>
                           </div>
                        </div>
                        <div class="grid-box">
                           <input type="hidden" name="to_id" id="to_id">
                           <div class="connect-box">
                              <div class="imgholder">
                                 <img src="images/connections-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                 <span>Bhadresh Ramani</span>
                                 </a>
                                 <span class="designation">Suggested by Bhadresh Ramani</span>
                                 <div class="btn-area travconnections_<?=$frdetails['_id']?>">
                                    <a href="javascript:void(0)" class="gray-text-555">
                                    <i class="mdi mdi-account-plus"></i>
                                    <i class="dis-none mdi mdi-account-minus"></i>
                                    </a>
                                    <a href="javascript:void(0)" class="tb-pyk-remove"></a>
                                 </div>
                                 
                              </div>
                           </div>
                        </div>
                        <div class="grid-box">
                           <input type="hidden" name="to_id" id="to_id">
                           <div class="connect-box">
                              <div class="imgholder">
                                 <img src="images/connections-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                 <span>Bhadresh Ramani</span>
                                 </a>
                                 <span class="designation">Suggested by Bhadresh Ramani</span>
                                 <div class="btn-area travconnections_<?=$frdetails['_id']?>">
                                    <a href="javascript:void(0)" class="gray-text-555">
                                    <i class="mdi mdi-account-plus"></i>
                                    <i class="dis-none mdi mdi-account-minus"></i>
                                    </a>
                                    <a href="javascript:void(0)" class="tb-pyk-remove"></a>
                                 </div>
                                 
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <span class="mob-title">Connect requests</span>
            <div class="content-box bshadow">
               <div class="cbox-title">
                  Respond to your connect request
               </div>
               <div class="cbox-desc">
                  <div class="connections-grid freq">
                     <div class="row">
                        <div class="grid-box" id="">
                           <input type="hidden" name="to_id" id="to_id" value="">
                           <div class="connect-box">
                              <div class="imgholder">
                                 <img src="images/connections-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                 <span>Bhadresh Ramani</span>
                                 </a>                                           
                                 <span class="info">Ahmedabad</span>
                                 <span class="info mutual">15 Mutual Connections</span>
                                 
                                 <div class="dropdown dropdown-custom mbl-menu">
                                    <a href="javascript:void(0)" class="dropdown-toggle gray-text-555" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="mdi mdi-chevron-down"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                       <li><a class="btn btn-primary btn-sm accept-connect" href="javascript:void(0)">Accept</a></li>
                                       <li><a class="btn btn-primary btn-sm delete-connect btn-gray" href="javascript:void(0)">Delete</a></li>
                                    </ul>
                                 </div>
                                 <div class="btn-area btns-holder desc-menu">
                                    <div class="req-btn">
                                       <button class="btn btn-primary btn-sm delete-connect btn-gray">Delete</button>
                                       <button class="btn btn-primary btn-sm accept-connect">Confirm</button>
                                    </div>
                                    <div class="showlabel">
                                       <div class="dropdown dropdown-custom req-btn">
                                          <a href="javascript:void(0)" class="dropdown-toggle gray-text-555" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                          <i class="mdi mdi-chevron-down"></i>
                                          </a>
                                          <ul class="dropdown-menu fetchconnectmenu">
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="grid-box" id="">
                           <input type="hidden" name="to_id" id="to_id" value="">
                           <div class="connect-box">
                              <div class="imgholder">
                                 <img src="images/connections-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                 <span>Bhadresh Ramani</span>
                                 </a>                                           
                                 <span class="info">Ahmedabad</span>
                                 <span class="info mutual">15 Mutual Connections</span>
                                 
                                 <div class="dropdown dropdown-custom mbl-menu">
                                    <a href="javascript:void(0)" class="dropdown-toggle gray-text-555" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="mdi mdi-chevron-down"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                       <li><a href="javascript:void(0)">Accept</a></li>
                                       <li><a href="javascript:void(0)">Delete</a></li>
                                    </ul>
                                 </div>
                                 <div class="btn-area btns-holder desc-menu">
                                    <div class="req-btn">
                                       <button class="btn btn-primary btn-sm delete-connect btn-gray">Delete</button>
                                       <button class="btn btn-primary btn-sm accept-connect">Confirm</button>
                                    </div>
                                    <div class="showlabel">
                                       <div class="dropdown dropdown-custom req-btn">
                                          <a href="javascript:void(0)" class="dropdown-toggle gray-text-555" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                          <i class="mdi mdi-chevron-down"></i>
                                          </a>
                                          <ul class="dropdown-menu fetchconnectmenu">
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="grid-box" id="">
                           <input type="hidden" name="to_id" id="to_id" value="">
                           <div class="connect-box">
                              <div class="imgholder">
                                 <img src="images/connections-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                 <span>Bhadresh Ramani</span>
                                 </a>                                           
                                 <span class="info">Ahmedabad</span>
                                 <span class="info mutual">15 Mutual Connections</span>
                                 
                                 <div class="dropdown dropdown-custom mbl-menu">
                                    <a href="javascript:void(0)" class="dropdown-toggle gray-text-555" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="mdi mdi-chevron-down"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                       <li><a href="javascript:void(0)">Accept</a></li>
                                       <li><a href="javascript:void(0)">Delete</a></li>
                                    </ul>
                                 </div>
                                 <div class="btn-area btns-holder desc-menu">
                                    <div class="req-btn">
                                       <button class="btn btn-primary btn-sm delete-connect btn-gray">Delete</button>
                                       <button class="btn btn-primary btn-sm accept-connect">Confirm</button>
                                    </div>
                                    <div class="showlabel">
                                       <div class="dropdown dropdown-custom req-btn">
                                          <a href="javascript:void(0)" class="dropdown-toggle gray-text-555" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                          <i class="mdi mdi-chevron-down"></i>
                                          </a>
                                          <ul class="dropdown-menu fetchconnectmenu">
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="grid-box" id="">
                           <input type="hidden" name="to_id" id="to_id" value="">
                           <div class="connect-box">
                              <div class="imgholder">
                                 <img src="images/connections-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                 <span>Bhadresh Ramani</span>
                                 </a>                                           
                                 <span class="info">Ahmedabad</span>
                                 <span class="info mutual">15 Mutual Connections</span>
                                 
                                 <div class="dropdown dropdown-custom mbl-menu">
                                    <a href="javascript:void(0)" class="dropdown-toggle gray-text-555" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="mdi mdi-chevron-down"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                       <li><a href="javascript:void(0)">Accept</a></li>
                                       <li><a href="javascript:void(0)">Delete</a></li>
                                    </ul>
                                 </div>
                                 <div class="btn-area btns-holder desc-menu">
                                    <div class="req-btn">
                                       <button class="btn btn-primary btn-sm delete-connect btn-gray">Delete</button>
                                       <button class="btn btn-primary btn-sm accept-connect">Confirm</button>
                                    </div>
                                    <div class="showlabel">
                                       <div class="dropdown dropdown-custom req-btn">
                                          <a href="javascript:void(0)" class="dropdown-toggle gray-text-555" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                          <i class="mdi mdi-chevron-down"></i>
                                          </a>
                                          <ul class="dropdown-menu fetchconnectmenu">
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="grid-box" id="">
                           <input type="hidden" name="to_id" id="to_id" value="">
                           <div class="connect-box">
                              <div class="imgholder">
                                 <img src="images/connections-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                 <span>Bhadresh Ramani</span>
                                 </a>                                           
                                 <span class="info">Ahmedabad</span>
                                 <span class="info mutual">15 Mutual Connections</span>
                                 
                                 <div class="dropdown dropdown-custom mbl-menu">
                                    <a href="javascript:void(0)" class="dropdown-toggle gray-text-555" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="mdi mdi-chevron-down"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                       <li><a href="javascript:void(0)">Accept</a></li>
                                       <li><a href="javascript:void(0)">Delete</a></li>
                                    </ul>
                                 </div>
                                 <div class="btn-area btns-holder desc-menu">
                                    <div class="req-btn">
                                       <button class="btn btn-primary btn-sm delete-connect btn-gray">Delete</button>
                                       <button class="btn btn-primary btn-sm accept-connect">Confirm</button>
                                    </div>
                                    <div class="showlabel">
                                       <div class="dropdown dropdown-custom req-btn">
                                          <a href="javascript:void(0)" class="dropdown-toggle gray-text-555" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                          <i class="mdi mdi-chevron-down"></i>
                                          </a>
                                          <ul class="dropdown-menu fetchconnectmenu">
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="grid-box" id="">
                           <input type="hidden" name="to_id" id="to_id" value="">
                           <div class="connect-box">
                              <div class="imgholder">
                                 <img src="images/connections-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                 <span>Bhadresh Ramani</span>
                                 </a>                                           
                                 <span class="info">Ahmedabad</span>
                                 <span class="info mutual">15 Mutual Connections</span>
                                 
                                 <div class="dropdown dropdown-custom mbl-menu">
                                    <a href="javascript:void(0)" class="dropdown-toggle gray-text-555" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="mdi mdi-chevron-down"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                       <li><a href="javascript:void(0)">Accept</a></li>
                                       <li><a href="javascript:void(0)">Delete</a></li>
                                    </ul>
                                 </div>
                                 <div class="btn-area btns-holder desc-menu">
                                    <div class="req-btn">
                                       <button class="btn btn-primary btn-sm delete-connect btn-gray">Delete</button>
                                       <button class="btn btn-primary btn-sm accept-connect">Confirm</button>
                                    </div>
                                    <div class="showlabel">
                                       <div class="dropdown dropdown-custom req-btn">
                                          <a href="javascript:void(0)" class="dropdown-toggle gray-text-555" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                          <i class="mdi mdi-chevron-down"></i>
                                          </a>
                                          <ul class="dropdown-menu fetchconnectmenu">
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="grid-box" id="">
                           <input type="hidden" name="to_id" id="to_id" value="">
                           <div class="connect-box">
                              <div class="imgholder">
                                 <img src="images/connections-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                 <span>Bhadresh Ramani</span>
                                 </a>                                           
                                 <span class="info">Ahmedabad</span>
                                 <span class="info mutual">15 Mutual Connections</span>
                                 <div class="dropdown dropdown-custom mbl-menu">
                                    <a href="javascript:void(0)" class="dropdown-toggle gray-text-555" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="mdi mdi-chevron-down"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                       <li><a href="javascript:void(0)">Accept</a></li>
                                       <li><a href="javascript:void(0)">Delete</a></li>
                                    </ul>
                                 </div>
                                 <div class="btn-area btns-holder desc-menu">
                                    <div class="">
                                       <button class="btn btn-primary btn-sm delete-connect btn-gray">Delete</button>
                                       <button class="btn btn-primary btn-sm accept-connect">Confirm</button>
                                    </div>
                                    <div class="showlabel">
                                       <div class="dropdown dropdown-custom req-btn">
                                          <a href="javascript:void(0)" class="dropdown-toggle gray-text-555" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                          <i class="mdi mdi-chevron-down"></i>
                                          </a>
                                          <ul class="dropdown-menu fetchconnectmenu">
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <span class="mob-title">People you may know</span>
            <?php include('common/people_you_may_know.php'); ?>
         </div>
         <?php include('common/chat.php'); ?>	
      </div>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>	
<?php include("script.php"); ?>
</body>
</html>