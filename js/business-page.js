var owl = $('.owl-carousel');
owl.owlCarousel({
margin: 10,
loop: false,		
dots: true,			
nav: true,
responsive: {
  0: {
	items: 1
  },
  600: {
	items: 2
  },
  1000: {
	items: 3
  }
}
})

$(document).ready(function() {			
	$('.new-tumb').carousel({
		dist:0,
		shift:0,
		padding:20,
	});

	$('.carousel_master .carousel').carousel();
 	
 	$(".action-icon.activity-link").click(function(){
 		$(".settings-content").addClass("showactivity");
		$(".tab-pane.main-tabpane.active").hide();
		$(".custom-wall-links .tab a").removeClass("active");
		setTimeout(function() { 
			$width = $(window).width();
			if($width <767) {
				$('.mainpage-name').hide();
				$('.innerpage-name').show().html('Page');
				$('.mobile-menu.topicon').find('.gotohome').show()
				$('.mobile-menu.topicon').find('.mbl-menuicon1').hide();
				$('.mobile-menu.topicon').find('.gotohome').find('i').removeClass().addClass('mdi mdi-arrow-left');
			} else {
				$('.innerpage-name').hide();
				$('.mainpage-name').hide();
				$('.mobile-menu.topicon').find('.mbl-menuicon1').show();
				$('.mobile-menu.topicon').find('.gotohome').hide();
				$('.tabs-detail').removeClass('gone');
				$('.tabs-detail').addClass('gone');
			}
	    }, 400);
	});
	 	
	$(".custom-wall-links .tab a").click(function(){
		$(".settings-content").removeClass("showactivity");
	});
	$("#upload_img_action").click(function(){
		$(".cover-slider").removeClass("openDrawer");			
	});		 
});
