var postBtnEle = $(".postbtn");	
var postbtn_hide = "HIDE";
var postbtn_show = "SHOW";
var lastModified = [];
var newct=0;
var lastSearchSection = "desktop";
var storedFiles = [];
var $post_privacy_array = ['Private', 'Connections', 'Custom', 'Public', 'Enabled', 'Disabled'];

function listCookies() {
	var theCookies = document.cookie.split(';');
	var aString = '';
	for (var i = 1 ; i <= theCookies.length; i++) {
		aString += i + ' ' + theCookies[i-1] + "\n";
	}
}

/* pagination demo */
function createDemo(name) {
	var container = $('#pagination-' + name);
	var sources = function () {
		var result = [];

		for (var i = 1; i < 196; i++) {
			result.push(i);
		}

		return result;
	}();

	var options = {
		dataSource: sources,
		callback: function (response, pagination) {
			window.console && console.log(response, pagination);

			var dataHtml = '<ul>';
			var m=0;
			$.each(response, function (index, item) {
				m++;
				var liblock="<div class='person-holder'><img src='images/people-1.png' /><div class='person-info'><h6>Mark Semuel</h6><span>New Zealand</span></div></div><div class='msg-holder'><span class='trip-duration'><span>Travel buddy to <b>Paris</b> :</span> <span class='tripdates'>Sun Nov 6 - Tue Nov 8</span><div class='settings-icon'><a class='dropdown-button more_btn' href='#' data-activates='offer_menu_"+m+"'><i class='mdi mdi-dots-horizontal'></i></a><ul id='offer_menu_"+m+"' class='dropdown-content custom_dropdown'><li><a href='javascript:void(0)' onclick='generateDiscard(\"dis_deloffer\")'>Delete this request</a></li><li><a href='javascript:void(0)' onclick='generateDiscard(\"dis_blockoffer\")'>Block this person request</a></li><li><a href='javascript:void(0)'>Report this request</a></li></ul></div></span><a class='offer-holder' href='javascript:void(0)' onclick='openOffer(this)'><div class='msg-bbl'><p>Dear Connect, Nice to meet you. I'm Hayi from Qatar. We will arrive in Qatar on 6 Nov, 2016. Would you be able to provide us hosting at your place?</p></div></a><span class='timestamp'>about a month ago</span></div>";
				dataHtml += '<li>' + liblock + '</li>';
			});

			dataHtml += '</ul>';

			container.prev().html(dataHtml);
			initDropdown();
		}
	};

	//$.pagination(container, options);

	container.addHook('beforeInit', function () {
		window.console && console.log('beforeInit...');
	});
	container.pagination(options);

	container.addHook('beforePageOnClick', function () {
		window.console && console.log('beforePageOnClick...');
		//return false
	});

	return container;
}
/* end pagination demo */

/************ ESSENTIALS FUNCTIONS ************/

/* FUN manage sliding pan - profile tip location */
	function manageSlidingPan(obj,which){
		var super_parent=$(obj).parents(".slidingpan-holder");
		if(super_parent.find(".sliding-pan."+which).hasClass("openSlide"))
			super_parent.find(".sliding-pan."+which).removeClass("openSlide");
		else		
			super_parent.find(".sliding-pan."+which).addClass("openSlide");		
	}
/* FUN end manage sliding pan - profile tip location */

/* FUN set mobile menu */
	function setMobileMenu(state){
		
		var isMobileMenu=$(".sidemenu-holder").hasClass("m-hide");
		if(isMobileMenu){
			if(state == 'open') {
				if(!$(".sidemenu-holder").hasClass("m-open")) {					
					$(".sidemenu-holder").addClass("m-hide m-open");
				}
			} else if( state == 'alt') {
				if($(".sidemenu-holder").hasClass("m-open")) {
					$(".sidemenu-holder").removeClass("m-open");					
				} else {					
					$(".sidemenu-holder").addClass("m-hide m-open");
				}
			} else {
				$(".sidemenu-holder").removeClass("m-open");				
			}
		}
		
		var isTabMenu=$(".sidemenu-holder").hasClass("tab-hide");		
		if(isTabMenu){	
			if(state == 'open') {
				if(!$(".sidemenu-holder").hasClass("tab-open")) {					
					$(".sidemenu-holder").addClass("tab-hide tab-open");
				}
			} else if( state == 'alt') {
				if($(".sidemenu-holder").hasClass("tab-open")) {
					$(".sidemenu-holder").removeClass("tab-open");					
				} else {					
					$(".sidemenu-holder").addClass("tab-hide tab-open");
				}
			} else {
				$(".sidemenu-holder").removeClass("tab-open");				
			}
		}
		setTimeout(function(){ initNiceScroll(".nice-scroll");},400);
	}
	
/* FUN end set mobile menu */

/* FUN set left menu */
	function setLeftMenu(){
		
		var w_width=$(window).width();
		if(!$(".page-wrapper").hasClass("hidemenu-wrapper")){			
		
			if(w_width>1024){
				$(".sidemenu-holder").removeClass("m-hide");
				$(".sidemenu-holder").removeClass("m-open");
				$(".sidemenu-holder").addClass("open");
				$(".with-lmenu").removeClass("m-hide");
				$(".with-lmenu").addClass("open");		

				$(".sidemenu-holder").removeClass("tab-hide");
				$(".sidemenu-holder").removeClass("tab-open");
			}
			else if(w_width>=768 && w_width<1024){	
				
				$(".sidemenu-holder").removeClass("m-hide");
				$(".sidemenu-holder").removeClass("m-open");
				$(".sidemenu-holder").removeClass("open");
				$(".with-lmenu").removeClass("m-hide");
				$(".with-lmenu").removeClass("open");
									
				$(".sidemenu-holder").addClass("tab-hide");						
			}	
			else{		
				$(".sidemenu-holder").removeClass("open");
				$(".sidemenu-holder").removeClass("tab-hide");
				$(".sidemenu-holder").removeClass("tab-open");
				$(".sidemenu-holder").addClass("m-hide");
				$(".with-lmenu").removeClass("open");		
				$(".with-lmenu").addClass("m-hide");

			}		
		}
	}
/* FUN end set left menu */

/* FUN close all floating drawer */
	function closeAllSideDrawer(which){
		
		$(".btnbox").each(function(){			
			var cls=$(this).attr("class");
			cls=cls.replace("box-open","").trim();		
			
			if(cls!=which){
				$(this).removeClass("box-open");
			}
		});	
	}
/* FUN end close all floating drawer */

/* FUN manage search section */		
	function closeSearchSection(e) {
		var trgt = $('.search-holder');
		
		if (!trgt.is(e.target) & trgt.has(e.target).length === 0) {			
			closeSearch();
		}
	
	}
	function openSearch(){	

		var main_sparent=$(".main-sholder");		
				
		if(!main_sparent.hasClass("keepopen")){
			
			main_sparent.addClass("opened");						
			main_sparent.find(".search-input").css("display", "block");						
			main_sparent.find(".search-input").css("width", "100%");						
			main_sparent.find(".search-input").css("background-color", "#fff");						
			main_sparent.find(".search-section").css("width", "100%");						
			main_sparent.find(".search-section").addClass("opened");
						
			$(".search-section .search-input").focus();			
		}	
		setTimeout(function(){
			setSearchResult();			
		},400);		
		$('.search-result').getNiceScroll().resize();
	}
	function closeSearch(){
		
		var main_sparent=$(".main-sholder");		
		
		main_sparent.find(".search-result").slideUp();
		
		if(!main_sparent.hasClass("keepopen")){
		
			setTimeout(function(){
				
				main_sparent.removeClass("opened");				
				main_sparent.find(".search-input").css("width", "0%");				
				main_sparent.find(".search-section").css("width", "0%");				
				main_sparent.find(".search-section").removeClass("opened");
				
				setTimeout(function () {
					main_sparent.find(".search-input").css("display", "none");					
					main_sparent.find(".search-input").css("background-color", "rgba(0,0,0,0)");					
				}, 200);					
				
			},300);
		}
	}
	function manageSearch(obj){
		
		var super_parent=$(obj).parents(".search-holder");		
		
		var sh_hasClass = super_parent.hasClass("opened");
		
		if(!sh_hasClass){						
			openSearch();
		}
		else{			
			closeSearch();
		}		
	
	}
	function setSearchResult(){
		
		var main_sparent=$(".main-sholder");				
		var isDeskSearchText=main_sparent.find(".search-input").val().trim();
		
		if(isDeskSearchText!="" && !(isDeskSearchText == null)){			
		
			if(isDeskSearchText != '') {
				if(lastSearchSection=="desktop"){
					main_sparent.find(".search-input").val(isDeskSearchText);					
				}
			}
			else{
				if(isDeskSearchText != ''){				
					main_sparent.find(".search-input").val(isDeskSearchText);					
					lastSearchSection=="desktop";
				}
			}
						
			setTimeout(function(){				
				main_sparent.find(".search-result").slideDown();				
			},400);
		}
		else{			
			main_sparent.find(".search-result").hide();			
		}
	}
	
/* FUN end manage search section */

/* FUN reset add photo popup */	
	function resetAddPhotoPopup(){		
		$(".popup-area#add-photo-popup .post-photos .img-box").each(function(){$(this).remove();});
	}
/* FUN end reset add photo popup */

/* FUN close IE notice */
	function closeIENotice(){		
		$(".ienotice").fadeOut();
	}
/* FUN end close IE notice */

/* FUN loader hide */
	function hideLoader(){
		clearInterval(interval);
		
		setTimeout(function(){
			$('body').addClass("loaded");
						
		},500);
	}	
/* FUN end loader hide */

/* FUN init nice scroll */		
	function initNiceScroll(secName){
		winw=$(window).width();
		if(winw>1024){
			if(secName=="homebody"){
				$("body").niceScroll({horizrailenabled:false,cursorcolor:"#666",cursorwidth:"10px",cursorborderradius:"0",cursorborder:"0px solid #fff",background:"rgba(255,255,255,0.8)"});
				$("body").getNiceScroll(0).rail.addClass('bodyScroll');
			}else{			
				if($(secName).hasClass("chatlist-scroll") || $(secName).hasClass("recentchat-scroll"))
					$(secName).niceScroll({horizrailenabled:false,cursorcolor:"#bbb",cursorwidth:"6px",cursorborderradius:"0",cursorborder:"0px solid #fff",background:"rgba(255,255,255,0.6)"});
				
				else if($(secName).parents(".invite-holder").length > 0 || $(secName).hasClass(".dest-list"))
					$(secName).niceScroll({horizrailenabled:false,cursorcolor:"#bbb",cursorwidth:"6px",cursorborderradius:"0",cursorborder:"0px solid #fff",background:"rgba(255,255,255,0.6)"});
					
				else
					$(secName).niceScroll({horizrailenabled:false,cursorcolor:"#bbb",cursorwidth:"6px",cursorborderradius:"0",cursorborder:"0px solid #fff",background:"rgba(255,255,255,0.6)"});
				
				// set opacity of sidemenu
				if($(secName).hasClass("sidemenu")){
					setTimeout(function(){
						$(".sidemenu").css("opacity",1);
					},400);
				}
			}
		}else{
			$(".sidemenu").css("opacity",1);
		}
	}
	function removeNiceScroll(secName){
		
		if(secName=="homebody"){			
			$("body").getNiceScroll().remove();
		}
		else{
			$(secName).getNiceScroll().remove();			
		}
	}
		
/* FUN end init nice scroll */

/************ END ESSENTIALS FUNCTIONS ************/

/************ COMMON FUNCTIONS ************/

/* FUN manage expandable -photo slider : hotels/places : more info */	
	function initSubtabSlider(obj){
		
		 if($(obj).find("a").attr("data-tab")=="subtab-photos"){
			setTimeout(function() {
				
				var sparent = $(obj).parents(".explandable-tabs").find(".tab-pane.subtab-photos");
				// new slider 
				if(sparent.find(".photo-gallery").length>0){					
					fixImageUI("photo-gallery");
				}
				
			}, 400);
		}
	}
/* FUN end manage expandable -photo slider : hotels/places : more info */

/* open like popup */
	function openLikePopup(thisId){
	}
/* end open like popup */

/* open comment popup */
	function openCommentPopup(dataid, datasection){
	}
/* end open comment popup */

/* FUN search string entered */
	function searchStringEntered(e){
		
		var thisValue=$(this).val();
		
		var super_parent=$(this).parents(".search-holder");
		
		if(super_parent.hasClass("main-sholder")){			
			lastSearchSection = "desktop";
		}
					
		$(".main-sholder").find(".search-input").val(thisValue);		
		
	}
/* FUN end search string entered */

/* FUN fix image centering */
	function resizeToHeight(image_width,image_height, container_width) {
		
		var newWidth = container_width;
		var width = image_width;
		var proportion = newWidth/width;
		var newHeight =image_height  * proportion;

		return newHeight;
	}
	function resizeToWidth(image_width,image_height, container_height) {
		
		var newHeight = container_height;
		var height = image_height;
		var proportion = newHeight/height;
		var newWidth =image_width  * proportion;

		return newWidth;
	}
	function resetPopupImageFixClass(super_parent){
		
		if($(super_parent+" .grid-list").hasClass("photo-list")){
			$(super_parent+" .photo-list .grid-box").each(function(){
					
				if($(this).find(".photo-box").hasClass("imgfix"))
					$(this).find(".photo-box").removeClass("imgfix");
					
				$(this).find(".photo-box").find("img").css("margin-left","0");
				$(this).find(".photo-box").find("img").css("margin-top","0");
			});
		}		
	}
	function resetImageFixClass(super_parent){
		
		if(super_parent==".saved-content" || super_parent==".reviews-list"){
			
			$(super_parent+" .post-holder").each(function(){
					
				if($(this).find(".pimg-holder").hasClass("imgfix"))
					$(this).find(".pimg-holder").removeClass("imgfix");
					
				$(this).find(".pimg-holder").find("img").css("margin-left","0");
				$(this).find(".pimg-holder").find("img").css("margin-top","0");				
				$(this).find(".pimg-holder").find("img").css("opacity",0);
			});
		}
		if(super_parent==".activity-content"){
			
			$(super_parent+" .activity-post-detail").each(function(){
				if($(this).find(".post-holder").find(".pimg-holder").hasClass("imgfix"))
					$(this).find(".pimg-holder").removeClass("imgfix");
				
				$(this).find(".post-holder").find(".pimg-holder").find("img").css("margin-left","0");
				$(this).find(".post-holder").find(".pimg-holder").find("img").css("margin-top","0");				
				$(this).find(".post-holder").find(".pimg-holder").find("img").css("opacity",0);
			});
		}
		
	}
	function resetReviewPostImageFixClass(){
		
		$(".reviews-column .post-holder").each(function(){
				
			if($(this).find(".pimg-holder").hasClass("imgfix"))
				$(this).find(".pimg-holder").removeClass("imgfix");
				
			$(this).find(".pimg-holder").find("img").css("margin-left","0");
			$(this).find(".pimg-holder").find("img").css("margin-top","0");				
			$(this).find(".pimg-holder").find("img").css("opacity",0);
		});
		
	}
	function fixImageUIPopup(){

		resetPopupImageFixClass(".pic-change-popup");
		
		setTimeout(function(){
			var gb_count=0;
				
			if($(".pic-change-popup .choosephoto .tab-pane .grid-list").hasClass("photo-list")){
				$(".pic-change-popup .choosephoto .tab-pane").each(function(){
					
					if(!$(this).hasClass("visited")){
						$(this).each(".grid-box",function(){
							
							gb_count++;
						
							var cont_width=$(this).width();
							var cont_height=$(this).height();
							var img_width=$(this).find("img").width();
							var img_height=$(this).find("img").height();
							
							if($(this).find(".photo-box").hasClass("himg-box")){
								if(img_width < cont_width){
									$(this).find(".photo-box").addClass("imgfix");
								}
							} 
							
							if(img_width > cont_width){
								
								var iwidth = resizeToWidth(img_width,img_height,cont_height);
								var lfix= ( iwidth - cont_width ) / 2;
								$(this).find(".photo-box").find("img").css("margin-left","-"+lfix+"px");							
							}
							
							
							if(img_height > cont_height || $(this).hasClass("imgfix")){
								var iheight = resizeToHeight(img_width,img_height,cont_width);
								var tfix= ( iheight - cont_height ) / 2;
								$(this).find(".photo-box").find("img").css("margin-top","-"+tfix+"px");							
							}
							
						});
					}
				});
			
			}
		},300);
	}
	function resetMessageImages(super_parent){
		
		$(super_parent+" .albums-grid .grid-box").each(function(){
				
			if($(this).find(".photo-box").hasClass("imgfix"))
				$(this).find(".photo-box").removeClass("imgfix");
				
			$(this).find(".photo-box").find("img").css("margin-left","0");
			$(this).find(".photo-box").find("img").css("margin-top","0");
		});
	
		
	}
	function fixMessageImagesPopup(){

		resetMessageImages(".photos-thread");
		
		setTimeout(function(){
		
			$(".photos-thread .albums-grid .grid-box").each(function(i, v){
					
				var cont_width=$(this).width();
				var cont_height=$(this).height();
				var img_width=$(this).find("img").width();
				var img_height=$(this).find("img").height();
				
				if($(this).find(".photo-box").hasClass("himg-box")){
					if(img_width < cont_width){
						$(this).find(".photo-box").addClass("imgfix");
					}
				} 
				
				if(img_width > cont_width){
					
					var iwidth = resizeToWidth(img_width,img_height,cont_height);
					var lfix= ( iwidth - cont_width ) / 2;
					$(this).find(".photo-box").find("img").css("margin-left","-"+lfix+"px");							
				}
				
				if(img_height > cont_height || $(this).hasClass("imgfix")){
					var iheight = resizeToHeight(img_width,img_height,cont_width);
					var tfix= ( iheight - cont_height ) / 2;
					$(this).find(".photo-box").find("img").css("margin-top","-"+tfix+"px");							
				}	
				
			});		
		
		},300);
		
	}
	function fixPostImages(which){
		
		var mainParent=$(".post-holder#"+which);
		if(mainParent.find(".post-img").length > 0 ){				
			
			mainParent.find(".pimg-holder").each(function(e, v){
				
				var cont_width=$(v).width();
				var cont_height=$(v).height();
				var img_width=$(v).find("img").width();
				var img_height=$(v).find("img").height();
									
				if($(v).hasClass("himg-box")){
					if(img_width < cont_width){
						$(v).addClass("imgfix");
					}
				} 
				
				if(img_width > cont_width){
					
					var iwidth = resizeToWidth(img_width,img_height,cont_height);
					var lfix= ( iwidth - cont_width ) / 2;
					$(v).find("img").css("margin-left","-"+lfix+"px");							
				}
				
				
				if(img_height > cont_height || $(v).hasClass("imgfix")){
					var iheight = resizeToHeight(img_width,img_height,cont_width);
					var tfix= ( iheight - cont_height ) / 2;
					$(v).find("img").css("margin-top","-"+tfix+"px");							
				}
				$(v).find("img").css({visibility:"visible", opacity: 0.0}).animate({opacity: 1.0},200);
			});
		
		}
		
	}
	function resetActivityImages(){
		
		$(".activity-content .activity-box").each(function(){
				
			if($(this).find(".pimg-holder").hasClass("imgfix"))
				$(this).find(".pimg-holder").removeClass("imgfix");
				
			$(this).find(".pimg-holder").find("img").css("margin-left","0");
			$(this).find(".pimg-holder").find("img").css("margin-top","0");
		});		
	}
	function resetPhotoGalleryImageFixClass(){
		
		$(".subtab-photos.active .photo-gallery .thumbs-img ul > li").each(function(){
				
			if($(this).find("a").hasClass("imgfix"))
				$(this).find("a").removeClass("imgfix");
				
			$(this).find("a").find("img").css("margin-left","0");
			$(this).find("a").find("img").css("margin-top","0");
		});		
	}

	function wallPhotoAlbumsSliderFixClass(){
		
		if($(".albums-area").css("display")=="block" && $(".albums-area").length>0){
			$(".albums-area .album-box").each(function(){
					
				if($(this).find("a").hasClass("imgfix"))
					$(this).find("a").removeClass("imgfix");
					
				$(this).find("a").find("img").css("margin-left","0");
				$(this).find("a").find("img").css("margin-top","0");
			});		
			
		}
		if($(".photos-area").css("display")=="block" && $(".photos-area").length>0){
			$(".photos-area .photo-box").each(function(){
					
				if($(this).find("a").hasClass("imgfix"))
					$(this).find("a").removeClass("imgfix");
					
				$(this).find("a").find("img").css("margin-left","0");
				$(this).find("a").find("img").css("margin-top","0");
			});		
			
		}
				
	}

	function wallImageSliderFixClass(){
		
		$(".user-photos ul.lightSlider > li").each(function(){
				
			if($(this).find("a").hasClass("imgfix"))
				$(this).find("a").removeClass("imgfix");
				
			$(this).find("a").find("img").css("margin-left","0");
			$(this).find("a").find("img").css("margin-top","0");
		});		
	}

	/*  responsible for centering images after upload */
	function fixImageUI(which){
		
		var pimgholder_count=0;	
		var pcount=0;
	
		/* post image fixes */		
		
		$(".post-list .post-holder").each(function(){
			pcount++;
			
			if($(this).hasClass("reviewpost-holder")){
				resetReviewPostImageFixClass();
			}
			
			//if((which=="newpost" && pcount==1) || (!which)){
			if((which=="newpost") || (!which)){
				
				if($(this).find(".post-img").length > 0 ){				
					pimgholder_count++;
					
					$(this).find(".pimg-holder").each(function(e, v){
						
						var cont_width=$(v).width();
						var cont_height=$(v).height();
						var img_width=$(v).find("img").width();
						var img_height=$(v).find("img").height();
						
						if($(v).hasClass("himg-box")){										
							
							if(img_width < cont_width){							
								$(v).addClass("imgfix");
							}						
						} 
						
						if(img_width > cont_width){
																			
							var iwidth = resizeToWidth(img_width,img_height,cont_height);
							var lfix= ( iwidth - cont_width ) / 2;
							$(v).find("img").css("margin-left","-"+lfix+"px");							
						}
						
						
						if(img_height > cont_height || $(v).hasClass("imgfix")){
							var iheight = resizeToHeight(img_width,img_height,cont_width);
							var tfix= ( iheight - cont_height ) / 2;
							$(v).find("img").css("margin-top","-"+tfix+"px");							
						}
						$(v).find("img").css({visibility:"visible", opacity: 0.0}).animate({opacity: 1.0},200);
					});
				
				}
			}
			//if(which=="newpost" && pcount==1){return false;}
			
		});
		
		/* end post image fixes */
		
		/* fix images inside popup - share */
		if(which=="popup-images"){
			pcount=0;
			$(".modal .post-list .post-holder").each(function(){
				pcount++;
				
				if($(this).hasClass("reviewpost-holder")){
					resetReviewPostImageFixClass();
				}
				
				if(pcount==1){
					
					if($(this).find(".post-img").length > 0 ){				
						pimgholder_count++;
						
						$(this).find(".pimg-holder").each(function(e, v){
							
							var cont_width=$(v).width();
							var cont_height=$(v).height();
							var img_width=$(v).find("img").width();
							var img_height=$(v).find("img").height();
							
							if($(v).hasClass("himg-box")){										
								
								if(img_width < cont_width){							
									$(v).addClass("imgfix");
								}						
							} 
							
							if(img_width > cont_width){
																				
								var iwidth = resizeToWidth(img_width,img_height,cont_height);
								var lfix= ( iwidth - cont_width ) / 2;
								$(v).find("img").css("margin-left","-"+lfix+"px");							
							}
							
							
							if(img_height > cont_height || $(v).hasClass("imgfix")){
								var iheight = resizeToHeight(img_width,img_height,cont_width);
								var tfix= ( iheight - cont_height ) / 2;
								$(v).find("img").css("margin-top","-"+tfix+"px");							
							}
							$(v).find("img").css({visibility:"visible", opacity: 0.0}).animate({opacity: 1.0},200);
						});
					
					}
				}
				if(which=="newpost" && pcount==1){return false;}
				
			});
			
		}
		/* end fix images inside popup - share */
		
		/* reivew post image fixes */
				
		if($("body").find(".reviewpost-tab").css("display")=="block" || $("body").find(".reviewphoto-tab").css("display")=="block"){
			
			resetImageFixClass(".reviews-list");
			/* post image fixes */		
			$(".reviews-list .post-holder").each(function(){
								
				if($(this).find(".post-img").length > 0 ){				
					pimgholder_count++;
					
					$(this).find(".pimg-holder").each(function(e, v){
						
						var cont_width=$(v).width();
						var cont_height=$(v).height();
						var img_width=$(v).find("img").width();
						var img_height=$(v).find("img").height();
											
						if($(v).hasClass("himg-box")){
							if(img_width < cont_width){
								$(v).addClass("imgfix");
							}
						} 
						
						if(img_width > cont_width){
							
							var iwidth = resizeToWidth(img_width,img_height,cont_height);
							var lfix= ( iwidth - cont_width ) / 2;
							$(v).find("img").css("margin-left","-"+lfix+"px");							
						}
						
						if(img_height > cont_height || $(v).hasClass("imgfix")){
							var iheight = resizeToHeight(img_width,img_height,cont_width);
							var tfix= ( iheight - cont_height ) / 2;
							$(v).find("img").css("margin-top","-"+tfix+"px");							
						}
						$(v).find("img").animate({opacity: 1},2000);
					});
				
				}			

			});
		}
		
		/* end reivew post image fixes */
		
		/* saved post image fixes */
				
		pimgholder_count=0;	
		pcount=0;
		if($("body").find(".saved-content").css("display")=="block"){
			
			resetImageFixClass(".saved-content");
			/* post image fixes */		
			$(".saved-content .post-holder").each(function(){
								
				if($(this).find(".post-img").length > 0 ){				
					pimgholder_count++;
					
					$(this).find(".pimg-holder").each(function(e, v){
						
						var cont_width=$(v).width();
						var cont_height=$(v).height();
						var img_width=$(v).find("img").width();
						var img_height=$(v).find("img").height();
											
						if($(v).hasClass("himg-box")){
							if(img_width < cont_width){
								$(v).addClass("imgfix");
							}
						} 
						
						if(img_width > cont_width){
							
							var iwidth = resizeToWidth(img_width,img_height,cont_height);
							var lfix= ( iwidth - cont_width ) / 2;
							$(v).find("img").css("margin-left","-"+lfix+"px");							
						}
						
						
						if(img_height > cont_height || $(v).hasClass("imgfix")){
							var iheight = resizeToHeight(img_width,img_height,cont_width);
							var tfix= ( iheight - cont_height ) / 2;
							$(v).find("img").css("margin-top","-"+tfix+"px");							
						}
						
					});
				
				}			

			});
		}
		
		pimgholder_count=0;	
		pcount=0;
		if(which=="openSavedPost"){
			$(".saved-post-detail .post-holder").each(function(){				
				pcount++;
				if($(this).parent(".saved-post-detail").css("display")=="block"){
					
					if($(this).find(".post-img").length > 0 ){				
						pimgholder_count++;
						
						$(this).find(".pimg-holder").each(function(e, v){
							
							var cont_width=$(v).width();
							var cont_height=$(v).height();
							var img_width=$(v).find("img").width();
							var img_height=$(v).find("img").height();
												
							if($(v).hasClass("himg-box")){
								if(img_width < cont_width){
									$(v).addClass("imgfix");
								}
							} 
							
							if(img_width > cont_width){
								
								var iwidth = resizeToWidth(img_width,img_height,cont_height);
								var lfix= ( iwidth - cont_width ) / 2;
								$(v).find("img").css("margin-left","-"+lfix+"px");							
							}
							
							
							if(img_height > cont_height || $(v).hasClass("imgfix")){
								var iheight = resizeToHeight(img_width,img_height,cont_width);
								var tfix= ( iheight - cont_height ) / 2;
								$(v).find("img").css("margin-top","-"+tfix+"px");							
							}							
							$(v).find("img").animate({opacity: 1},2000);						
						});
					
					}
					
				}
			});
		}	
		
		/* end saved post image fixes */
		
		/* popup post image fixes */		
		$(".mfp-content .popup-area .post-holder").each(function(){

			if($(this).find(".post-img").length > 0 ){				
				pimgholder_count++;
				
				$(this).find(".pimg-holder").each(function(e, v){
					
					var cont_width=$(v).width();
					var cont_height=$(v).height();
					var img_width=$(v).find("img").width();
					var img_height=$(v).find("img").height();
					
					if($(v).hasClass("himg-box")){
						if(img_width < cont_width){
							$(v).addClass("imgfix");
						}
					} 
					
					if(img_width > cont_width){
						
						var iwidth = resizeToWidth(img_width,img_height,cont_height);
						var lfix= ( iwidth - cont_width ) / 2;
						$(v).find("img").css("margin-left","-"+lfix+"px");							
					}
					if(img_height > cont_height || $(v).hasClass("imgfix")){
						var iheight = resizeToHeight(img_width,img_height,cont_width);
						var tfix= ( iheight - cont_height ) / 2;
						$(v).find("img").css("margin-top","-"+tfix+"px");							
					}
					
				});
			
			}			

		});
		
		/* end popup post image fixes */		
		
		/* wall page stuff - image fixes */
		var gb_count=0;
		if($(".page-wrapper").hasClass("wallpage")){
			
			var allSections=["photo","destination","like"];
			$.each(allSections,function(i,v){	
				
				if($(".wallcontent-column .grid-list").hasClass("photo-list")){
					$(".wallcontent-column ."+v+"-list .grid-box").each(function(){
						gb_count++;
						
						var cont_width=$(this).width();
						if(v=="like")
							cont_width=$(this).find(".imgholder").width();
						
						var cont_height=$(this).height();
						if(v=="like")
							cont_height=$(this).find(".imgholder").height();
						
						var img_width=$(this).find("img").width();
						var img_height=$(this).find("img").height();
						
						if($(this).find("."+v+"-box").hasClass("himg-box")){
							if(img_width < cont_width){
								$(this).find("."+v+"-box").addClass("imgfix");
							}
						} 
					
						if(img_width > cont_width){							
							var iwidth = resizeToWidth(img_width,img_height,cont_height);
							var lfix= ( iwidth - cont_width ) / 2;
							$(this).find("."+v+"-box").find("img").css("margin-left","-"+lfix+"px");							
						}						
						if(img_height > cont_height || $(this).hasClass("imgfix")){
							var iheight = resizeToHeight(img_width,img_height,cont_width);
							var tfix= ( iheight - cont_height ) / 2;
							$(this).find("."+v+"-box").find("img").css("margin-top","-"+tfix+"px");							
						}
						
					});
				}
			});			
		}
	
		/* share popup - image fixes */
		pcount=0;
		if(which=="share-popup"){
			$(".sharepost-popup .org-post .post-holder").each(function(){
				pcount++;	
				if($(this).find(".post-img").length > 0 ){				
					pimgholder_count++;
					
					$(this).find(".pimg-holder").each(function(e, v){
						
						var cont_width=$(v).width();
						var cont_height=$(v).height();
						var img_width=$(v).find("img").width();
						var img_height=$(v).find("img").height();
											
						if($(v).hasClass("himg-box")){
							if(img_width < cont_width){
								$(v).addClass("imgfix");
							}
						} 
						
						if(img_width > cont_width){
							
							var iwidth = resizeToWidth(img_width,img_height,cont_height);
							var lfix= ( iwidth - cont_width ) / 2;
							$(v).find("img").css("margin-left","-"+lfix+"px");							
						}
						
						
						if(img_height > cont_height || $(v).hasClass("imgfix")){
							var iheight = resizeToHeight(img_width,img_height,cont_width);
							var tfix= ( iheight - cont_height ) / 2;
							$(v).find("img").css("margin-top","-"+tfix+"px");							
						}
						$(v).find("img").css({visibility:"visible", opacity: 0.0}).animate({opacity: 1.0},200);
					});
				
				}			
				
			});			
		}
		
		/* not-messages - image fixes */
		pcount=0;
		if(which=="not-messages"){
			
			$(".msg-listing li").each(function(){
					
				if($(this).find(".chat-img-wrapper").length > 0 ){				
						
					var img_width=$(this).find(".chat-img-wrapper").find("img").width();
					var img_height=$(this).find(".chat-img-wrapper").find("img").height();
										
				
					
					if(img_width > img_height){
					
						$(this).find(".chat-img-wrapper").find("img").addClass('himg');	
						$(this).find(".chat-img-wrapper").find("img").removeClass('vimg');								
					}
					else{
					
						$(this).find(".chat-img-wrapper").find("img").removeClass('himg');	
						$(this).find(".chat-img-wrapper").find("img").addClass('vimg');	
					}			
				}			
				
			});			
		}
		
		/* not-messages - image fixes */
		pcount=0;
		if(which=="main-msgwindow"){
			
			$(".albums-grid .grid-box").each(function(){
					
				var img_width=$(this).find(".imgholder").find("img").width();
				var img_height=$(this).find(".imgholder").find("img").height();
				
				if(img_width > img_height){
				
					$(this).find(".imgholder").find("img").addClass('himg');	
					$(this).find(".imgholder").find("img").removeClass('vimg');								
				}
				else{
				
					$(this).find(".imgholder").find("img").removeClass('himg');	
					$(this).find(".imgholder").find("img").addClass('vimg');	
				}
									
			});	
			
		}
					
		/* activity post image fixes */
				
		pimgholder_count=0;	
		pcount=0;
		if($("body").find(".activity-content").css("display")=="block"){
			
			resetImageFixClass(".activity-content");
			/* post image fixes */		
			$(".activity-content .post-holder").each(function(){
								
				if($(this).find(".post-img").length > 0 ){				
					pimgholder_count++;
					
					$(this).find(".pimg-holder").each(function(e, v){
						
						var cont_width=$(v).width();
						var cont_height=$(v).height();
						var img_width=$(v).find("img").width();
						var img_height=$(v).find("img").height();
											
						if($(v).hasClass("himg-box")){
							if(img_width < cont_width){
								$(v).addClass("imgfix");
							}
						}						
						if(img_width > cont_width){
							
							var iwidth = resizeToWidth(img_width,img_height,cont_height);
							var lfix= ( iwidth - cont_width ) / 2;
							$(v).find("img").css("margin-left","-"+lfix+"px");							
						}
						
						
						if(img_height > cont_height || $(v).hasClass("imgfix")){
							var iheight = resizeToHeight(img_width,img_height,cont_width);
							var tfix= ( iheight - cont_height ) / 2;
							$(v).find("img").css("margin-top","-"+tfix+"px");							
						}
						
					});
				
				}			

			});
		}
		
		pimgholder_count=0;	
		pcount=0;
		if(which=="openActivityPost"){
			$(".activity-post-detail .post-holder").each(function(){				
				pcount++;
				if($(this).parent(".activity-post-detail").css("display")=="block"){
					
					if($(this).find(".post-img").length > 0 ){				
						pimgholder_count++;
						
						$(this).find(".pimg-holder").each(function(e, v){
							
							var cont_width=$(v).width();
							var cont_height=$(v).height();
							var img_width=$(v).find("img").width();
							var img_height=$(v).find("img").height();
												
							if($(v).hasClass("himg-box")){
								if(img_width < cont_width){
									$(v).addClass("imgfix");
								}
							} 
							
							if(img_width > cont_width){
								
								var iwidth = resizeToWidth(img_width,img_height,cont_height);
								var lfix= ( iwidth - cont_width ) / 2;
								$(v).find("img").css("margin-left","-"+lfix+"px");							
							}
							
							
							if(img_height > cont_height || $(v).hasClass("imgfix")){
								var iheight = resizeToHeight(img_width,img_height,cont_width);
								var tfix= ( iheight - cont_height ) / 2;
								$(v).find("img").css("margin-top","-"+tfix+"px");							
							}							
							$(v).find("img").animate({opacity: 1},2000);						
						});
					
					}
					
				}
			});
		}	
		
		/* end activity post image fixes */
		
		/* activitiy post - image fixes */
		
		if(which=="activity"){
			
			resetActivityImages();
			
			$(".activity-content .activity-box").each(function(){
				if($(this).find(".imgpreview").length > 0 ){				
										
					$(this).find(".pimg-holder").each(function(e, v){
						
						var cont_width=$(v).width();
						var cont_height=$(v).height();
						var img_width=$(v).find("img").width();
						var img_height=$(v).find("img").height();
						
						if($(v).hasClass("himg-box")){										
							
							if(img_width < cont_width){							
								$(v).addClass("imgfix");
							}
						
						} 
						
						if(img_width > cont_width){
																			
							var iwidth = resizeToWidth(img_width,img_height,cont_height);
							var lfix= ( iwidth - cont_width ) / 2;
							$(v).find("img").css("margin-left","-"+lfix+"px");							
						}
						
						
						if(img_height > cont_height || $(v).hasClass("imgfix")){
							var iheight = resizeToHeight(img_width,img_height,cont_width);
							var tfix= ( iheight - cont_height ) / 2;
							$(v).find("img").css("margin-top","-"+tfix+"px");							
						}
						$(v).find("img").css({visibility:"visible", opacity: 0.0}).animate({opacity: 1.0},200);
					});
				
				}
				
			});
			
			/* activity - photos image fix */
			$(".activity-content .activity-post-detail").each(function(){
				$(this).find(".albums-grid .grid-box").each(function(){
						
					var img_width=$(this).find(".imgholder").find("img").width();
					var img_height=$(this).find(".imgholder").find("img").height();
					
					if(img_width > img_height){
					
						$(this).find(".imgholder").find("img").addClass('himg');	
						$(this).find(".imgholder").find("img").removeClass('vimg');								
					}
					else{
					
						$(this).find(".imgholder").find("img").removeClass('himg');	
						$(this).find(".imgholder").find("img").addClass('vimg');	
					}
										
				});
			});
			
		}
		if(which=="photo-gallery"){
			
			resetPhotoGalleryImageFixClass();
		
			$(".subtab-photos.active .photo-gallery .thumbs-img ul > li").each(function(){
														
				$(this).find("a").each(function(e, v){
					
					var cont_width=$(v).width();
					var cont_height=$(v).height();
					var img_width=$(v).find("img").width();
					var img_height=$(v).find("img").height();
					
					if($(v).hasClass("himg-box")){										
						
						if(img_width < cont_width){							
							$(v).addClass("imgfix");
						}
					
					} 
					
					if(img_width > cont_width){
																		
						var iwidth = resizeToWidth(img_width,img_height,cont_height);
						var lfix= ( iwidth - cont_width ) / 2;
						$(v).find("img").css("margin-left","-"+lfix+"px");							
					}
					
					if(img_height > cont_height || $(v).hasClass("imgfix")){
						var iheight = resizeToHeight(img_width,img_height,cont_width);
						var tfix= ( iheight - cont_height ) / 2;
						$(v).find("img").css("margin-top","-"+tfix+"px");							
					}
					
				});
			
			});
		
		}
	
		/*  wall page : photos - image slider fixing */
		if(which=="wall-photoslider"){
			
			wallImageSliderFixClass();
		
			$(".user-photos ul.lightSlider > li").each(function(){
														
				$(this).find("a").each(function(e, v){
					
					var cont_width=$(v).width();
					var cont_height=$(v).height();
					var img_width=$(v).find("img").width();
					var img_height=$(v).find("img").height();
					
					if($(v).hasClass("himg-box")){										
						
						if(img_width < cont_width){							
							$(v).addClass("imgfix");
						}
					
					} 
					
					if(img_width > cont_width){
																		
						var iwidth = resizeToWidth(img_width,img_height,cont_height);
						var lfix= ( iwidth - cont_width ) / 2;
						$(v).find("img").css("margin-left","-"+lfix+"px");							
					}
					
					if(img_height > cont_height || $(v).hasClass("imgfix")){
						var iheight = resizeToHeight(img_width,img_height,cont_width);
						var tfix= ( iheight - cont_height ) / 2;
						$(v).find("img").css("margin-top","-"+tfix+"px");							
					}
				});
			
			});
		
		}

		if(which=="wall-photoalbums") {
			wallPhotoAlbumsSliderFixClass();
			if($(".albums-area").css("display")=="block" && $(".albums-area").length>0){
				$(".albums-area .album-box").each(function(){
															
					$(this).find("a").each(function(e, v){
						
						var cont_width=$(v).width();
						var cont_height=$(v).height();
						var img_width=$(v).find("img").width();
						var img_height=$(v).find("img").height();
						
						if($(v).hasClass("himg-box")){										
							if(img_width < cont_width){							
								$(v).addClass("imgfix");
							}
						
						} 
						
						if(img_width > cont_width){
																			
							var iwidth = resizeToWidth(img_width,img_height,cont_height);
							var lfix= ( iwidth - cont_width ) / 2;
							$(v).find("img").css("margin-left","-"+lfix+"px");							
						}
						
						if(img_height > cont_height || $(v).hasClass("imgfix")){
							var iheight = resizeToHeight(img_width,img_height,cont_width);
							var tfix= ( iheight - cont_height ) / 2;
							$(v).find("img").css("margin-top","-"+tfix+"px");							
						}
					});
				
				});
			}
			
			if($(".photos-area").css("display")=="block" && $(".photos-area").length>0){
				//alert('ALERT III');
				$(".photos-area .photo-box").each(function(){
															
					$(this).find("a").each(function(e, v){
						
						var cont_width=$(v).width();
						var cont_height=$(v).height();
						var img_width=$(v).find("img").width();
						var img_height=$(v).find("img").height();
						
						if($(v).hasClass("himg-box")){										
							
							if(img_width < cont_width){							
								$(v).addClass("imgfix");
							}
						
						} 
						
						if(img_width > cont_width){
																			
							var iwidth = resizeToWidth(img_width,img_height,cont_height);
							var lfix= ( iwidth - cont_width ) / 2;
							$(v).find("img").css("margin-left","-"+lfix+"px");							
						}
						
						if(img_height > cont_height || $(v).hasClass("imgfix")){
							var iheight = resizeToHeight(img_width,img_height,cont_width);
							var tfix= ( iheight - cont_height ) / 2;
							$(v).find("img").css("margin-top","-"+tfix+"px");							
						}
					});
				});
			}
		}
	}
	/* end responsible for centering images after upload */

/* FUN end fix image centering */

/* FUN after resize */
	function doneResizing(){
		/* manage left menu */
			setLeftMenu();		
		/* end manage left menu */
		
		/* set floating chat */
			setFloatChat();		
		/* end set floating chat */
				
		/* set home layout*/
			var isWall=$(".page-wrapper").hasClass("wallpage");
			if(isWall){			
				initPhotoCarousel("wallpage");
			}
		
		/* refresh sub tabs */		
			var win_w=$(window).width();
			if(win_w>1024){				
				initNiceScroll(".nice-scroll");
			} else {
				$(".sidemenu").css("opacity",1);				
				removeNiceScroll(".nice-scroll");
			}
		
		/* FUN set single page height */
			setSinglePageHeight();	
		/* FUN end set single page height */
		
		/* manage message list for mobile */
			manageMessagePage();
		/* end manage message list for mobile */

		/* manage chat windows for mobile */
			manageChatbox();
		/* end manage chat windows for mobile */
		
		/* show travelbuddy advanced search */
			reset_drop_searcharea();
		/* end show travelbuddy advanced search */
		
		/* show general-details page summery expandable */
			reset_gdetails_expandable();
		/* end general-details page summery expandable */
		
		/* manage places tabs selection */		
			if($(window).width()<=450){
				var activeTab=$(".places-page .tablist .text-menu li.active");
				if( activeTab.length>0){
					var activeTabHtml=activeTab.find("a").html().trim();				
					if(activeTabHtml!="Reviews" && activeTabHtml!="Travellers" && activeTabHtml!="Locals" && activeTabHtml!="Photos" && activeTabHtml!="Events" && activeTabHtml!="Tip" && activeTabHtml!="Ask"){
						activeTabHtml="all";
						manageNewPostBubble("hideit");
					}else{
						if(activeTabHtml=="Reviews" || activeTabHtml=="Hangout" || activeTabHtml=="Tip" || activeTabHtml=="Ask"){
							manageNewPostBubble("showit");
						}
					}
					$(".select2places").val(activeTabHtml.toLowerCase()).change();
				}
			}		
		/* end manage places tabs selection */
		
		/* manage header hide */
			if(win_w<=767){
				$(".places-column .subtab").each(function(){
					if($(this).hasClass("active")){
						$('body').addClass("hideHeader");
					}
				});
				
			}else{
				if($('body').hasClass("hideHeader")){
					$('body').removeClass("hideHeader");
				}
			}
		/* manage end header hide */
		
		/* manage image width */
			if($(".main-content").hasClass("places-page")){
				resetPlacesImageUI();
			}
		/* end manage image width */
		
		/* manage keepopen */
			if($(".page-wrapper").hasClass("place-wrapper") || $(".page-wrapper").hasClass("full-wrapper")){				
				if( win_w<=450 ){
					if($(".search-holder").hasClass("keepopen")){
						$(".search-holder").removeClass("keepopen");
					}
				}else{
					
					if(!$(".page-wrapper").hasClass("noopened-search")){
						if(!$(".search-holder").hasClass("keepopen")){
							$(".search-holder").addClass("keepopen");
						}
					}
				}				
			}			
		/* end manage keepopen */
		
		/* reset places more info / sliding for desktop */
			resetPlacesInfoSliding();
		/* end reset places more info / sliding for desktop */
		
		/* trip page */
			if($('.trip-page').length) {
				/* reset left section */
				if(win_w>568){
					$(".trip-page .trip-mapview .desc-box").attr("style","");
				}
				/* end reset left section */
				
				/* reset mobile fullview */		
				if(win_w>568){
					if($(".trip-wrapper").hasClass("mobile-fullview")){
						$(".trip-wrapper").removeClass("mobile-fullview");
					}
				}else{
					$(".page-wrapper .trip-mapview .side-section .section-layer").each(function(){
						
						if($(this).hasClass("front") && $(this).attr("id")!="trip-list"){
							$(".trip-wrapper").addClass("mobile-fullview");
						}
						
					});
				}
				/* end reset mobile fullview */
			}
		/* end trip page */
		
			var fromTop=$(window).scrollTop();
			if($('.page-wrapper').length > 0) {			
				var top = $('.page-wrapper').offset().top;
				setNewPostBubble(win_w,top,'page-wrapper');
			}

		/* reset split page : mbl-fiter-icon */
			resetMblFilterIcon();
		
		/* reset settings menu for mobile */
			if($(".page-wrapper").hasClass(".settings-wrapper")){
				if(win_w < 768)
					resetInnerPage("settings","hide");
			}
		
		/* reset wall & places menu for mobile */
			if(win_w < 768){
				if($(".page-wrapper").hasClass("wallpage") || $(".page-wrapper").hasClass("fixed-wrapper")){
					$(".page-wrapper").removeClass("fixed-wrapper");
				}
				if($(".page-wrapper").hasClass("wallpage")) {
					var selTab = $(".main-content .main-tabpane.active").attr("tabname");
					if(selTab != "Wall"){			
						resetInnerPage("wall","hide");
					}
				}
		
				if($(".page-wrapper").hasClass("place-wrapper")) {
					var selTab = $(".tablist li.tab").find('a.active').html();
					if(selTab != "undefined"){			
						resetInnerPage("places","hide");
					}
				}
			}	

		/* reset fixed header*/
			var fromTop=$('body').scrollTop();
			var fromTop1=$(window).scrollTop();
			
		/* fixed header */				
			if(fromTop > 0 || fromTop1 > 0){
				if(win_w>1024){
					$(".header-section").addClass("fixed-header");
					$(".page-wrapper").addClass('fixed-wrapper');			
				}else{
					$(".header-section").removeClass("fixed-header");
					$(".page-wrapper").removeClass('fixed-wrapper');
				}

				if($(".page-wrapper").hasClass("transheadereffectall")) {
					$(".page-wrapper").addClass('page-scrolled');
					$(".page-wrapper").removeClass('JIS3829');
				} else if(win_w<=742) {
					if($(".page-wrapper").hasClass("transheadereffect")){
						$(".page-wrapper").addClass('page-scrolled')
					}
				}
			} else {
				$(".header-section").removeClass("fixed-header");
				$(".page-wrapper").removeClass('fixed-wrapper');
				$(".page-wrapper").removeClass('page-scrolled');
				
				if($(".page-wrapper").hasClass("transheadereffectall")) {
					$(".page-wrapper").removeClass('page-scrolled');
					$(".page-wrapper").addClass('JIS3829');
				} else if(win_w<=742) {
					if($(".page-wrapper").hasClass("transheadereffect")){
						$(".page-wrapper").removeClass('page-scrolled')
					}
				}
			}
		/* end fixed header */
		
		/* places page- add filter "fixed-filder" for mobile */
			if($(".main-page").hasClass("places-page") && win_w<=992){
				$(".wallcontent-column .search-area").addClass("fixed-filter");
			}
			
		/* groups/events detail page : reset menu scrollbar */
			if($(".main-page").hasClass("generaldetails-page") && win_w<768){
				if($(".main-page").hasClass("groups-page") || $(".main-page").hasClass("commevents-page")){
					$(".hori-menus").niceScroll({horizrailenabled:true,cursorcolor:"#bbb",cursorwidth:"6px",cursorborderradius:"0",cursorborder:"0px solid #fff",background:"rgba(255,255,255,0.6)"});
				}
			}	
		
	}
/* FUN end after resize */

/* FUN unreg user login popup */
	function openLoginPopup(){
		flipSectionTo("login");
		
		resetRegisterSteps();
		$(".signup-part #create-account").show();
		
		/*setTimeout(function(){			
			$(".select2.genderDrop").select2({minimumResultsForSearch:-1, placeholder: "Select Gender",  allowClear: true});
		},300);*/
	}
/* FUN end unreg user login popup */

/* FUN home page layout */
	function flipSectionTo(which){
		
		$('html, body').animate({ scrollTop: 0 }, 'slow');
				
		if(which=="login"){
			$(".signup-part").css("display","none");
			$(".forgot-part").css("display","none");								
			$(".login-part").css("display","inline-block");								
			$(".search-part").css("display","none");				
		}
		else if(which=="signup"){
			resetRegisterSteps();
			$(".signup-part #create-account").show();
		
			$(".signup-part").css("display","inline-block");
			$(".forgot-part").css("display","none");								
			$(".login-part").css("display","none");				
			$(".search-part").css("display","none");				
			
		}else if(which=="search"){
			$(".search-part").css("display","inline-block");
			$(".forgot-part").css("display","none");								
			$(".login-part").css("display","none");				
			$(".signup-part").css("display","none");
			
		}
		else{				
			$(".forgot-part").css("display","inline-block");
			$(".signup-part").css("display","none");
			$(".login-part").css("display","none");				
			$(".search-part").css("display","none");				
			
		}	
	}
		
/* FUN end home page layout */

/* FUN signup navigation */
	function resetRegisterSteps(){
		$(".homebox.signup-box").hide();
	}
	function signupNavigation(obj){
		var dclass=$(obj).attr('data-class');
		var objval=$(obj).html();
		if(objval=="skip"){
			/* no validation check */
		}
		else{
			resetRegisterSteps();
			$("#"+dclass).show();
			/*
			if(dclass=="profile-setting")
				$(".select2.genderDrop").select2({minimumResultsForSearch:-1, placeholder: "Select Gender",  allowClear: true});*/
		}		
	}
	function resetForgotPassSteps(){
		$(".homebox.forgot-box").hide();
	}
	function setForgotPassStep(){
		$(".homebox.forgot-box").hide();
		$(".homebox.forgot-box#fp-step-1").show();
	}
	function forgotPassNavigation(obj){
		var dclass=$(obj).attr('data-class');
		var objval=$(obj).html();
		if(objval=="skip"){
			/* no validation check */
		}
		else{
			resetForgotPassSteps();
			$("#"+dclass).show();
		}
	}

/* FUN end signup navigation */

/* FUN mark notification as read */
	function markNotRead(obj){
		var sparent=$(obj).parents("li");
		sparent.addClass("read");		
		if($(obj).parents(".noti-listing").hasClass("page-activity")){
			$(obj).attr("onclick","markRead(this)");
			$(obj).attr("title","Mark as unread");
		}
	}	
/* FUN end mark notification as read */

/* FUN set dropdown value on change */
	function setDropVal(obj){		
		$(obj).parents(".dropdown").find('.dropdown-toggle').html($(obj).html()+ " <span class='caret'></span>");
		$(obj).parents(".dropdown").find('.dropdown-toggle').val($(obj).html());
	}
/* FUN end set dropdown value on change */

/* swap normal/edit mode */
	function swapMode(obj){
		
		var nearestMainParent=$(obj).parents(".swapping-parent");
		
		var whichMode;
		if(nearestMainParent.find(".normal-mode").css("display")=="none")
			whichMode="edit"
		else
			whichMode="normal";
		
		if(whichMode=="normal"){
			nearestMainParent.find(".normal-mode").slideUp();
			nearestMainParent.find(".edit-mode").slideDown();
			setWallEditTextarea(obj);
		}
		else{
			nearestMainParent.find(".normal-mode").slideDown();
			nearestMainParent.find(".edit-mode").slideUp();		
		}
	}
/* end swap normal/edit mode */

/* show all content - through read more */
	function showAllContent(obj){
		var sparent=$(obj).parents(".para-section");
		sparent.find(".para").addClass("opened");
		$(obj).hide();
	}
/* end show all content - through read more */

/* FUN set single page height */
	function setSinglePageHeight(){
		
		var win_w=$(window).width();
		var win_h=$(window).height();
		
		var header_h=$(".header-section").height();
		var mcontent_h=$(".main-content").height();
		var footer_h=$(".footer-section").height();
		if( (header_h + mcontent_h + footer_h ) < win_h){
			$(".footer-section").addClass("abs-footer");
		}
		else{
			$(".footer-section").removeClass("abs-footer");
		}
	}
/* FUN end set single page height */

/* FUN pin image */ 
function pinImage(iname, isPermission=false){
	if(iname != '')
	{
		var trimname = iname.substr(iname.lastIndexOf('.') );
		trimname=iname.replace(trimname,"");
		trimname.trim();
		
		var getParent=$(".imgpin[data-imgid='"+trimname+"']");
		var pinattr = getParent.attr("data-pinit");
		
		if(pinattr =="pin"){var pintext = 'You are unpinning this photo from your gallery ?';var pinbtn='Unpin';}
		if(pinattr =='unpin'){var pintext = 'You are pinning this photo to your gallery ?';var pinbtn='Pinit';}

		if(isPermission) {
			$.ajax({
				type: 'POST',
				url: '?r=site/pinimage',
				data: "iname="+iname,
				success: function (data)
				{
					$(".discard_md_modal").modal("close");
					Materialize.toast(pinbtn+'ed.', 2000, 'green');
					imgname = iname.substr(iname.lastIndexOf('.') );
					iname=iname.replace(imgname,"");
					iname.trim();
					
					var getParent=$(".imgpin[data-imgid='"+iname+"']");				
					if(data=='1') getParent.attr("data-pinit","unpin");
					else getParent.attr("data-pinit","pin");
									
					var curPin=$(".pinlink.pin_"+iname);
					var haspinclass=curPin.hasClass("active");			
					if(haspinclass) { curPin.removeClass("active"); }
					else { curPin.addClass("active"); }
				}
			});			
		} else {
			var disText = $(".discard_md_modal .discard_modal_msg");
		    var btnKeep = $(".discard_md_modal .modal_keep");
		    var btnDiscard = $(".discard_md_modal .modal_discard");
	    	disText.html(pintext);
	        btnKeep.html("Keep");
	        btnDiscard.html(pinbtn);
	        btnDiscard.attr('onclick', 'pinImage(\''+iname+'\', true)');
	        $(".discard_md_modal").modal("open");	
		}
	}
}
/* FUN end pin image */ 

/* FUN general initializations */ 
function setGeneralThings(){	

	//autosize.destroy(document.getElementsByClassName("autogrow-tt"));
	//autosize(document.getElementsByClassName("autogrow-tt"));
	
	//$(".select2").select2({minimumResultsForSearch:-1});
}
/* FUN end general initializations */ 

/* set new post bubble */
	function setNewPostBubble(winw,top,fromWhere){
			
		if (winw > 767) {
			if(fromWhere == "window"){
				if (top > 250) {
					$('.new-post-mobile').find('a').show();
				}
				else{
					$('.new-post-mobile').find('a').hide();
				}
			}else if(fromWhere == "fixed-layout"){
				if (top < 250) {
					
					$('.new-post-mobile').find('a').show();
				}
				else{
					$('.new-post-mobile').find('a').hide();
				}
			}else if(fromWhere == "page-wrapper"){
				
				var fromTop=$(window).scrollTop();
				if (fromTop > 250 || top < -250) {
					var win_w = $(window).width();
					$('.new-post-mobile').find('a').show();
				}
				else{
					$('.new-post-mobile').find('a').hide();
				}
			}else if(fromWhere == "body"){
				if (top < -250) {
					$('.new-post-mobile').find('a').show();
				}
				else{
					$('.new-post-mobile').find('a').hide();
				}
			}else{ // fromWhere =  'docready'
				if (top > 250) {
					$('.new-post-mobile').find('a').show();
				}
				else{
					$('.new-post-mobile').find('a').hide();
				}
			}
		}else{
			$('.new-post-mobile').find('a').show();			
		}		
	}
/* end set new post bubble */

/* manage new post bubble */
	function manageNewPostBubble(doWhat){
		if(doWhat == "hideit"){
			$(".fixed-layout").addClass("hide-addflow");
		}else{ // doWhat == "showit"			
			if($(".fixed-layout").hasClass("hide-addflow"))
				$(".fixed-layout").removeClass("hide-addflow");
		}
	}
/* end manage new post bubble */

/* open this popup */
	function openThisPopup(popupid){
		//#popipid popup open
		setTimeout(function(){
			setGeneralThings();
		},400);
	}
/* end open this popup */

/* FUN normal/detail mode */
	function close_all_detail(){
	   
	   $(".mode-holder").each(function(){
			
		   var detailmode=$(this).find(".detail-mode").css("display");		
		
		   if(detailmode!="none"){
				
				$(this).find(".normal-mode").slideDown(300);
				$(this).find(".detail-mode").slideUp(300);
				$(this).removeClass("opened");
			}
	   });
   }
	function open_detail(obj){
		
		close_all_detail();

		var sparent=$(obj).parents(".mode-holder");
				
		var detailmode=sparent.find(".detail-mode").css("display");

		if(detailmode=="none"){
			sparent.find(".normal-mode").slideUp(300);
			sparent.find(".detail-mode").slideDown(300);
			sparent.addClass("opened");
		}
		else{
			sparent.find(".detail-mode").slideUp(300);
			sparent.find(".normal-mode").slideDown(300);
			sparent.removeClass("opened");
		}
		setTimeout(function(){
			setGeneralThings();
		},400);			
   }
	function close_detail(obj){
		
		var objParent=$(obj).parents(".mode-holder");
		var dmode=objParent.find(".detail-mode");
		var nmode=objParent.find(".normal-mode");
				
		var detailmode=dmode.css("display");

		if(detailmode=="none"){
			nmode.slideUp(300);
			dmode.slideDown(300);
			objParent.addClass("opened");
		}
		else{
			nmode.slideDown(300);
			dmode.slideUp(300);
			objParent.removeClass("opened");
		}
   }
/* FUN end normal/detail mode */

/* FUN range slider */
	function initRangeSlider(sliderParentId){
		var isFullRange=false;
		isFullRange=$("."+sliderParentId).hasClass("fullrange");
		if(isFullRange){
			$("."+sliderParentId).find(".slider-range").slider({
			  range: true,
			  min: 0,
			  max: 100,
			  values: [ 0, 100 ],
			  slide: function( event, ui ) {
				
				$("."+sliderParentId).find( ".min-value" ).css("left",ui.values[ 0 ]+"%");
				$("."+sliderParentId).find( ".min-value" ).html(ui.values[ 0 ]);
				$("."+sliderParentId).find( ".max-value" ).css("left",ui.values[ 1 ]+"%");
				$("."+sliderParentId).find( ".max-value" ).html(ui.values[ 1 ]);
			  }
			});
		}else{
		
			if($(".main-content").hasClass("travelbuddy-page")){			
				$("."+sliderParentId).find(".slider-range").slider({
				  range: true,
				  min: 0,
				  max: 100,
				  values: [ 0, 100 ],
				  slide: function( event, ui ) {
				
					$("."+sliderParentId).find( ".min-value" ).css("left",ui.values[ 0 ]+"%");
					$("."+sliderParentId).find( ".min-value" ).html(ui.values[ 0 ]);
					$("."+sliderParentId).find( ".max-value" ).css("left",ui.values[ 1 ]+"%");
					$("."+sliderParentId).find( ".max-value" ).html(ui.values[ 1 ]);
				  }
				});
			}
			else if($(".main-content").hasClass("advertmanager-page")){
				
				if(sliderParentId=="fixmin-slider"){
					
					$("."+sliderParentId).find(".slider-range").slider({
					  range: "min",
					  value: 6000,
					  min: 0,
					  max: 20000,
					  slide: function( event, ui ) {
						$( ".max-amount" ).html( ui.value + " people on Iwasinqatar");
					  }
					});												
				}else{
					$("."+sliderParentId).find(".slider-range").slider({
					  range: true,
					  min: 0,
					  max: 100,
					  values: [ 10, 90 ],
					  slide: function( event, ui ) {
						
						$("."+sliderParentId).find( ".min-value" ).css("left",ui.values[ 0 ]+"%");
						$("."+sliderParentId).find( ".min-value" ).html(ui.values[ 0 ]);
						$("."+sliderParentId).find( ".max-value" ).css("left",ui.values[ 1 ]+"%");
						$("."+sliderParentId).find( ".max-value" ).html(ui.values[ 1 ]);
					  }
					});			
				}
				
			}
			else{
				
				$("."+sliderParentId).find(".slider-range").slider({
				  range: true,
				  min: 0,
				  max: 100,
				  values: [ 25, 60 ],
				  slide: function( event, ui ) {
					
					$("."+sliderParentId).find( ".min-value" ).css("left",ui.values[ 0 ]+"%");
					$("."+sliderParentId).find( ".min-value" ).html(ui.values[ 0 ]);
					$("."+sliderParentId).find( ".max-value" ).css("left",ui.values[ 1 ]+"%");
					$("."+sliderParentId).find( ".max-value" ).html(ui.values[ 1 ]);
				  }
				});
				
				
			}
		}
	}
	function initPriceSlider(sliderParentId,min,max,curval){
		if($("."+sliderParentId).find(".slider-range").length) {
			$("."+sliderParentId).find(".slider-range").slider({
			  range: "min",
			  value: curval,
			  min: min,
			  max: max,
			  slide: function( event, ui ) {					
				$("."+sliderParentId).find( ".ui-slider-handle" ).html( "<span>$" + ui.value +"</span>" );
			  }
			});
			$("."+sliderParentId).find(".slider-range").find( ".ui-slider-handle" ).html( "<span>$" + $("."+sliderParentId).find( ".slider-range" ).slider( "value") +"</span>");
		}	
	}
	function initDistSlider(sliderParentId,min,max,curval){
		if($("."+sliderParentId).find(".slider-range").length) {
			$("."+sliderParentId).find(".slider-range").slider({
			  range: "min",
			  value: curval,
			  min: min,
			  max: max,
			  slide: function( event, ui ) {					
				$("."+sliderParentId).find( ".ui-slider-handle" ).html( "<span>" + ui.value +"km</span>" );
			  }
			});
			$("."+sliderParentId).find(".slider-range").find( ".ui-slider-handle" ).html( "<span>" + $("."+sliderParentId).find( ".slider-range" ).slider( "value") +"km</span>");
		}		
	}
/* FUN end range slider */
 
/* FUN set rating stars */
	function ratingJustOver(obj) {
		var rate = $(obj).attr('data-value');
		var digitArray = ['1', '2', '3', '4', '5'];

		if($.inArray(rate, digitArray) !== -1) {
			dummyStarHelp234II(obj, rate);
		}
	}

	function resettostart(obj) {
		if($.inArray(selectStartPoint, starsArray) !== -1) {
			$this = $(obj);
			dummyStarHelp234II($this, selectStartPoint);
		}
	}

	function pickrate(obj, value) {
		if($.inArray(value, starsArray) !== -1) {
			selectStartPoint = value;
			$this = $(obj);
			dummyStarHelp234II($this, selectStartPoint);
		}
	}

	function ratingJustOut(obj) {
		dummyStarHelp234II(obj);
	}

	function dummyStarHelp234II(obj, rate=null) {
		if(rate=='1') { 
			$(obj).parents('.rating-stars').find('.mdi.mdi-star').removeClass('active');
			$(obj).parents('.rating-stars').find('.ratecls1').addClass('active');
			$(obj).parents('.rating-stars').find('.star-text').html('Poor');
		} else if(rate=='2') {
			$(obj).parents('.rating-stars').find('.mdi.mdi-star').removeClass('active');
			$(obj).parents('.rating-stars').find('.ratecls2').addClass('active');
			$(obj).parents('.rating-stars').find('.star-text').html('Good');
		} else if(rate=='3') {
			$(obj).parents('.rating-stars').find('.mdi.mdi-star').removeClass('active');
			$(obj).parents('.rating-stars').find('.ratecls3').addClass('active');
			$(obj).parents('.rating-stars').find('.star-text').html('Better');
		} else if(rate=='4') {
			$(obj).parents('.rating-stars').find('.mdi.mdi-star').removeClass('active');
			$(obj).parents('.rating-stars').find('.ratecls4').addClass('active');
			$(obj).parents('.rating-stars').find('.star-text').html('Superb');
		} else if(rate=='5') {
			$(obj).parents('.rating-stars').find('.mdi.mdi-star').removeClass('active');
			$(obj).parents('.rating-stars').find('.ratecls5').addClass('active');
			$(obj).parents('.rating-stars').find('.star-text').html('Excellent');
		} else {
			$(obj).closest('.rating-stars').find('.mdi.mdi-star').removeClass('active');
			$(obj).closest('.rating-stars').find('.star-text').html('');
		}
	}

	function dummyStarHelp234IIII(rate=null) {
		$obj = '';

		if($('#compose_review').length) {
			$obj = $('#compose_review');
		} else if($('#compose_newreview').length) {
			$obj = $('#compose_newreview');
		}

		if($obj != '') { 
			if(rate=='1') {
				$obj.find('.rating-stars').find('.mdi.mdi-star').removeClass('active');
				$obj.find('.rating-stars').find('.ratecls1').addClass('active');
				$obj.find('.rating-stars').find('.star-text').html('Poor');
			} else if(rate=='2') {
				$obj.find('.rating-stars').find('.mdi.mdi-star').removeClass('active');
				$obj.find('.rating-stars').find('.ratecls2').addClass('active');
				$obj.find('.rating-stars').find('.star-text').html('Good');
			} else if(rate=='3') {
				$obj.find('.rating-stars').find('.mdi.mdi-star').removeClass('active');
				$obj.find('.rating-stars').find('.ratecls3').addClass('active');
				$obj.find('.rating-stars').find('.star-text').html('Better');
			} else if(rate=='4') {
				$obj.find('.rating-stars').find('.mdi.mdi-star').removeClass('active');
				$obj.find('.rating-stars').find('.ratecls4').addClass('active');
				$obj.find('.rating-stars').find('.star-text').html('Superb');
			} else if(rate=='5') {
				$obj.find('.rating-stars').find('.mdi.mdi-star').removeClass('active');
				$obj.find('.rating-stars').find('.ratecls5').addClass('active');
				$obj.find('.rating-stars').find('.star-text').html('Excellent');
			} else {
				$obj.find('.rating-stars').find('.mdi.mdi-star').removeClass('active');
				$obj.find('.rating-stars').find('.star-text').html('');
			}
		}
	}

	function setRating(obj,rate){
		var win_w = $(window).width();
		var mparent = $(obj).parents(".setRating");
		var curRate=0;

		if(win_w < 100){
			
			if($(obj).parents(".popup-area").length > 0){
				
				mparent.find(".fa").each(function(){						
					$(this).removeClass("active");
					var dvalue=$(this).attr("data-value");
					if(dvalue<=rate){
						$(this).addClass("active");
						if(curRate < dvalue)
							curRate = dvalue;
					}
				});				
				$(obj).parents(".new-post").addClass("expandReview");
				expandNewpost($(obj).parents(".new-post"));				
			}
			
		}else{
			mparent.find(".fa").each(function(){						
				$(this).removeClass("active");
				var dvalue=$(this).attr("data-value");
				if(dvalue<=rate){
					$(this).addClass("active");
					if(curRate < dvalue)
						curRate = dvalue;
				}
			});
			$(obj).parents(".new-post").addClass("expandReview");
			expandNewpost($(obj).parents(".new-post"));			
		}
		
		if(mparent.find(".star-text").length>0){
						
			var startext="";
			if(curRate==1)
				startext="Poor";
			else if(curRate==2)
				startext="Good";
			else if(curRate==3)
				startext="Better";
			else if(curRate==4)
				startext="Superb";
			else if(curRate==5)
				startext="Excellent";
			else
				startext="Roll over stars, then click to rate";
			
			mparent.find(".star-text").html(startext);
			
		}
	}

	function setStarText(obj,rate){
		
		var startext="Roll over stars, then click to rate";
		
		var mparent = $(obj).parent();
		var isActive=false;
		var curRate=0;
		
		mparent.find(".fa").each(function(){
			if($(this).hasClass("active")){
				isActive=true;
				var dvalue=$(this).attr("data-value");
				if(curRate < dvalue)
					curRate = dvalue;
			}
		});
		if(!isActive){
				
			if(rate==1)
				startext="Poor";
			else if(rate==2)
				startext="Good";
			else if(rate==3)
				startext="Better";
			else if(rate==4)
				startext="Superb";
			else if(rate==5)
				startext="Excellent";
			else
				startext="Roll over stars, then click to rate";
			
		}else{
			
			var deftext="";
			if(curRate==1)
				deftext="Poor";
			else if(curRate==2)
				deftext="Good";
			else if(curRate==3)
				deftext="Better";
			else if(curRate==4)
				deftext="Superb";
			else
				deftext="Excellent";

			if(rate > curRate){
				
				if(rate==1)
					startext="Poor";
				else if(rate==2)
					startext="Good";
				else if(rate==3)
					startext="Better";
				else if(rate==4)
					startext="Superb";
				else if(rate==5)
					startext="Excellent";				
				else
					startext=deftext;
			}else{
				startext=deftext;
			}
		}
		mparent.find(".star-text").html(startext);
		
	}
/* FUN end set rating stars */

/* FUN manage search area */
	function mng_drop_searcharea(obj){
		
		var sparent=$(obj).parents(".search-area");
		if(!sparent.hasClass("expanded")){
			sparent.addClass("expanded");
		
		}
		else{
			sparent.removeClass("expanded");
			
			if($(".gdetails-summery .search_string").length > -1){				
				$(".gdetails-summery .search_string").val(""); // clear search field			
			}
		}		
	}
	function mbl_mng_drop_searcharea(obj,searcharea_id){
		$(".search-area#"+searcharea_id).find(".expand-link").trigger("click");
	}
	function reset_drop_searcharea(){
		
		var pageClass="";
		if($(".main-content").hasClass("travelbuddy-page"))
			pageClass=".travelbuddy-page";
		if($(".main-content").hasClass("hotels-page"))
			pageClass=".hotels-page";
		if($(".main-content").hasClass("hangout-page"))
			pageClass=".hangout-page";
		if($(".general-page").hasClass("refers-page"))
			pageClass=".refers-page";
		if($(".general-page").hasClass("tripexperience-page"))
			pageClass=".tripexperience-page";
		
		if(pageClass!=""){
			var sparent=$(pageClass).find(".search-area");
			
			var win_w=$(window).width();
			if(win_w>800){
				if(sparent.hasClass("expanded")){
					sparent.removeClass("expanded");
				}
			
			}
			else{
			
			}
		}
			
	}
/* FUN end manage search area */

/* FUN manage expandable area */
	function invertExpandLink(obj, direction,maindir){
		// needs a fix for replacing tags
		var currentText=$(obj).html()+"";
		var actionText="";
		if(direction=="up"){						
			var replacing = '<i class="mdi mdi-menu-up"></i>';
			
			if(currentText.indexOf('<i class="mdi mdi-menu-down"></i>')>-1 && maindir=="maindown"){
				actionText = currentText.substring(0,currentText.indexOf('<i class="mdi mdi-menu-down"></i>'));				
				replacing = '<i class="mdi mdi-menu-up"></i>';
			}
			if(currentText.indexOf('<i class="mdi mdi-chevron')>-1 && maindir=="maindown"){
				actionText = currentText.substring(0,currentText.indexOf('<i class="mdi mdi-chevron-down"></i>'));
				replacing = '<i class="mdi mdi-chevron-up"></i>';
			}
			if(currentText.indexOf('<i class="mdi mdi-menu-right"></i>')>-1 && maindir=="mainright"){
				actionText = currentText.substring(0,currentText.indexOf('<i class="mdi mdi-menu-right"></i>'));				
				replacing = '<i class="mdi mdi-menu-down"></i>';
			}
			if(actionText.length>0){
				$(obj).html(actionText + replacing);				
			}
			else{
				$(obj).html(replacing);				
			}
		}
		else{
			var replacing = '<i class="mdi mdi-menu-down"></i>';			
						
			if(currentText.indexOf('<i class="mdi mdi-menu-up"></i>')>-1 && maindir=="maindown"){
				actionText = currentText.substring(0,currentText.indexOf('<i class="mdi mdi-menu-up"></i>'));				
				replacing = '<i class="mdi mdi-menu-down"></i>';
			}
			if(currentText.indexOf('<i class="mdi mdi-chevron')>-1 && maindir=="maindown"){
				actionText = currentText.substring(0,currentText.indexOf('<i class="mdi mdi-chevron-up"></i>'));				
				replacing = '<i class="mdi mdi-chevron-down"></i>';
			}
			if(currentText.indexOf('<i class="mdi mdi-menu-down"></i>')>-1 && maindir=="mainright"){
				actionText = currentText.substring(0,currentText.indexOf('<i class="mdi mdi-menu-down"></i>'));				
				replacing = '<i class="mdi mdi-menu-right"></i>';
			}
			if(actionText.length>0){
				$(obj).html(actionText + replacing);
			}
			else{
				$(obj).html(replacing);
			}
				
		}

		setTimeout(function(){
			$('.payment-info').animate({
		         scrollTop: 500
		    }, 1500);
		}, 400);
	}
	function mng_expandable(obj,maindir){				
		
		var dir="";
		if(maindir=="hasClose" || maindir=="closeIt"){
			dir="maindown";			
		} else {
			if(maindir==undefined)
				dir="maindown";
			else
				dir=maindir;
		}
		var win_w=$(window).width();
		
		var sparent=$(obj).parents(".expandable-holder");
		if(maindir=="hasClose"){
			if(win_w<768){
				if(sparent.hasClass("mobilelist")){
					openPlacesMoreInfo(obj);
				}
			}else{
				
				if(!sparent.hasClass("expanded")){			
					sparent.addClass("expanded");
					sparent.find(".expandable-area").slideDown();
					$(obj).addClass("active");
					if($(obj).hasClass("invertsign")){
						invertExpandLink(obj, "up",dir);
					}
				}
			}
		}else if(maindir=="closeIt"){
			if(win_w<768){ 
				if(sparent.hasClass("mobilelist")){
					closePlacesMoreInfo(obj);
				}
			}else{
				
				if(sparent.hasClass("expanded")){			
					sparent.removeClass("expanded");
					sparent.find(".expandable-area").slideUp();		
					$(obj).removeClass("active");
					sparent.find(".expand-link").each(function(){
						if($(this).hasClass("active"))
							$(this).removeClass("active");	
					});	
				}
			}
		}else if(maindir=="none"){
				
			if(sparent.hasClass("expanded")){			
				sparent.removeClass("expanded");
				sparent.find(".expandable-area").slideUp();		
				$(obj).removeClass("active");
				sparent.find(".expand-link").each(function(){
					if($(this).hasClass("active"))
						$(this).removeClass("active");	
				});	
			}else{
				sparent.addClass("expanded");
				sparent.find(".expandable-area").slideDown();
				$(obj).addClass("active");				
			}
		
		}else{			
			if(!sparent.hasClass("expanded")){			
				sparent.addClass("expanded");
				sparent.find(".expandable-area").slideDown();
				$(obj).addClass("active");
				if($(obj).hasClass("invertsign")){
					invertExpandLink(obj, "up",dir);
				}
			}
			else{
				
				if($(obj).hasClass("active")){				
					sparent.removeClass("expanded");
					sparent.find(".expandable-area").slideUp();		
					$(obj).removeClass("active");
					sparent.find(".expand-link").each(function(){
						if($(this).hasClass("active"))
							$(this).removeClass("active");	
					});
					if($(obj).hasClass("invertsign")){
						invertExpandLink(obj, "down",dir);
					}
				}else{
					$(obj).addClass("active");
					if($(obj).hasClass("invertsign")){
						invertExpandLink(obj, "up",dir);
					}
				}
				
			}
			
			if($(".main-page").hasClass("generaldetails-page") && (!$(".main-page").hasClass("groups-page") && !$(".main-page").hasClass("commevents-page"))){
				var win_w = $(window).width();
				if($(obj).parents(".gdetails-summery").length > -1 && win_w < 768){				
					var initScroll = 180;				
					$('html, body').animate({ scrollTop: initScroll }, 'slow');
				}
			}
		}
	
		setTimeout(function(){
			$(".hori-menus").niceScroll({horizrailenabled:true,cursorcolor:"#bbb",cursorwidth:"6px",cursorborderradius:"0",cursorborder:"0px solid #fff",background:"rgba(255,255,255,0.6)"});
			setGeneralThings();
		},400);
	}
/* FUN end manage expandable area */

/* FUN manage offers */
	function openOffer(obj){
		
		var fparent=$(obj).parents(".offer-ul");
		var sparent=$(obj).parents(".offer-tab");
		
		fparent.hide();
		sparent.find(".offer-details").show();
		
		$(obj).parents(".main-page").find(".side-area.main-search").hide();
		$(obj).parents(".main-page").find(".side-area.offer-profile").show();
		
		var win_w=$(window).width();	
		
		if(win_w<=800){
			$(".mbl-filter-icon.main-icon").hide();
			$(".mbl-filter-icon.offer-profile-icon").show();
		}
		
	}
	function backToOffer(obj){
		
		var fparent=$(obj).parents(".offer-details");
		var sparent=$(obj).parents(".offer-tab");
		
		fparent.hide();
		sparent.find(".offer-ul").show();
		
		$(obj).parents(".main-page").find(".side-area.main-search").show();
		$(obj).parents(".main-page").find(".side-area.offer-profile").hide();
		
		var win_w=$(window).width();
		
		if(win_w<=800){
			$(".mbl-filter-icon.main-icon").show();
			$(".mbl-filter-icon.offer-profile-icon").hide();
		}
	}
	function resetMblFilterIcon(){
		if($(".main-page").hasClass("split-page")) {
			
			var offerTabFound = $(".tab-pane.offer-tab").length;
			var isOfferDetailTab = $(".tab-pane.offer-tab .offer-details").css("display");
			var win_w=$(window).width();
			
			if(offerTabFound){
				
				if(isOfferDetailTab=="none"){
					if(win_w>800){
						$(".mbl-filter-icon.main-icon").attr("style","");
						$(".mbl-filter-icon.offer-profile-icon").attr("style","");
					}
				}else{				
					if(win_w>800){
						$(".mbl-filter-icon.main-icon").attr("style","");
						$(".mbl-filter-icon.offer-profile-icon").attr("style","");
					}else{
						$(".mbl-filter-icon.main-icon").hide();
						$(".mbl-filter-icon.offer-profile-icon").show();
						
						$(".main-page").find(".side-area.main-search").hide();
						$(".main-page").find(".side-area.offer-profile").show();
					}
				}
			}else{
				if(win_w>800){
					$(".mbl-filter-icon.main-icon").attr("style","");
					$(".mbl-filter-icon.offer-profile-icon").attr("style","");
				}
			}
		}
	}
	function resetOfferSidebar(obj){
		
		var fparent=$(obj).parents(".main-page").find(".offer-details");
		var sparent=$(obj).parents(".main-page").find(".offer-tab");
		
		$(obj).parents(".main-page").find(".side-area.main-search").show();
		$(obj).parents(".main-page").find(".side-area.offer-profile").hide();
		
		$(".mbl-filter-icon.main-icon").attr("style","");
		$(".mbl-filter-icon.offer-profile-icon").attr("style","");
		
		setTimeout(function(){
			fparent.hide();
			sparent.find(".offer-ul").show();			
		},400);
		
	}
	function resetGeneralPageTabs(obj){
		
		var whichTab = $(obj).html().trim();
		if(whichTab == "Yours"){
			manageNewPostBubble("showit");
		}else{
			manageNewPostBubble("hideit");			
		}
	}
/* FUN end manage offers */

/* FUN preview gallery */
	function previewImage(obj){		
		var sparent=$(obj).parents(".photo-gallery");
		var imgsrc=$(obj).find("img").attr("src");
		sparent.find(".img-preview").find("img").attr("src",imgsrc);
	}
/* FUN end preview gallery */

/* Expand text for longer comment text */
	function explandReadMore(obj){		
		var sparent = $(obj).parents(".shorten");
		sparent.removeClass("shorten");
		sparent.find("a.overlay").remove();
		
	}
/* End Expand text for longer comment text */

/* open view map inside about section */
	function showEventMap(obj){
		var sparent=$(obj).parents(".eventinfo-row");
		var mapholder=sparent.find(".mapholder");
		
		if(mapholder.css("display")=="block"){
			mapholder.slideUp();
			$(obj).html("Show map");
		}else{
			mapholder.slideDown();
			$(obj).html("Close map");
		}
	}
/* end open view map inside about section */

/* unverified user block */
	function toggleLoadingBtn(action){
		if(action == "SHOW"){
			$(postBtnEle).attr("disabled",false);
			$(".loading").css("display","none");
		}else{
			$(postBtnEle).attr("disabled",true);
			$(".loading").css("display","inline-block");
		}
	}
	function account_verify(t){
		if(t == 2) {
			$(".notice1 .post_loadin_img").css("display","inline-block");			
		}
		else if(t == 1){
			$(".unreg-modal .loading").css("display","inline-block");
		}
     
		var email = 'manthan@123.com';
 
  
		if(email != '')
		{
			
		}
    }
/* end unverified user block */

/* photo input changed */
	function changePhotoFileInput(cls, obj, t){
	
		var valde = $(".imgfile-count").val();
		$(".imgfile-count").val(parseInt(valde,10)+1);
	   
	   //Get count of selected files
		var countFiles = $(obj)[0].files.length;	   
		if(t == true) {
			var formdata;
			formdata = new FormData($('form')[1]);
			var imgcounter=$(".counter").val(countFiles);
			formdata.append('counter', imgcounter);

		}
		
		var file, img; 
		var _URL = window.URL || window.webkitURL;			  
		var imgPath = $(obj)[0].value;
		var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		var image_holder = $(cls);
				
		if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg" || extn == "tif" || extn == "webp") {
			
			if(image_holder.parents(".main_modal").attr("id")=="compose_tool_box" || image_holder.parents(".main_modal").attr("id")=="composeeditpostmodal"){		
			//if(image_holder.parents(".modal").attr("id")=="compose_tool_box"){		
				$('#compose_uploadphotomodal').modal('close');
				//$('.modal.open .post_active_btn').prop('disabled', false).addClass('active_post_btn');
			}
			
			if (typeof (FileReader) != "undefined") {
				
				var imgCls=[];
				
				for (var i = 0; i < countFiles; i++) {
					file = obj.files[i];
					  
					  lastModified.push(file.lastModified);

					  storedFiles.push(file);
						var reader = new FileReader();
						reader.onload = function (e) {
							
							var img = new Image();							
							img.addEventListener("load", function () {
							  var imageInfo = file.name    +' '+
											  img.width  +'×'+
											  img.height +' '+
											  file.type    +' '+
											  Math.round(file.size/1024) +'KB';
								
							});
							
							img.src = window.URL.createObjectURL(file); 
							
						}
					
					reader.readAsDataURL($(obj)[0].files[i]);
				}				
				
				 //loop for each file selected for uploaded.
				 for (var i = 0; i < countFiles; i++) {
					  file = obj.files[i];
					  lastModified.push(file.lastModified);

					  storedFiles.push(file);
						var reader = new FileReader();
						reader.onload = function (e) {
							var img = new Image();
							
							img.onload = function() {
								var curr_cls='';
								
								if(this.width>this.height){
									setCurrClass("himg");
									imgCls.push("himg");
								} else if(this.width<this.height) {
									setCurrClass("vimg");
									imgCls.push("himg");
								} else {
									setCurrClass("himg");
									imgCls.push("himg");
								}

								$("<img />", {
								"src": e.target.result,
								"class": "thumb-image "+imgCls[i],
								})
								.add("<div class='loader'></div><a href='javascript:void(0)' class='removePhotoFile' data-code='"+lastModified[newct]+"'><i class='mdi mdi-close' ></i></a>") 
								.wrapAll("<div class='img-box'></div>")
								.parents()
								.prependTo(image_holder);
								managePostButton(postbtn_show);
								newct++; 
							};
							
							img.onerror = function() {};
							img.src = _URL.createObjectURL(file); 
						}
					
					
					
					$(cls.replace(".img-row","").trim()).show();                              
					image_holder.show();					
					reader.readAsDataURL($(obj)[0].files[i]);					
					
					$(".nice-scroll").getNiceScroll().resize();
					$(".post-photos").niceScroll({horizrailenabled:true,cursorcolor:"#bbb",cursorwidth:"8px",cursorborderradius:"0",cursorborder:"0px solid #fff",background:"rgba(255,255,255,0.6)",autohidemode:"false"});
				}
				
			} else {
				Materialize.toast('This browser does not support FileReader.', 2000, 'red');
			}

			ShowImage();
			setTimeout(function () {
				$('.post_active_btn').prop('disabled', false).addClass('active_post_btn');

				if (valde == 0) {

					$(cls).append("<div class='img-box'><div class='custom-file addimg-box'><div class='addimg-icon'><span class='material-icons dp48'>add</span></div><input type='file' name='upload' class='upload custom-upload' title='Choose a file to upload' required='' data-class='" + cls + "' multiple='true'/></div></div>");
				}
				HideImage();
				setPostBtnStatus();

			}, 400);

		} else {
			Materialize.toast('Please upload jpg, gif, png, tif and webp image.', 2000, 'red');
			$("#imageFile1").val("");
	   }
	
	}
/* end photo input changed */

/* FUN clear input text animation line */
	function titleUnderline(e){			
		clearUnderline();
		$(e).toggleClass("focused");
		$(e).children("input[type='text']").focus();
	}
	function clearUnderline(){
		$(".sliding-middle-out").each(function(){
			if($(this).hasClass("focused")) $(this).removeClass("focused");
		});
	}
/* FUN end clear input text animation line */

/* FUN share post image fix */
	function fixImageSharePopup(){	
		setTimeout(function(){fixImageUI("popup-images");},400);
	}
/* FUN end share post image fix */

/************ END COMMON FUNCTIONS ************/

/************ POST FUNCTIONS ************/

/* FUN post button manage */
	function managePostButton(action){
		if(action == "SHOW"){
			$(postBtnEle).attr("disabled",false);
		}else{
			$(postBtnEle).attr("disabled",true);
		}
	}
/* FUN end post button manage */

/* FUN post photos input file */
	function setCurrClass(name){curr_cls=name;}	 
	function getCurrClass(){return curr_cls;}
	
	function removePhotoFile(e) {
	   
		var code = $(e).data("code");
		
		$(e).parent().remove();
		$(e).closest('div').remove();
		setPostBtnStatus();
		
	    for(var i=0;i<storedFiles.length;i++) {
	    	str = storedFiles[i].lastModified;
	        if(str == code) {
	            storedFiles.splice(i,1);
	            break;
	        }
	    }
	    		
	}
	
/* FUN end post photos input file */

/* FUN remove video link */	
	function removeVideoLink(e){
		$(this).parents(".pvideo-holder").remove();
	}
/* FUN end remove video link */

/* FUN pre-edit post */	
	function hidePostExtras(areaid){		
		$("#"+areaid).find(".post-photos").hide();
		$("#"+areaid).find(".npost-title").hide();
		$("#"+areaid).find(".post-location").hide();
		if($(".main-content").hasClass("travelstore-page")){		
			$("#"+areaid).find(".post-category").hide();
			$("#"+areaid).find(".post-price").hide();
		}
	}
	function preEditPostSetup(e){
		
		// hide all post extras : not input value inside - parameter = ID of super parent
		hidePostExtras("editpost-popup");
		
		if($(".main-content").hasClass("travelstore-page")){
			var hasPrice=$("#editpost-popup").find(".post-price").find(".pprice").val().trim();
			if(hasPrice)
				$("#editpost-popup").find(".post-price").show();
			
			$("#editpost-popup").find(".post-category").show();
		}
	
		var hasImages=$("#editpost-popup").find(".post-photos").find(".img-row").length;
		if(hasImages)
			$("#editpost-popup").find(".post-photos").show();
		
		var hasTitle=$("#editpost-popup").find(".npost-title").find(".title").val().trim();
		if(hasTitle!="")
			$("#editpost-popup").find(".npost-title").show();
		
		var hasLocation=$("#editpost-popup").find(".post-location").find(".plocation").val().trim();
		if(hasLocation!="")
			$("#editpost-popup").find(".post-location").show();
		
		var hasTag=$("#editpost-popup").find(".post-tag").find(".ptag").val().trim();
		if(hasTag!="")
			$("#editpost-popup").find(".post-tag").show();
		
		var tt=$("#editpost-popup").find(".desc").find("textarea");
				
		$("#editpost-popup").find(".desc").removeClass("more");
		
		if(tt.get(0).scrollHeight > 50)
			$(".readmore").show();
	}
/* FUN end pre-edit post */

function deletePost(e){
	openstrapPopup("deletepost",this);
}

/* FUN pre-delete post */		
	function preDeletePostSetup(mainObj){		
		$(mainObj).parents(".post-holder").remove();
	}
/* FUN end pre-delete post */

/* FUN save post */		
	function savePost(e){
		openstrapPopup("savepost",this);
	}
	function preSavePostSetup(mainObj){
		openstrapPopup("alert-savedpost",mainObj);
	}
/* FUN end save post */


/* FUN open read more */	
	function openReadMore(e){		
		var descDiv = $(this).parents(".post-mcontent").find(".desc");
				
        descDiv.addClass("more");
		$(this).hide();
		
	}
/* FUN end open read more */	

/* FUN open full post */	
	function showFullPost(e){	
		
		var getText=$(this).html();
		if(getText.indexOf("Show All") >= 0){
			$(this).html("Collapse <span class='glyphicon glyphicon-arrow-up'></span>");
			//$(this).parents(".org-post").addClass("open-fullpost");
			$(this).parents(".org-post").toggleClass("open-fullpost");
			$(this).parents(".scroll_div").animate({scrollTop: 100}, "slow");
		}
		else{
			$(this).html("Show All <span class='glyphicon glyphicon-arrow-down'></span>");
			//$(this).parents(".org-post").removeClass("open-fullpost");
			$(this).parents(".org-post").toggleClass("open-fullpost");
			$(this).parents(".scroll_div").animate({scrollTop: 0}, "slow");
		}
	}
/* FUN end open full post */	

/* FUN open reply comment textarea*/
	function resetAllReplyComments(obj){
		
		$(".comment-reply-holder.comment-addreply.currOpen").each(function(){			
			
			if(!$(obj).hasClass("currOpen")){				
				$(this).removeClass("currOpen");								
				$(this).find(".addnew-comment").slideUp(200);
			}
		});
	}
	function openReplyComment(e){
		
		resetAllReplyComments($(this).parents(".pcomment-holder").find(".comment-addreply"));
		$(this).parents(".pcomment-holder").find(".comment-addreply").find(".addnew-comment").slideDown(200);	
		$(this).parents(".pcomment-holder").find(".comment-addreply").addClass("currOpen");
		
		setTimeout(function(){			
			setGeneralThings();
		},400);
	}
/* FUN end open reply comment textarea*/

/* FUN open edit comment textarea*/
	function openEditComment(e){		
		$(this).parents(".pcomment").find(".normal-mode").slideUp();
		$(this).parents(".pcomment").find(".edit-mode").slideDown();
		
		var tt=$(this).parents(".pcomment").find(".edit-mode").find("textarea");

		setTimeout(function(){			
			setGeneralThings();
		},400);
	}
/* FUN end open edit comment textarea*/

/* FUN delete edit comment*/
	function deleteEditComment(e){		
		var isMainComment=$(this).parents(".pcomment").hasClass("main-comment");
		if(isMainComment)
			$(this).parents(".pcomment-holder").remove();
		else
			$(this).parents(".pcomment").remove();			
	}
/* FUN end delete edit comment */

/* FUN cancel edit comment textarea*/
	function cancelEditComment(e){		
		$(this).parents(".pcomment").find(".normal-mode").slideDown();
		$(this).parents(".pcomment").find(".edit-mode").slideUp();
	}
/* FUN end cancel edit comment textarea*/

/* FUN save edited comment */
	function saveEditComment(e,id){		
		if((e.keyCode || e.which) == 13) { //Enter keycode		  
			$(this).parents(".pcomment").find(".normal-mode").slideDown();
			$(this).parents(".pcomment").find(".edit-mode").slideUp();		  
		}		
	}
/* FUN end save edited comment */
	
/* FUN like clicked */
	function likeClicked(e){		
		var likeid=$(this).parents(".likeholder").attr("id");
	}
/* FUN end like clicked */

/* FUN open share to connections */
	function openShareToConnections(){
		$(".share-connections").slideDown("slow");
	}
/* FUN end open share to connections */

/* FUN open group wall */
	function openShareToGroupwall(){
		$(".share-groupwall").slideDown("slow");
	}
/* FUN end open group wall */

/* FUN open share as message */
	function openShareAsMessage(){
		$(".share-message").slideDown("slow");
	}
/* FUN end open share as message */

/* FUN share privacy clicked */
	function sharePrivacyClicked(){
		if(!$(this).hasClass("share-to-connections"))
			$(".share-connections").slideUp("slow");
		if(!$(this).hasClass("share-to-groupwall"))
			$(".share-groupwall").slideUp("slow");
		if(!$(this).hasClass("share-as-message"))
			$(".share-message").slideUp("slow");
	}
/* FUN end share privacy clicked */

/* FUN pre-share post */	
	function hideShareExtras(){		
		$(".share-connections").hide();
		$(".share-groupwall").hide();
		$(".share-message").hide();
	}
	function preSharePostSetup(e){
		// hide all post extras : not input value inside - parameter = ID of super parent
		hideShareExtras();	
	}
/* FUN end pre-share post */

/* FUN open comment replies */
	function openReplies(obj){		
		$(obj).parents(".comments-reply-summery").slideUp();
		$(obj).parents(".comment-reply-holder").find(".comments-reply-details").slideDown();
	}
/* FUN end open comment replies */

/* FUN expand new post */
	function expandNewpost(obj){		
		if($(obj).parents(".reviews-column").length){		
			
			if($(obj).hasClass("expandReview")){
				$(obj).addClass("active");
				$(obj).find(".post-bcontent").show();
				$(obj).find(".expandable").slideDown();				
			}
		}else{
			/*
			$(obj).addClass("active");
			$(obj).find(".post-bcontent").slideDown();
			if($(".main-content").hasClass("travelstore-page")){					
				openAdCategory(obj);
			}
			*/
		}
	}

/* FUN end expand new post */

/* FUN open post category */
	function openAdCategory(obj){
		$(obj).find(".post-category").show("slow");
	}
/* FUN end open post category */

/* open view map inside post */
	function openViewMap(obj){
		var sparent=$(obj).parents(".post-content");
		var mapholder=sparent.find(".map-holder");
		
		if(mapholder.css("display")=="block"){
			mapholder.slideUp();
			$(obj).html("View on map");
		}else{
			mapholder.slideDown();
			$(obj).html("Close map");
		}
	}
/* end open view map inside post */

/* FUN open post title */
	function openTitle(obj){		
		var getParent=$(obj).parents(".new-post");			
		getParent.find('.npost-title').toggle("slow");		
		var slideOut=getParent.find(".sliding-middle-out");				
		setTimeout(function(){
			titleUnderline(slideOut);
		},800);		
	}
/* FUN end open post title */

/************ END POST FUNCTIONS ************/

/************ SETTINGS FUNCTIONS ************/

/* FUN set settings menu, this code in code folder we have move in accountsetting.js file */
	function resetInnerPage(fromWhere,state){
		if(fromWhere == "settings"){			
			// settings page
			var sparent = $(".settings-menuholder");
			if(state == "hide") {			
				sparent.addClass("gone");			
				var subtitle = $(".settings-content.active .formtitle h4").html();
				$(".page-wrapper .header-themebar .innerpage-name").html(subtitle);
				$(".page-wrapper .header-themebar").addClass("innerpage");
			} else {			
				if(sparent.hasClass("gone")) {
					sparent.removeClass("gone");
				}
				$(".page-wrapper .header-themebar .innerpage-name").html("");
				$(".page-wrapper .header-themebar").removeClass("innerpage");
			}			
		}

		if(fromWhere == "wall"){
			// wall page 
			var sparent = $(".main-content");
			if(state == "hide"){
				// opening a new tab except wall
				if($(".wallpage .tabs li.active").length > 0){
					var tabname = $(".wallpage .tabs li a.active").attr("tabname");				
					if(tabname!="Wall"){
						$(".page-wrapper .header-themebar .innerpage-name").html(tabname);
						$(".page-wrapper .header-themebar").addClass("innerpage");
						if(sparent.hasClass("gone")){
							sparent.removeClass("gone");
						}
					}
				}
			} else {
				$("ul.tabs li a[href='#wall-content']").trigger("click");	
				// closing a new tab except wall
				$(".page-wrapper .header-themebar .innerpage-name").html("");
				$(".page-wrapper .header-themebar").removeClass("innerpage");

				
				sparent.addClass("gone");
			}			
		}
	}

	function setplacetab() {
		var sparent = $(".main-content");
		var win_w = $(window).width();
		if(win_w < 768) {
			$('.header-section').show();
			if($('.header-section').hasClass('hide_header')) {
				$('.header-section').removeClass('hide_header');
			}

			$tabArray = ['Wall', 'Discussion', 'Reviews', 'Travellers', 'Locals', 'Photos', 'Tips', 'Ask'];
			$tabname = $('.tablist').find('.tab').find('a.active').attr('tabname');
			if($.inArray($tabname, $tabArray) !== -1) {
				if($tabname!="Wall") {
					$(".page-wrapper .header-themebar .innerpage-name").html($tabname).show();
					$(".page-wrapper .header-themebar").addClass("innerpage");
					if(sparent.hasClass("gone")) {
						sparent.removeClass("gone");
					}
					$(".page-wrapper .header-themebar").addClass("ismobile");
					$(".place-wrapper").find(".mobile-menu.topicon").find(".gotohome").show();
				} else {
					$(".page-wrapper .header-themebar").removeClass("innerpage ismobile");
					sparent.addClass("gone");
					$(".place-wrapper").find(".mobile-menu.topicon").find(".gotohome").hide();
				}
			}
		} else {
			$tabname = $('.tablist').find('.tab').find('a.active').attr('tabname');
			$(".page-wrapper .header-themebar .innerpage-name").html($tabname).hide();
			$(".page-wrapper .header-themebar .mainpage-name").show();
			$(".place-wrapper").find(".mobile-menu.topicon").find(".gotohome").hide();
		}
	}
	
	function resetPlacesTab() {
		var sparent = $(".main-content");
		var win_w = $(window).width();
		if(win_w < 768) { 
			$('.header-section').addClass('hide_header');
			$tabArray = ['Wall', 'Discussion', 'Reviews', 'Travellers', 'Locals', 'Photos', 'Tips', 'Ask'];
			$tabname = $('.tablist').find('.tab').find('a.active').attr('tabname');
			if($.inArray($tabname, $tabArray) !== -1) {
				$(".page-wrapper .header-themebar").removeClass("innerpage ismobile");
				sparent.addClass("gone");
				$("ul.tabs li a[href='#places-all']").trigger("click");	
				$(".place-wrapper").find(".mobile-menu.topicon").find(".gotohome").hide();
			}
		}
	}

	function setPageMainTab(fromWhere, obj){
		
		if(fromWhere == "settings"){
			
			var contentClass=$(obj).attr("class");
			
			$(".settings-content").each(function(){			
				if($(this).attr("id")!=contentClass){
					$(this).hide();
					if($(this).hasClass("active")){
						$(this).removeClass("active");
					}
				}
				else{
					if(!$(this).hasClass("active")){
						$(this).addClass("active");
						$(this).show();					
					}
				}	
			});
			$(".settings-menu .has_sub").each(function(){			
				$(this).find(".submenu > li").each(function(){
					$(this).removeClass("active");
				});
				if(!$(this).hasClass("opened") && $(this).hasClass("active")){
					$(this).removeClass("active");
				}
			});
			var getA = $(".settings-menu .has_sub .submenu > li a[class='"+contentClass+"']")
			getA.parents("li").addClass("active");
			resetInnerPage("settings","hide"); // hide settings menu in mobile			
		}
		setTimeout(function(){
			setGeneralThings();
			var winw = $(window).width();
			if(winw < 768 ){
				$('.main-content').animate({
					scrollTop: $(".settings-content-holder").offset().top},
				'slow');
			}else{
				$('html, body').animate({
					scrollTop: 0},
				'slow');
			}
		},1000);
	}
	function setSettingsMenu(obj){
		
		var thisParent=$(obj).parent("li");
		
		if(thisParent.hasClass("opened"))
		{
			thisParent.children(".submenu").slideUp(300);
			thisParent.removeClass("opened");					
		}
		else{					
			closeAllExpandedMenu(thisParent);
			thisParent.children(".submenu").slideDown(300);
			thisParent.addClass("opened");
		}
			
		resetInnerPage("settings","show"); // show settings menu in mobile
		
	} 
	function closeAllExpandedMenu(e){
			
		var thisParent=e;		
		$("ul#settings-menu li").each(function(){

			var regLi=$(this).parent("li");
		
			if($(this).hasClass("has_sub")){
				
				if($(this).hasClass("opened")){
					
					$(this).children(".submenu").slideUp(300);
					$(this).removeClass("opened");					
																					
				}							
			}
		});			
	}
	
	
// Add active class to the current button (highlight it)
$(function(){
    $('.settings-menu li').click(function(){
        $('li.active').removeClass('active');
        $(this).addClass('active');
    });
});
/* FUN end set settings menu */

/* FUN edit/normal mode settings page */
	function close_all_edit(){
	   var count=0;
	   $(".settings-ul .settings-group").each(function(){
			
		   var editmode=$(this).children(".edit-mode").css("display");		
		
		   if(editmode!="none"){
				
				$(this).children(".normal-mode").slideDown(300);
				$(this).children(".edit-mode").slideUp(300);
			}
	   });
   }

   	function open_edit_act_bf(obj) {
   		$('#menu-basicinfo').find('.editicon1').hide();
   		$('#menu-basicinfo').find('.editicon1').addClass('dis-none');

   		$('#menu-basicinfo').find('.editicon2').hide();
   		$('#menu-basicinfo').find('.editicon2').addClass('dis-none');

   		$('#menu-basicinfo').find('.normal-part').addClass('dis-none');
   		$('#menu-basicinfo').find('.normal-part').css('display', 'none');
   		$('#menu-basicinfo').find('.edit-part').removeClass('dis-none');
   		$('#menu-basicinfo').find('.edit-part').find('.edit-mode').show();
   		$('select').material_select();

		$('html, body').animate({
		    scrollTop: 0
		}, 400);

		$(".main-content.with-lmenu").scrollTop(0);
   	}

   	function open_edit_act_bf_cl(obj) {
   		var w = $(window).width();
   		if(w<=768) {
	   		$('#menu-basicinfo').find('.editicon2').show();
	   		$('#menu-basicinfo').find('.editicon2').removeClass('dis-none');

	   		$('#menu-basicinfo').find('.editicon1').hide();
   			$('#menu-basicinfo').find('.editicon1').addClass('dis-none');
   		} else {
   			$('#menu-basicinfo').find('.editicon1').show();
	   		$('#menu-basicinfo').find('.editicon1').removeClass('dis-none');

	   		$('#menu-basicinfo').find('.editicon2').hide();
   			$('#menu-basicinfo').find('.editicon2').addClass('dis-none');
   		}

   		$('#menu-basicinfo').find('.normal-part').removeClass('dis-none');
   		$('#menu-basicinfo').find('.normal-part').css('display', 'block');
   		$('#menu-basicinfo').find('.edit-part').addClass('dis-none');
   		$('#menu-basicinfo').find('.edit-part').find('.edit-mode').hide();

   		$('html, body').animate({
		    scrollTop: 0
		}, 400);

		$(".main-content.with-lmenu").scrollTop(0);
   	}

   	function open_edit_act_ss(obj) {
   		$('#menu-security').find('.editicon1').hide();
   		$('#menu-security').find('.editicon1').addClass('dis-none');

   		$('#menu-security').find('.editicon2').hide();
   		$('#menu-security').find('.editicon2').addClass('dis-none');

   		$('#menu-security').find('.normal-part').addClass('dis-none');
   		$('#menu-security').find('.normal-part').css('display', 'none');
   		$('#menu-security').find('.edit-part').removeClass('dis-none');
   		$('#menu-security').find('.edit-part').find('.edit-mode').show();
   		$('select').material_select();
		$('.dropdown-button').dropdown();

		$('html, body').animate({
		    scrollTop: 0
		}, 400);

		$(".main-content.with-lmenu").scrollTop(0);
   	}

   	function open_edit_act_ss_cl(obj) {
   		var w = $(window).width();
   		if(w<=768) {
	   		$('#menu-security').find('.editicon2').show();
	   		$('#menu-security').find('.editicon2').removeClass('dis-none');

	   		$('#menu-security').find('.editicon1').hide();
   			$('#menu-security').find('.editicon1').addClass('dis-none');
   		} else {
   			$('#menu-security').find('.editicon1').show();
	   		$('#menu-security').find('.editicon1').removeClass('dis-none');

	   		$('#menu-security').find('.editicon2').hide();
   			$('#menu-security').find('.editicon2').addClass('dis-none');
   		}

   		$('#menu-security').find('.normal-part').removeClass('dis-none');
   		$('#menu-security').find('.normal-part').css('display', 'block');
   		$('#menu-security').find('.edit-part').addClass('dis-none');
   		$('#menu-security').find('.edit-part').find('.edit-mode').hide();

   		$('html, body').animate({
		    scrollTop: 0
		}, 400);

		$(".main-content.with-lmenu").scrollTop(0);
   	}

   	function open_edit_act_blocking(obj) {
   		$('#menu-block').find('.editicon1').hide();
   		$('#menu-block').find('.editicon1').addClass('dis-none');

   		$('#menu-block').find('.editicon2').hide();
   		$('#menu-block').find('.editicon2').addClass('dis-none');

   		$('#menu-block').find('.normal-part').addClass('dis-none');
   		$('#menu-block').find('.normal-part').css('display', 'none');
   		$('#menu-block').find('.edit-part').removeClass('dis-none');
   		$('#menu-block').find('.edit-part').find('.edit-mode').show();
   		$('select').material_select();
		$('.dropdown-button').dropdown();

		$('html, body').animate({
		    scrollTop: 0
		}, 400);

		$(".main-content.with-lmenu").scrollTop(0);
   	}

   	function open_edit_act_blocking_cl(obj) {
   		var w = $(window).width();
   		if(w<=768) {
	   		$('#menu-block').find('.editicon2').show();
	   		$('#menu-block').find('.editicon2').removeClass('dis-none');

	   		$('#menu-block').find('.editicon1').hide();
   			$('#menu-block').find('.editicon1').addClass('dis-none');
   		} else {
   			$('#menu-block').find('.editicon1').show();
	   		$('#menu-block').find('.editicon1').removeClass('dis-none');

	   		$('#menu-block').find('.editicon2').hide();
   			$('#menu-block').find('.editicon2').addClass('dis-none');
   		}
   		
   		$('#menu-block').find('.normal-part').removeClass('dis-none');
   		$('#menu-block').find('.normal-part').css('display', 'block');
   		$('#menu-block').find('.edit-part').addClass('dis-none');
   		$('#menu-block').find('.edit-part').find('.edit-mode').hide();

   		$('html, body').animate({
		    scrollTop: 0
		}, 400);

		$(".main-content.with-lmenu").scrollTop(0);
   	}

   	function open_edit_bp_general(obj) {
   		$('#pagesettings-general').find('.editicon1').hide();
   		$('#pagesettings-general').find('.editicon1').addClass('dis-none');

   		$('#pagesettings-general').find('.editicon2').hide();
   		$('#pagesettings-general').find('.editicon2').addClass('dis-none');

   		$('#pagesettings-general').find('.normal-part').addClass('dis-none');
   		$('#pagesettings-general').find('.normal-part').css('display', 'none');
   		$('#pagesettings-general').find('.edit-part').removeClass('dis-none');
   		$('#pagesettings-general').find('.edit-part').find('.edit-mode').show();
   		$('select').material_select();
		$('.dropdown-button').dropdown();

		$(".tabs-detail").scrollTop(0);

		$('html, body').animate({
		    scrollTop: $(".tabs-detail").offset().top - 100
		}, 400);
   	}

   	function open_edit_bp_general_cl(obj) {
   		var w = $(window).width();
   		if(w<=767) {
	   		$('#pagesettings-general').find('.editicon2').show();
	   		$('#pagesettings-general').find('.editicon2').removeClass('dis-none');

	   		$('#pagesettings-general').find('.editicon1').hide();
   			$('#pagesettings-general').find('.editicon1').addClass('dis-none');
   		} else {
   			$('#pagesettings-general').find('.editicon1').show();
	   		$('#pagesettings-general').find('.editicon1').removeClass('dis-none');

	   		$('#pagesettings-general').find('.editicon2').hide();
   			$('#pagesettings-general').find('.editicon2').addClass('dis-none');
   		}
   		
   		$('#pagesettings-general').find('.normal-part').removeClass('dis-none');
   		$('#pagesettings-general').find('.normal-part').css('display', 'block');
   		$('#pagesettings-general').find('.edit-part').addClass('dis-none');
   		$('#pagesettings-general').find('.edit-part').find('.edit-mode').hide();

   		$(".tabs-detail").scrollTop(0);

		$('html, body').animate({
		    scrollTop: $(".tabs-detail").offset().top - 100
		}, 400);
   	}

   	function open_edit_bp_blocking(obj) {
   		$('#pagesettings-blocking').find('.editicon1').hide();
   		$('#pagesettings-blocking').find('.editicon1').addClass('dis-none');

   		$('#pagesettings-blocking').find('.editicon2').hide();
   		$('#pagesettings-blocking').find('.editicon2').addClass('dis-none');

   		$('#pagesettings-blocking').find('.normal-part').addClass('dis-none');
   		$('#pagesettings-blocking').find('.normal-part').css('display', 'none');
   		$('#pagesettings-blocking').find('.edit-part').removeClass('dis-none');
   		$('#pagesettings-blocking').find('.edit-part').find('.edit-mode').show();
   		$('select').material_select();
		$('.dropdown-button').dropdown();

		$(".tabs-detail").scrollTop(0);

		$('html, body').animate({
		    scrollTop: $(".tabs-detail").offset().top - 100
		}, 400);
   	}

   	function open_edit_bp_blocking_cl(obj) {
   		var w = $(window).width();
   		if(w<=767) {
	   		$('#pagesettings-blocking').find('.editicon2').show();
	   		$('#pagesettings-blocking').find('.editicon2').removeClass('dis-none');

	   		$('#pagesettings-blocking').find('.editicon1').hide();
   			$('#pagesettings-blocking').find('.editicon1').addClass('dis-none');
   		} else {
   			$('#pagesettings-blocking').find('.editicon1').show();
	   		$('#pagesettings-blocking').find('.editicon1').removeClass('dis-none');

	   		$('#pagesettings-blocking').find('.editicon2').hide();
   			$('#pagesettings-blocking').find('.editicon2').addClass('dis-none');
   		}
   		
   		$('#pagesettings-blocking').find('.normal-part').removeClass('dis-none');
   		$('#pagesettings-blocking').find('.normal-part').css('display', 'block');
   		$('#pagesettings-blocking').find('.edit-part').addClass('dis-none');
   		$('#pagesettings-blocking').find('.edit-part').find('.edit-mode').hide();

   		$(".tabs-detail").scrollTop(0);

		$('html, body').animate({
		    scrollTop: $(".tabs-detail").offset().top - 100
		}, 400);
   	}


	function open_edit(obj, type=null){
		close_all_edit();

		var obj=$(obj).parents(".settings-group");
				
		var editmode=obj.children(".edit-mode").css("display");

		if(editmode=="none"){
			obj.children(".normal-mode").slideUp(300);
			obj.children(".edit-mode").slideDown(300);
		}
		else{
			obj.children(".normal-mode").slideDown(300);
			obj.children(".edit-mode").slideUp(300);
		}

		var typeArray = ['my_view_status', 'connect_request', 'connect_list', 'view_photos', 'my_post_view_status', 'add_public_wall', 'review_posts', 'review_tags', 'add_post_on_your_wall_view'];

		if($.inArray(type, typeArray) !== -1) {
			$selectore = $(obj).find('.edit-mode').find('ul');
			var winw = $(window).width();
			if(winw <= 480) {
				if($selectore.length) {
					$selectore.parents('.left').find('a').attr('onclick', 'privacymodal(this)');
					$selectore.parents('.left').find('a').attr('data-activates', '');
					$selectore.addClass('forcefullyhide');
				}
			} else {
				if($selectore.length) {
					$data_activities = $selectore.attr('id');
					$selectore.parents('.left').find('a').attr('data-activates', $data_activities);
					$selectore.parents('.left').find('a').attr('onclick', '');
					$selectore.removeClass('forcefullyhide');
					$selectore.hide();
					$('.dropdown-button').dropdown();
				}
			}
		}

		setTimeout(function(){
			setGeneralThings();
		},400);
   }
	function close_edit(obj){ 
		
		var objParent=$(obj).parents(".settings-group");
		var emode=objParent.children(".edit-mode");
		var nmode=objParent.children(".normal-mode");
				
		var editmode=emode.css("display");

		if(editmode=="none"){
			nmode.slideUp(300);
			emode.slideDown(300);
		}
		else{
			nmode.slideDown(300);
			emode.slideUp(300);
		}
   }
/* FUN end edit/normal mode settings page */

/************ END SETTINGS FUNCTIONS ************/

/************ MESSAGE FUNCTIONS ************/
/* FUN mark message as read */
	function markRead(obj){
		var sparent=$(obj).parents("li");
		if(sparent.hasClass("read")){
			sparent.removeClass("read");
			$(obj).attr("title","Mark as read");
		}
		else{
			sparent.addClass("read");
			$(obj).attr("title","Mark as unread");
		}
	}
	function showAllMsgRead(){
		$(".msg-listing li").each(function(){
			$(this).addClass("read");
			$(this).find("a.readicon").attr("title","Mark as unread");
		});
	}
/* FUN end mark message as read */ 
/************ end MESSAGE FUNCTIONS ************/

/************ CHAT FUNCTIONS ************/

/* FUN set floating chat */
	function setFloatChat(){		
		var w_width=$(window).width();
		$(".float-chat").addClass("floating");		
	}
/* FUN end set floating chat */

/* FUN set chat window height */
	function setChatHeight(){
		var windowW = $(window).width();
		if (windowW <= 1024){
			var chatwindH = $('.chat-window ul.mainul').height();
			var backbtnH = $('.backChatList').height();
			var chattitleH = $('.chatwin-box .topbar').height();
			var chatTextareaH = $('.chat-add').height();
			var totaldiductH = backbtnH + chattitleH + chatTextareaH;

			$('.chat-conversation .chat-area').height(chatwindH - totaldiductH - 170);			
		}
		else{			
			$('.chat-conversation .chat-area').height(200);
			$(".nice-scroll").getNiceScroll().resize();
		}
	}

	$.fn.isVisible = function() {
		// Current distance from the top of the page
		var windowScrollTopView = $(window).scrollTop();

		// Current distance from the top of the page, plus the height of the window
		var windowBottomView = windowScrollTopView + $(window).height();

		// Element distance from top
		var elemTop = $(this).offset().top;

		// Element distance from top, plus the height of the element
		var elemBottom = elemTop + $(this).height();

		return ((elemBottom <= windowBottomView) && (elemTop >= windowScrollTopView));
	}	
/* FUN end set chat window height */

/* FUN manage chatbox */
	function manageChatbox(){
		var winw=$(window).width();				
		if(winw<=1024){			
			if(!$(".float-chat").hasClass("floating")){				
				$(".float-chat").find(".chat-tabs").addClass("shown");
				$(".float-chat").find(".chat-window").addClass("hidden");
			}
		}
		else{
			
			$(".float-chat").find(".chat-tabs").removeClass().addClass("chat-tabs");
			$(".float-chat").find(".chat-window").removeClass().addClass("chat-window");
		
		}
	
	}
/* FUN end manage chatbox */

/* FUN open-close chat window */
	function resetChatboxHolder(){
		$(".chat-window .mainul li").removeClass("active");
	}
	function openChatbox(obj){		
		$(obj).addClass("active");
		
		var userid=$(obj).attr("id");
		
		/* AJAX call to replace messages part */
		var exists=false;
		$(".chat-window .mainul li.mainli").each(function(){
			var li_id=$(this).attr("id");
			li_id=li_id.replace("li-","");
			
			if(li_id==userid){
				exists=true;				
			}
		});
		if(exists){
			resetChatboxHolder();
			$(".chat-window .mainul li#li-"+userid).addClass("active");
		}
		else{
			// generate new li
			resetChatboxHolder();			
			
			var imgsrc=$(obj).find(".img-holder").find("img").attr("src");
			var username=$(obj).find(".desc-holder").find(".uname").html();
					
			$(".chat-window .mainul").append("<li class='mainli active' id='li-"+userid+"'> <div class='chatwin-box bshadow'> <div class='topbar'> <div class='userinfo'> <img src='"+imgsrc+"'/> <h6>"+username+"</h6> <span class='online-dot'></span> </div> <div class='actions'> <div class='chat-search'><input type='text' placeholder='search a keyword'><div class='btn-holder'><a href='#' onclick='openChatSearch(this,\"close\")'><img src='images/cross-icon.png'/></a></div> </div> <a href='javascript:void(0)' onclick='openChatSearch(this,\"open\")'><i class='mdi mdi-magnify'></i></a> <a href='javascript:void(0)' onclick='closeChatbox(this)'><i class='mdi mdi-close'></i></a> </div> </div> <div class='chat-scroll nice-scroll'> <div class='chat-conversation'> <div class='chat-area nice-scroll'> <div class='dayinfo'>Today</div> <ul class='chat-mainul images-container'> <li class='chatli received'> <div class='imgholder'><img src='"+imgsrc+"'/></div> <ul class='submsg'> <li class='text-start'> <div class='msgholder'><p>Hi, Good Morning</p></div> <div class='timestamp'>10:00 am</div> </li> <li> <div class='msgholder'><p>How are you?</p></div> <div class='timestamp'>10:00 am</div> </li> </ul> </li><li class='date-divider'><span>31 Jan 2016</span></li> <li class='chatli'> <ul class='submsg'> <li> <div class='msgholder'><p>Hi, Good Morning</p></div> <div class='timestamp'>10:00 am</div> </li> <li> <div class='msgholder'><p>I am good.</p><p>How about you? What's going in life? Let's plan to meet up this weekend.</p></div> <div class='timestamp'>10:00 am</div> </li> </ul> </li> <li class='chatli received'> <div class='imgholder'><img src='"+imgsrc+"'/></div> <ul class='submsg'> <li> <div class='msgholder'><p>How is your day?</p></div> <div class='timestamp'>10:00 am</div> </li> </ul> </li> <li class='chatli received'> <div class='imgholder'><img src='"+imgsrc+"'/></div> <ul class='submsg'> <li> <div class='msgholder'><p>Hi, Good Morning</p></div> <div class='timestamp'>10:00 am</div> </li> <li> <div class='msgholder'><p>How are you?</p></div> <div class='timestamp'>10:00 am</div> </li> </ul> </li> <li class='chatli'> <ul class='submsg'> <li> <div class='msgholder'><p>Hi, Good Morning</p></div> <div class='timestamp'>10:00 am</div> </li> <li> <div class='msgholder'><p>I am good.</p></div> <div class='timestamp'>10:00 am</div> </li> </ul> </li> <li class='chatli received'> <div class='imgholder'><img src='"+imgsrc+"'/></div> <ul class='submsg'> <li> <div class='msgholder'><p>How is your day?</p></div> <div class='timestamp'>10:00 am</div> </li> </ul> </li> <li class='chatli received'> <div class='imgholder'><img src='"+imgsrc+"'/></div> <ul class='submsg'> <li> <div class='msgholder'><p>Hi, Good Morning</p></div> <div class='timestamp'>10:00 am</div> </li> <li> <div class='msgholder'><p>How are you?</p></div> <div class='timestamp'>10:00 am</div> </li> </ul> </li> <li class='date-divider'><span>31 Jan 2016</span></li><li class='chatli received'> <div class='imgholder'><img src='"+imgsrc+"'/></div> <ul class='submsg'> <li> <div class='msgholder'><p>Hi, Good Morning</p></div> <div class='timestamp'>10:00 am</div> </li> <li> <div class='msgholder'><p>How are you?</p></div> <div class='timestamp'>10:00 am</div> </li> </ul> </li> <li class='chatli'> <ul class='submsg'> <li> <div class='msgholder'><p>Hi, Good Morning</p></div> <div class='timestamp'>10:00 am</div> </li> <li> <div class='msgholder'><p>I am good.</p></div> <div class='timestamp'>10:00 am</div> </li> </ul> </li><li class='chatli'> <ul class='submsg'> <li> <div class='msgholder fileholder'> <div class='msg-img'><figure><a href='images/msg1.jpg' data-size='1600x1600' data-med='images/msg1.jpg' data-med-size='1024x1024' data-author='Folkert Gorter'><img src='images/msg1.jpg'/></a><a href='#' class='download'><i class='mdi mdi-download'></i></a></figure><div class='overlay'></div></div> </div> <div class='timestamp'>10:00 am</div> </li> <li> <div class='msgholder'><p>I am good.</p></div> <div class='timestamp'>10:00 am</div> </li> </ul> </li> <li class='chatli received'> <div class='imgholder'><img src='"+imgsrc+"'/></div> <ul class='submsg'> <li> <div class='msgholder'><p>How is your day?</p></div> <div class='timestamp'>10:00 am</div> </li> </ul> </li><li class='date-divider'><span>31 Jan 2016</span></li><li class='chatli received'> <div class='imgholder'><img src='"+imgsrc+"'/></div> <ul class='submsg'> <li> <div class='msgholder fileholder'><div class='msg-img'><figure><a href='images/msg2.jpg' data-size='1600x1600' data-med='images/msg2.jpg' data-med-size='1024x1024' data-author='Folkert Gorter'><img src='images/msg2.jpg'/></a><a href='#' class='download'><i class='mdi mdi-download'></i></a></figure><div class='overlay'></div></div></div> <div class='timestamp'>10:00 am</div> </li> </ul> </li> </ul> </div> </div> <div class='chat-add'> <textarea placeholder='Write your message here' class='materialize-textarea' onkeypress='chatLineAdded(event,this)'></textarea> <div class='chat-action emotion-holder'><div class='emotion-box'><div class='nice-scroll emotions'><ul class='emotion-list'></ul></div></div><a href='javascript:void(0)' class='emotion-btn' onclick='manageEmotionBox(this)'><i class='zmdi zmdi-mood'></i></a><div class='custom-file'><div class='title'><a href='#'><i class='mdi mdi-paperclip'></i></a></div><input name='upload' class='upload custom-upload' title='Choose a file to upload' required='' data-class='.main-content .post-photos .img-row' multiple='' type='file'></div><a href='#giftlist-popup' class='popup-modal giftlink' onmouseover='$(this).find('img').attr('src','images/giftcard-icon-hover.png');' onmouseout='$(this).find('img').attr('src','images/giftcard-icon.png');' onclick='manageEmoStickersBox('popular')'><img src='images/giftcard-icon.png'></a><a href='#' class='send-chat'><i class='mdi mdi-telegram'></i></a> </div> </div> </div> </div> </li>");
		}
		
		var isMobileChat=$(".float-chat").hasClass("floating");
		if(isMobileChat){
			var winw=$(window).width();				
			if(winw<=1024){			
				$(".float-chat").find(".chat-tabs").addClass("hidden");
			}
			if($(".float-chat").find(".chat-window").hasClass("hidden")){
				$(".float-chat").find(".chat-window").removeClass("hidden");
				$(".float-chat").find(".chat-window").addClass("shown");
			}else{
				$(".float-chat").find(".chat-window").addClass("shown");
			}
		}
		else{
			$(".float-chat").find(".chat-tabs").removeClass().addClass("chat-tabs");
			$(".float-chat").find(".chat-window").removeClass().addClass("chat-window");
		}	
		setChatHeight();
		setTimeout(function(){scrollChatBottom();},500);
				
	}
	function closeChatboxes(){
		var isMobileChat=$(".float-chat").hasClass("floating");
		if(isMobileChat){
			$(".float-chat").find(".chat-window").addClass("hidden");
			if($(".float-chat").find(".chat-tabs").hasClass("hidden")){
				$(".float-chat").find(".chat-tabs").removeClass("hidden");
				$(".float-chat").find(".chat-tabs").addClass("shown");
			}			
		}else{
			resetChatboxHolder();
		}
	}
	function closeChatbox(obj){
		var sparent=$(obj).parents(".mainli");		
		sparent.removeClass("active");
		var isMobileChat=$(".float-chat").hasClass("floating");
		if(isMobileChat){
			closeChatboxes();		
		} 
	}
	function manageEmotionBox(obj,fromWhere){
		var sparent=$(obj).parents(".emotion-holder");
		sparent.find(".emotion-box").toggle(200);
		initNiceScroll(".nice-scroll");
		
		/* SM */
		
		var $faces = [':)',':(',':D','8)',':o',';(','(sweat)',':*',':P','(blush)',':^)','|-)','|(','(inlove)',']:)','(talk)','|-(','(puke)','(doh)','x-(','(wasntme)','(party)',':S','(mm)','8-|',':x','(hi)','(call)','(devil)','(angel)','(envy)','(wait)','(hug)','(makeup)','(giggle)','(clap)',':?','(bow)','(rofl)','(whew)','(happy)','(smirk)','(nod)','(shake)','(punch)','(emo)','(y)','(n)','(handshake)','(skype)','<3','(u)','(e)','(f)','(rain)','(sun)','(time)','(music)','(movie)','(ph)','(coffee)','(pizza)','(cash)','(muscle)','(cake)','(beer)','(d)','(dance)','(ninja)','(*)','(mooning)','(finger)','(bandit)','(drunk)','(smoking)','(toivo)','(rock)','(headbang)','(bug)','(fubar)','(poolparty)','(swear)','(tmi)','(heidy)','(MySpace)','(malthe)','(tauri)','(priidu)'];
		$(".emotion-box ul.emotion-list").html("");
		$.each($faces, function(i, v){
			var faces = $.emoticons.replace(v);
            $(".emotion-box ul.emotion-list").append('<li><a href="javascript:void(0)" data-class="'+v+'" onclick="appendEmotion(\''+ v + '\',\''+ fromWhere + '\');">'+faces+'</a></li>');
		});
		
	}
	function appendEmotion(emovalue,fromWhere){		
		if(fromWhere=="messages"){
			var getText=$(".addnew-msg .write-msg").find("textarea").val();
			$(".addnew-msg .write-msg").find("textarea").val(getText+emovalue);
		}else{
			var getText=$(".chat-add").find("textarea").val();
			$(".chat-add").find("textarea").val(getText+emovalue);
		}
	}
/* FUN end open-close chat window */

/* FUN open chat search */	
	function openChatSearch(obj,doWhat){
		
		var sparent=$(obj).parents(".actions");
		if(doWhat=="open")
			sparent.find(".chat-search").fadeIn();
		else
			sparent.find(".chat-search").fadeOut();
	}
/* FUN end open chat search */

/* FUN new chat line added */
	function scrollChatBottom(){
		
		if($('.chat-window').find('.mainli.active').length) {
			var scrollHeight= $(".chat-window .mainli.active .chat-mainul").height();		
			
			setTimeout(function(){
				
				var windowW = $(window).width();
				if (windowW > 1024){
					if(!$(".chat-window .mainli.active .chat-area.nice-scroll").getNiceScroll().length){
						initNiceScroll(".nice-scroll");
					}
					$(".chat-window .mainli.active .chat-area.nice-scroll").getNiceScroll(0).doScrollTop(scrollHeight, -1); 
				}else{
					removeNiceScroll(".nice-scroll");
				}
				setOpacity($('.chat-window .mainli.active .chat-area.nice-scroll'),1);				
			},500);
		}
	}
	function setOpacity(obj,opacity){
		$(obj).css("opacity",opacity).change();
	}
	function removeLoaderImg(obj){
		$(obj).css("background","none");
	}
	function setLoaderImg(obj){		
		$(obj).css("background","auto");
	}
	function appendEmotions(obj,comment){
		
		var textWithEmoticons = $.emoticons.replace(comment);
		var addHtml="<li class='chatli'> <ul class='submsg'> <li> <div class='msgholder'>"+textWithEmoticons+"</div> <div class='timestamp'>10:00 am</div> </li>  </ul> </li>";
		$(obj).parents(".chat-scroll").find(".chat-conversation ul.chat-mainul").append(addHtml);
		
	}
	function chatLineAdded(e,obj){	
			
		
		e = e || window.event;
		if (e.keyCode == 13) {
				
			var getText=$(obj).val();				
			appendEmotions(obj,getText);
			
			$(obj).val("");
			scrollChatBottom();
		}
	}
	function scrollMessageBottom(){
		if($('.main-msgwindow .current-messages').find('.mainli.active').length) {
			var scrollHeight= $(".main-msgwindow .current-messages .mainli.active ul.outer").height();		
			setTimeout(function(){
				var windowW = $(window).width();
				if (windowW > 1024){
					if(!$(".main-msgwindow .current-messages .mainli.active .msgdetail-list.nice-scroll").getNiceScroll().length){
						initNiceScroll(".nice-scroll");
					}				
					$('.main-msgwindow .current-messages .mainli.active .msgdetail-list.nice-scroll').getNiceScroll(0).doScrollTop(scrollHeight, -1);
				}else{
					removeNiceScroll("nice-scroll");
				}
				setOpacity($('.main-msgwindow .current-messages .mainli.active .msgdetail-list.nice-scroll'),1);				
			},500);
		}
	}
	
/* FUN end new chat line added */

/************ END CHAT FUNCTIONS ************/

/************ CHANNELS FUNCTIONS ************/

/* FUN  subscribe / unsubscribe channels */	
	function channelSubscribe(event,col_id,obj){
		
		var isNoClick=$(event.target).hasClass("noClick");
		
		event.stopPropagation();
		if(isNoClick)
		{
		   // Follow clicked
		   var stat=$(obj).html();

			if(stat=="Subscribe"){
				// start following
				$(obj).html("Unsubscribe");
				$(obj).addClass("disabled");
			}else{
				// do unfollow
				$(obj).html("Subscribe");
				$(obj).removeClass("disabled");
			}
		}
		
	}
/* FUN end subscribe / unsubscribe channels */

/************ END CHANNELS FUNCTIONS ************/

/************ COLLECTIONS FUNCTIONS ************/

/* FUN follow / unfollow collection */	
	function collectionFollow(event,col_id,obj){
		
		var isNoClick=$(event.target).hasClass("noClick");
		
		event.stopPropagation();
		if(isNoClick)
		{
		   // Follow clicked
		   var stat=$(obj).html();

			if(stat=="Follow"){
				// start following
				$(obj).html("Following...");
			}else{
				// do unfollow
				$(obj).html("Follow");
			}
		} 
		
	}
/* FUN end follow / unfollow collection */

/************ END COLLECTIONS FUNCTIONS ************/

/************ PAGES FUNCTIONS ************/

/* create page : mobile dropdown */
	function resetPageCreateTabs(){
		$(".pages-page .vertical-tabs .text-menu ul.tabs li").each(function(){
			$(this).removeClass("active");
		});
	}  
/* end create page : mobile dropdown */

/* tab box : tabs navigation */
	function navigateTabs(nextTab,obj){
		
		var superParent=$(obj).parents(".tabs-box");		
		superParent.find('a[href="#'+nextTab+'"]').trigger('click');
	}	
/* end tab box : tabs navigation */

/* FUN like / unlike page */	
	function doPageLike(event,col_id,obj){
		
		var isNoClick=$(event.target).hasClass("noClick");
		
		event.stopPropagation();
		if(isNoClick)
		{
		   // Follow clicked
		   var stat=$(obj).html();

			if(stat=="Like"){
				// start following
				$(obj).html("Liked");
				$(obj).addClass("disabled");
			}else{
				// do unfollow
				$(obj).html("Like");
				$(obj).removeClass("disabled");
			}
		} 
		
	}
/* FUN end like / unlike page */

/************ END PAGES FUNCTIONS ************/

/************ BUSINESS PAGE FUNCTIONS ************/

/* FUN manage endorsement */
	function manageEndorsement(obj){
		var doWhat="";
		var sparent=$(obj).parents(".servicedetail");
		var mparent=$(obj).parents(".service-box");
		
		var getText=$(obj).html();
		if(getText=='<i class="mdi mdi-plus"></i>'){doWhat="add";}
		else{doWhat="remove";}
		
		if(doWhat=="add"){
			$(obj).addClass("remove");
			$(obj).html('<i class="mdi mdi-minus"></i>');
			if(sparent.find("ul").find("li:eq(0)").length){
				sparent.find("ul").find("li:eq(0)").before('<li class="active"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></li>');
			}else{
				sparent.find("ul").append('<li class="active"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></li>');
			}
			
			var newcount = parseInt(mparent.find(".count").html(), 10);
			newcount++;
			mparent.find(".count").html(newcount);
			
		}else{
			$(obj).removeClass("remove");
			$(obj).html('<i class="mdi mdi-plus"></i>');
			sparent.find("ul li:first-child").remove();
			
			var newcount = parseInt(mparent.find(".count").html(), 10);
			newcount--;
			mparent.find(".count").html(newcount);
			
		}
	}
/* FUN end manage endorsement */

/* FUN manage settings section of business page */
	function hideTabContents(){		
		if($(".page-wrapper").hasClass("businesspage")){
			$(".main-tabs .tabs > li").each(function(){
				if($(this).hasClass("active")) $(this).removeClass("active");
			});
			$(".sub-tabs .tabs > li").each(function(){
				if($(this).hasClass("active")) $(this).removeClass("active");
			});
			$(".main-tab-content .outer-tab").each(function(){
				if($(this).hasClass("active in")) $(this).removeClass("active in");
			});
		}
	}
	function closePageSettings(){
		if($(".page-wrapper").hasClass("businesspage")){			
			$("#wall-content").addClass("active in");
			$(".main-tabs .tabs > li").each(function(){
				if($(this).find("a").html()=="Wall"){
					$(this).addClass("active");
				}
			});
			$("#settings-content").removeClass("active in");
		}
	}
	function openPageSettings(state){
		
		if($(".page-wrapper").hasClass("businesspage")){
			hideTabContents();
			$("#wall-content").removeClass("active in");
			$("#settings-content").addClass("active in");			
		}
	}
/* FUN end manage settings section of business page */

/* FUN manaage info text - business page settings : page role */
	function resetInfoText(obj,what){
		var msg="";
		if(what=="Editor")msg="Can edit the page, send messages, settings expect assigning role, publish the page, create ads, create a post or comment and view insights.";
		else if(what=="Supporter")msg="Can respond to and delete comments on the page, send messages as the page and respond to the messages, create ads and view insights.";
		else msg="Can manage all aspects of the page inlcuding sending messages and publishing as the page, creating ads, seeing which admin created a post or comment, viewing insights and assigning page roles.";
		
		$(".infotext").html(msg);
	}
/* FUN end manaage info text - business page settings : page role */

/* FUN remove page role - business page settings : page role */
	function removePageRole(obj){		
		$(obj).parents(".pagerole-box").remove();
	}
/* FUN end remove page role - business page settings : page role */

/* FUN add page role - business page settings : page role */
	function resetAddRoleBox(){		
		$(".addrole-name").val("");
		$(".chooseRole").html('Admin <span class="caret"></span>');		
	}
	function addPageRole(){
		var whichRole="";
		var chooseRole=$(".chooseRole").html().trim();
		chooseRole=chooseRole.replace('<span class="caret"></span>','');
		whichRole=chooseRole.trim();
		
		var userName=$(".pagerole-holder.addrole-box").find(".addrole-name").val();
		if(userName==undefined || userName=="")
			userName="User name";
				
		var pagerole_html="";
		var parentDiv="";
		
		if(whichRole=="Admin"){
			parentDiv="adminrole";
			
			pagerole_html="<div class='pagerole-box'><div class='imgholder'><img src='images/demo-profile.jpg'/></div> <div class='descholder'> <h5>"+userName+"</h5> <div class='frow'> <p>Admin</p> </div><a href='javascript:void(0)' class='closebtn' onclick='removePageRole(this)'><i class='mdi mdi-close'></i></a></div></div>";
			
		}else if(whichRole=="Editor"){
			parentDiv="editorrole";
			
			pagerole_html="<div class='pagerole-box'><div class='imgholder'><img src='images/demo-profile.jpg'/></div> <div class='descholder'> <h5>"+userName+"</h5> <div class='frow'> <p>Editor</p> </div><a href='javascript:void(0)' class='closebtn' onclick='removePageRole(this)'><i class='mdi mdi-close'></i></a></div></div>";
		}else{
			parentDiv="supporterrole";
			
			pagerole_html="<div class='pagerole-box'><div class='imgholder'><img src='images/demo-profile.jpg'/></div> <div class='descholder'> <h5>"+userName+"</h5> <div class='frow'> <p>Supporter</p> </div><a href='javascript:void(0)' class='closebtn' onclick='removePageRole(this)'><i class='mdi mdi-close'></i></a></div></div>";
		}
		
		$(".pagerole-holder."+parentDiv).append(pagerole_html);
		
		resetAddRoleBox();
	}
/* FUN end add page role - business page settings : page role */

/* FUN hide post from review list */
	function hideReviewPost(obj,status){
		if(status=="approved"){			
			// code for adding the post to main list
		}else{
			// code for removing the post from review list
		}		
		$(obj).parents(".mainli").remove();
	}
/* FUN end hide post from review list */

/* FUN test business button - business page */
	function resetAdditionalInfo(){
		$(".additional-info").find(".info").hide();
	}
	function testBusinessButton(){
		if($(".hidden-businessbtn").find(".simple-tooltip").length>0){			
			$(".simple-tooltip").trigger("click");
		}
		if($(".hidden-businessbtn").find(".simple-link").length>0){
			var href = $('.simple-link').attr('href');
			window.open(href, '_blank');
		}
	}
	function setAdditionalInfo(){
		
		resetAdditionalInfo();
		var getType=$("#businessbtn-type").val();
		if(getType=="Call Now" || getType=="Contact Us"){
			$(".additional-info").find(".info.phone-info").show();
		}
		else if(getType=="Send Email" || getType=="Send Message"){			
			$(".additional-info").find(".info.mail-info").show();
		}
		else{
			$(".additional-info").find(".info.url-info").show();
		}
	}
	function deleteBusinessButton(){
		$(".hidden-businessbtn").html("");
		$(".businessbtn-caption").html("Add action button");
		$(".businessbtn-caption").parents(".dropdown").addClass("grayed-out");
	}
	function updateBusinessButton(){
		var getValue="";
		var getType=$("#businessbtn-type").val();
		
		$(".hidden-businessbtn").html("");		
		
		if(getType=="Call Now" || getType=="Contact Us"){			
			var getValue=$("#phone-info").val();
			$(".hidden-businessbtn").html('<a href="javascript:void(0)" class="simple-tooltip" title="'+getValue+'">'+getType+'</a>');
			$('.simple-tooltip').tooltipster({
				contentAsHTML: true,
				trigger:'click',
				theme: ['tooltipster-borderless']
			});			
		}
		else if(getType=="Send Email" || getType=="Send Message"){			
			var getValue=$("#mail-info").val();
			$(".hidden-businessbtn").html('<a href="mailto:'+getValue+'" class="simple-link">Link</a>');
		}
		else{
			getValue=$("#url-info").val();			
			$(".hidden-businessbtn").html('<a href="http://'+getValue+'" target="blank" class="simple-link">Link</a>');
		
		}
		
		$(".businessbtn-caption").html(getType);
		if($(".hidden-businessbtn").parents(".dropdown").hasClass("grayed-out")){
			$(".hidden-businessbtn").parents(".dropdown").removeClass("grayed-out");
		}
	}
/* FUN end test business button - business page */

/************ END BUSINESS PAGE FUNCTIONS ************/

/************ COMMUNITY EVENTS FUNCTIONS ************/

/* FUN join / unjoin community events */	
	function commeventsGoing(event,ce_id,obj){
		
		var isNoClick=$(event.target).hasClass("noClick");
		
		event.stopPropagation();
		if(isNoClick)
		{
		   // Follow clicked
		   var stat=$(obj).html();

			if(stat=="Attend"){
				// going
				$(obj).html("Going...");
			}else{
				// will attend
				$(obj).html("Attend");
			}
		} 
		else
		{
		   // redirtect to collection detail
		}
		
	}	
/* FUN end join / unjoin community events */

/************ END COMMUNITY EVENTS FUNCTIONS ************/

/************ GROUPS FUNCTIONS ************/

/* FUN join / unjoin groups */	
	function groupsJoin(event,ce_id,obj){
		
		var isNoClick=$(event.target).hasClass("noClick");
		
		event.stopPropagation();
		if(isNoClick)
		{
		   // Follow clicked
		   var stat=$(obj).html();

			if(stat=="Join"){
				// start following
				$(obj).html("Request Sent");				
			}
			else{
				// do unfollow
				$(obj).html("Join");				
			}
		}		
	}	
/* FUN end join / unjoin groups */

/************ END GROUPS FUNCTIONS ************/

/************ GENERAL PAGE FUNCTIONS ************/

/* FUN general details manage expandable - summery */
	function reset_gdetails_expandable(){
		
		if($(".main-content").hasClass("generaldetails-page")){			
			var sparent=$(".generaldetails-page").find(".gdetails-summery").find(".gdetails-moreinfo");
			
			var win_w=$(window).width();			
			//if(win_w>800){
				sparent.find(".expandable-area").slideDown();
				sparent.addClass("expanded");
				var obj=sparent.find(".invertsign");
				invertExpandLink(obj, "up","maindown");
			//}
			/*
			else{
				sparent.find(".expandable-area").slideUp();
				sparent.removeClass("expanded");
				var obj=sparent.find(".invertsign");
				invertExpandLink(obj, "down","maindown");
			}
			*/
		}
		
	}
/* FUN end general details manage expandable - summery */

/* FUN general-details page : reset detail tabs */	
	function resetDetailTabs(obj, pageName, tabContent){
		
		$("."+pageName+" .post-column .tab-pane.main-pane").each(function(){
			if(tabContent!=$(this).attr("id")){
				$(this).removeClass("active in");
			}
		});
		$("."+pageName+" .gdetails-summery .tabs").each(function(){
			var thisNav=$(this);
			thisNav.find("a").each(function(){
				if($(this).attr("href")!=("#"+tabContent)){
					$(this).parent("li").removeClass("active");					
				}else{
					if(pageName=="groups-details"){
						var what="";
						if(tabContent=="groups-discussion"){what="discussion";$(".details.searcharea").show();}
						else if(tabContent=="groups-events"){what="event";$(".details.searcharea").show();}
						else if(tabContent=="groups-photos"){what="photos";$(".details.searcharea").show();
							initGalleryImageSlider(".images-container");
						}
						else if(tabContent=="groups-members"){what="member";$(".details.searcharea").show();}
						else{what="";$(".details.searcharea").hide();}
						
						$(".groupsearch-input").attr("placeholder","Search "+what);						
					}
				}
			});
		});		
		if(tabContent == "commevents-discussion" || tabContent == "groups-discussion"){
			//$(".discuss-search").show();
			manageNewPostBubble("showit");
		}else{
			//$(".discuss-search").hide();
			manageNewPostBubble("hideit");
		}
		
		// reset the active tabs menu
		if( $(this).parents("people").length > -1 ){
			$(".gdetails-summery .tabs").each(function(){				
				if($(this).parents("people").length <= 0 ){
					$(this).find("li.active").removeClass("active");
				}
			});
		}else{
			$(".gdetails-summery .people .tabs li.active").removeClass("active");
		}
		
		var win_w = $(window).width();
		if(win_w < 1024){
			var invertLink = $(obj).parents(".gdetails-moreinfo").find(".expandable-link");
			invertLink.trigger("click");			
		}
		// scroll upto the section
		var win_w = $(window).width();
		if(win_w < 768){
			$('html,body').animate({
				scrollTop: $(".general-details .post-column").offset().top - 110},
			'slow');
		}else{
			$('html,body').animate({
			scrollTop: 0},
			'slow');
		}
		
	}
/* FUN end general-details page : reset detail tabs */

/************ END GENERAL PAGE FUNCTIONS ************/

/************ HANGOUT FUNCTIONS ************/

/* reset hangout forms */
	function resetHangoutForm(fromWhere){
		var sparent;
		if(fromWhere == "desktop"){
			sparent = $(".main-page");
		}else{
			sparent = $(".popup-area.editpost-popup");			
		}
		var formobj = sparent.find(".hangoutevent-form");
		
		formobj.find(".locinput").removeAttr('value');
		formobj.find(".addinput").removeAttr('value');
		formobj.find(".dateinput").removeAttr('value');
		formobj.find(".timeinput").removeAttr('value');
		
		formobj.find(".descinput").val("");
	}
/* end reset hangout forms */
	
/************ END HANGOUT FUNCTIONS ************/

/************ WEEKEND ESCAPE FUNCTIONS ************/

/* reset weekend escape forms */
	function resetEscapeForm(fromWhere){
		var sparent;
		if(fromWhere == "desktop"){
			sparent = $(".main-page");
		}else{
			sparent = $(".popup-area.editpost-popup");			
		}
		var formobj = sparent.find(".escapeevent-form");
		
		formobj.find(".locinput").removeAttr('value');
		formobj.find(".dateinput").removeAttr('value');		
		formobj.find(".descinput").val("");
	}
/* end reset weekend escape forms */

/************ END WEEKEND ESCAPE FUNCTIONS ************/

/************ HIRE A GUIDE FUNCTIONS ************/

/* reset local guide form */
	function resetHireGuideForm(fromWhere){
		var sparent;
		if(fromWhere == "desktop"){
			sparent = $(".main-page");
		}else{
			sparent = $(".popup-area.editpost-popup");			
		}
		var formobj = sparent.find(".localguideevent-form");
		
		formobj.find(".descinput").val("");		
		//$(".feeinput").select2("val", null);
		formobj.find(".activity-checkboxes input[type='checkbox']").each(function(){
			$(this).attr('checked', false);
		});
		
	}
/* end reset local guide forms */

/************ END HIRE A GUIDE FUNCTIONS ************/

/************ TRAVELBUDDY FUNCTIONS ************/

/* reset travelbuddy form */
	function resetTravelbuddyForm(fromWhere,which){
		var sparent;
		if(fromWhere == "desktop"){
			sparent = $(".main-page");
		}else{
			sparent = $(".popup-area.editpost-popup");			
		}
		var formobj;
		if(which == "trip"){
			formobj = sparent.find(".travelbuddytrip-form");
			
			formobj.find(".citynameinput").removeAttr('value');
			formobj.find(".arrivinginput").removeAttr('value');
			formobj.find(".leavinginput").removeAttr('value');
			formobj.find(".plansinput").val("");		
		}else{
			formobj = sparent.find(".travelbuddyinfo-form");
			
			formobj.find(".host-services input[type='checkbox']").each(function(){
				$(this).attr('checked', false);
			});
			formobj.find(".messageinput").val("");		
		}
		
		
	}
/* end reset travelbuddy forms */

/* flip travelbuddy forms */
	function flipTravelbuddyForms(toWhere){
		if(toWhere == "trip"){
			$(".travelbuddy-profile.profile-tab .flipper").attr("class","flipper front");
		}else{
			$(".travelbuddy-profile.profile-tab .flipper").attr("class","flipper back");
		}
		setTimeout(function(){setGeneralThings();},400);
	}
/* end flip travelbuddy forms */

/************ END TRAVELBUDDY FUNCTIONS ************/

/************ ADVERTISE FUNCTIONS **********/

	function adPhotoInputChange(obj, evt, imgSize){
		
		// open pop up before upload image processing...........
		// change adphoto popup
		var tgt = evt.srcElement || evt.target,
		files = tgt.files;
		
		// FileReader support
		var fr = new FileReader();
		fr.onload = function (e) {	
			//adImgCrop('');
			adImgCrop(e.target.result,imgSize);
			
			if(imgSize=="adImageOne")
				changeAdPhoto('adImageOne'); // 575 x 215
			else if(imgSize=="adImageTwo")
				changeAdPhoto('adImageTwo'); // 575 x 243
			else if(imgSize=="adImageThree")
				changeAdPhoto('adImageThree'); // 575 x 260
			else if(imgSize=="adImageFour")
				changeAdPhoto('adImageFour'); // 575 x 290
			else
				changeAdPhoto('adImageLogo'); // 150 x 150
		}
		fr.readAsDataURL(files[0]);
	}

/* resetting functions */
	function resetAdDetailsDone(){
		var win_w=$(window).width();
		
		$(".detail-box .travadvert-section").each(function(){
			$(this).removeClass("done");
		});
		
	}
	function resetAdDetails(){		
		var win_w=$(window).width();
	
		$(".detail-box .travadvert-section").each(function(){
			$(this).removeClass("active");				
		});
		$('html, body').animate({ scrollTop: 0 }, 'slow');
		
	}
	function resetAdLinks(){
		$(".travad-navs > li").each(function(){
			if($(this).hasClass("active"))
				$(this).addClass("done");
			$(this).removeClass("active");
		});
	}
	function resetAdObjective(obj){
		$(".travad-accordion .mainhead").each(function(){			
			$(this).removeClass("active");
		});
		
		$(obj).parents(".mainhead").addClass("active");
	}
	function resetObjectDetails(adObjective,objName){
		
		$(".main-accContent .travad-detailbox").hide();
		$(".travad-accordion.travad-details .sub-title").html(objName);
		$(".main-accContent .travad-detailbox."+adObjective).show();
		
	}
/* end resetting functions */
	
/* navigation function */
	function navigateAdDetails(obj, fromWhere){
		
		if(fromWhere=="topnav"){
			
			var fparent=$(obj).parent("li");
			var isDisabled=fparent.attr("class");
			
			if(isDisabled!="disabled" && isDisabled!="active"){
				
				var dataClass=fparent.attr("data-class");
				var dataDetailid=fparent.attr("data-detailid");
				var stepCount=dataClass.replace("step","");				
				
				var prevStep=getPrevStep();
				var isValidated=getAdValidate(prevStep, dataClass);
				if(isValidated){
					resetAdLinks();
					fparent.addClass("active");
									
					resetAdDetails();
					$(".detail-box .travadvert-section#"+dataDetailid).addClass("active");
					
				}
				else{
					$(".detail-box .settings-notice .error-note").html("Please fill the mendatory fields!");
					$(".detail-box .settings-notice .error-note").fadeIn(3000).delay(3000).fadeOut(3000)
				}
				
				if(dataClass=="step2"){
					// audience meter
					$(".audience-meter").click(function(){
					   var rand = Math.floor((Math.random() * 100) + 1);
					   gauge = new Gauge($('.gauge2'), {
								values: {
									0 : 'Specific',
									20: 'Good',
									100: 'Broad'
								},
								colors: {
									0 : '#ED1C24',
									20 : '#39B54A',
									80: '#FDD914'
								},
								angles: [
									180,
									360
								],
								lineWidth: 20,
								arrowWidth: 10,
								arrowColor: '#ccc',
								inset:true,

								value: rand});
							});
					$('.gauge2').gauge({
						values: {
							0 : 'Specific',
							20: 'Good',
							100: 'Broad'
						},
						colors: {
							0 : '#ED1C24',
							20 : '#39B54A',
							80: '#FDD914'
						},
						angles: [
							180,
							360
						],
						lineWidth: 20,
						arrowWidth: 10,
						arrowColor: '#ccc',
						inset:true,

						value: 10
					});
							
				}
			}
		}
		else{
			var fparent=$(obj).parent(".dirinfo");
			var nextDataClass=fparent.attr("data-ndataclass");
			var currStep=fparent.attr("data-class");
			
			if(nextDataClass=="step2"){
				var adObjective="pagelikes";
				var objName="Page Engagement";					
									
				adObjective=$(obj).attr("dataobj");
				objName=$(obj).attr("objname");

				resetAdObjective(obj);
				resetObjectDetails(adObjective,objName);
			}
			
			if(nextDataClass=="step3"){
				var selectedObj="";
				$(".travad-accordion .mainhead").each(function(){			
					if($(this).hasClass("active")){
						selectedObj=$(this).find(".dirinfo").find("a").attr("dataobj");
					}
				});
									
			}
			
			var isValidated=getAdValidate(currStep, nextDataClass);
			
			var linkObjParent = $(".travad-navs > li[data-class='"+nextDataClass+"']");

			if(isValidated){
				
				linkObjParent.attr("class","done"); // remove Disabled class from next step
				
				var linkObj = linkObjParent.find("a");
				navigateAdDetails(linkObj, "topnav");
			}
			else{
				$(".detail-box .settings-notice .error-note").html("Please fill the mendatory fields!");
				$(".detail-box .settings-notice .error-note").fadeIn(3000).delay(3000).fadeOut(3000);
			}
		}
	}
/* end navigation function */

/* manage function */
	function getAdValidate(currentStep, nextStep){
		// check validation for $(".detail-box .travadvert-section#"+currentStep+"-detail")
		return true;
	}
	function getPrevStep(){
		var step="";
		$(".travad-navs > li").each(function(){
			if($(this).hasClass("active"))
				step=$(this).attr("data-calss");			
		});
		return step;
	}
	function textCounter(obj,uplimit,counter) {
		
		var tt = $(obj).val().length;
		if(tt<=uplimit){
			var charLeft=uplimit-tt;
			$(counter).html(charLeft);
		}
	}
/* end manage function */

/* upload image / crop popup function */
	// function resetAdPhotoSection(){
	// 	$(".popup-area#change-adphoto .pc-section").hide();
		
	// }
	// function changeAdPhoto(which){
	// 	resetAdPhotoSection();
	// 	$(".popup-area#change-adphoto .pc-section."+which).show();			
	// 	$(".popup-area#change-adphoto").removeClass("adlogo-popup");
	// 	if(which=="adImageLogo"){
	// 		$(".popup-area#change-adphoto").addClass("adlogo-popup");
	// 	}
	// }
	function adImgCrop($imgPath,adImgSize) {
     if($imgPath == '') {
		if(adImgSize=="adImageOne")			
			$imgPath = 'images/ad-sizeone.jpg';
		else if(adImgSize=="adImageTwo")
			$imgPath = 'images/ad-sizetwo.jpg';
		else if(adImgSize=="adImageThree")
			$imgPath = 'images/ad-sizethree.jpg';
		else if(adImgSize=="adImageFour")
			$imgPath = 'images/ad-sizefour.jpg';
		else
			$imgPath = 'images/ad-sizelogo.jpg';
	   }   

	   var adPhotoCropOptions = {
		 loadPicture:$imgPath,
		 onAfterImgUpload: function(){ 
		 	
		 }		
	   }

		if(adImgSize=="adImageOne")
			var adPhotoCrop = new Croppic('adImageOneCrop', adPhotoCropOptions);
		else if(adImgSize=="adImageTwo")
			var adPhotoCrop = new Croppic('adImageTwoCrop', adPhotoCropOptions);
		else if(adImgSize=="adImageThree")
			var adPhotoCrop = new Croppic('adImageThreeCrop', adPhotoCropOptions);
		else if(adImgSize=="adImageFour")
			var adPhotoCrop = new Croppic('adImageFourCrop', adPhotoCropOptions);
		else
			var adPhotoCrop = new Croppic('adImageLogoCrop', adPhotoCropOptions);
   }
/* end upload image / crop popup function */

/* manage ad details */
	function backToAds(obj){
		var sparent = $(obj).parents(".manageadvert-details");
		sparent.find(".sub-content").hide();		
	}
	function openManageAdDetails(obj){
		var sparent = $(obj).parents(".manageadvert-details");
		
		// @dev : replace the content in sub-content section 
		
		sparent.find(".sub-content").show();
		
		$('html,body').animate({
			scrollTop: $(".sub-content").offset().top - 78},
        'slow');
	}
	function openEditAd(){
		$(".manageadvert-page").find(".edit-travad").show();
		setTimeout(function(){
			initNiceScroll(".nice-scroll"); 
			
			$(".audience-meter").click(function(){
			   var rand = Math.floor((Math.random() * 100) + 1);
			   gauge = new Gauge($('.gauge2'), {
						values: {
							0 : 'Specific',
							20: 'Good',
							100: 'Broad'
						},
						colors: {
							0 : '#ED1C24',
							20 : '#39B54A',
							80: '#FDD914'
						},
						angles: [
							180,
							360
						],
						lineWidth: 20,
						arrowWidth: 10,
						arrowColor: '#ccc',
						inset:true,

						value: rand});
					});
				$('.gauge2').gauge({
					values: {
						0 : 'Specific',
						20: 'Good',
						100: 'Broad'
					},
					colors: {
						0 : '#ED1C24',
						20 : '#39B54A',
						80: '#FDD914'
					},
					angles: [
						180,
						360
					],
					lineWidth: 20,
					arrowWidth: 10,
					arrowColor: '#ccc',
					inset:true,

					value: 10
				});
			},400);
			
			$('html, body').animate({ scrollTop: 0 }, 'slow');
	}
	function closeEditAd(){
		$(".manageadvert-page").find(".edit-travad").hide();
	}
/* end manage ad details */

/************ END ADVERTISE FUNCTIONS **********/

/************ PLACES FUNCTIONS **********/

/* manage map / list section */
	function closeMapView(){
		
		var win_w=$(window).width();
		if(win_w>767){
			if($(".places-mapview").hasClass("showMapView")){
				$(".places-mapview").removeClass("showMapView");
			}
			
			$(".places-page .places-column .tab-pane.active .top-stuff").each(function(){
				
				if($(this).find(".manageMap").length > 0){
					
					closeMapSection($(this).find(".manageMap"));
				
				}
			});
		}
		var backarrow = $(".places-mapview").find(".moreinfo-box").find(".backarrow");
		backarrow.trigger("click");
		
	}
	function closeMapSection(obj){
		var win_w=$(window).width();		
		if(win_w<768){
			
			var sparent=$(obj).parents(".tcontent-holder");
			sparent.find(".places-content-holder").find(".map-holder").slideUp();
			$(obj).html('<i class="mdi mdi-map-marker"></i>Map');
			$(obj).attr("onclick","openMapSection(this)");
			
		}
		if($(".places-mapview").hasClass("showMapView")){
			$(".places-mapview").removeClass("showMapView");
		}
		
	}
	function openMapSection(obj){
		var win_w=$(window).width();		
		if(win_w<768){			
			var sparent=$(obj).parents(".tcontent-holder");
			sparent.find(".places-content-holder").find(".map-holder").slideDown();
			$(obj).html('<i class="mdi mdi-close"></i>Close Map');
			$(obj).attr("onclick","closeMapSection(this)");
		}
		
		$(".places-mapview").addClass("showMapView");
		setTimeout(function(){		
			initNiceScroll(".places-mapview .nice-scroll");
			if($(".range-slider.price-slider").length>0)
				initPriceSlider("price-slider",500,10000,2500);			
		},500);
		$('body').animate({
			scrollTop:0
		}, 1000);
	}

	function closeListSection(obj){
		var sparent=$(obj).parents(".tcontent-holder");
		sparent.find(".places-content-holder").find(".list-holder").slideUp();
		$(obj).html('<i class="mdi mdi-view-list"></i>List');
		$(obj).attr("onclick","openListSection(this)");
	}
	function openListSection(obj){
		var sparent=$(obj).parents(".tcontent-holder");
		sparent.find(".places-content-holder").find(".list-holder").slideDown();
		$(obj).html('<i class="mdi mdi-close"></i>Close List');
		$(obj).attr("onclick","closeListSection(this)");
	}
/* end maange map / list section */

/* FUN manage banner buttons */	
	function manageBannerButtons(obj){
		var sparent=$(obj).parents(".mbl-banner-buttons");
		if($(sparent).hasClass('open')){
			sparent.find(".btn-holder").fadeOut();
			$(sparent).removeClass('open');
			$(obj).html('<i class="mdi mdi-chevron-down"></i>');
		}
		else{
			sparent.find(".btn-holder").fadeIn();
			$(sparent).addClass('open');
			$(obj).html('<i class="mdi mdi-close"></i>');
		}
	}

/* FUN end manage banner buttons */

/* FUN manage expand map */
	function expandMap(obj,target){		
		var sparent=$(obj).parents(".map-holder");
		sparent.find(".overlay").hide();
		sparent.find("a.closelink").show();
		sparent.addClass("expanded");	
		
		var orgScroll = $('body').scrollTop()-30;		
		if(($(target).offset().top)>100 && !$(obj).parents(".subtab").length>0){
			$('body').animate({
				scrollTop: (eval($(target).offset().top-30))+orgScroll
			}, 1000);
		}
		
		$(".places-mapview").addClass("showMapView");
		setTimeout(function(){initNiceScroll(".places-mapview .nice-scroll");},500);
		$('body').animate({
			scrollTop:0
		}, 1000);
		
	}
	function shrinkMap(obj){
		var sparent=$(obj).parents(".map-holder");
		sparent.find(".overlay").show();
		sparent.find("a.closelink").hide();
		sparent.removeClass("expanded");
		
		if($(".places-mapview").hasClass("showMapView")){
			$(".places-mapview").removeClass("showMapView");
		}
	}
/* FUN end manage expand map */

/* FUN manage clicking tabs randomly */
	function setPlacesSidebar(status) {	
		console.log(status);
		if(status=="hide") {
			$(".places-content .wallcontent-column").addClass("hideSides");
		} else {
			$(".places-content .wallcontent-column").removeClass("hideSides");	
		}
	}

	function openDirectTab(whichId) {
		if(whichId=="places-lodge") {
			setPlacesSidebar("hide");
		} else {
			setPlacesSidebar("show");
		}  

		if(whichId !=" places-lodge" && whichId != "places-dine" && whichId != "places-todo") {
			$('.placesall-content').hide();
			$('.placesdine-content').hide();
			$('.placestodo-content').hide();
			$('.placeslodge-content').show();
		} else if(whichId == "places-dine") {
			$('.placeslodge-content').hide();
			$('.placestodo-content').hide();
			$('.placesall-content').hide();
			$('.placesdine-content').show();
		} else if(whichId == "places-todo") {
			$('.placeslodge-content').hide();
			$('.placesall-content').hide();
			$('.placesdine-content').hide();
			$('.placestodo-content').show();
		} else {
			$('.placeslodge-content').hide();
			$('.placestodo-content').hide();
			$('.placesdine-content').hide();
			$('.placesall-content').show();
		}
		$('html, body').animate({ scrollTop: 0 }, 'slow');
	}
/* FUN end manage clicking tabs randomly */

/* FUN set hide header for mobile */
	function setHideHeader(obj,pagename,doWhat){
		
		var win_w=$(window).width();		
		if(pagename=="places"){			
			var whichTab=$(obj).find("a").attr("href");
			
			if(whichTab!="#places-all"){
				if(win_w<=767){
					$('body').addClass("hideHeader");
				}else{
					if($('body').hasClass("hideHeader")){
						$('body').removeClass("hideHeader");
					}
				}			
			}else{
				if($('body').hasClass("hideHeader")){
					$('body').removeClass("hideHeader");
				}
			}
			
			var dropVal=whichTab.replace("#places-","");
			$(".select2places").val(dropVal).change();
			
			setTimeout(function(){fixPlacesImageUI(dropVal);},400);
			
			resetTagging(); // limit tagging to be in single line
		}
		if(pagename=="hotels"){
			
			if(win_w<=767){
				if(doWhat == "hide") $('body').addClass("hideHeader");
				else $('body').removeClass("hideHeader");
			}else{
				if($('body').hasClass("hideHeader")){
					$('body').removeClass("hideHeader");
				}
			}
		}
	}
/* FUN end set hide header for mobile */

/* FUN manage add button */
	function addConnectBtn(obj){
		
		var html=$(obj).html();
		
		if(html=='<i class="mdi mdi-account-plus"></i> Add Connect'){
			$(obj).html('<i class="mdi mdi-account-minus"></i> Request Sent');
		}
	}
/* FUN end manage add button */

/* FUN manage image width */	
	function resetPlacesImageUI(){
		var activeContent = $(".places-column .tab-pane.active.in").attr("id");
		
		var which=activeContent.replace("places-","");		
		fixPlacesImageUI(which);
	}
	function resetPlacesImageFix(which){
		
		if(which=="all"){
			$(".places-column .placebox").each(function(){
				if($(this).find(".imgholder").hasClass("imgfix"))
					$(this).find(".imgholder").removeClass("imgfix");
					
				$(this).find(".imgholder").find("img").css("margin-left","0");
				$(this).find(".imgholder").find("img").css("margin-top","0");			
			});
			$(".places-column .f-placebox").each(function(){
					
				if($(this).find(".imgholder").hasClass("imgfix"))
					$(this).find(".imgholder").removeClass("imgfix");
					
				$(this).find(".imgholder").find("img").css("margin-left","0");
				$(this).find(".imgholder").find("img").css("margin-top","0");
			});
		}
		
		if(which=="events" || which=="groups" || which=="guides" || which=="dine" || which=="lodge" || which=="todo"){
			
			$(".places-column .tab-pane.active .hotel-list .hotel-li").each(function(){
				if($(this).find(".imgholder").hasClass("imgfix"))
					$(this).find(".imgholder").removeClass("imgfix");
					
				$(this).find(".imgholder").find("img").css("margin-left","0");
				$(this).find(".imgholder").find("img").css("margin-top","0");
			});
		}
	}
	
	function fixPlacesImageUI(which){
		/* post image fixes */		
		if(which=="all"){
			resetPlacesImageFix(which);
			$(".places-column .placebox").each(function(){
				
				if($(this).find(".imgholder").length > 0 ){				
					
					$(this).find(".imgholder").each(function(e, v){
						
						var cont_width=$(v).width();
						var cont_height=$(v).height();
						var img_width=$(v).find("img").width();
						var img_height=$(v).find("img").height();
						
						if($(v).hasClass("himg-box")){										
							
							if(img_width < cont_width){							
								$(v).addClass("imgfix");
							}
						
						} 
						
						if(img_width > cont_width){
																			
							var iwidth = resizeToWidth(img_width,img_height,cont_height);
							var lfix= ( iwidth - cont_width ) / 2;
							$(v).find("img").css("margin-left","-"+lfix+"px");							
						}
						
						
						if(img_height > cont_height || $(v).hasClass("imgfix")){
							var iheight = resizeToHeight(img_width,img_height,cont_width);
							var tfix= ( iheight - cont_height ) / 2;
							$(v).find("img").css("margin-top","-"+tfix+"px");							
						}
					});
				
				}
				
			});
			$(".places-column .f-placebox").each(function(){
				
				if($(this).find(".imgholder").length > 0 ){				
					
					$(this).find(".imgholder").each(function(e, v){
						
						var cont_width=$(v).width();
						var cont_height=$(v).height();
						var img_width=$(v).find("img").width();
						var img_height=$(v).find("img").height();
						
						if($(v).hasClass("himg-box")){										
							
							if(img_width < cont_width){							
								$(v).addClass("imgfix");
							}
						
						} 
						
						if(img_width > cont_width){
																			
							var iwidth = resizeToWidth(img_width,img_height,cont_height);
							var lfix= ( iwidth - cont_width ) / 2;
							$(v).find("img").css("margin-left","-"+lfix+"px");							
						}
						
						
						if(img_height > cont_height || $(v).hasClass("imgfix")){
							var iheight = resizeToHeight(img_width,img_height,cont_width);
							var tfix= ( iheight - cont_height ) / 2;
							$(v).find("img").css("margin-top","-"+tfix+"px");							
						}
					});
				
				}
				
			});
		}
	
		if(which=="events" || which=="groups" || which=="guides" || which=="dine" || which=="lodge" || which=="todo"){
			resetPlacesImageFix(which);
			$(".places-column .tab-pane.active .hotel-list .hotel-li").each(function(){
				
				if($(this).find(".imgholder").length > 0 ){				
					
					$(this).find(".imgholder").each(function(e, v){
						
						var cont_width=$(v).width();
						var cont_height=$(v).height();
						var img_width=$(v).find("img").width();
						var img_height=$(v).find("img").height();
						
						if($(v).hasClass("himg-box")){										
							
							if(img_width < cont_width){							
								$(v).addClass("imgfix");
							}
						
						} 
						
						if(img_width > cont_width){
																			
							var iwidth = resizeToWidth(img_width,img_height,cont_height);
							var lfix= ( iwidth - cont_width ) / 2;
							$(v).find("img").css("margin-left","-"+lfix+"px");							
						}
						
						
						if(img_height > cont_height || $(v).hasClass("imgfix")){
							var iheight = resizeToHeight(img_width,img_height,cont_width);
							var tfix= ( iheight - cont_height ) / 2;
							$(v).find("img").css("margin-top","-"+tfix+"px");							
						}
					});
				
				}
				
			});
		}
		
		if(which == '.placestravellers-content' || which == '.placeslocals-content') {
			setTimeout(function(){		
				if($(which).length) {
					if($(which).find(".imgholder").length > 0 ){	
						$(which).find(".imgholder").each(function(e, v){
							var cont_width=$(v).width();
							var cont_height=$(v).height();
							var img_width=$(v).find("img").width();
							var img_height=$(v).find("img").height();
							if($(v).hasClass("himg-box")){										
								if(img_width < cont_width){							
									$(v).addClass("imgfix");
								}
							} 
							if(img_width > cont_width){
								var iwidth = resizeToWidth(img_width,img_height,cont_height);
								var lfix= ( iwidth - cont_width ) / 2;
								$(v).find("img").css("margin-left","-"+lfix+"px");
							}
							if(img_height > cont_height || $(v).hasClass("imgfix")){
								var iheight = resizeToHeight(img_width,img_height,cont_width);
								var tfix= ( iheight - cont_height ) / 2;
								$(v).find("img").css("margin-top","-"+tfix+"px");
							}
						});
					}
				}
			},2000);
		}
	}
/* FUN end manage image width */

/* FUN manage add new box */	
	function openAddNewBox(which){
		
		var win_w = $(window).width();
		var target = $("#"+which).find(".addnew-box");
		if(win_w >= 768){
			target.slideDown();
		}else{
			if(target.find(".new-post").hasClass("review-newpost")){
				//newreview-popup
			}
			if(target.find(".new-post").hasClass('tip-newpost')){
				//newtip-popup
			}
			if(target.find(".new-post").hasClass('ask-newpost')){
				//newquestion-popup
			}
		}
		
		setTimeout(function(){setGeneralThings();},400);
	}
/* FUN end manage add new box */

/* FUN manage expand tags */
	function explandTags(obj){
		$(obj).addClass("expandTags");
	}
	function resetTagging(){
		$(".places-page .tagging").each(function(){
			
			if($(this).hasClass("expandTags")){
				$(this).removeClass("expandTags");
			}
		});
	}
/* FUN end manage expand tags */

/* FUN manage more info holder */	
	function openPlacesMoreInfo(obj){
		var sparent = $(obj).parents(".moreinfo-outer");
		sparent.find(".moreinfo-box").show();
	}
	function closePlacesMoreInfo(obj){
		var sparent = $(obj).parents(".moreinfo-outer");
		sparent.find(".moreinfo-box").hide();
	}
/* FUN end manage more info holder */

/* FUN reset places more info / sliding for desktop */
	function resetPlacesInfoSliding(){
		$(".places-page .tab-pane").each(function(){
			
			var obj=$(this);
			obj.find(".moreinfo-outer").each(function(){
				
				closePlacesMoreInfo( $(this).find(".moreinfo-box") );
				
				var sparent=$(this).find(".expandable-holder");
				
				if(sparent.hasClass("expanded")){
					
					sparent.removeClass("expanded");
					sparent.find(".expandable-area").slideUp();							
					sparent.find(".expand-link").each(function(){
						if($(this).hasClass("active"))
							$(this).removeClass("active");	
					});	
				
				}
			});
		});
	}
/* FUN end reset places more info / sliding for desktop */

/* FUN reset places tabs */
	function resetPlaceTabs(sparent){
		$(sparent+" li").each(function(){
			$(this).removeClass("active");
		});
	}
/* FUN end reset places tabs */

/* FUN replace with slider */

function replaceWithSlider(what){
	
	if(what=="top-places"){
		
		var replacedHTML = "<div class='carousel carousel-albums slide' id='topplace_carousel'><div class='carousel-inner'><div class='item active'><div class='col-sm-4 col-xs-12 fpb-holder'><div class='f-placebox'><div class='imgholder himg-box'><img src='images/topplaces-2.jpg' class='himg'/></div><div class='descholder'><h5>Abu Dhabi<span>United Arab Emirates</span></h5><div class='tags'><span>Shopping</span><span>Architechture</span></div><a href='javascript:void(0)' class='right btn btn-primary btn-sm'>Discover</a></div></div></div></div><div class='item'><div class='col-sm-4 col-xs-12 fpb-holder'><div class='f-placebox'><div class='imgholder himg-box'><img src='images/topplaces-3.jpg' class='himg'/></div><div class='descholder'><h5>Cairo<span>Egypt</span></h5><div class='tags'><span>People</span><span>Historical</span><span>Food</span></div><a href='javascript:void(0)' class='right btn btn-primary btn-sm'>Discover</a></div></div></div></div><div class='item'><div class='col-sm-4 col-xs-12 fpb-holder'><div class='f-placebox'><div class='imgholder himg-box'><img src='images/topplaces-1.jpg' class='himg'/></div><div class='descholder'><h5>Brussels<span>Belgium</span></h5><div class='tags'><span>Architechture</span><span>Museum</span><span>Local Beer</span></div><a href='javascript:void(0)' class='right btn btn-primary btn-sm'>Discover</a></div></div></div></div><div class='item'><div class='col-sm-4 col-xs-12 fpb-holder'><div class='f-placebox'><div class='imgholder himg-box'><img src='images/topplaces-2.jpg' class='himg'/></div><div class='descholder'><h5>Abu Dhabi<span>United Arab Emirates</span></h5><div class='tags'><span>Shopping</span><span>Architechture</span></div><a href='javascript:void(0)' class='right btn btn-primary btn-sm'>Discover</a></div></div></div></div><div class='item'><div class='col-sm-4 col-xs-12 fpb-holder'><div class='f-placebox'><div class='imgholder himg-box'><img src='images/topplaces-3.jpg' class='himg'/></div><div class='descholder'><h5>Cairo<span>Egypt</span></h5><div class='tags'><span>People</span><span>Historical</span><span>Food</span></div><a href='javascript:void(0)' class='right btn btn-primary btn-sm'>Discover</a></div></div></div></div><div class='item'><div class='col-sm-4 col-xs-12 fpb-holder'><div class='f-placebox'><div class='imgholder himg-box'><img src='images/topplaces-1.jpg' class='himg'/></div><div class='descholder'><h5>Brussels<span>Belgium</span></h5><div class='tags'><span>Architechture</span><span>Museum</span><span>Local Beer</span></div><a href='javascript:void(0)' class='right btn btn-primary btn-sm'>Discover</a></div></div></div></div></div><a class='left carousel-control' href='#topplace_carousel' data-slide='prev'><i class='glyphicon glyphicon-chevron-left'></i></a><a class='right carousel-control' href='#topplace_carousel' data-slide='next'><i class='glyphicon glyphicon-chevron-right'></i></a></div>";
		
		$(".topplaces-section .list-holder .row").html(replacedHTML);
		$(".topplaces-section .cbox-title .top-stuff").remove();
		
		setTimeout(function(){
			initPhotoCarousel("topplaces");
			fixPlacesImageUI("all");
			$(".topplaces-section .carousel-albums").css("opacity",1);			
		},500);
	}
	if(what=="top-attractions"){
		
		var replacedHTML = "<div class='carousel carousel-albums slide' id='topattr_carousel'><div class='carousel-inner'><div class='item active'><div class='col-sm-3 col-xs-12 fpb-holder'><div class='f-placebox'><div class='imgholder himg-box'><img src='images/topplaces-1.jpg' class='himg'/></div><div class='descholder'><h5>Attraction Name<span>Belgium</span></h5><div class='tags'><p><strong>Peak Time :</strong> Feb - Apr</p><p><strong>Visitors :</strong> 12000 / year</p></div><a href='javascript:void(0)' class='right btn btn-primary btn-sm'>Visit</a></div></div></div></div><div class='item'><div class='col-sm-3 col-xs-12 fpb-holder'><div class='f-placebox'><div class='imgholder himg-box'><img src='images/topplaces-2.jpg' class='himg'/></div><div class='descholder'><h5>Attraction Name<span>United Arab Emirates</span></h5><div class='tags'><p><strong>Peak Time :</strong> Feb - Apr</p><p><strong>Visitors :</strong> 12000 / year</p></div><a href='javascript:void(0)' class='right btn btn-primary btn-sm'>Visit</a></div></div></div></div><div class='item'><div class='col-sm-3 col-xs-12 fpb-holder'><div class='f-placebox'><div class='imgholder himg-box'><img src='images/topplaces-3.jpg' class='himg'/></div><div class='descholder'><h5>Attraction Name<span>Egypt</span></h5><div class='tags'><p><strong>Peak Time :</strong> Feb - Apr</p><p><strong>Visitors :</strong> 12000 / year</p></div><a href='javascript:void(0)' class='right btn btn-primary btn-sm'>Visit</a></div></div></div></div><div class='item'><div class='col-sm-3 col-xs-12 fpb-holder'><div class='f-placebox'><div class='imgholder himg-box'><img src='images/topplaces-1.jpg' class='himg'/></div><div class='descholder'><h5>Attraction Name<span>Belgium</span></h5><div class='tags'><p><strong>Peak Time :</strong> Feb - Apr</p><p><strong>Visitors :</strong> 12000 / year</p></div><a href='javascript:void(0)' class='right btn btn-primary btn-sm'>Visit</a></div></div></div></div><div class='item'><div class='col-sm-3 col-xs-12 fpb-holder'><div class='f-placebox'><div class='imgholder himg-box'><img src='images/topplaces-2.jpg' class='himg'/></div><div class='descholder'><h5>Attraction Name<span>United Arab Emirates</span></h5><div class='tags'><p><strong>Peak Time :</strong> Feb - Apr</p><p><strong>Visitors :</strong> 12000 / year</p></div><a href='javascript:void(0)' class='right btn btn-primary btn-sm'>Visit</a></div></div></div></div><div class='item'><div class='col-sm-3 col-xs-12 fpb-holder'><div class='f-placebox'><div class='imgholder himg-box'><img src='images/topplaces-3.jpg' class='himg'/></div><div class='descholder'><h5>Attraction Name<span>Egypt</span></h5><div class='tags'><p><strong>Peak Time :</strong> Feb - Apr</p><p><strong>Visitors :</strong> 12000 / year</p></div><a href='javascript:void(0)' class='right btn btn-primary btn-sm'>Visit</a></div></div></div></div></div><a class='left carousel-control' href='#topattr_carousel' data-slide='prev'><i class='glyphicon glyphicon-chevron-left'></i></a><a class='right carousel-control' href='#topattr_carousel' data-slide='next'><i class='glyphicon glyphicon-chevron-right'></i></a></div>";
		
		$(".topattractions-section .list-holder .row").html(replacedHTML);
		$(".topattractions-section .cbox-title .top-stuff").remove();
		
		setTimeout(function(){
			initPhotoCarousel("topattractions");
			fixPlacesImageUI("all");
			$(".topattractions-section .carousel-albums").css("opacity",1);
		},500);
	}
	
}

/* FUN end replace with slider */

/************ END PLACES FUNCTIONS **********/

/************ TRIP FUNCTIONS **********/

	function resetAllSectionLayers(){
		
		if($(".main-content").hasClass("trip-page")){
			
			$(".trip-mapview .side-section .section-layer").each(function(){
				if($(this).hasClass("front"))
					$(this).removeClass("front");
			});
		}
	}
	function resetAllSectionSubLayers(){
		
		if($(".main-content").hasClass("trip-page")){
			
			$(".trip-mapview .side-section .section-sublayer").each(function(){
				
				$(this).attr("class","section-sublayer");
			});
		}
	}
	function showLayer(obj,which){
		
		var sparent = $(obj).parents(".trip-mapview").find(".side-section");
		
		//hide map filter
		BackTripRouteMap(obj);
		
		resetAllSectionLayers();
		resetAllSectionSubLayers();
		
		// hide bookmark details
		sparent.find(".section-layer#trip-bookmark").removeClass("showit");
				
		if(which=="alltrip")
			sparent.find(".section-layer#trip-list").addClass("front");
		
		if(which=="viewtrip")
			sparent.find(".section-layer#trip-view").addClass("front");
		
		if(which=="edittrip")
			sparent.find(".section-layer#trip-edit").addClass("front");
		
		if(which=="newtrip")
			sparent.find(".section-layer#trip-new").addClass("front");
		
		var win_w=$(window).width();
		if(win_w<=568){
			if(which=="alltrip"){
				$(".trip-wrapper").removeClass("mobile-fullview");
			}else{
				$(".trip-wrapper").addClass("mobile-fullview");
			}
		}
		
		setTimeout(function(){setGeneralThings(); },300);
	}
	// to open side layer
	function showSubLayer(obj,which,mode){
		
		var sparent = $(obj).parents(".side-section");
		
		resetAllSectionSubLayers();
		
		sparent.find(".section-sublayer#"+which).attr("class","section-sublayer "+mode);
		sparent.find(".section-sublayer#"+which).addClass("shown");
	
		setTimeout(function(){ initNiceScroll(".nice-scroll"); setGeneralThings(); },300);
	}
	function hideSubLayer(obj){
		
		var fparent = $(obj).parents(".section-sublayer");
		
		if(fparent.hasClass("shown"))
			fparent.attr("class","section-sublayer");
			
	}
	function showBookmarkDetail(obj){
		
		var sparent = $(obj).parents(".side-section");
		resetAllSectionSubLayers();
		sparent.find(".section-layer#trip-bookmark").addClass("showit");
	}
	function hideBookmarkDetail(obj){
		
		var sparent = $(obj).parents(".side-section");
		sparent.find(".section-layer#trip-bookmark").removeClass("showit");
	}	
	function openDetailTripMap(obj){
		var leftSection=$(obj).parents(".desc-box");
		leftSection.hide();
		
		//hide map-filter
		var mapView=$(obj).parents(".trip-mapview").find(".map-box");
		mapView.find(".map-filter").hide();
		
	}
	function closeDetailTripMap(obj){
		var leftSection=$(obj).parents(".trip-mapview").find(".desc-box");
		leftSection.show();
		
		//hide map-filter
		var mapView=$(obj).parents(".trip-mapview").find(".map-box");
		mapView.find(".map-filter").hide();
		
	}
	function openAccommodations(obj,cityName){
		resetAllSectionSubLayers();
		
		var mapView=$(obj).parents(".trip-mapview").find(".map-box");
		mapView.find(".map-filter").show();
		
		var win_w=$(window).width();
		if(win_w<=568){			
			var leftSection=$(obj).parents(".desc-box");			
			leftSection.hide();			
		}
	}
	function BackTripRouteMap(obj){
		var mapView=$(obj).parents(".trip-mapview").find(".map-box");
		mapView.find(".map-filter").hide();
	
	}

/************ END TRIP FUNCTIONS **********/

function ShowImage() {
	$(".loader").show();
}
function HideImage() {
	$(".loader").hide();
}


if(window.location.href.indexOf("local-guide") > -1) {
	$(document).ready(function() {
		var activateOption = function(collection, newOption) {
		    if (newOption) {
		        collection.find('li.selected').removeClass('selected');

		        var option = $(newOption);
		        option.addClass('selected');
		    }
		};

	    $('select').material_select();
	    $('.test-input > .select-wrapper > .select-dropdown').prepend('<li class="toggle selectall"><span><label></label>Select all</span></li>');
	    $('.test-input > .select-wrapper > .select-dropdown').prepend('<li style="display:none" class="toggle selectnone"><span><label></label>Select none</span></li>');
	    $('.test-input > .select-wrapper > .select-dropdown .selectall').on('click', function() {
	        selectAll();
	        $('.test-input > .select-wrapper > .select-dropdown .toggle').toggle();
	    });
	    $('.test-input > .select-wrapper > .select-dropdown .selectnone').on('click', function() {
	        selectNone();
	        $('.test-input > .select-wrapper > .select-dropdown .toggle').toggle();
	    });
	});

	function callmyfun(id)
	{
		var objDiv = document.getElementById(id);
	     objDiv.scrollTop = objDiv.scrollHeight;
	}

	function selectNone(obj) {
	    $('.custom-localguide select option:selected').not(':disabled').prop('selected', false);
	    $('.custom-localguide .dropdown-content.multiple-select-dropdown input[type="checkbox"]:checked').not(':disabled').prop('checked', '');
	    //$('.dropdown-content.multiple-select-dropdown input[type="checkbox"]:checked').not(':disabled').trigger('click');
	    var values = $('.custom-localguide .dropdown-content.multiple-select-dropdown input[type="checkbox"]:disabled').parent().text();
	    $('.custom-localguide input.select-dropdown').val(values);
	    $(obj).attr('onChange', 'selectAll(this)');
	}

	function selectAll(obj) {

	    $('.custom-localguide select option:not(:disabled)').not(':selected').prop('selected', true);
	    $('.custom-localguide .dropdown-content.multiple-select-dropdown input[type="checkbox"]:not(:checked)').not(':disabled').prop('checked', 'checked');
	    //$('.dropdown-content.multiple-select-dropdown input[type="checkbox"]:not(:checked)').not(':disabled').trigger('click');
	    var values = $('.custom-localguide .dropdown-content.multiple-select-dropdown input[type="checkbox"]:checked').not(':disabled').parent().map(function() {
	        return $(this).text();
	    }).get();
	    $('.custom-localguide input.select-dropdown').val(values.join(', '));
	    $(obj).attr('onChange', 'selectNone(this)');
	}
} else if(window.location.href.indexOf("home") > -1) {
	new WOW().init();
} else if(window.location.href.indexOf("manage-ad") > -1) {
	var slider = document.getElementById('test-slider');
	  noUiSlider.create(slider, {
	   start: [20, 80],
	   connect: true,
	   step: 1,
	   orientation: 'horizontal', // 'horizontal' or 'vertical'
	   range: {
	     'min': 0,
	     'max': 100
	   },
	   format: wNumb({
	     decimals: 0
	   })
	  });
	  
	  
	var slider1 = document.getElementById('test-slider1');
	  noUiSlider.create(slider1, {
	   start: [20, 80],
	   connect: true,
	   step: 1,
	   orientation: 'horizontal', // 'horizontal' or 'vertical'
	   range: {
	     'min': 0,
	     'max': 100
	   },
	   format: wNumb({
	     decimals: 0
	   })
	  });

	if ($(window).height() <= 767) {
		$(".manageadvert-details td a").click(function(){
			$("body").addClass("remove_scroller");			
		});
	} else {
		$(".manageadvert-details td a").click(function(){
			$("body").removeClass("remove_scroller");
		});
	}
	$(document).ready(function() {
		$(".advertmanager-page .btn, .advertmanager-page .btn-custom").click(function(){
			$("body").removeClass("remove_scroller");			
		});	
	});

	$('.change_head').keyup(function() {
	    $('.travad-title').html($(this).val());
	});  
	$('.sub_title_you_ad').keyup(function() {
	    $('.travad-subtitle').html($(this).val());
	}); 
	$('.catch_phase').keyup(function() {
	    $('.change_ad_word').html($(this).val());
	});   
	$('.aware_head').keyup(function() {
	    $('.change_awar_head').html($(this).val());
	});  
	$('.aware_text').keyup(function() {
	    $('.change_awar_text').html($(this).val());
	}); 
	$('.advert_title').keyup(function() {
	    $('.lead_title').html($(this).val());
	}); 
	$('.advert_phrase').keyup(function() {
	    $('.lead_phrase').html($(this).val());
	}); 
	$('.advert_head').keyup(function() {
	    $('.lead_head').html($(this).val());
	}); 
	$('.advert_text').keyup(function() {
	    $('.lead_text').html($(this).val());
	}); 
	$('.advert_url').keyup(function() {
	    $('.lead_url').html($(this).val());
	}); 
	$('.conversion_title').keyup(function() {
	    $('.con_create_title').html($(this).val());
	}); 
	$('.conversion_phrase').keyup(function() {
	    $('.con_create_phrase').html($(this).val());
	}); 
	$('.conversion_head').keyup(function() {
	    $('.con_create_head').html($(this).val());
	}); 
	$('.conversion_text').keyup(function() {
	    $('.con_create_text').html($(this).val());
	});
	$('.inbox_title').keyup(function() {
	    $('.inbox_create_title').html($(this).val());
	});
	$('.inbox_main_catch').keyup(function() {
	    $('.inbox_create_main').html($(this).val());
	});
	$('.inbox_sub_catch').keyup(function() {
	    $('.inbox_create_sub').html($(this).val());
	});
	$('.inbox_head').keyup(function() {
	    $('.inbox_create_head').html($(this).val());
	});
	$('.inbox_text').keyup(function() {
	    $('.inbox_create_text').html($(this).val());
	});
	$('.endorsement_phrase').keyup(function() {
	    $('.endorsement_create_phrase').html($(this).val());
	});
	$('.endorsement_head').keyup(function() {
	    $('.endorsement_create_head').html($(this).val());
	});
	$('.endorsement_text').keyup(function() {
	    $('.endorsement_create_text').html($(this).val());
	});       
	function main_change_button(x) {    
	    $('.con_create_button').text(x);
	}      
	function main_inbox_button(y) {    
	    $('.inbox_create_button').text(y);
	}    
	function pageendorse_change(z) {    
	    $('.endorsement_create_value').text(z);
	}
	function main_title_change() {
	    var x = document.getElementById("mySelect").selectedIndex;
	    $('.head_chtitle').text(document.getElementsByTagName("option")[x].value);
	} 
} else if(window.location.href.indexOf("places") > -1) {
	var owl = $('.owl-carousel');
	    owl.owlCarousel({
	    margin: 10,
	    loop: true,     
	    dots: true,         
	    nav: true,
	    responsive: {
	      0: {
	        items: 2
	      },
	      600: {
	        items: 3
	      },   
	      1000: {
	        items: 4
	      }
	}
	})


	$(document).ready(function() {
		if ( $(window).width() > 767) {      
	        $(".places-tabs .sub-tabs li a").click(function(){
	            $(".header-section").removeClass("hide_header");
	            $("body").removeClass("remove_scroller");
	        }); 
	        $(".tabs.icon-menu.tabsnew li a").click(function(){
	            $(".header-section").removeClass("hide_header");
	            $("body").removeClass("remove_scroller");
	        }); 
	        $(".mbl-tabnav").click(function(){
	            $(".header-section").removeClass("hide_header");
	            $("body").removeClass("remove_scroller");
	        }); 
	        $(".clicable.viewall-link").click(function(){
	            $(".header-section").removeClass("hide_header");
	            $("body").removeClass("remove_scroller");
	        }); 
	    } else {
	        $(".places-tabs .sub-tabs li a").click(function(){
	            $(".header-section").addClass("hide_header");
	            $("body").addClass("remove_scroller");
	        }); 
	        $(".clicable.viewall-link").click(function(){
	            $(".header-section").addClass("hide_header");
	            $("body").addClass("remove_scroller");
	        });         
	        $(".tabs.icon-menu.tabsnew li a").click(function(){
	            $(".header-section").addClass("hide_header");
	            $("body").addClass("remove_scroller");
	        }); 
	        $(".mbl-tabnav").click(function(){
	            $(".header-section").removeClass("hide_header");
	            $("body").removeClass("remove_scroller");
	        }); 
	    }

	    $('.new-tumb').carousel({
	        dist:0,
	        shift:0,
	        padding:20,
	    });

	    $(document).on('click', '.tabsnew a', function() {
	        tabsNewInit();
	    });

	    tabsNewInit();

	    $('.profiletooltip_content').hide();
	    
	    $('.set_re_height .new_post_comment').on('keyup', function (ev) {
	        $('.set_re_height .settings-icon').toggleClass('btn_enable', !!$(this).val().trim());
	    });
	});

	function tabsNewInit() {
	    var tabArray1 = ['places-all', 'places-reviews', 'places-travellers', 'places-locals', 'places-photos', 'places-guides', 'places-tip', 'places-ask'];
	    var tabArray2 = ['places-lodge', 'places-dine', 'places-todo'];

	    $('ul.tabs').tabs({
	        onShow: function(data) {
	            if(data[0]['id'] != undefined && data[0]['id'] != null && data[0]['id'] != '') {
	                var tabId = data[0]['id'];

	                if($.inArray(tabId, tabArray1) !== -1) {
	                    $.each(tabArray2, function(i, v) {
	                        $('#'+v).removeClass('active');
	                        $('#'+v).hide();
	                    });
	                } else if($.inArray(tabId, tabArray2) !== -1) {
	                    $.each(tabArray1, function(i, v) {
	                        $('#'+v).removeClass('active');
	                        $('#'+v).hide();
	                    });
	                }
	            }
	        }
	    });
	} 
} else if(window.location.href.indexOf("travelbuddy") > -1) {
	$(document).ready(function() {			
		$("select.radio-change").change(function(){
	        $("select.radio-change").find("option:selected").each(function(){
	            var optionValue = $(this).attr("value");
	            if(optionValue){
	                $(".box11").not("." + optionValue).addClass("dimme");
	                $(".disable-radi input").attr("disabled", true);
	                $(".disable-radi textarea").attr("disabled", true);
	            } else{
	                $(".box11").removeClass("dimme");
	                $(".disable-radi input").attr("disabled", false);
	                $(".disable-radi textarea").attr("disabled", false);
	            }
	        });
	    }).change();
	});

	function callmyfun(id)
	{
		var objDiv = document.getElementById(id);
	     objDiv.scrollTop = objDiv.scrollHeight;
	}
} else if(window.location.href.indexOf("wall") > -1) {
	
	$(document).ready(function(){ 
		$(".action-icon.activity-link").click(function(){
			$(".activity-content").addClass("showactivity");
			$(".tab-pane.main-tabpane.active").hide();
			$(".custom-wall-links .tab a").removeClass("active");
		});
		$(".custom-wall-links .tab a").click(function(){
			$(".activity-content").removeClass("showactivity");
		});
		$("#upload_img_action").click(function(){
			$(".cover-slider").removeClass("openDrawer");			
		});		 

		$('.new-tumb').carousel({
			dist:0,
			shift:0,
			padding:20,
	  	});

	  	$('.carousel_master .carousel').carousel();

	});

	function myMap() {
	  var myCenter = new google.maps.LatLng(51.508742,-0.120850);
	  var mapCanvas = document.getElementById("umappam");
	  var mapOptions = {center: myCenter, zoom: 5};
	  var map = new google.maps.Map(mapCanvas, mapOptions);
	  var marker = new google.maps.Marker({position:myCenter});
	  marker.setMap(map);
	  var contentString = '<div>User Name</div>';

	        var infowindow = new google.maps.InfoWindow({
	          content: contentString
	        });
	  marker.addListener('click', function() {
	          infowindow.open(map, marker);
	        });

	}
} else if(window.location.href.indexOf("weekendescape") > -1) {
	function callmyfun(id)
	{
		var objDiv = document.getElementById(id);
         objDiv.scrollTop = objDiv.scrollHeight;
	}
} else if(window.location.href.indexOf("who-is-around") > -1) {
	var slider = document.getElementById('test-slider');
	  noUiSlider.create(slider, {
	   start: [20, 80],
	   connect: true,
	   step: 1,
	   orientation: 'horizontal', // 'horizontal' or 'vertical'
	   range: {
	     'min': 0,
	     'max': 100
	   },
	   format: wNumb({
	     decimals: 0
	   })
	  });
} else if(window.location.href.indexOf("ad-manager") > -1) {
	var slider = document.getElementById('test-slider');
	  noUiSlider.create(slider, {
	   start: [20, 80],
	   connect: true,
	   step: 1,
	   orientation: 'horizontal', // 'horizontal' or 'vertical'
	   range: {
	     'min': 0,
	     'max': 100
	   }, 
	   format: wNumb({
	     decimals: 0
	   })
	  });
	  
	  
	var slider1 = document.getElementById('test-slider1');
	  noUiSlider.create(slider1, {
	   start: [20, 80],
	   connect: true,
	   step: 1,
	   orientation: 'horizontal', // 'horizontal' or 'vertical'
	   range: {
	     'min': 0,
	     'max': 100
	   },
	   format: wNumb({
	     decimals: 0
	   })
	});

	$(document).ready(function() {			
		$(".pos_rel .image_save_btn.image_save").click(function(){
			$(".pos_rel .collection_image_trash.image_trash").removeClass("class-show");
		});

		$('.change_head').keyup(function() {
		    $('.travad-title').html($(this).val());
		});  
		$('.sub_title_you_ad').keyup(function() {
		    $('.travad-subtitle').html($(this).val());
		}); 
		$('.catch_phase').keyup(function() {
		    $('.change_ad_word').html($(this).val());
		});   
		$('.aware_head').keyup(function() {
		    $('.change_awar_head').html($(this).val());
		});  
		$('.aware_text').keyup(function() {
		    $('.change_awar_text').html($(this).val());
		}); 
		$('.advert_title').keyup(function() {
		    $('.lead_title').html($(this).val());
		}); 
		$('.advert_phrase').keyup(function() {
		    $('.lead_phrase').html($(this).val());
		}); 
		$('.advert_head').keyup(function() {
		    $('.lead_head').html($(this).val());
		}); 
		$('.advert_text').keyup(function() {
		    $('.lead_text').html($(this).val());
		}); 
		$('.advert_url').keyup(function() {
		    $('.lead_url').html($(this).val());
		}); 
		$('.conversion_title').keyup(function() {
		    $('.con_create_title').html($(this).val());
		}); 
		$('.conversion_phrase').keyup(function() {
		    $('.con_create_phrase').html($(this).val());
		}); 
		$('.conversion_head').keyup(function() {
		    $('.con_create_head').html($(this).val());
		}); 
		$('.conversion_text').keyup(function() {
		    $('.con_create_text').html($(this).val());
		});
		$('.inbox_title').keyup(function() {
		    $('.inbox_create_title').html($(this).val());
		});
		$('.inbox_main_catch').keyup(function() {
		    $('.inbox_create_main').html($(this).val());
		});
		$('.inbox_sub_catch').keyup(function() {
		    $('.inbox_create_sub').html($(this).val());
		});
		$('.inbox_head').keyup(function() {
		    $('.inbox_create_head').html($(this).val());
		});
		$('.inbox_text').keyup(function() {
		    $('.inbox_create_text').html($(this).val());
		});
		$('.endorsement_phrase').keyup(function() {
		    $('.endorsement_create_phrase').html($(this).val());
		});
		$('.endorsement_head').keyup(function() {
		    $('.endorsement_create_head').html($(this).val());
		});
		$('.endorsement_text').keyup(function() {
		    $('.endorsement_create_text').html($(this).val());
		});    
	}); 

	function main_change_button(x) {    
	    $('.con_create_button').text(x);
	}      
	function main_inbox_button(y) {    
	    $('.inbox_create_button').text(y);
	}    
	function pageendorse_change(z) {    
	    $('.endorsement_create_value').text(z);
	}
	function main_title_change() {
	    var x = document.getElementById("mySelect").selectedIndex;
	    $('.head_chtitle').text(document.getElementsByTagName("option")[x].value);
	}     
} else if(window.location.href.indexOf("hangout") > -1) {
	function callmyfun(id)
	{
		var objDiv = document.getElementById(id);
         objDiv.scrollTop = objDiv.scrollHeight;
	}
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function gridBoxImgUINew($parent) {
	setTimeout(function(){
		$($parent).each(function() {
			$(this).find(".photo-holder").each(function(e, v) {
				var cont_width=$(v).width();
				var cont_height=$(v).height();

				$(v).find("img").css('width', cont_width+'px');
				$(v).find("img").css('height', cont_height+'px');				
			});
		});
	},400);
}

function gridBoxImgUI($parent) {
	setTimeout(function(){  
		$($parent).each(function(){
			$(this).find(".photo-holder").each(function(e, v) {
				$imageName = $(v).find('img').attr('src');
				if($imageName.indexOf('additem-collections.png') != -1){
				    $(v).find('img').addClass('defaultprofile');
				} else if($imageName.indexOf('additem-channels.png') != -1){
				    $(v).find('img').addClass('defaultprofile');
				} else if($imageName.indexOf('additem-commevents.png') != -1){
				    $(v).find('img').addClass('defaultprofile');
				} else if($imageName.indexOf('additem-groups.png') != -1){
				    $(v).find('img').addClass('defaultprofile');
				} else {
					$imageH = $(v).find('img')[0].clientHeight;
					$imageW = $(v).find('img')[0].clientWidth;
					if($imageH > $imageW) { 
			            $imgclass = 'himg';
			        } else if($imageW > $imageH) { 
			            $imgclass = 'vimg';
			        } else {
			            $imgclass = 'himg';
			        }
			        $(v).addClass($imgclass+"-box");
			        $(v).find('img').addClass($imgclass);
				}
			});
		});

		$($parent).each(function(){
			if(!$(this).find(".photo-holder").find('img').hasClass('defaultprofile')) {
				if($(this).find('.general-box').hasClass("imgfix")) {
					$(this).find('.general-box').removeClass("imgfix");
				}
					
				$(this).find(".photo-holder").find('img').css("margin-left","0");
				$(this).find(".photo-holder").find("img").css("margin-top","0");
			}
		});	

		$($parent).each(function() {
			$(this).find(".photo-holder").each(function(e, v) {
				if($(v).find('img').hasClass('defaultprofile')) {
					var cont_width=$(v).width();
					var cont_height=$(v).height();
					$(v).find("img").css('width', cont_width+'px');
					$(v).find("img").css('height', cont_height+'px');
				} else {
					var cont_width=$(v).width();
					var cont_height=$(v).height();
					var img_width=$(v).find("img").width();
					var img_height=$(v).find("img").height();
					
					if($(v).hasClass("himg-box")) {										
						if(img_width < cont_width) {							
							$(v).addClass("imgfix");
						}
					} 
					
					if(img_width > cont_width) {						
						var iwidth = resizeToWidth(img_width,img_height,cont_height);
						var lfix= ( iwidth - cont_width ) / 2;
						$(v).find("img").css("margin-left","-"+lfix+"px");							
					}
					
					if(img_height > cont_height || $(v).hasClass("imgfix")){
						var iheight = resizeToHeight(img_width,img_height,cont_width);
						var tfix= ( iheight - cont_height ) / 2;
						$(v).find("img").css("margin-top","-"+tfix+"px");							
					}

					if(img_width < cont_width) {
						$(v).find("img").css("width","100%")
					}		

					if(img_height < cont_height) {
						$(v).find("img").css("height","100%")
					}
				}
			});
		});
	}, 1000);
}
  
function temp() {
	$uploadpopupJIDSphototitle = $('.upload-popupJIDS-phototitle').val();
	$uploadpopupJIDSdescription = $('.upload-popupJIDS-description').val();
	$uploadpopupJIDSlocation = $('.upload-popupJIDS-location').val();
	$uploadpopupJIDStaggedconnections = $('.upload-popupJIDS-taggedconnections').val();
	$uploadpopupJIDSphotocategories = $('.upload-popupJIDS-photocategories').val();

	var puttedData = {
		'$uploadpopupJIDSphototitle' : $uploadpopupJIDSphototitle,
		'$uploadpopupJIDSdescription' : $uploadpopupJIDSdescription,
		'$uploadpopupJIDSlocation' : $uploadpopupJIDSlocation,
		'$uploadpopupJIDStaggedconnections' : $uploadpopupJIDStaggedconnections,
		'$uploadpopupJIDSphotocategories' : $uploadpopupJIDSphotocategories				
	}

	if($('.img-row.layered').find('.img-box.activelayered').length) {
		$oldindex = $('.img-row.layered').find('.img-box.activelayered').index();
		photoUpload[$oldindex] = puttedData;			
	} else {
		photoUpload[0] = puttedData;			
	}
}

/* manage message page */
	function manageMessagePage(){
		var winw=$(window).width();		
		var isMessagePage=$(".main-content").hasClass("messages-page");
		
		$('.messages-page').find('.messages-list').removeClass("newmsg-mode");
		
		if(isMessagePage){
			if(winw<800){
				if(!$(".main-content").hasClass("mblMessagePage")){					
					$(".main-content").addClass("mblMessagePage");								
					$(".messages-list").find(".left-section").addClass("shown");
					$(".messages-list").find(".right-section").addClass("hidden");
				}
				removeNiceScroll(".nice-scroll");
			}
			else{
				$('.messages-page').removeClass("innerpage");
				
				if($(".main-content").hasClass("mblMessagePage")){
					$(".main-content").removeClass("mblMessagePage");
					$(".messages-list").find(".left-section").removeClass().addClass("left-section");
					$(".messages-list").find(".right-section").removeClass().addClass("right-section");
				}
				initNiceScroll(".nice-scroll");
			}
		}
		
		var isBusinessPage=$(".page-wrapper").hasClass("businesspage");
		if(isBusinessPage){
			if(winw<800){
				if(!$(".messages-content").hasClass("mblMessagePage")){					
					$(".messages-content").addClass("mblMessagePage");								
					$(".messages-list").find(".left-section").addClass("shown");
					$(".messages-list").find(".right-section").addClass("hidden");
				}
				removeNiceScroll(".nice-scroll");
			}
			else{
				if($(".messages-content").hasClass("mblMessagePage")){
					$(".messages-content").removeClass("mblMessagePage");
					$(".messages-list").find(".left-section").removeClass().addClass("left-section");
					$(".messages-list").find(".right-section").removeClass().addClass("right-section");
				}
				initNiceScroll(".nice-scroll");
			}
		}
		
		fixMessageImagesPopup();
		
	}
/* end manage message page */
	
	function closeAddNewMsg() {

		if ($(window).width() <= 799) {
			$('.messagesearch-xs-btn').hide();
			$('.search-xs-btn').show();
			$('.globel_setting').show();
			$('.person_dropdown').hide();
		}
		$(".allmsgs-holder .newmessage").hide();
		$('.messages-page').find('.messages-list').removeClass("newmsg-mode");
		
		var isMblMessagePage=$(".main-content").hasClass("mblMessagePage");
		if(isMblMessagePage) {
			
			$(".messages-list").find(".right-section").addClass("hidden");
			
			if($(".messages-list").find(".left-section").hasClass("hidden")){
				$(".messages-list").find(".left-section").removeClass("hidden");
				$(".messages-list").find(".left-section").addClass("shown");
				
				$(".header-themebar .mbl-innerhead").hide();
				$(".header-nav").find(".mobile-menu").show();
				$('.mobile-footer').show();				
				$(".mblMessagePage").removeClass("innerpage");
			}			
		}
		
		var isBusinessPage=$(".page-wrapper").hasClass("businesspage");
		if(isBusinessPage){
			var isMblMessageTab=$(".messages-content").hasClass("mblMessagePage");
			if(isMblMessageTab){
				$(".messages-list").find(".right-section").addClass("hidden");
			
				if($(".messages-list").find(".left-section").hasClass("hidden")){
					$(".messages-list").find(".left-section").removeClass("hidden");
					$(".messages-list").find(".left-section").addClass("shown");
					
					$(".header-themebar .mbl-innerhead").hide();
					
					$('.mobile-footer').show();				
					$(".mblMessagePage").removeClass("innerpage");
				}						
			}
		}

		closeactivemessageblock();
	}

	function openalert() {
		// check booking box is open or not if open then close....
		$('.footer-menu').removeClass('active');

		if($('.master_alert').length) {
			if($('.master_alert').hasClass('active')) {
				$('.master_alert').removeClass('active');
			} else {
				$('.master_alert').addClass('active');
			}
		}
	}

	function removepic(obj)
	{	
		$this = $(obj);
    	Materialize.toast('Deleted', 2000, 'green');
    	$this.parents('.allow-gallery').remove();

    	justifiedGalleryinitialize();
		
    	if($('.gallery-content').find('.allow-gallery').length <=0) {
    		$('#gallery-content').html('<div class="combined-column"> <div class="cbox-desc"> <div class="right upload-gallery" style="cursor: pointer;">Upload Photo</div> </div> </div> <div id="lgt-gallery-photoGallery" class="lgt-gallery-photoGallery lgt-gallery-justified dis-none"><div class="content-box bshadow"> <div class="post-holder bshadow"> <div class="joined-tb"> <i class="mdi mdi-file-outline"></i> <p>No pinned photos</p> </div> </div> </div></div>');
    	}
	}

	function openCustomSearchMain() {
		if($('.main-msgwindow').length) {
			if($('.main-msgwindow').hasClass('customsearchmainactive')) {
				$('.main-msgwindow').removeClass('customsearchmainactive');
			} else {
				$('.main-msgwindow').addClass('customsearchmainactive');
			}
		}
	}

	function setteledBox() {
		setTimeout(function(){
			$hrefs = ['#places-all', '#places-discussion', '#places-reviews', '#places-travellers', '#places-locals', '#places-photos', '#places-tip', '#places-ask'];
			$hrefs1 = ['#places-all'];
			$hrefs2 = ['#places-travellers', '#places-locals'];
			$hrefs3 = ['#places-discussion', '#places-reviews', '#places-tip', '#places-ask'];
			$window = $(window).width();

			$container = $('.container.cshfsiput').width();
			$placescolumn = $('.places-column.cshfsiput');
			$wallcontentcolumn = $('.wallcontent-column.cshfsiput'); // 360
			
			$tabName = $('.tablist').find('.tab').find('a.active').attr('href');
			if($.inArray($tabName, $hrefs) !== -1) {
				if($.inArray($tabName, $hrefs1) !== -1) {
					if($window <= 992) {
						$placescolumn.css('width', '100%');
					} else {
						$wallcontentcolumn.css('width', '360px');
						$s = $container - $wallcontentcolumn.width() - 26;
						$placescolumn.css('width', $s+'px');
					}
				} else if($.inArray($tabName, $hrefs2) !== -1) {
					if($window <= 1050) {
						$placescolumn.css('width', '100%');
					} else if($window <= 1100) {
						$wallcontentcolumn.css('width', '300px');
						$s = $container - $wallcontentcolumn.width() - 26;
						$placescolumn.css('width', $s+'px');
					} else if($window <= 1275) {
						$wallcontentcolumn.css('width', '330px');
						$s = $container - $wallcontentcolumn.width() - 26;
						$placescolumn.css('width', $s+'px');
					} else if($window <= 1050) {
						$placescolumn.css('width', '100%');
					} else {
						$wallcontentcolumn.css('width', '360px');
						$s = $container - $wallcontentcolumn.width() - 26;
						$placescolumn.css('width', $s+'px');
					}
				} else if($.inArray($tabName, $hrefs3) !== -1) {
					if($window > 1225) {
						$wallcontentcolumn.css('width', '360px');
						$s = $container - $wallcontentcolumn.width() - 1;
						$placescolumn.css('width', $s+'px');
					} else if($window >= 1150 && $window <= 1225) {
						$wallcontentcolumn.css('width', '330px');
						$s = $container - $wallcontentcolumn.width() - 1;
						$placescolumn.css('width', $s+'px');
					} else if($window >= 900  && $window <= 1000) {
						$wallcontentcolumn.css('width', '310px');
						$s = $container - $wallcontentcolumn.width() - 15;
						$placescolumn.css('width', $s+'px');
					} else if($window > 767 && $window <= 1150) {
						$wallcontentcolumn.css('width', '360px');
						$s = $container - $wallcontentcolumn.width() - 15;
						$placescolumn.css('width', $s+'px');
					}
				}
				$('.places-column').css('opacity', 1);
			}
		},800);
	}