$(window).resize(function () {
    if($('.profile-settingblock3').length) {
        $('.profile-settingblock1').show();
        $('.profile-settingblock2').hide();
        $('.profile-settingblock3').hide();
    }
});

$(document).ready(function() {
    $(document).on("click",".profile-settingblock3 #pac-input",function () {
        $('.search_container .close_span').hide();
        $('.search_container .back_arrow').show();
        $('#pac-input_new').addClass('pac_input_new');
        $('.search_span').hide();
        $('.empty_input').show();
    });

    $(document).on("click",".profile-settingblock3 .back_arrow",function () {
        $('.search_container .close_span').show();
        $('.search_container .back_arrow').hide();
        $('.search_span').show();
        $('.empty_input').hide();
        $('#pac-input_new').val("");
    });

    $(document).on("click",".profile-settingblock3 .search_span",function () {
        $('#pac-input_new').focus();
    });

    $(document).on("click",".profile-settingblock3 .empty_input",function () {
        $('#pac-input_new').val('')
    });

    $(document).on('click', '.profile-settingblock3 .close_span', function (e) {
        if($('.profile-settingblock3').length) {
            $('.profile-settingblock1').show();
            $('.profile-settingblock2').hide();
            $('.profile-settingblock3').hide();
        }
    });

    $(document).on("click",".profile-settingblock3 #pac-input_new",function () {
        $('.search_container .close_span').hide();
        $('.search_container .back_arrow').show();
        $('.profile-settingblock3 #pac-input_new').addClass('pac_input_new');
        $('.search_span').hide();
        $('.empty_input').show();
    });

    $(document).on('click', '.map_location_container', function (e) {
        $('#pac-input_new').val(location_name);

        if($('.profile-settingblock3').length) {
            $('.profile-settingblock1').show();
            $('.profile-settingblock2').hide();
            $('.profile-settingblock3').hide();
        }
        $('#'+$isMapLocationId).val(location_name);
    });

    $(document).on("focus keyup","#pac-input_new", function() {
        initAutocompletenew();
    });

});

function initAutocompletenew() {
    var input = document.getElementById('pac-input_new');
    var searchBox = new google.maps.places.SearchBox(input);
    
    searchBox.addListener('places_changed', function () {
        var places = searchBox.getPlaces();
        var location = $("#pac-input_new").val();

        $('#'+$isMapLocationId).focus().val(location);
        if($('.profile-settingblock3').length) {
            $('.profile-settingblock1').show();
            $('.profile-settingblock2').hide();
            $('.profile-settingblock3').hide();
        }
    });
}


function filderMapLocationModalSignup(obj) {

    var $value = $('#pac-input').val();

    $('.pac-container').remove();
    $query = $(obj).attr('data-query');
    $isMapLocationId = $(obj).attr('id');
    var $value = $('#'+$isMapLocationId).val();

    if($query != undefined && $query != null && $query != '') {
        if($query == 'all') {
            
            if($('.profile-settingblock3').length) {
                $('.profile-settingblock1').hide();
                $('.profile-settingblock2').hide();
                $('.profile-settingblock3').show();

                currentlocationsignup();
            }

            $('#pac-input_new').val($value);
        } 
    }
}

function failGetLatLongsignup($location='') {
  if($location == '' || $location == undefined || $location == null) {
    $location = '"Amman"';
  } else if($location.code == 1 && $location.message == 'User denied Geolocation') {
    $location = '"Amman"';
  }

  var geocoder =  new google.maps.Geocoder();
  geocoder.geocode( { 'address': $location }, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
        var lat = results[0].geometry.location.lat();
        var lng = results[0].geometry.location.lng();
        var exPos = {lat, lng};
        currentlocation2signup(exPos);
    }
  });
}

function getLatLongsignup(position) {
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;
    var exPos = {lat, lng};
    currentlocation2signup(exPos);
}

function currentlocationsignup() {
    var place_name = "";
    if($('#'+$isMapLocationId).length) {
        var location = $('#'+$isMapLocationId).val();

        if(location == '') {
            if(navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(getLatLongsignup, failGetLatLongsignup);
            } else {
              failGetLatLongsignup();
            }
        } else {
            var geocoder =  new google.maps.Geocoder();
            geocoder.geocode( { 'address': location }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var lat = results[0].geometry.location.lat();
                var lng = results[0].geometry.location.lng();
                var exPos = {lat, lng};
                currentlocation2signup(exPos);
            }
          });
        }
   } 
   else if($isMapLocationId == undefined || $isMapLocationId == '') {
        if(navigator.geolocation) {
            if($locationLabel != '') {
                failGetLatLongsignup($locationLabel);
            } else {
                navigator.geolocation.getCurrentPosition(getLatLongsignup, failGetLatLongsignup);   
            }
        } else {
            if($locationLabel != '') {
                failGetLatLongsignup($locationLabel);
            } else {
                failGetLatLongsignup();
            }
        }
   }
}

function currentlocation2signup(pos) {
    var geocoder =  new google.maps.Geocoder();
    geocoder.geocode({'location': pos}, function(results, status) {
        if (status === 'OK') {

            map = new google.maps.Map(document.getElementById('map_new'), {
                center: pos,
                zoom: 9
            });

            var options = {
                types: ['(cities)'],
            };

            var markers = [];

            markers.forEach(function (marker) {
                marker.setMap(null);
            });

            markers = [];
            markers.push(new google.maps.Marker({
                map: map,
                position: pos
            }));

            var service = new google.maps.places.PlacesService(map);
                var div = "";
                $(".map_container").html('');
                for (var i = 0; i < results.length; i++) {

                    var address_components = results[i].address_components;
                    var country = '';

                    $.each(address_components, function (i, address_component) {
                        if (address_component.types[0] == "administrative_area_level_3"){ 
                            if(country != '') {
                                country = country + ', ' + address_component.long_name;
                            } else {
                                country = address_component.long_name;
                            }
                        }

                        if (address_component.types[0] == "locality"){
                            if(country != '') {
                                country = country + ', ' + address_component.long_name;
                            } else {
                                country = address_component.long_name;
                            }
                        }

                        if (address_component.types[0] == "country"){ 
                            if(country != '') {
                                country = country + ', ' + address_component.long_name;
                            } else {
                                country = address_component.long_name;
                            }
                        }

                        if (address_component.types[0] == "postal_code_prefix") {
                            if(country != '') {
                                country = country + ', ' + address_component.long_name;
                            } else {
                                country = address_component.long_name;
                            }
                        }
                    });
                    //place_name = results[0].name;
                    div = "<div class='map_location_container' data-id='" + i + "'>" +
                        "<div class='location_container'>" +
                        "<span class='map_icon'>" +
                        "<i class='zmdi zmdi-hc-2x zmdi-pin'></i>" +
                        "</span>" +
                        "<div class='location'>" +
                        "<p class='location_name' id='dev" + i + "' >" + results[i].formatted_address + "</p>" +
                        "<p class='place_address'>" + country + "</p>" +
                        "</div>" +
                        "</div>" +
                        "</div>";
                    
                    $('.profile-settingblock3').find(".map_container").append(div);
                }
                setTimeout(function(){
                    initNiceScroll(".nice-scroll");                     
                },400);

            google.maps.event.addListener(markers[0], 'click', function () {
                if($('.profile-settingblock3').length) {
                    $('.profile-settingblock1').show();
                    $('.profile-settingblock2').hide();
                    $('.profile-settingblock3').hide();
                }

                var location = $("#pac-input_new").val();
                if($.trim(location) == '') {
                    location = 'Amman';
                }
                $('#'+$isMapLocationId).focus().val(location);
            });

        }
    });
}

