﻿var person = [];
var person = [];
var personSearch = [];
var testArray = [];
var map;
var pos;
var location_name = "";
var lname = "";
var editperson = [{
    id: "1", name: "Nimish Parekh", post_edittitle: "Qatar is Nice",
    post_comment: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    post_location: "Surat"
}];
var t = 0;
var testArrayMessage = [];
var $isMapLocationId = '';
var $locationLabel = '';
var selectStartPoint = '';
var starsArray = ['1', '2', '3', '4', '5', 1, 2, 3, 4, 5];
var photoUpload = [];

$(document).ready(function () {
    setModelMaxHeight();
    datepikerfndestoy();
    setTimeout(function(){
        gridBoxImgUINew('.gridBox127');
    },400);

    // js for active class to check upload images
    jQuery(function($) {
        $('.check-image').click(function() {
            $(this).toggleClass('active-class');
            return false;
        });
    });

    // js for sticky navbar
    $(window).scroll(function(){
        if ($(window).scrollTop() >= 280) {
            $('.index-page .sticky-nav').css('display', 'block');
            $('.index-page .header-section .header-themebar').css('box-shadow', 'none');
        }
        else {
            $('.index-page .sticky-nav').css('display', 'none');
            $('.index-page .header-section .header-themebar').css('box-shadow', '0 1px 8px rgba(0,0,0,.3)');
        }
    });

    // js for header box-shadow
    $("li.rem-shadow-none").click(function(){
        $(".wallpage .header-section .header-themebar.innerpage").addClass("box-shadow-none");
    });
    $(".wall-tabs .tabs li").not( document.getElementsByClassName("rem-shadow-none")).click(function(){
      $(".wallpage .header-section .header-themebar.innerpage").removeClass("box-shadow-none");
    });
    
    // js to add class 'waves-effect' and 'waves-effect waves-light' elements
    $("ul.dropdown-content li, a.pa-like, a.pa-share, a.pa-comment, a.pa-publish, a.done_btn, a.btn-invite-close, a.cross, #account_setting li a").addClass("waves-effect");
    $("a.popup-window, a.btn-floating, a.homebtn").addClass("waves-effect waves-light");

    // js for dropdown initialization
    $('.dropdown .dropdown-button, .public_dropdown_container').dropdown({
      inDuration: 500,
      outDuration: 225,
      constrainWidth: true, // Does not change width of dropdown to that of the activator
      hover: false, // Activate on hover
      gutter: 0, // Spacing from edge
      belowOrigin: false, // Displays dropdown below the button
      alignment: 'right', // Displays dropdown with edge aligned to the left of button
      stopPropagation: true // Stops event propagation
    });

    // js for dynamically add/remove dishes
    $(document).ready(function() {
        var max_fields      = 10; 
        var wrapper         = $(".dish-wrapper");
        var add_button      = $("#addDish"); 
        
        var x = 1; 
        $(add_button).on("click",function(e){ 
            e.preventDefault();
            if(x < max_fields){ 
                x++; 
                $(wrapper).append('<div class="dish-wrapper"><div class="fulldiv mobile275"><div class="frow"><div class="caption-holder"><label>Dish name</label></div><div class="detail-holder"><div class="input-field"><input type="text" placeholder="i.e Appetiser, Main Dish, Dessert" class="fullwidth locinput "/></div></div></div></div><a href="#" class="remove-field"><i class="mdi mdi-close"></i></a><div class="fulldiv mobile275"><div class="frow"><div class="caption-holder mb0"><label>Summary</label></div><div class="detail-holder"><div class="input-field"><textarea class="materialize-textarea md_textarea item_tagline" placeholder="Tell your guest what you are cooking. Detail description get the most guests joining up!"></textarea></div></div></div></div></div></div>'); //add input box
            }
        });
        
        $(wrapper).on("click",".remove-field", function(e){ 
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });

    // js to toggle icons
    $(document).on("click", ".wasin-country", function () {
        if($(window).width()<=400){
            $(this).find('i').toggleClass('zmdi zmdi-chevron-down mdi-22px zmdi zmdi-close mdi-20px');
        }
    });

    // js for expandable text
    // $(function() {
    //   $(window).scroll(function() {
    //     var mass = Math.max(2, 4-0.01*$(this).scrollTop()) + 'rem';
    //     $('.expandable-text').css({'font-size': mass, 'line-height': mass});
    //   });
    // });
    

	$('.collapsible').collapsible();
	
	var slider = document.getElementById('age-slider');
	if(slider){
		noUiSlider.create(slider, {
				start: [20, 80],
				connect: true,
				step: 1,
				orientation: 'horizontal', // 'horizontal' or 'vertical'
				range: {
				 'min': 0,
				 'max': 100
			},
			format: wNumb({
			 decimals: 0
			})
		});		
	}
	
    
	$('.timepicker').pickatime({
		default: 'now', // Set default time: 'now', '1:30AM', '16:30'
		fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
		twelvehour: true, // Use AM/PM or 24-hour format
		donetext: 'OK', // text for done-button
		cleartext: 'Clear', // text for clear-button
		canceltext: 'Cancel', // Text for cancel-button
		autoclose: false, // automatic close timepicker
		ampmclickable: true, // make AM PM clickable
        container:'body',
		aftershow: function(){} //Function for after opening timepicker
	});

    //**************FUN for modal initialization **************/
	
    //FUN for post comment modal
    $('#comment_modal_xs').modal({
        dismissible: true,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            $("body").addClass("modal_open");
        },
        complete: function () { $("body").removeClass("modal_open"); } // Callback for Modal close
    });

	//modal for compose tool box
    $('.compose_tool_box').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            $("body").addClass("modal_open");
			totalPerson=[];
			$(".selected_person_text").html(totalPerson.length+" person selected");
			initDropdown();
			
			setTimeout(function(){
				initNiceScroll(".nice-scroll");						
				//var modal_height = $(".modal.open").visibleHeight();
				setModelMaxHeight();				
			},400);
			$("#compose_tool_box").find(".comment_textarea").focus();
        },
        complete: function () { 
			$("body").removeClass("modal_open");
			totalPerson=[]; 
			$(".selected_person_text").html(totalPerson.length+" person selected");
		}
    });

    //setting blocking popup
    $('#compose_addperson').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            $("body").addClass("modal_open");
			initDropdown();
			
        },
        complete: function () { 
			$("body").removeClass("modal_open");
			totalPerson=[]; 
			$(".selected_person_text").html(totalPerson.length+" person selected");
		}
    });
	//modal for tb post

	//modal for edit post
    $('.edit_post_modal').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            $("body").addClass("modal_open");
			setTimeout(function(){				
				setModelMaxHeight();
			},400);
        },
        complete: function () { $("body").removeClass("modal_open"); }  // Callback for Modal close
    });
	
	//modal for share post
	$('.sharepost_modal').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            $("body").addClass("modal_open");
			 setTimeout(function(){
				fixImageUI("popup-images");
				initNiceScroll(".nice-scroll");
				//var modal_height = $(".modal.open").visibleHeight();
				setModelMaxHeight();
			},400);
        },
        complete: function () { $("body").removeClass("modal_open"); }  // Callback for Modal close
    });
	//modal for preference
    $('.preference_modal').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            $("body").addClass("modal_open");			
        },
        complete: function () { 
			$("body").removeClass("modal_open");
		} // Callback for Modal close
    });
	
	//login modal
    $('#login_modal').modal({
        dismissible: true,
        opacity: .5,
        inDuration: 500,
        outDuration: 400,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            $("body").addClass("modal_open");
        },
        complete: function () { $("body").removeClass("modal_open"); }  // Callback for Modal close
    });
	// gift list modal
	$('.gift_modal').modal({
		dismissible: false,
		opacity: .5,
		inDuration: 500,
		outDuration: 700,
		startingTop: '50%',
		endingTop: '50%',
		ready: function (modal, trigger) {
			$("body").addClass("modal_open");
			setModelMaxHeight();
		},
		complete: function () { $("body").removeClass("modal_open"); } // Callback for Modal close

	});
	// use gift modal
	$('.usegift_modal').modal({
		dismissible: false,
		opacity: .5,
		inDuration: 500,
		outDuration: 700,
		startingTop: '50%',
		endingTop: '50%',
		ready: function (modal, trigger) {
			$("body").addClass("modal_open");
			setModelMaxHeight();
		},
		complete: function () { $("body").removeClass("modal_open"); } // Callback for Modal close

	});
    // add person modal
	$('#compose_addperson').modal({
		startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            //totalPerson=[];			
			var countPeople = $('[class="chk_person"]:checked').length;
			$(".selected_person_text").html(countPeople+" person selected");
        },
		complete: function () { 
			//totalPerson=[];
		} // Callback for Modal close
    });
	//fUN for custom md modal
    $('.custom_md_modal').modal({
        dismissible: true,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%'
    });
    //fUN for custom md modal
    $('.item_members').modal({
        dismissible: true,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%'
    });
	//FUN for post comment modal
    $('.add-item-popup').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            $("body").addClass("modal_open");
			$('ul.tabs-vert').tabs();
        },
        complete: function () { $("body").removeClass("modal_open"); } // Callback for Modal close
    });
	//FUN for manage modal
    $('.manage-modal').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            $("body").addClass("modal_open");
        },
        complete: function () { $("body").removeClass("modal_open"); } // Callback for Modal close
    });
	//modal for report abuse
    $('.reportabuse_modal').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            $("body").addClass("modal_open");
			setTimeout(function(){
				setModelMaxHeight();				
			},400);
			$(".reportabuse_modal").find(".comment_textarea").focus();
        },
        complete: function () { 
			$("body").removeClass("modal_open");
		}
    });	

     $('#payment-popup').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) { 
            $("body").addClass("modal_open GENPAY_popupbody");
            setModelMaxHeight();
        },
        complete: function () { $("body").removeClass("modal_open GENPAY_popupbody"); } // Callback for Modal close
    });

    $('#custom_user_modal').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',    
        ready: function (modal, trigger) {
            $('#custom_user_modal').find('.search_box').val('');
            $('#custom_user_modal').css('z-index', '1100');       
        },
        complete: function () { 
            
        }
    });

   
    //discard modal
    $('.discard_md_modal').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 400,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            if($(".main-content").hasClass("general-page") && !$(".main-content").hasClass("generaldetails-page")){
                $('.discard_md_modal').removeAttr("data-id");
            }
            $("body").addClass("modal_open");           
        },
        complete: function () { 
            $("body").removeClass("modal_open"); 
            $('#compose_discard_modal').removeClass('notallclose');
        } 
    });
	
    $('.tbpost_modal').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            $("body").addClass("modal_open");
            initDropdown();
            
        },
        complete: function () { 
            $("body").removeClass("modal_open");
            totalPerson=[]; 
            $(".selected_person_text").html(totalPerson.length+" person selected");
        }
    });

     //inner compose tool box modal
    $('.compose_inner_modal').modal({
        dismissible: true,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%', 
        endingTop: '50%',
        ready: function (modal) {
            if (modal.hasClass('map_modalUniq')) {
                var $value = $('#'+$isMapLocationId).val();
                $('#compose_mapmodal').find('#pac-input').val($value);
            }
        },
        complete: function (modal) {
            if (modal.hasClass('map_modal')) {
                var $value = $('#compose_mapmodal').find('#pac-input').val();
                if($value != '') {
                    $('#selectedlocation').find('.tagged_location_name').html($value);
                } else {
                    $('#selectedlocation').find('.tagged_location_name').html('');
                }
                $('.search_container .close_span').show();
                $('.search_container .back_arrow').hide();
                $('.search_span').show();
                $('.empty_input').hide();
            } else if (modal.hasClass('map_modalUniq')) {
                var $value = $('#compose_mapmodal').find('#pac-input').val();
                if($value != '') {
                    $('#'+$isMapLocationId).val($value);
                }
            }
        } // Callback for Modal close 
    });

	//**************FUN for material dropdown**************/
	initDropdown();

    //share it popup - collections/events/groups/channels
    $(document).on("click",".share-it",function (event) {
        shareInfoPost();
    });
	
    //keep button
    $(document).on("click",".modal_keep",function (event) {
        $(this).parents(".modal.open").modal("close");
		removeDisClasses();
    });
	//discard button
	$(document).on("click",".modal_discard",function (event) {
        if($(this).parents(".modal").hasClass("dis_createitem"))
            CollectionClearPost();
        if($(this).parents(".modal").hasClass("dis_postpopup"))
            clearPost();
        $allclose = true;
        if($('.modal.open').hasClass('notallclose')) {
            $allclose = false;
        }

        $(this).parents(".modal.open").modal("close");
        setTimeout(function() {
            if($allclose) {
                $(".modal.open").modal("close");      
            }      
        },400);
        removeDisClasses();
    });
	
    //compose box checkbox for 3dots
	$(document).on("click",".settings-icon .dropdown-button + .dropdown-content",function (event) {
        event.stopPropagation();
    });

    //**************FUN for material POST Comment**************
    //FUN for POST comment collapase js
    $(document).on("click",".pa-comment",function () {
        if ($(window).width() >= 569) {
            $(this).parents('.post-data').find(".comments-section").slideToggle("slow");
        }
        else {
			$('#comment_modal_xs').modal('open');
        }
    });
    //End of FUN for POST comment collapase js
	
	// bind setPostBtnStatus
	$(document).on("keypress keyup",".comment_textarea",function () {
        setPostBtnStatus();
    });
	$(document).on("click",".comment_modal_xs .cancel_poup",function () {
        $('.comment_modal_xs').modal('close');
    });
    	
	$(document).on("click",".readlink",function () {        
		var sparent=$(this).parents(".para-section");
		sparent.find(".para").toggleClass("opened");
		
		var thisObj = $(this);
		var itext = thisObj.html();
		if(itext == "Read More"){
			itext="Read Less";
		}else{
			itext="Read More";
		}
		setTimeout(function(){						
			thisObj.html(itext);			
		},200);

    });

    // js to hide/show readmore link
    $('.para-section').each(function(i, obj) {
       var paraLength = $(this).text().length;
        $(this).find('.readlink').hide();
       if(paraLength > 660){
         $(this).find('.readlink').show();
       }
    });


    $(document).on('click', '#composetoolboxAction,.composetoolboxAction', function () {
        $(".profile_name").text(editperson[0].name);
        $('#compose_tool_box').modal('open');
    });    
    
    $(document).on('click', '.compose_review', function (e) {
        selectStartPoint = '';
        if(e.target.tagName == 'I') {
            $('#compose_review').modal('open');
            var rate = $(e.target).data('value');
            selectStartPoint = rate;
            setTimeout(function(){
                dummyStarHelp234IIII(rate);
            },400);
        } else {
            $(".profile_name").text(editperson[0].name);
            $('#compose_review').modal('open');
            setTimeout(function(){
                dummyStarHelp234IIII();
            },400);
        }
    });
	$(document).on('click', '.compose_Reference', function () {
        $('#compose_Reference').modal('open');
    });	
	$(document).on('click', '.compose_discus', function () {
        $('#compose_discus').modal('open');
    });	
	$(document).on('click', '.edit_discus', function () {
        $('#edit_discus').modal('open');
    });	
	$(document).on('click', '.compose_newreview', function (e) {
        selectStartPoint = '';
        if(e.target.tagName == 'I') {
            $('#compose_newreview').modal('open');
            var rate = $(e.target).data('value');
            selectStartPoint = rate;
            setTimeout(function(){
                dummyStarHelp234IIII(rate);
            },400);
        } else {
            $(".profile_name").text(editperson[0].name);
            $('#compose_newreview').modal('open');
            setTimeout(function(){
                dummyStarHelp234IIII();
            },400);
        }
    });
	$(document).on('click', '.edit_newreview', function () {
        $('#edit_newreview').modal('open');
    });	
	$(document).on('click', '.compose_newtrip', function () {
        $('#compose_newtrip').modal('open');
    });	
	$(document).on('click', '.edit_newtrip', function () {
        $('#edit_newtrip').modal('open');
    });	
	$(document).on('click', '.compose_newask', function () {
        $('#compose_newask').modal('open');
    });	
	$(document).on('click', '.edit_newask', function () {
        $('#edit_newask').modal('open');
    });	
	$(document).on('click', '.composeTbPostAction', function () {
        $('#compose_tb_post').modal('open');
    });
    $(document).on('click', '.campCreateAction', function () {
        $('#campCreateModal').modal('open');
    });
    $(document).on('click', '.campEditAction', function () {
        $('#campEditModal').modal('open');
    });
    $(document).on('click', '.dineCreateAction', function () {
        $('#dineCreateModal').modal('open');
    });
    $(document).on('click', '.dineEditAction', function () {
        $('#dineEditModal').modal('open');
    });
    $(document).on('click', '.homestayCreateAction', function () {
        $('#homestayCreateModal').modal('open');
    });
    $(document).on('click', '.homestayEditAction', function () {
        $('#homestayEditModal').modal('open');
    });
    $(document).on('click', '.createGuideAction', function () {
        $('#createLocalGuideModal').modal('open');
    });
    $(document).on('click', '.editGuideAction', function () {
        $('#editLocalGuideModal').modal('open');
    });
    $(document).on('click', '.createDriverAction', function () {
        $('#createLocalDriverModal').modal('open');
    });
    $(document).on('click', '.editDriverAction', function () {
        $('#editLocalDriverModal').modal('open');
    });
    $(document).on('focus', '.upload-gallery', function () {
        photoUpload = [];
        $('#upload-gallery-popup').modal('open');
    });
    $(document).on('click', '.edit-gallery', function () {
        $('#edit-gallery-popup').modal('open');
    });
	$(document).on('click', '.Complete_Pro', function () {
        $('#Complete_Pro').modal('open');
    });
	$(document).on('click', '.Complete_loged', function () {
        $('#Complete_loged').modal('open');
    });
	$(document).on('click', '.composeTbCommentAction', function () {
        $('#compose_Comment_Action').modal('open');
    });
	$(document).on('click', '.composetravelbuddy', function () {
        $('#compose_tb_travel').modal('open');
    });
	$(document).on('click', '.composetravelhost', function () {
        $('#compose_tb_travelhost').modal('open');
    });
    $(document).on('click', '#new_post_btn', function () {
        $(".profile_name").text(editperson[0].name);
        $('#compose_tool_box').modal('open');        
    });
	$(document).on('click', '.suggest-connections', function () {
        $('#suggest-connections').modal('open');
    });
	$(document).on('click', '.compose_addpersonAction_as', function () {
        $('#compose_addperson').modal('open');
    });
    $(document).on('click', '.compose_visitcountryAction', function () {
        $('#compose_visitcountry').modal('open');
    });
    $(document).on('click', '.compose_iwasinCountryAction', function () {
        $('#compose_iwasincountry').modal('open');
    });
    $(document).on('focus', '#photoTags', function () {
        $('#compose_addperson').modal('open');
    });
    $(document).on('focus', '#photoCategories', function () {
        $('#compose_addcategories').modal('open');
    });
	$(document).on('click', '.edit_button_business', function () {
        $('#edit_button_business').modal('open');
    });	
	$(document).on('click', '.payment_popup', function () {
        $('#payment_popup').modal('open');
    });	
	$(document).on('click', '.login_account', function () {
        $('#login_modal').modal('open');
    });
	$(document).on('change', '.compose_camera', function () {
        $('#compose_camera').modal('open');
    });
	// for wall cropper
	$(".js-cropper-result--btn").addClass('');
    //FUN for location modal in compose tool box modal
    $(document).on('click', '#compose_mapmodalAction,.compose_mapmodalAction', function () {
        currentlocation();
        $('#compose_mapmodal').modal('open');
        $('#pac-input').val("");
    });
	
	
    $(document).on('click', '.compose_mapmodalAction1', function () {
        currentlocation();
        $('#compose_mapmodal1').modal('open');
    });
    $(document).on('click', '.location_div', function () {
        currentlocation();
        $('#compose_mapmodal').modal('open');
        var select_val = $('#selectedlocation').find('.tagged_location_name').text();
        $('#pac-input').val(select_val);
    });
    
    //FUN for upload photo modal in compose tool box modal
    $(document).on('click', '#compose_uploadphotomodalAction', function () {
        $('#compose_uploadphotomodal').modal('open');
    });
    
	$(document).on("click",".modal .close_span,.modal .custom_close,.modal .close_modal, .modal .img-cancel-btn, .modal .js-cropper-result--btn",function () {		
		if($(this).find(".compose_discard_popup").length > 0 || $(this).hasClass("open_discard_modal")){
			if($(this).parents(".modal").hasClass("add-item-popup"))
				generateDiscard("dis_createitem");
			else if($(this).parents(".modal").hasClass("post-popup"))
				generateDiscard("dis_postpopup");
			else
				generateDiscard();
		}else{	
			if($(this).hasClass("post_btn")){
				var modalID = $(this).parents(".modal").attr("id");
				if($(this).hasClass("active_post_btn")){
					if($(".discard_md_modal").hasClass("open")){
						$(".discard_md_modal").modal("close");
						setTimeout(function(){
							$(".modal.open").modal("close");			
						},400);
					}else{						
						$('#'+modalID+'').modal('close');
					}
				} 
			}else{		
				var modalID = $(this).parents(".modal").attr("id");
				if($(".discard_md_modal").hasClass("open")){
					$(".discard_md_modal").modal("close");
					setTimeout(function(){
						$(".modal.open").modal("close");			
					},400);
				}else{						
					$('#'+modalID+'').modal('close');
				}
			}
		}
    });

    //FUN for tag person modal in compose tool box modal
    $(document).on('click', '#compose_addpersonAction, .compose_addpersonAction', function () {
        $('#compose_addperson').modal('open');
    });

    //FUN of search input
	$(document).on("click",".search_span",function () {
        $('#pac-input').focus();
    });
	
    $(document).on("click",".back_arrow",function () {
        $('.search_container .close_span').show();
        $('.search_container .back_arrow').hide();
        $('.search_span').show();
        $('.empty_input').hide();
        $('#pac-input').val("");
    });

	$(document).on("click","#pac-input",function () {
        $('.search_container .close_span').hide();
        $('.search_container .back_arrow').show();
        $('#pac-input').addClass('pac_input_new');
        $('.search_span').hide();
        $('.empty_input').show();
    });

	$(document).on("click",".empty_input",function () {
        $('#pac-input').val('')
    });
    //end of search input

    //post title for compose tool box
    $(document).on("click", ".compose_titleAction", function () {
        $(this).parents(".modal").find(".title_post_container").slideToggle(500);
        $(this).parents(".modal").find(".title_post_container").focus();
		
		var scrollDiv = $(this).parents(".modal").find('.scroll_div');
		scrollDiv.animate({
			scrollTop: 0},
		'slow');
    });
	
    //compose modal privacy select dropdown
	$(document).on("click",".dropdown-content li",function () {
        $(this).parent().siblings('.dropdown_text').find("span").html($(this).find('a').html());
    });

    $(document).on("click", ".active_post_btn", function () {
		if($(window).width()>568){			
			if($(this).parents(".modal").hasClass("reportabuse_modal")){
				Materialize.toast('Reporting...', 2000, 'green');
				setTimeout(function () {
					Materialize.toast('Reported', 2000, 'green');
				}, 2000);				
			}else{				
				Materialize.toast('Posting...', 2000, 'green');
				setTimeout(function () {
					Materialize.toast('Posted', 2000, 'green');
				}, 2000);
			}
		}
    });
    $(document).on("click", ".compose_save_btn", function () {
		if($(window).width()>568){
			Materialize.toast('Saving...', 4000, 'green');
			setTimeout(function () {
				Materialize.toast('Saved', 2000, 'green');
			}, 2000);
		}
    });
    $(document).on("click", ".share_btn", function () {
		if($(window).width()>568){
			Materialize.toast('Sharing...', 4000, 'green');
			setTimeout(function () {
				Materialize.toast('Shared', 2000, 'green');
			}, 2000);
		}
    });

    $(document).on('click', '.composeeditpostAction', function () {		
        $('#composeeditpostmodal').modal('open');
		setTimeout(function(){
			initNiceScroll(".nice-scroll");						
		},400);

        $(".profile_name").text(editperson[0].name);
        //$("#new_editpost_comment").text(editperson[0].post_comment);
        $("#post_edittitle").show();
        $("#post_edittitle").val(editperson[0].post_edittitle)
        $("#new_editpost_comment").val(editperson[0].post_comment);

        var image_tag = "<div class='img-box'>" +
            "<img src='images/post-img.jpg' class='thumb-image undefined'>" +
            "<a href='javascript:void(0)' class='removePhotoFile' data-code='1498812224457'><i class='mdi mdi-close'></i></a>" +
            "</div>"+
            "<div class='img-box'><div class='custom-file addimg-box'><div class='addimg-icon'><span class='material-icons dp48'>add</span></div><input type='file' name='upload' class='upload custom-upload' title='Choose a file to upload' required='' data-class='.main-content .post-photos .img-row' multiple='true'></div></div></div>";
			;

        $(".img-row").append(image_tag);
        $("#composeeditpostmodal .post-photos").show();
        $("#edit_selectedlocation").html("<div class='location_div'><span class='tagged_location'></span><span class='tagged_location_name'>" + editperson[0].post_location + "</span></div><a href='javascript:void(0)'  class='removelocation'><i class='mdi mdi-close'></i></a>");
        testArray = ["Adel Google2", "Adel Google1"];
        t = testArray.length - 1;
        $("#edit_tag_person").html("<span>&nbsp; With &nbsp;</span><span class='tagged_person_name' id='compose_addpersonAction'>&nbsp;" + testArray[0] + "</span><span>&nbsp; and &nbsp;</span><span class='tagged_person_number'>" + t + " Others</span>")
    });
	
	$(document).on('click', '.editTbPostAction', function () {		
        $('#edit_tb_post').modal('open');
		setTimeout(function(){
			initNiceScroll(".nice-scroll");						
		},400);
    });
   
    //compose upload photo modal js for edit tool box
    $(document).on('click', '#compose_edituploadphotomodalAction', function () {
        $('#compose_uploadphotomodal').modal('open');
    });

    //post title for edit tool box
    $(document).on('click', '#compose_edittitleAction',function () {
        $("#post_edittitle").toggle();
    });
	
    //share choice modal js
    $(document).on('click', '.sharepostmodalAction', function () {
        $('#sharepostmodal').modal('open');
        $(".profile_name").text(editperson[0].name);		
    });
	
    $(document).on('click', '.show_all_btn',function () {
        $('.show_all_btn').hide();
        $('.collpase_btn').show();
    });
    $(document).on('click', '.collpase_btn',function () {
        $('.collpase_btn').hide();
        $('.show_all_btn').show();
    });
    //share info from share post modal
    $(document).on('click', '.show_all_btn',function () {
        $('.share_info_container').slideDown();
    });
    $(document).on('click', '.collpase_btn',function () {
        $('.share_info_container').slideUp();
    });

    //compose add person modal js - //share on connections wall - //share as message
    $(document).on('click', '#compose_addpersonAction, #share_connections, #share_message', function () {
        $(".person_box").empty();
        $('#compose_addperson').modal('open');
        person = [{ id: "1", name: "Adel Google1" },
        { id: "2", name: "Adel Google2" },
        { id: "3", name: "Adel Google3" },
        { id: "4", name: "Adel Google4" }
        ];
        personSearch = person;
        fillPerson();
    });

    //share on group 
    $(document).on('click', '#share_Group', function () {
        $(".person_box").empty();
        $('#compose_addperson').modal('open');
        person = [{ id: "1", name: "Group 1" },
        { id: "2", name: "Group 2" },
        { id: "3", name: "Group 3" },
        { id: "4", name: "Group 4" }
        ];
        personSearch = person;
        fillPerson();
    });
	
    $(document).on("click","#compose_addperson .close_span",function () {
       // $('#compose_addperson').modal('close');
        if (testArray.length == 0) {
            $("#tag_person").html("")
            $("#edit_tag_person").html("")
            $("#share_tag_person").html("")
        }
    });
	
    $(document).on("click","#chk_person_done",function () {
		
        $('#compose_addperson').modal('close');        
        t = testArray.length - 1;
        if (testArray.length > 1) {
            $("#tag_person").html("<span>&nbsp; With &nbsp;</span><span class='tagged_person_name' id='compose_addpersonAction'>&nbsp;" + testArray[0] + "</span><span>&nbsp; and &nbsp;</span><span class='tagged_person_number'>" + t + " Others</span>");
            $("#edit_tag_person").html("<span>&nbsp; With &nbsp;</span><span class='tagged_person_name' id='compose_addpersonAction'>&nbsp;" + testArray[0] + "</span><span>&nbsp; and &nbsp;</span><span class='tagged_person_number'>" + t + " Others</span>");
            $("#share_tag_person").html("<span>&nbsp; With &nbsp;</span><span class='tagged_person_name' id='compose_addpersonAction'>&nbsp;" + testArray[0] + "</span><span>&nbsp; and &nbsp;</span><span class='tagged_person_number'>" + t + " Others</span>");

        } else if (testArray.length == 1) {			
			$("#tag_person").html(" <span>With</span> <span class='tagged_person_name' id='compose_addpersonAction'>" + testArray[0] + "</span>");
            $("#edit_tag_person").html(" <span>With</span> <span class='tagged_person_name' id='compose_addpersonAction'>" + testArray[0] + "</span>");
            $("#share_tag_person").html(" <span>With</span> <span class='tagged_person_name' id='compose_addpersonAction'>" + testArray[0] + "</span>");

        } else {
            $("#tag_person").html("");
            $("#edit_tag_person").html("");
            $("#share_tag_person").html("");
        }
    });

    $(document).on("click", ".removelocation", function () {
        $("#selectedlocation").text("");
        $("#edit_selectedlocation").text("");
        $("#share_selectedlocation").text("");
        $('.post_active_btn').prop('disabled', true).removeClass('active_post_btn');
        $('.location_content').removeClass('location_parent')
    })

    $(document).on('click', '.map_location_container', function (e) {
        try {
            if ($("#selectedlocation").text() == "") {
                $('.post_active_btn').prop('disabled', false).addClass('active_post_btn');
            }
            var n = $(this).attr('data-id');
            location_name = $(this).find("#dev" + n).text();

            if($('#compose_mapmodal').hasClass('map_modalUniq')) {
                $('#compose_mapmodal').find('#pac-input').val(location_name);
                $('#compose_mapmodal').modal('close');
                $('#'+$isMapLocationId).val(location_name);
            } else {
                $locationLabel = location_name;
                $('#compose_mapmodal').find('#pac-input').val(location_name);
                $('#compose_mapmodal').modal('close');
                $('.location_content').addClass('location_parent');
                $("#selectedlocation").html("<div class='location_div'><span class='tagged_location'></span><span class='tagged_location_name'>" + location_name + "</span></div><a href='javascript:void(0)'  class='removelocation'><i class='mdi mdi-close'></i></a>");
                $("#edit_selectedlocation").html("<div class='location_div'><span class='tagged_location'></span><span class='tagged_location_name'>" + location_name + "</span></div><a href='javascript:void(0)'  class='removelocation'><i class='mdi mdi-close'></i></a>");
                $("#share_selectedlocation").html("<div class='location_div'><span class='tagged_location'></span><span class='tagged_location_name'>" + location_name + "</span></div><a href='javascript:void(0)'  class='removelocation'><i class='mdi mdi-close'></i></a>");
            }

        } catch (err) {
            
        }
    });
	
    $(document).on("keyup", "#search_box", function () {
        try {
            $(".person_box").empty();
            if ($("#search_box").val() != null) {
                personSearch = [];
                var str = $("#search_box").val().trim();
                $.each(person, function (index, value) {
                    if (value.name.toString().indexOf(str) >= 0) {
                        personSearch.push(value);
                    }
                });
            }
            else {
                personSearch = person;
            }
            fillPerson();
        } catch (err) {
            
        }
    });
    $('.requestsent').hide();

    //Fun add connect
    $(document).on('click', '.add_connect',function () {
        $(this).parents('.desc-holder').find('.requestsent').show();
        $(this).parents('.desc-holder').find('.add_connect').hide();
        $(this).parents('.desc-holder').find('.remove_connect').show();
    });
    $(document).on('click', '.remove_connect',function () {
        $(this).parents('.desc-holder').find('.requestsent').hide();
        $(this).parents('.desc-holder').find('.add_connect').show();
        $(this).parents('.desc-holder').find('.remove_connect').hide();
    });
    //end of Fun add connect

    $(document).on('click', '.datepickerinput', function() {
        $this = $(this);
        $query = $($this).attr('data-query');
        datepickerfn($this, $query);
    });

    $(document).on('click', '.datepickerinputtemp', function() {
        $this = $(this);
        $query = $($this).attr('data-query');
        datepickerfn($this, $query, true);
    });

    //FUN of height of compose_tool_box modal
    $(window).resize(function () {
		setModelMaxHeight();
        datepikerfndestoy();
        setTimeout(function(){
            gridBoxImgUINew('.gridBox127'); 
        },400);     
        dropdown929Ivalidation(); 
    });    
    //end of FUN of height of compose_tool_box modal

	$(document).on("click", ".overlay-link.postdetail-popup", function () {
		
        $('#postopenmodal').modal('open');
        $(".profile_name").text(editperson[0].name);
        //$("#new_editpost_comment").text(editperson[0].post_comment);
        $("#post_opentitle").show();
        $("#post_opentitle").val(editperson[0].post_edittitle)
        $("#new_openpost_comment").val(editperson[0].post_comment);
        var image_tag = "<img src='images/post-img.jpg' class='thumb-image undefined'>";
        $(".img-row").append(image_tag);
        $("#postopenmodal .post-photos").show();
        $("#edit_selectedlocation").html("<div class='location_div'><span class='tagged_location'></span><span class='tagged_location_name'>" + editperson[0].post_location + "</span></div><a href='javascript:void(0)'  class='removelocation'><i class='mdi mdi-close'></i></a>");
        testArray = ["Adel Google2", "Adel Google1"];
        t = testArray.length - 1;
        $("#open_tag_person").html("<span>&nbsp; With &nbsp;</span><span class='tagged_person_name' >&nbsp;" + testArray[0] + "</span><span>&nbsp; and &nbsp;</span><span class='tagged_person_number'>" + t + " Others</span>")

    });

    $(document).on("click", ".closedatepicker", function () {
        if($('#datepickerDropdown').hasClass('open')) {
            $('#datepickerDropdown').modal('close');
        }

        if($('.profile-settingblock2').length) {
            $('.profile-settingblock2').hide();
            $('.profile-settingblock1').show();
        }
    });

    $(document).on("click", ".resetdatepicker", function () {
        $('[data-toggle="datepicker"]').datepicker('reset');
        if($('.profile-settingblock2').length){
            $('.profile-settingblock2').hide();
            $('.profile-settingblock1').show();
        }
    });

	//Fun for tag person 
	$(".person_box").on("click", ".person_detail_div", function (e) {
		// if ($(e.target).hasClass("person_profile")) {
		$(this).find(".chk_person").click();
		// }

	});
	var totalPerson = [];
	$(document).on("click", ".chk_person", function (e) {
		var checkbox = $(this);
		var pn = $(this).parents(".person_name_container").find(".person_name").text()	
		
		if (checkbox[0].checked == true) {
			// checkbox.attr("checked", "true");
			testArray.push(pn);
			totalPerson.push(pn);
			//checkbox.removeAttr("checked");
			
		} else {
			testArray1 = testArray.filter(function (value) {
				return value == pn;
			});
			
			if (testArray1.length > 0) {
				var index = testArray.indexOf(testArray1[0]);
				testArray.splice(index, 1);
			}

			if(totalPerson.length > 0){
				var i = totalPerson.indexOf(pn);			
				totalPerson.splice(i, 1);
			}		
		}

		if (totalPerson.length > 1) {
			$(".selected_person_text").html(totalPerson.length+" persons selected");
		} else {
			$(".selected_person_text").html(totalPerson.length+" person selected");
		}
		
		if (testArray.length == 1) {
			$('.done_btn').prop('disabled', false).addClass('active_check');
		} else { 
			if (testArray.length == 0) {
				$('.done_btn').prop('disabled', true).removeClass('active_check');
			}
		}
	});
	
	$(document).on("click", "#saveimg",function () {
		$('#upload_img_box').modal('close');
		document.getElementById("imageid").src = image.src;
	});
	
    //**************collection page js**************
	
    $(document).on('click', '#upload_img_action', function () {
        $("#fileinput").click();
        $(".cropit-preview").css('display', 'block')
    });

    //FUN for create Toast on collection
    $(document).on("click", ".create_btn", function () {
		if($(window).width()>568){			
			Materialize.toast('Creating...', 2000, 'green');
			setTimeout(function () {
				Materialize.toast('Created', 2000, 'green');
			}, 2000);
		}
    });

    //FUN for create Toast on collection
    $(document).on("click", ".request_btn", function () {
		if($(window).width()>568){			
			Materialize.toast('Connect request sent', 2000, 'green');		
		}
    });

    //FUN for create Toast on collection
    $(document).on("click", ".Cancelling_btn", function () {
		if($(window).width()>568){			
			Materialize.toast('Cancelling connect request', 2000, 'green');
			
		}
    });
	
    //FUN for create Toast on collection
    $(document).on("click", ".update_btn", function () {
		if($(window).width()>568){			
			Materialize.toast('Updating...', 2000, 'green');
			setTimeout(function () {
				Materialize.toast('Updated', 2000, 'green');
			}, 2000);
		}
    });
	
	//**************travelbuddy detail page js**************
	//travelbuddy privacy select dropdown
	$(".travelbuddy_privacy_dropdown li").click(function() {
		$(this).parent().siblings('.travelbuddy_text').find("span").html($(this).find('a').html());
	});
	$('.messagesearch-xs-btn').hide();
	//**************FUN for travelbuddy page**************

	var public_trip_height = $('.public_trip_modal').height();
	if ($(window).height() <= 568) {
		$('.public_trip_content').css('height', public_trip_height - 55);
	} else {
		$('.public_trip_content').css('height', public_trip_height - 110);
	}

    $(document).on('click', '#privacymodal .head, #privacymodal .uniquehead', function (e) {
        if(e.toElement != undefined) {
            if(e.toElement.className == 'label') {
            } else {
                $(this).find('.label').trigger('click');
            }
        }
    });
	      
}); 
// document ready ends

$(document).on('mouseover', '.dropdown782 ul.select-dropdown', function() {
    $width = $(window).width();
    $dummy = null;
    if($width<480) {
       $fill = $(this).parent('div').find('select').data('fill');
       $action = $(this).parent('div').find('select').data('action');
       $selectore = $(this).parent('div').find('select').data('selectore');
       if($fill == 'y') {
            $dummy = $('select.'+$selectore).val();
       }
       dropdown929I($action, $fill, $selectore, $dummy);
    } else {
        $('#custom_dropdown_modal').modal('close');
    }

    $(this).css('overflow-y', 'auto');
    removeNiceScroll($(this));
});
 
$('#menu-security .selectore').on('click', function() {
    $selectore = $(this).find('a').html().trim();
    if($(this).parents('li').find('.getvalue').length) {
        $(this).parents('li').find('.getvalue').html($selectore);
    }
});

$(document).on('click', '#menu-security1 .selectore', function() {
    $selectore = $(this).find('a').html().trim();
    if($(this).parents('.left').find('.getvalue').length) {
        $(this).parents('.left').find('.getvalue').html($selectore);
    }
});

function dropdown929Ivalidation() {
    if($('#custom_dropdown_modal').length) {
        if($('#custom_dropdown_modal').hasClass('open')) {
            $('#custom_dropdown_modal').modal('close');                                                       
        }
    }
}

function privacymodal(obj) {
    $modeltag = $(obj).attr('data-modeltag');
    $('#privacymodal').find('.head').show();    
    $('#privacymodal').find('.uniquehead').hide();    


    var customNot = ['lookupsettingprivacylabel', 'connectrequestsettingprivacylabel', 'connectlistprivacylabel', 'tagreviewprivacylabel'];

    if($.inArray($modeltag, customNot) !== -1) {
        $('#privacymodal').find('.head.customhead').hide();    
    }

    if($modeltag == 'lookupsettingprivacylabel') {
        $('#privacymodal').find('.head.privatehead').hide();    
    }

    if($modeltag == 'postreviewprivacylabel') {
        $('#privacymodal').find('.head').hide();    
        $('#privacymodal').find('.uniquehead').show();    
    }

    $('#privacymodal').find('#customdoneprivacymodal').attr('data-privacy', $modeltag);
    $('#privacymodal').modal('open');
}

function datepickerfn($obj, $device, $isNew=false) {
    $('[data-toggle="datepicker"]').datepicker('destroy');
    $w = $(window).width();
    $isOpen = 'normal';

    if($device == 'M') {
        if($w <= 480) {
            $isOpen = 'modal';
        }
    } else if($device == 'MT') {
        if($w <= 1024) {
            $isOpen = 'modal';
        }
    } else if($device == 'all') {
        $isOpen = 'modal';
    } else if($device == 'none') {
        $isOpen = 'normal';
    }

    if($isNew) {
        if($w <= 420) {
            $isNew = false;
        }
    }

    if($isNew) {
        $($obj).datepicker({
            format: 'yyyy-mm-dd',
            container: $('#datepickerBlocktemp'),
            inline: true,
            autoShow: true,
            autoHide: false
        });
        $('.profile-settingblock1').hide();
        $('.profile-settingblock2').show();
    } else {
        if($isOpen == 'modal') {
            $('#datepickerDropdown').modal('open');
            $($obj).datepicker({
                format: 'yyyy-mm-dd',
                container: $('#datepickerBlock'),
                inline: true,
                autoShow: true,
                autoHide: false,
                pick:function() {
                    //$('#datepickerDropdown').modal('close');
                }
            });
        } else {
            if($('#datepickerDropdown').hasClass('open')) {
                $('#datepickerDropdown').modal('close');
            }
            $($obj).datepicker({
                format: 'yyyy-mm-dd',
                autoShow: true,
                autoHide: true,
                autoHide: false,
                zIndex: 2000
            });
        }
    }
}


function datepikerfndestoy() {
    $('[data-toggle="datepicker"]').datepicker('destroy');
    if($('#datepickerDropdown').hasClass('open')) {
        $('#datepickerDropdown').modal('close');
    }

    if($('.profile-settingblock2').length) {
        $('.profile-settingblock2').hide();
        $('.profile-settingblock1').show();
    }
}

function dropdown929I($action, $fill, $selectore, $dummy){
    $.ajax({
        type: 'POST',
        url: "dynamicdropdownlist.php", 
        data: {$action, $fill, $selectore, $dummy},
        success: function (data) {
            $('#custom_dropdown_modal').html(data);
            setTimeout(function() { 
                $('#custom_dropdown_modal').modal('open');
            },400);
        }
    });
}

function savedrpfilterdatachk($lbl) {
    $checkedArray = [];
    $("input:checkbox[class=filterdrpchk]:checked").each(function () {
        $thisValue = $(this).val();
        $checkedArray.push($thisValue);
    });
    
    $('#custom_dropdown_modal').modal('close');
    $($lbl).material_select('destroy');
    $($lbl).val($checkedArray).material_select();
}

function savedrpfilterdatardo($lbl) {
    $checkedArray = [];
    $sdata = $("input[name='filterdrpchk']:checked").val();
    $checkedArray.push($sdata);
    $('#custom_dropdown_modal').modal('close');
    $($lbl).material_select('destroy');
    $($lbl).val($checkedArray).material_select();
}

//FUN for collection share post  modal
function shareInfoPost() {
    $('#sharepostmodal').modal('open');
}
//FUN for compose add person  modal
function addpersonmodal() {
    $('#compose_addperson').modal('open');
}
//FUN for managing members
function manageMembers() {
	$('#managemembers-popup').modal('open');
}
//FUN for managing organizer
function manageOrganizer() {
	$('#manageorganizer-popup').modal('open');
}
//FUN for collection followers  modal
function CollectionFollowers() {
    $('.item_members').modal('open');
}
function promoteAdminModalOpen() {
    $('#manageadmin-popup').modal('open');
    setTimeout(function() { 
        allmemberforadmin();
    },1500);
}
function promoteOrganizerModalOpen() {
    $('#manageorganizer-popup').modal('open');
    setTimeout(function() { 
        allmemberfororganizer();
    },1500);
}
function promoteOwnerModalOpen() {
    $('#manageowner-popup').modal('open');
    setTimeout(function() { 
        allmemberforowner();
    },1500);
}
function manageOrganizernew() {
    setTimeout(function(){
       $('#manageorganizer-popup').modal('open');
    },500);
}

//FUN for members list modal
function openMembersList() {
    $('.item_members').modal('open');
}

//FUN for report abuse modal
function reportAbuseModal() {
    $('.reportabuse_modal').modal('open');	
}
//FUN for delete collection modal
function DeleteCollection() {
    //$('.delete_item').modal('open');
	generateDiscard("dis_deleteitem");
}
//FUN for delete item modal
function DeleteItem() {
    //$('.delete_item').modal('open');
	generateDiscard("dis_deleteitem");
}
function leaveItem() {
    //$('.delete_item').modal('open');
	generateDiscard("dis_leaveitem");
}
//FUN for edit collection modal
function EditCollection() {
	$('.dropdown-button').dropdown("close");
    $('.add-item-popup').modal('open');
}

//FUN for opening add item modal
function openAddItemModal() {
	$('.dropdown-button').dropdown("close");
    $('.add-item-popup').modal('open');
}
//discard modal 
function discardModel() {
    //$('.additem_discard_modal').modal('open');
    $('.discard_md_modal').modal('open');
}
//FUN for Preference Modal
function PreferenceModel() {
    $('#preference_modal').modal('open');
}

//coollection clear post
function CollectionClearPost() {
    $('.item_title').val('');
    $('.item_title').blur();
    $('.item_tagline').val('');
    $('.item_address').val('');
    $('.item_about').val('');
    $('.item_time').val('');
    $('.item_website').val('');
    $('.item_ticket').val('');
    $('.item_youtube').val('');
    $('.item_parking').val('');
    //$('.additem_discard_modal').modal('close');
    //$('.modal.open').modal('close');
	$('.dropdown-button').dropdown('close');
}

function KeepPost() {
    //$('.additem_discard_modal').modal('close');
    //$('#compose_discard_modal').modal('close');
    $('.compose_discard_modal').modal('close');
}
/*
function discardmodal() {
    $('#compose_discard_modal').modal('open');
}
*/
//clear data in  compose tool box 
function clearPost() {
    $('.title').val("");
    $('.comment_textarea').val("");
    $('.share_comment_textarea').val("");
    $("#post_edittitle").val("");
    $("#selectedlocation").text("");
    $("#edit_selectedlocation").text("");
    $("#share_selectedlocation").text("");
    $("#tag_person").text("");
    $("#edit_tag_person").text("");
    $("#share_tag_person").text("");
    testArray = [];
    $(".img-row").children().remove();
    $(".imgfile-count").val(0);
    $('.location_content').removeClass('location_parent');
    if ($(".comment_textarea").val().length == 0) {
        $('.post_active_btn').prop('disabled', true).removeClass('active_post_btn');
    }
	$('.dropdown-button').dropdown('close');
	//$('.modal.open').modal('close');
}


//disable btn when clear data in compose tool box
function stoppedTyping() {
	if ($(".new_post_comment").val().length == 0) {
        $('.post_active_btn').prop('disabled', true).removeClass('active_post_btn');

    } else {
        $('.post_active_btn').prop('disabled', false).addClass('active_post_btn');
    }
}
function setPostBtnStatus() {
    var $selectore = $('.main_modal.open').length;  
    if($selectore >0) {
        var $custId = $('.main_modal.open').attr("id");     
        var $textValue = $('#'+$custId).find('.post-mcontent').find('.comment_textarea').val().trim();
        if($textValue.length == 0) {
            var $imagesCount = $('#'+$custId).find('.post-photos').find('img').length;
            if($imagesCount <=0) {
                $('#'+$custId+' .post_btn').prop('disabled', true).removeClass('active_post_btn').css('cursor', 'not-allowed');
				$('#'+$custId+' .post_btn').addClass("disabled");
            } else {
                $('#'+$custId+' .post_btn').prop('disabled', false).addClass('active_post_btn').css('cursor', 'pointer');
				$('#'+$custId+' .post_btn').removeClass("disabled");	
            }
        } else {
            $('#'+$custId+' .post_btn').prop('disabled', false).addClass('active_post_btn').css('cursor', 'pointer');
			$('#'+$custId+' .post_btn').removeClass("disabled");		
        }   
    }
}

function fillPerson() {
    var div = "";
    for (var i = 0; i < personSearch.length; i++) {
        if ($.inArray(personSearch[i].name, testArray) > -1) {
            div += "<div class='person_detail_container person_detail_div'>" +
                "<span class='person_profile'>" +
                "<img src='images/people-1.png'>" +
                "</span>" +
                "<div class='person_name_container'>" +
                "<p class='person_name' id='checkPerson" + i + "'>" + personSearch[i].name + "</p>" +
                "<p class='user_checkbox' style='z-index:99999;'>" +
                "<input type='checkbox' checked id='filled_for_person_" + i + "' data-chkperson_id='" + i + "' class='chk_person'>" +
                "<label for='filled_for_person_" + i + "'></label>" +
                "</p>" +
                "</div>" +
                "</div>";
        }
        else {
            div += "<div class='person_detail_container person_detail_div'>" +
                "<span class='person_profile'>" +
                "<img src='images/people-1.png'>" +
                "</span>" +
                "<div class='person_name_container'>" +
                "<p class='person_name' id='checkPerson" + i + "'>" + personSearch[i].name + "</p>" +
                "<p class='user_checkbox' style='z-index:99999;'>" +
                "<input type='checkbox' id='filled_for_person_" + i + "' data-chkperson_id='" + i + "' class='chk_person'>" +
                "<label for='filled_for_person_" + i + "'></label>" +
                "</p>" +
                "</div>" +
                "</div>";
        }
    }
    $(".person_box").append(div);
}

$(document).on("focus keyup","#pac-input", function() {
    initAutocompletenew();
});

function initAutocompletenew() {
    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    
    //search_places
    // more details for that place.
    searchBox.addListener('places_changed', function () {
        var places = searchBox.getPlaces();
        if ($("#selectedlocation").text() == "") {
            $('.post_active_btn').prop('disabled', false).addClass('active_post_btn');
        }
        
        if($('#compose_mapmodal').hasClass('map_modalUniq')) {
            var location = $('#compose_mapmodal').find("#pac-input").val();
            $('#'+$isMapLocationId).focus().val(location);
        } else {
            var location = $("#pac-input").val();
            $locationLabel = location;
            $('.location_content').addClass('location_parent');
            $("#selectedlocation").html("<div class='location_div'><span class='tagged_location'></span><span class='tagged_location_name'>" + location + "</span></div><a href='javascript:void(0)'  class='removelocation'><i class='mdi mdi-close'></i></a>");
            $("#edit_selectedlocation").html("<div class='location_div'><span class='tagged_location'></span><span class='tagged_location_name'>" + location + "</span></div><a href='javascript:void(0)'  class='removelocation'><i class='mdi mdi-close'></i></a>");
            $("#share_selectedlocation").html("<div class='location_div'><span class='tagged_location'></span><span class='tagged_location_name'>" + location + "</span></div><a href='javascript:void(0)'  class='removelocation'><i class='mdi mdi-close'></i></a>");
        }

        $('#compose_mapmodal').modal('close');
        if (places.length == 0) {
            return;
        }
    });
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageid')
                .attr('src', e.target.result)

        };

        reader.readAsDataURL(input.files[0]);
    }
}

//Fun for croppit

$("#file").change(function () {
    loadImage(this);
});

function loadImage(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();
        canvas = null;
        reader.onload = function (e) {
            image = new Image();
            image.onload = validateImage;
            image.src = e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$(function () {	
    $('#image-cropper.mian-crop').cropit({
        height: 300,
        width: 1005,
        smallImage: "allow",
        onImageLoaded: function (x) {			
            $(".cropit-preview").removeClass("class-hide");
            $(".cropit-preview").addClass("class-show");
            $(".btn-save").removeClass("class-hide");
            $(".btn-save").addClass("class-show");
            $("#removeimg").removeClass("class-hide");
            $("#removeimg").addClass("class-show");
        }
    });
	// Ad page 
    $('#image-cropper.mian-crop1').cropit({
        height: 190,
        width: 366,
        smallImage: "allow",
        onImageLoaded: function (x) {			
            $(".cropit-preview").removeClass("class-hide");
            $(".cropit-preview").addClass("class-show");
            $(".btn-save").removeClass("class-hide");
            $(".btn-save").addClass("class-show");
            $("#removeimg").removeClass("class-hide");
            $("#removeimg").addClass("class-show");
        }
    });
    $('#image-cropper').cropit({
        height: 188,
        width: 355,
        smallImage: "allow",
        onImageLoaded: function (x) {
			
            $(".cropit-preview").removeClass("class-hide");
            $(".cropit-preview").addClass("class-show");

            $(".btn-save").removeClass("class-hide");
            $(".btn-save").addClass("class-show");
            $("#removeimg").removeClass("class-hide");
            $("#removeimg").addClass("class-show");
        }
    });
	
	
    // Exporting cropped image
    $('.btn-save').click(function () {
        var imageData = $('#image-cropper').cropit('export');
        $(".main-img1").find('img').attr('src', imageData);

        $(".cropit-preview").removeClass("class-show");
        $(".cropit-preview").addClass("class-hide");

        $(".btn-save").removeClass("class-show");
        $(".btn-save").addClass("class-hide");

        $(".main-img1").removeClass("class-hide");
        $(".main-img1").addClass("class-show");
        $(".main-img").removeClass("class-show");
        $(".main-img").addClass("class-hide");
    });

    $("#removeimg").click(function () {
        $(".main-img").removeClass("class-hide");
        $(".main-img").addClass("class-show");

        $(".cropit-preview").removeClass("class-show");
        $(".cropit-preview").addClass("class-hide");

        $(".main-img1").removeClass("class-show");
        $(".main-img1").addClass("class-hide");

        $(".btn-save").removeClass("class-show");
        $(".btn-save").addClass("class-hide");
        $("#removeimg").removeClass("class-show");
        $("#removeimg").addClass("class-hide");
    })
});

function openLogoutDialog(){	
	$('#logout_modal').modal('open');
}
function initDropdown(){
	//custom js for MD dropdown 
    $('select').material_select();
    $('.dropdown-button').dropdown({
        alignment: 'right'
    });
    $('.dropdown-button-left').dropdown({
        alignment: 'left'
    });
	$('.dropdown_right .dropdown-button').dropdown({
        alignment: 'right'
    });
}
function setModelMaxHeight(){
	var modal_height = $(window).height() * 0.7;
	if($(window).width() > 767)
		$('.modal.open .scroll_div').css('max-height', modal_height - 180 + 'px');
	else if($(window).width() > 420)
		$('.modal.open .scroll_div').css('max-height', modal_height - 150 + 'px');
	else{			
		$('.modal.open .scroll_div').css('max-height', $(window).height() - 210 + 'px');
	}	
}

function generateDiscard(forWhich=""){
	var disText = $(".discard_md_modal .discard_modal_msg");
	var btnKeep = $(".discard_md_modal .modal_keep");
	var btnDiscard = $(".discard_md_modal .modal_discard");
	
	if(forWhich!=undefined && forWhich!=null && forWhich!=""){		
		if(forWhich == "dis_createitem"){
			//disText.html();
			//btnKeep.html();
			//btnDiscard.html();
			$(".discard_md_modal").addClass(forWhich);
		}
		if(forWhich == "dis_postpopup"){
			disText.html("Discard Changes?");
			btnKeep.html("Keep");
			btnDiscard.html("Discard");
			$(".discard_md_modal").addClass(forWhich);
		}	
		if(forWhich == "dis_logout"){
			disText.html("Are you sure you want to logout?");
			btnKeep.html("Stay");
			btnDiscard.html("Logout");
			$(".discard_md_modal").addClass(forWhich);
		}	
		if(forWhich == "dis_deleteitem"){
			disText.html("Do you want to delete this collection permanently?");
			btnKeep.html("Keep");
			btnDiscard.html("Delete");
			$(".discard_md_modal").addClass(forWhich);
		}	
		if(forWhich == "dis_leaveitem"){
			disText.html("Leave this group?");
			btnKeep.html("Keep");
			btnDiscard.html("Leave");
			$(".discard_md_modal").addClass(forWhich);
		}	
		if(forWhich == "dis_unfollow"){
			disText.html("Are you sure you want to unfollow this collection?");
			btnKeep.html("Keep");
			btnDiscard.html("Unfollow");
			$(".discard_md_modal").addClass(forWhich);
		}
        if(forWhich == "dis_hidePhoto"){
            disText.html("Are you sure you want to hide this photo?");
            btnKeep.html("Keep");
            btnDiscard.html("Hide");
            $(".discard_md_modal").addClass(forWhich);
        }		
		if(forWhich == "dis_unsub"){
			disText.html("Are you sure you want to unsubscribe this channel?");
			btnKeep.html("Keep");
			btnDiscard.html("Unsubscribe");
			$(".discard_md_modal").addClass(forWhich);
		}
		if(forWhich == "dis_unattend"){
			disText.html("Are you sure you are not going to this event?");
			btnKeep.html("Cancel");
			btnDiscard.html("Not Going");
			$(".discard_md_modal").addClass(forWhich);
		}
		if(forWhich == "dis_delhangout"){
			disText.html("Are you sure you want to delete hangout?");
			btnKeep.html("Cancel");
			btnDiscard.html("Delete");
			$(".discard_md_modal").addClass(forWhich);
		}	
		if(forWhich == "dis_delescape"){
			disText.html("Are you sure you want to delete weekend escape?");
			btnKeep.html("Cancel");
			btnDiscard.html("Delete");
			$(".discard_md_modal").addClass(forWhich);
		}
		if(forWhich == "dis_hidescape"){
			disText.html("Are you sure you want to hide this event ?");
			btnKeep.html("Cancel");
			btnDiscard.html("Hide");
			$(".discard_md_modal").addClass(forWhich);
		}
		if(forWhich == "dis_mutescape"){
			disText.html("Are you sure you want to mute this person plans?");
			btnKeep.html("Cancel");
			btnDiscard.html("mute");
			$(".discard_md_modal").addClass(forWhich);
		}
		if(forWhich == "dis_blockscape"){
			disText.html("Blocking this person?");
			btnKeep.html("Cancel");
			btnDiscard.html("Block");
			$(".discard_md_modal").addClass(forWhich);
		}
		if(forWhich == "dis_blockoffer"){
			disText.html("Blocking this person?");
			btnKeep.html("Cancel");
			btnDiscard.html("Block");
			$(".discard_md_modal").addClass(forWhich);
		}
		if(forWhich == "dis_deleoffer"){
			disText.html("Blocking this person?");
			btnKeep.html("Cancel");
			btnDiscard.html("Block");
			$(".discard_md_modal").addClass(forWhich);
		}
		if(forWhich == "dis_hideexp"){
			disText.html("Are you sure you want to hide this experience?");
			btnKeep.html("Cancel");
			btnDiscard.html("Hide");
			$(".discard_md_modal").addClass(forWhich);
		}	
		
		if(forWhich == "dis_saveexp"){
			disText.html("Are you sure you want to save this experience?");
			btnKeep.html("Cancel");
			btnDiscard.html("Save");
			$(".discard_md_modal").addClass(forWhich);
		}	
		
		if(forWhich == "dis_notiexp"){
			disText.html("Are you sure you want to mute notification for this experience?");
			btnKeep.html("Cancel");
			btnDiscard.html("Mute");
			$(".discard_md_modal").addClass(forWhich);
		}	
		
		if(forWhich == "dis_frnexp"){
			disText.html("Are you sure you want to mute this connect experience?");
			btnKeep.html("Cancel");
			btnDiscard.html("Mute");
			$(".discard_md_modal").addClass(forWhich);
		}		
		if(forWhich == "dis_savepost"){
			disText.html("Are you sure you want to save this post?");
			btnKeep.html("Cancel");
			btnDiscard.html("Save");
			$(".discard_md_modal").addClass(forWhich);
		}	
		if(forWhich == "dis_notioff"){
			disText.html("Are you sure you want to turn off notification for this post?");
			btnKeep.html("Cancel");
			btnDiscard.html("Mute");
			$(".discard_md_modal").addClass(forWhich);
		}	
		if(forWhich == "dis_delpost"){
			disText.html("Are you sure you want to delete this post?");
			btnKeep.html("Cancel");
			btnDiscard.html("Delete");
			$(".discard_md_modal").addClass(forWhich);
		}	
		if(forWhich == "dis_deloffer"){
            disText.html("Delete this request?");
            btnKeep.html("Keep");
            btnDiscard.html("Delete");
            $(".discard_md_modal").addClass(forWhich);
        }
        if(forWhich == "dis_blockoffer"){
            disText.html("Block this person requests?");
            btnKeep.html("Keep");
            btnDiscard.html("Block");
            $(".discard_md_modal").addClass(forWhich);
        }
		if(forWhich == "dis_delmsg"){
			disText.html("Are you sure you want delete this message?");
			btnKeep.html("Cancel");
			btnDiscard.html("Delete");
			$(".discard_md_modal").addClass(forWhich);
		}
		if(forWhich == "dis_delaccount"){
			disText.html("Are you sure you want delete this account?");
			btnKeep.html("Delete");
			btnDiscard.html("Confirm");
			$(".discard_md_modal").addClass(forWhich);
		}
		if(forWhich == "dis_delgroup"){
			disText.html("Are you sure you want delete this group?");
			btnKeep.html("Cancel");
			btnDiscard.html("Delete");
			$(".discard_md_modal").addClass(forWhich);
		}
		if(forWhich == "dis_delcollection"){
			disText.html("Are you sure you want delete this collection?");
			btnKeep.html("Cancel");
			btnDiscard.html("Delete");
			$(".discard_md_modal").addClass(forWhich);
		}
		if(forWhich == "dis_delpage"){
			disText.html("Are you sure you want delete this page?");
			btnKeep.html("Cancel");
			btnDiscard.html("Delete");
			$(".discard_md_modal").addClass(forWhich);
		}
		if(forWhich == "dis_delevent"){
			disText.html("Are you sure you want delete this event?");
			btnKeep.html("Cancel");
			btnDiscard.html("Delete");
			$(".discard_md_modal").addClass(forWhich);
		}
		if(forWhich == "dis_mutepost"){
			disText.html("Are you sure you want Mute this person post?");
			btnKeep.html("Cancel");
			btnDiscard.html("Mute");
			$(".discard_md_modal").addClass(forWhich);
		}
	}
	else{
		disText.html("Discard Changes?");
		btnKeep.html("Keep");
		btnDiscard.html("Discard");
	}
	$(".discard_md_modal").modal("open");
}

function removeDisClasses(){
	
	var classes = $(".discard_md_modal").attr("class").split(' ');
	$.each(classes, function(i, c) {
		if (c.indexOf("dis_") == 0) {                    
			$(".discard_md_modal").removeClass(c);
		}
	}); 
}

function filderMapLocationModal(obj) {
    $('.pac-container').remove();
    $query = $(obj).attr('data-query');
    $isMapLocationId = $(obj).attr('id');

    if($query != undefined && $query != null && $query != '') {
        if($query == 'all') {
            currentlocation();
            $('#pac-input').val("");
            $('#compose_mapmodal').modal('open');
        } else if($query == 'none') {
            geolocate();
        } else if($query == 'MT') {
            $width = $( window ).width();
            if($width >1024) {
                geolocate();
            } else {
                currentlocation();
                $('#pac-input').val("");
                $('#compose_mapmodal').modal('open');
            }
        } else if($query == 'M') {
            $width = $( window ).width(); 
            if($width >480) {
                geolocate();
            } else {
                currentlocation();
                $('#pac-input').val("");
                $('#compose_mapmodal').modal('open');
            }
        } 
    }
}

// js to toggle active class on path color for trip botton
$('ul.color-palette li').click(function(e){
    e.preventDefault();
   $(this).addClass('active');
    $(this).siblings().each(function(){
        $(this).removeClass('active') ;
    
    });
});

function failGetLatLong($location='') {
  if($location == '' || $location == undefined || $location == null) {
    $location = '"Amman"';
  } else if($location.code == 1 && $location.message == 'User denied Geolocation') {
    $location = '"Amman"';
  }

  var geocoder =  new google.maps.Geocoder();
  geocoder.geocode( { 'address': $location }, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
        var lat = results[0].geometry.location.lat();
        var lng = results[0].geometry.location.lng();
        var exPos = {lat, lng};
        currentlocation2(exPos);
    } else {
      //alert("Something got wrong " + status);
    }
  });
}

function getLatLong(position) {
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;
    var exPos = {lat, lng};
    currentlocation2(exPos);
}

function currentlocation() {
    var place_name = "";
    if($('#'+$isMapLocationId).length) {
        var location = $('#'+$isMapLocationId).val();

        if(location == '') {
            if(navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(getLatLong, failGetLatLong);
            } else {
              failGetLatLong();
            }
        } else {
            var geocoder =  new google.maps.Geocoder();
            geocoder.geocode( { 'address': location }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var lat = results[0].geometry.location.lat();
                var lng = results[0].geometry.location.lng();
                var exPos = {lat, lng};
                currentlocation2(exPos);
            }
          });
        }
   } 
   else if($isMapLocationId == undefined || $isMapLocationId == '') {
        if(navigator.geolocation) {
            if($locationLabel != '') {
                failGetLatLong($locationLabel);
            } else {
                navigator.geolocation.getCurrentPosition(getLatLong, failGetLatLong);   
            }
        } else {
            if($locationLabel != '') {
                failGetLatLong($locationLabel);
            } else {
                failGetLatLong();
            }
        }
   }
}

function currentlocation2(pos) {
    var geocoder =  new google.maps.Geocoder();
    geocoder.geocode({'location': pos}, function(results, status) {
        if (status === 'OK') {

            map = new google.maps.Map(document.getElementById('map'), {
                center: pos,
                zoom: 9
            });

            var options = {
                types: ['(cities)'],
            };

            var markers = [];

            markers.forEach(function (marker) {
                marker.setMap(null);
            });

            markers = [];
            markers.push(new google.maps.Marker({
                map: map,
                position: pos
            }));

            var service = new google.maps.places.PlacesService(map);

 //           if (status === google.maps.places.PlacesServiceStatus.OK) {
                var div = "";
                $(".map_container").html('');
                for (var i = 0; i < results.length; i++) {

                    var address_components = results[i].address_components;
                    var country = '';

                    $.each(address_components, function (i, address_component) {
                        if (address_component.types[0] == "administrative_area_level_3"){ 
                            if(country != '') {
                                country = country + ', ' + address_component.long_name;
                            } else {
                                country = address_component.long_name;
                            }
                        }

                        if (address_component.types[0] == "locality"){
                            if(country != '') {
                                country = country + ', ' + address_component.long_name;
                            } else {
                                country = address_component.long_name;
                            }
                        }

                        if (address_component.types[0] == "country"){ 
                            if(country != '') {
                                country = country + ', ' + address_component.long_name;
                            } else {
                                country = address_component.long_name;
                            }
                        }

                        if (address_component.types[0] == "postal_code_prefix") {
                            if(country != '') {
                                country = country + ', ' + address_component.long_name;
                            } else {
                                country = address_component.long_name;
                            }
                        }
                    });
                    //place_name = results[0].name;
                    div = "<div class='map_location_container' data-id='" + i + "'>" +
                        "<div class='location_container'>" +
                        "<span class='map_icon'>" +
                        "<i class='zmdi zmdi-hc-2x zmdi-pin'></i>" +
                        "</span>" +
                        "<div class='location'>" +
                        "<p class='location_name' id='dev" + i + "' >" + results[i].formatted_address + "</p>" +
                        "<p class='place_address'>" + country + "</p>" +
                        "</div>" +
                        "</div>" +
                        "</div>";
                    
                    $(".map_container").append(div);
                }
                setTimeout(function(){
                    initNiceScroll(".nice-scroll");                     
                },400)
   //         }

            google.maps.event.addListener(markers[0], 'click', function () {
                $('#compose_mapmodal').modal('close');
                if($('#compose_mapmodal').hasClass('map_modalUniq')) {
                    var location = $('#compose_mapmodal').find("#pac-input").val();
                    if($.trim(location) == '') {
                        location = 'Amman';
                    }
                    $('#'+$isMapLocationId).focus().val(location);
                } else {
                    var location = $("#pac-input").val();
                    $('.location_content').addClass('location_parent');
                    $("#selectedlocation").html("<div class='location_div'><span class='tagged_location'></span><span class='tagged_location_name'>" + location + "</span></div><a href='javascript:void(0)'  class='removelocation'><i class='mdi mdi-close'></i></a>");
                    $("#edit_selectedlocation").html("<div class='location_div'><span class='tagged_location'></span><span class='tagged_location_name'>" + location + "</span></div><a href='javascript:void(0)'  class='removelocation'><i class='mdi mdi-close'></i></a>");
                    $("#share_selectedlocation").html("<div class='location_div'><span class='tagged_location'></span><span class='tagged_location_name'>" + place_name + "</span></div><a href='javascript:void(0)'  class='removelocation'><i class='mdi mdi-close'></i></a>");
                }
            });

        }
    });
}



