<div id="custom_dropdown_modal" class="modal tbpost_modal custom_dropdown_modal"></div>

<?php
	$file = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);
	$file = str_replace('.php','',$file);
?>
<script src="js/materialize.js" type="text/javascript" charset="utf-8" ></script>
<script src='js/wNumb.min.js'></script>
<script src="js/nouislider.js" type="text/javascript" charset="utf-8" ></script>	

<script src="js/jquery.justifiedGallery.js"></script>
<script src="js/picturefill.min.js"></script>
<script src="js/lightgallery-all.min.js"></script>
<script src="js/jquery.mousewheel.min.js"></script>

<!-- <script src="js/jquery-migrate-1.2.1.min.js"></script> -->
<!-- <script src="js/jquery.mousewheel.min.js"></script> -->
<script src="js/tooltipster.bundle.min.js" type="text/javascript" charset="utf-8"></script>
<!-- <script src="js/moment-with-locales.js" type="text/javascript" charset="utf-8"></script> -->
<!-- <script src="js/jquery.easing.1.3.js"></script> -->
<script src="js/jquery.nicescroll.min.js"></script>
<!-- <script src="js/jquery.nicescroll.plus.js"></script> -->
<!-- <script src="js/jquery.placeholder.js"></script> -->
<script src="js/pagination.js"></script>
<script src="js/emoticons.js"></script>
<script src="js/custom-emotions.js"></script>
<script src="js/emostickers.js"></script>
<script src="js/jquery.cropit.js"></script>
<script src="js/croppie.min.js"></script>
<script src="js/custom-emostickers.js"></script>
<script src="js/waterfall-light.js"></script>	
<script src="js/jquery-gauge.min.js" type="text/javascript"></script>
<!-- <script src="js/autosize.min.js"></script> -->
<script src="js/custom-functions.js"></script>
<script src="js/custom-handler.js"></script>
<script src="js/modal.js"></script>
<script src="js/address-suggestion.js"></script>
<script src="js/initGalleryImageSlider.js"></script>
<script src="js/moment.min.js"></script>
<script src="js/daterangepicker.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCwGxnRZ3TdIouvIfC6H2jEcqpPDFma5fY&libraries=places&callback=initAutocomplete"></script>

<?php if($file == "settings"){ ?>
<script src="js/setting-page.js"></script>	
<?php } ?>

<script src="js/loginsignup.js"></script>	

