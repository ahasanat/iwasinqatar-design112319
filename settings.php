<?php include("header.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div>
</div>
<div class="clear"></div>
<?php include("common/leftmenu.php"); ?>
<div class="fixed-layout ipad-mfix">
   <div class="settings-holder">
      <div class="settings-menuholder">
         <div class="sidemenu">
            <div class="side-user setting-mobile">
               <span class="img-holder"><img class="circle" src="images/demo-profile.jpg" /></span>
               <a href="wall.php"><span class="desc-holder">Nimish Parekh</span></a>
            </div>
            <a href="javascript:void(0)" class="closemenu"><i class="mdi mdi-close mdi-20px"></i>`</a>				
            <div class="settings-menu">
               <div class="settingpic-holder open">
                  <div class="setting-pic">
                     <img class="circle" src="images/demo-profile.jpg"/>
                  </div>
               </div>
               <div class="sidemenu-setting">
                  <ul id="settings-menu" class="submenu side-fbmenu set-menu-add">
                     <li class='active'>
                        <a href="javascript:void(0)" class="menu-basicinfo">Basic Information</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="menu-profilepic">Profile Photo</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="menu-communication">Communication</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="menu-security">Security Setting</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="menu-notification">Notifications</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="menu-block">Blocking</a>
                     </li>
                     <!-- <li>
                        <a href="javascript:void(0);" class="choose-theme">Choose Theme</a>
                     </li> -->
                     <li>
                        <a href="javascript:void(0)" class="menu-close">Close Account</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="menu-close1">Close Account 1</a>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <div class="main-content with-lmenu">
         <div class="settings-content-holder">
            <div id="go-top"></div>
            <div id="menu-basicinfo" class="basicinfo-content settings-content active">
               <div class="formtitle">
                  <h4>Basic Information
                     <span class="right">
                     <a href="javascript:void(0)" class="editiconCircleEffect editicon1 waves-effect waves-theme" onclick="open_edit_act_bf(this)"><i class="mdi mdi-pencil"></i></a>
                     </span>
                  </h4>
               </div>
               <ul class="settings-ul basicinfo-ul normal-part">
                  <!-- first/last name -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l2 caption-holder">
                                 <div class="caption">
                                    <label>Name</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10 detail-holder">
                                 <div class="info">
                                    <label>Nimish Parekh</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- email -->                
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l2 caption-holder">
                                 <div class="caption">
                                    <label>Email</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10 detail-holder">
                                 <div class="info">
                                    <label>nimishparekh@gmail.com</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- alternate email -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l2 caption-holder">
                                 <div class="caption">
                                    <label>Alternate Email</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10 detail-holder">
                                 <div class="info">
                                    <label>No alternate email set</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- password -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l2 caption-holder">
                                 <div class="caption">
                                    <label>Password</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10 detail-holder">
                                 <div class="info">
                                    <label>Password updated 6 week 6 day 23 hr 34 min ago</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- city -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l2 caption-holder">
                                 <div class="caption">
                                    <label>City</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10 detail-holder">
                                 <div class="info">
                                    <label>Ahmedabad, Gujarat, India</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- country -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l2 caption-holder">
                                 <div class="caption">
                                    <label>Country</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10 detail-holder">
                                 <div class="info">
                                    <label>India</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- mobile -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l2 caption-holder">
                                 <div class="caption">
                                    <label>Mobile</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10 detail-holder">
                                 <div class="info">
                                    <label>India +91 9898989898</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- birth date -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l2 caption-holder">
                                 <div class="caption">
                                    <label>Birth Date</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10 detail-holder">
                                 <div class="info">                              
                                    <label id="birth_date"> 
                                    27-03-1982
                                    </label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- gender -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l2 caption-holder">
                                 <div class="caption">
                                    <label>Gender</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10 detail-holder">
                                 <div class="info">                           
                                    <label id="birth_date"> 
                                    Male
                                    </label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- about us -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l2 caption-holder">
                                 <div class="caption">
                                    <label>About Yourself</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10 detail-holder">
                                 <div class="info">
                                    <label>Something about you...</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- language -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l2 caption-holder">
                                 <div class="caption">
                                    <label>Language</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10 detail-holder">
                                 <div class="info">
                                    <label>No language set</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- education -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l2 caption-holder">
                                 <div class="caption">
                                    <label>Education</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10 detail-holder">
                                 <div class="info">
                                    <label>No education set</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- interests -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l2 caption-holder">
                                 <div class="caption">
                                    <label>Interest</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10 detail-holder">
                                 <div class="info">
                                    <label>No interest set</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- occupation -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l2 caption-holder">
                                 <div class="caption">
                                    <label>Occupation</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10 detail-holder">
                                 <div class="info">
                                    <label>No occupation set</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
               </ul>

               <ul class="settings-ul basicinfo-ul edit-part dis-none">
                  <!-- first/last name -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l2">
                                 <div class="caption">
                                    <label>Name</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10">
                                 <div class="row">
                                    <div class="col s12 m6 l6">
                                       <div class="sliding-middle-out anim-area underlined fullwidth">
                                          <input type="text" class="title" placeholder="First Name" value="Nimish"/>
                                       </div>
                                    </div>
                                    <div class="col s12 m6 l6">
                                       <div class="sliding-middle-out anim-area underlined fullwidth">
                                          <input type="text" class="title" placeholder="Last Name" value="Parekh"/>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- email -->                
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l2">
                                 <div class="caption">
                                    <label>Email</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10">
                                 <div class="row">
                                    <div class="col s12 m12 l6">
                                       <div class="sliding-middle-out anim-area underlined fullwidth">
                                          <input type="text" class="title" placeholder="Your Email Address" value="nimishparekh@gmail.com"/>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- alternate email -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l2">
                                 <div class="caption">
                                    <label>Alternate Email</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10">
                                 <div class="row">
                                    <div class="col s12 m12 l6">
                                       <div class="sliding-middle-out anim-area underlined fullwidth">
                                          <input type="text" class="title" placeholder="Alternate Email Address"/>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- password -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l2">
                                 <div class="caption">
                                    <label>Password</label>
                                 </div>
                              </div>
                              <div class="col s12 m5 l10">
                                 <div class="row">
                                    <div class="col s12 m12 l6">
                                       <div class="form-group eyeicon">
                                          <div class="sliding-middle-out anim-area underlined fullwidth">
                                             <input type="password" class="title" placeholder="Old Password"/>
                                             <a href="javascript:void(0)" class="showPass"><i class=”mdi mdi-eye”></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col s12 m12 l6">
                                       <div class="form-group eyeicon">
                                          <div class="sliding-middle-out anim-area underlined fullwidth">
                                             <input type="password" class="title" placeholder="Password"/>
                                             <a href="javascript:void(0)" class="showPass"><i class=”mdi mdi-eye”></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col s12 m12 l6">
                                       <div class="eyeicon">
                                          <div class="sliding-middle-out anim-area underlined fullwidth">
                                             <input type="password" class="title" placeholder="Retype Password"/>
                                             <a href="javascript:void(0)" class="showPass"><i class=”mdi mdi-eye”></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- city -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l2">
                                 <div class="caption">
                                    <label>City</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10">
                                 <div class="row">
                                    <div class="col s12 m12 l6">
                                       <div class="sliding-middle-out anim-area underlined fullwidth">
                                          <input type="text" class="title" placeholder="City" value="Ahmedabad, Gujarat, India" data-query="M" onfocus="filderMapLocationModal(this)" autocomplete="off"/>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- country -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l2">
                                 <div class="caption">
                                    <label>Country</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10">
                                 <div class="row">
                                    <div class="col s12 m12 l6">
                                       <div class="sliding-middle-out anim-area underlined fullwidth">
                                          <input type="text" class="title" placeholder="Country" value="India" data-query="M" onfocus="filderMapLocationModal(this)" autocomplete="off"/>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- mobile -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l2">
                                 <div class="caption">
                                    <label>Mobile</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10">
                                 <div class="row">
                                    <div class="col s3 m4 l2 col-lg-2">
                                       <input type="text" value="" placeholder="isd code" readonly="true"/>
                                    </div>
                                    <div class="col s8 m8 l4">
                                       <div class="sliding-middle-out anim-area underlined fullwidth">
                                          <input type="text" class="title" placeholder="City" value="9898989898"/>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- about us -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l2">
                                 <div class="caption">
                                    <label>About Yourself</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10">
                                 <div class="row">
                                    <div class="col s12 m12 l12">
                                       <div class="sliding-middle-out anim-area underlined fullwidth">
                                          <textarea class="materialize-textarea mb0 md_textarea descinput" placeholder="Something about you...">jkdsn jklasdnjksan sad jnfdksfn jsafndksfn jknsdkjnsf ajksndjksnf jkndfjksnfnsdf jkdsfnjkd jkdsn jklasdnjksan sad jnfdksfn jsafndksfn jknsdkjnsf ajksndjksnf jkndfjksnfnsdf jkdsfnjkd nd hdasj jsa bdjis bjb ji bjfb sdj fsjjdfbbsdfbabf sdfbsdifbisb fisdabfisfb sdif ds</textarea>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- language -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l2">
                                 <div class="caption">
                                    <label>Language</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10 dropdown782">
                                 <div class="row">
                                    <div class="col s12 m12 l12">
                                       <div class="sliding-middle-out anim-area underlined fullwidth width500">
                                          <select id="language1" data-fill="y" data-action="language" class="languagedrp select-dropdown" data-selectore="languagedrp" multiple="multiple">  <option value="" disabled selected>Choose language</option>
                                             <?php
                                                $language = array("English" => "English", "Afar" => "Afar", "Abkhazian" => "Abkhazian", "Afrikaans" => "Afrikaans", "Amharic" => "Amharic", "Arabic" => "Arabic", "Assamese" => "Assamese", "Aymara" => "Aymara", "Azerbaijani" => "Azerbaijani", "Bashkir" => "Bashkir", "Belarusian" => "Belarusian", "Bulgarian" => "Bulgarian", "Bihari" => "Bihari", "Bislama" => "Bislama", "Bengali/Bangla" => "Bengali/Bangla", "Tibetan" => "Tibetan", "Breton" => "Breton", "Catalan" => "Catalan", "Corsican" => "Corsican", "Czech" => "Czech", "Welsh" => "Welsh", "Danish" => "Danish", "German" => "German", "Bhutani" => "Bhutani", "Greek" => "Greek", "Esperanto" => "Esperanto", "Spanish" => "Spanish", "Estonian" => "Estonian", "Basque" => "Basque", "Persian" => "Persian", "Finnish" => "Finnish", "Fiji" => "Fiji", "Faeroese" => "Faeroese", "French" => "French", "Frisian" => "Frisian", "Irish" => "Irish", "Scots/Gaelic" => "Scots/Gaelic", "Galician" => "Galician", "Guarani" => "Guarani", "Gujarati" => "Gujarati", "Hausa" => "Hausa", "Hindi" => "Hindi", "Croatian" => "Croatian", "Hungarian" => "Hungarian", "Armenian" => "Armenian", "Interlingua" => "Interlingua", "Interlingue" => "Interlingue", "Inupiak" => "Inupiak", "Indonesian" => "Indonesian", "Icelandic" => "Icelandic", "Italian" => "Italian", "Hebrew" => "Hebrew", "Japanese" => "Japanese", "Yiddish" => "Yiddish", "Javanese" => "Javanese", "Georgian" => "Georgian", "Kazakh" => "Kazakh", "Greenlandic" => "Greenlandic", "Cambodian" => "Cambodian", "Kannada" => "Kannada", "Korean" => "Korean", "Kashmiri" => "Kashmiri", "Kurdish" => "Kurdish", "Kirghiz" => "Kirghiz", "Latin" => "Latin", "Lingala" => "Lingala", "Laothian" => "Laothian", "Lithuanian" => "Lithuanian", "Latvian/Lettis" => "Latvian/Lettis", "Malagasy" => "Malagasy", "Maori" => "Maori", "Macedonian" => "Macedonian", "Malayalam" => "Malayalam", "Mongolian" => "Mongolian", "Moldavian" => "Moldavian", "Marathi" => "Marathi", "Malay" => "Malay", "Maltese" => "Maltese", "Burmese" => "Burmese", "Nauru" => "Nauru", "Nepali" => "Nepali", "Dutch" => "Dutch", "Norwegian" => "Norwegian", "Occitan" => "Occitan", "Afan" => "Afan", "Punjabi" => "Punjabi", "Polish" => "Polish", "Pashto/Pushto" => "Pashto/Pushto", "Portuguese" => "Portuguese", "Quechua" => "Quechua", "Rhaeto-Romance" => "Rhaeto-Romance", "Kirundi" => "Kirundi", "Romanian" => "Romanian", "Russian" => "Russian", "Kinyarwanda" => "Kinyarwanda", "Sanskrit" => "Sanskrit", "Sindhi" => "Sindhi", "Sangro" => "Sangro", "Serbo-Croatian" => "Serbo-Croatian", "Singhalese" => "Singhalese", "Slovak" => "Slovak", "Slovenian" => "Slovenian", "Samoan" => "Samoan", "Shona" => "Shona", "Somali" => "Somali", "Albanian" => "Albanian", "Serbian" => "Serbian", "Siswati" => "Siswati", "Sesotho" => "Sesotho", "Sundanese" => "Sundanese", "Swedish" => "Swedish", "Swahili" => "Swahili", "Tamil" => "Tamil", "Telugu" => "Telugu", "Tajik" => "Tajik", "Thai" => "Thai", "Tigrinya" => "Tigrinya", "Turkmen" => "Turkmen", "Tagalog" => "Tagalog", "Setswana" => "Setswana", "Tonga" => "Tonga", "Turkish" => "Turkish", "Tsonga" => "Tsonga", "Tatar" => "Tatar", "Twi" => "Twi", "Ukrainian" => "Ukrainian", "Urdu" => "Urdu", "Uzbek" => "Uzbek", "Vietnamese" => "Vietnamese", "Volapuk" => "Volapuk", "Wolof" => "Wolof", "Xhosa" => "Xhosa", "Yoruba" => "Yoruba", "Chinese" => "Chinese", "Zulu" => "Zulu");
                                                foreach ($language as $s9032n) {
                                                   echo "<option value=".$s9032n.">$s9032n</option>";
                                                }
                                             ?>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- education -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l2">
                                 <div class="caption">
                                    <label>Education</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10 dropdown782">
                                 <div class="row">
                                    <div class="col s12 m12 l12">
                                       <div class="sliding-middle-out anim-area underlined fullwidth width500">
                                          <select id="education1" data-fill="y" data-action="education" data-selectore="educationdrp" class="educationdrp select-dropdown" multiple="multiple">
                                             <option value="" disabled selected>Choose education</option>
                                             <?php
                                                $education = array("Aerospace Studies" => "Aerospace Studies", "African American Studies" => "African American Studies", "Agricultural and Resource Economics" => "Agricultural and Resource Economics", "American Studies" => "American Studies", "Ancient History and Mediterranean Archaeology" => "Ancient History and Mediterranean Archaeology", "Anthropology" => "Anthropology", "Applied Science and Technology" => "Applied Science and Technology", "Arabic" => "Arabic", "Architecture" => "Architecture", "Armenian" => "Armenian", "Art" => "Art", "Asian American Studies" => "Asian American Studies", "Asian Studies" => "Asian Studies", "Astronomy" => "Astronomy", "Bengali" => "Bengali", "Bibliography" => "Bibliography", "Bioengineering" => "Bioengineering", "Biology" => "Biology", "Biophysics" => "Biophysics", "Bosnian, Croatian, Serbian" => "Bosnian, Croatian, Serbian", "Buddhist Studies" => "Buddhist Studies", "Bulgarian" => "Bulgarian", "Burmese" => "Burmese", "Bba" => "Bba", "Catalan" => "Catalan", "Celtic Studies" => "Celtic Studies", "Chemical Engineering" => "Chemical Engineering", "Chemistry" => "Chemistry", "Chicano Studies" => "Chicano Studies", "Chinese" => "Chinese", "City and Regional Planning" => "City and Regional Planning", "Civil and Environmental Engineering" => "Civil and Environmental Engineering", "Classics" => "Classics", "Cognitive Science" => "Cognitive Science", "College Writing Programs" => "College Writing Programs", "Comparative Biochemistry" => "Comparative Biochemistry", "Comparative Literature" => "Comparative Literature", "Computational Biology" => "Computational Biology", "Computer Science" => "Computer Science", "Creative Writing" => "Creative Writing", "Critical Theory Graduate Group" => "Critical Theory Graduate Group", "Cuneiform" => "Cuneiform", "Czech" => "Czech", "Danish" => "Danish", "Data Science" => "Data Science", "Demography" => "Demography", "Design Innovation" => "Design Innovation", "Development Engineering" => "Development Engineering", "Development Practice" => "Development Practice", "Development Studies" => "Development Studies", "Dutch" => "Dutch", "Earth and Planetary Science" => "Earth and Planetary Science", "East Asian Languages and Cultures" => "East Asian Languages and Cultures", "Economics" => "Economics", "Egyptian" => "Egyptian", "Electrical Engineering and Computer Sciences" => "Electrical Engineering and Computer Sciences", "Electrical Engineering" => "Electrical Engineering", "Energy and Resources Group" => "Energy and Resources Group", "Engineering" => "Engineering", "Environmental Design" => "Environmental Design", "Environmental Economics and Policy" => "Environmental Economics and Policy", "Environmental Science, Policy, and Management" => "Environmental Science, Policy, and Management", "Environmental Sciences" => "Environmental Sciences", "Ethnic Studies" => "Ethnic Studies", "Ethnic Studies Graduate Group" => "Ethnic Studies Graduate Group", "European Studies" => "European Studies", "Filipino" => "Filipino", "Film and Media" => "Film and Media", "Financial Engineering" => "Financial Engineering", "Finnish" => "Finnish", "Folklore" => "Folklore", "French" => "French", "Geography" => "Geography", "German" => "German", "Global Metropolitan Studies" => "Global Metropolitan Studies", "Global Poverty and Practice" => "Global Poverty and Practice", "Global Studies" => "Global Studies", "Health and Medical Sciences" => "Health and Medical Sciences", "Hebrew" => "Hebrew", "Hindi-Urdu" => "Hindi-Urdu", "History" => "History", "Hungarian" => "Hungarian", "Icelandic" => "Icelandic", "Indigenous Languages of Americas" => "Indigenous Languages of Americas", "Industrial Engineering and Operations Research" => "Industrial Engineering and Operations Research", "Information" => "Information", "Integrative Biology" => "Integrative Biology", "Interdisciplinary Studies Field Major" => "Interdisciplinary Studies Field Major", "International and Area Studies" => "International and Area Studies", "Iranian" => "Iranian", "Italian Studies" => "Italian Studies", "Japanese" => "Japanese", "Jewish Studies" => "Jewish Studies", "Journalism" => "Journalism", "Khmer" => "Khmer", "Korean" => "Korean", "Landscape Architecture" => "Landscape Architecture", "Language Proficiency Program" => "Language Proficiency Program", "Latin American Studies" => "Latin American Studies", "Latin" => "Latin", "Legal Studies" => "Legal Studies", "Lesbian Gay Bisexual Transgender Studies" => "Lesbian Gay Bisexual Transgender Studies", "Letters and Science" => "Letters and Science", "Library and Information Studies" => "Library and Information Studies", "Linguistics" => "Linguistics", "Malay/Indonesian" => "Malay/Indonesian", "Materials Science and Engineering" => "Materials Science and Engineering", "Mathematics" => "Mathematics", "Mechanical Engineering" => "Mechanical Engineering", "Media Studies" => "Media Studies", "Medieval Studies" => "Medieval Studies", "Middle Eastern Studies" => "Middle Eastern Studies", "Military Affairs" => "Military Affairs", "Military Science" => "Military Science", "Molecular and Cell Biology" => "Molecular and Cell Biology", "Mongolian" => "Mongolian", "Music" => "Music", "Nanoscale Science and Engineering" => "Nanoscale Science and Engineering", "Native American Studies" => "Native American Studies", "Natural Resources" => "Natural Resources", "Naval Science" => "Naval Science", "Near Eastern Studies" => "Near Eastern Studies", "Neuroscience" => "Neuroscience", "New Media" => "New Media", "Norwegian" => "Norwegian", "Nuclear Engineering" => "Nuclear Engineering", "Nutritional Sciences and Toxicology" => "Nutritional Sciences and Toxicology", "Optometry" => "Optometry", "Peace and Conflict Studies" => "Peace and Conflict Studies", "Persian" => "Persian", "Philosophy" => "Philosophy", "Physical Education" => "Physical Education", "Physics" => "Physics", "Plant and Microbial Biology" => "Plant and Microbial Biology", "Polish" => "Polish", "Political Economy" => "Political Economy", "Political Science" => "Political Science", "Portuguese" => "Portuguese", "Psychology" => "Psychology", "Public Affairs" => "Public Affairs", "Public Health" => "Public Health", "Public Policy" => "Public Policy", "Punjabi" => "Punjabi", "Religious Studies" => "Religious Studies", "Rhetoric" => "Rhetoric", "Romanian" => "Romanian", "Russian" => "Russian", "Sanskrit" => "Sanskrit", "Scandinavian" => "Scandinavian", "Science and Mathematics Education" => "Science and Mathematics Education", "Science and Technology Studies" => "Science and Technology Studies", "Semitics" => "Semitics", "Slavic Languages and Literatures" => "Slavic Languages and Literatures", "Social Welfare" => "Social Welfare", "Sociology" => "Sociology", "South and Southeast Asian Studies" => "South and Southeast Asian Studies", "South Asian" => "South Asian", "Southeast Asian" => "Southeast Asian", "Spanish" => "Spanish", "Special Education" => "Special Education", "Statistics" => "Statistics", "Swedish" => "Swedish", "Tamil" => "Tamil", "Telugu" => "Telugu", "Thai" => "Thai", "Theater, Dance, and Performance Studies" => "Theater, Dance, and Performance Studies", "Tibetan" => "Tibetan", "Turkish" => "Turkish", "Undergraduate Interdisciplinary Studies" => "Undergraduate Interdisciplinary Studies", "Vietnamese" => "Vietnamese", "Vision Science" => "Vision Science", "Visual Studies" => "Visual Studies", "Yiddish" => "Yiddish", "Aerospace" => "Aerospace", "African" => "African", "Visual" => "Visual", "Vision" => "Vision");
                                                foreach ($education as $s9032n) {
                                                   echo "<option value=".$s9032n.">$s9032n</option>";
                                                }
                                             ?>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- interests -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l2">
                                 <div class="caption">
                                    <label>Interest</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10 dropdown782">
                                 <div class="row">
                                    <div class="col s12 m12 l12">
                                       <div class="sliding-middle-out anim-area underlined fullwidth  width500">
                                          <select data-fill="y" data-action="interest" id="interests1" data-selectore="interestdrp" class="interestdrp select-dropdown" multiple="multiple" size="4">
                                             <option value="" disabled selected>Choose interest</option>
                                             <?php
                                                $interest = array("Aircraft spotting" => "Aircraft spotting", "Amateur astronomy" => "Amateur astronomy", "Astrology" => "Astrology", "Birdwatching" => "Birdwatching", "Bus spotting" => "Bus spotting", "Geocaching" => "Geocaching", "Gongoozling" => "Gongoozling", "Herping" => "Herping", "Meteorology" => "Meteorology", "Photography" => "Photography", "Trainspotting" => "Trainspotting", "Traveling" => "Traveling", "Hiking/backpacking" => "Hiking/backpacking", "Whale watching" => "Whale watching", "Satellite watching" => "Satellite watching", "Fishkeeping" => "Fishkeeping", "Learning" => "Learning", "Microscopy" => "Microscopy", "Reading" => "Reading", "Shortwave listening" => "Shortwave listening", "Videophilia" => "Videophilia", "Airsoft" => "Airsoft", "American football" => "American football", "Archery" => "Archery", "Association football" => "Association football", "Australian rules football" => "Australian rules football", "Auto racing" => "Auto racing", "Baseball" => "Baseball", "Beach Volleyball" => "Beach Volleyball", "Breakdancing" => "Breakdancing", "Climbing" => "Climbing", "Cricket" => "Cricket", "Cycling" => "Cycling", "Disc golf" => "Disc golf", "Dog sport" => "Dog sport", "Equestrianism" => "Equestrianism", "Exhibition drill" => "Exhibition drill", "Field hockey" => "Field hockey", "Figure skating" => "Figure skating", "Fishing" => "Fishing", "Footbag" => "Footbag", "Golfing" => "Golfing", "Handball" => "Handball", "Ice hockey" => "Ice hockey", "Judo" => "Judo", "Jukskei" => "Jukskei", "Kart racing" => "Kart racing", "Knife throwing" => "Knife throwing", "Lacrosse" => "Lacrosse", "Model aircraft" => "Model aircraft", "Marching Band" => "Marching Band", "Racquetball" => "Racquetball", "Radio-controlled car racing" => "Radio-controlled car racing", "Roller derby" => "Roller derby", "Rugby league football" => "Rugby league football", "Sculling or Rowing" => "Sculling or Rowing", "Shooting sport" => "Shooting sport", "Skateboarding" => "Skateboarding", "Speed skating" => "Speed skating", "Squash" => "Squash", "Surfing" => "Surfing", "Swimming" => "Swimming", "Table tennis" => "Table tennis", "Tennis" => "Tennis", "Tour skating" => "Tour skating", "Triathlon" => "Triathlon", "Ultimate Frisbee" => "Ultimate Frisbee", "Water Polo" => "Water Polo", "Volleyball" => "Volleyball", "Horseback riding" => "Horseback riding", "Animal fancy" => "Animal fancy", "Badminton" => "Badminton", "Baton Twirling" => "Baton Twirling", "Billiards" => "Billiards", "Bowling" => "Bowling", "Boxing" => "Boxing", "Bridge" => "Bridge", "Cheerleading" => "Cheerleading", "Chess" => "Chess", "Color guard" => "Color guard", "Curling" => "Curling", "Dancing" => "Dancing", "Darts" => "Darts", "Debate" => "Debate", "Fencing" => "Fencing", "Go" => "Go", "Gymnastics" => "Gymnastics", "Ice skating" => "Ice skating", "Kabaddi" => "Kabaddi", "Laser tag" => "Laser tag", "Marbles" => "Marbles", "Martial arts" => "Martial arts", "Mahjong" => "Mahjong", "Poker" => "Poker", "Slot car racing" => "Slot car racing", "Speedcubing" => "Speedcubing", "Sport stacking" => "Sport stacking", "Table football" => "Table football", "Weightlifting" => "Weightlifting", "Action figure" => "Action figure", "Antiquing" => "Antiquing", "Art collecting" => "Art collecting", "Book collecting" => "Book collecting", "Card collecting" => "Card collecting", "Coin collecting" => "Coin collecting", "Comic book collecting" => "Comic book collecting", "Deltiology" => "Deltiology", "Die-cast toy" => "Die-cast toy", "Element collecting" => "Element collecting", "Movie and movie memorabilia collecting" => "Movie and movie memorabilia collecting", "Record collecting" => "Record collecting", "Stamp collecting" => "Stamp collecting", "Video game collecting" => "Video game collecting", "Vintage cars" => "Vintage cars", "Antiquities" => "Antiquities", "Auto audiophilia" => "Auto audiophilia", "Flower collecting and pressing" => "Flower collecting and pressing", "Fossil hunting" => "Fossil hunting", "Insect collecting" => "Insect collecting", "Magnet fishing" => "Magnet fishing", "Metal detecting" => "Metal detecting", "Stone collecting" => "Stone collecting", "Mineral collecting" => "Mineral collecting", "Rock balancing" => "Rock balancing", "Sea glass collecting" => "Sea glass collecting", "Seashell collecting" => "Seashell collecting", "Air sports" => "Air sports", "Astronomy" => "Astronomy", "BASE jumping" => "BASE jumping", "Basketball" => "Basketball", "Beekeeping" => "Beekeeping", "Bird watching" => "Bird watching", "Blacksmithing" => "Blacksmithing", "Board sports" => "Board sports", "Bodybuilding" => "Bodybuilding", "Brazilian jiu-jitsu" => "Brazilian jiu-jitsu", "Camping" => "Camping", "Canyoning" => "Canyoning", "Dowsing" => "Dowsing", "Driving" => "Driving", "Flag football" => "Flag football", "Flying" => "Flying", "Flying disc" => "Flying disc", "Foraging" => "Foraging", "Freestyle football" => "Freestyle football", "Gardening" => "Gardening", "Ghost hunting" => "Ghost hunting", "Graffiti" => "Graffiti", "High-power rocketry" => "High-power rocketry", "Hiking" => "Hiking", "Hooping" => "Hooping", "Hunting" => "Hunting", "Inline skating" => "Inline skating", "Jogging" => "Jogging", "Kayaking" => "Kayaking", "Kite flying" => "Kite flying", "Kitesurfing" => "Kitesurfing", "LARPing" => "LARPing", "Letterboxing" => "Letterboxing", "Motor sports" => "Motor sports", "Mountain biking" => "Mountain biking", "Mountaineering" => "Mountaineering", "Mushroom hunting/Mycology" => "Mushroom hunting/Mycology", "Netball" => "Netball", "Nordic skating" => "Nordic skating", "Orienteering" => "Orienteering", "Paintball" => "Paintball", "Parkour" => "Parkour", "Polo" => "Polo", "Powerlifting" => "Powerlifting", "Rafting" => "Rafting", "Rappelling" => "Rappelling", "Road biking" => "Road biking", "Rock climbing" => "Rock climbing", "Roller skating" => "Roller skating", "Rugby" => "Rugby", "Running" => "Running", "Sailing" => "Sailing", "Sand art" => "Sand art", "Scouting" => "Scouting", "Scuba diving" => "Scuba diving", "Topiary" => "Topiary", "Shooting" => "Shooting", "Shopping" => "Shopping", "Skiing" => "Skiing", "Skimboarding" => "Skimboarding", "Skydiving" => "Skydiving", "Slacklining" => "Slacklining", "Snowboarding" => "Snowboarding", "Stone skipping" => "Stone skipping", "Travel" => "Travel", "Taekwondo" => "Taekwondo", "Tai chi" => "Tai chi", "Urban exploration" => "Urban exploration", "Vacation" => "Vacation", "Vehicle restoration" => "Vehicle restoration", "Walking" => "Walking", "Water sports" => "Water sports", "3D printing" => "3D printing", "Acting" => "Acting", "Amateur radio" => "Amateur radio", "Baton twirling" => "Baton twirling", "Board/Tabletop games" => "Board/Tabletop games", "Book restoration" => "Book restoration", "Cabaret" => "Cabaret", "Calligraphy" => "Calligraphy", "Candle making" => "Candle making", "Coffee roasting" => "Coffee roasting", "Coloring" => "Coloring", "Computer programming" => "Computer programming", "Cooking" => "Cooking", "Cosplaying" => "Cosplaying", "Couponing" => "Couponing", "Creative writing" => "Creative writing", "Crocheting" => "Crocheting", "Cross-stitch" => "Cross-stitch", "Crossword puzzles" => "Crossword puzzles", "Cryptography" => "Cryptography", "Dance" => "Dance", "Digital arts" => "Digital arts", "Do it yourself" => "Do it yourself", "Drama" => "Drama", "Drawing" => "Drawing", "Electronics" => "Electronics", "Embroidery" => "Embroidery", "Fantasy Sports" => "Fantasy Sports", "Fashion" => "Fashion", "Flower arranging" => "Flower arranging", "Foreign interests learning" => "Foreign interests learning", "Genealogy" => "Genealogy", "Glassblowing" => "Glassblowing", "Gunsmithing" => "Gunsmithing", "Herp keeping" => "Herp keeping", "Homebrewing" => "Homebrewing", "Hydroponics" => "Hydroponics", "Jewelry making" => "Jewelry making", "Jigsaw puzzles" => "Jigsaw puzzles", "Juggling" => "Juggling", "Knapping" => "Knapping", "Knife making" => "Knife making", "Knitting" => "Knitting", "Kombucha Brewing" => "Kombucha Brewing", "Lacemaking" => "Lacemaking", "Lapidary" => "Lapidary", "Leather crafting" => "Leather crafting", "Lego building" => "Lego building", "Listening to music" => "Listening to music", "Machining" => "Machining", "Macrame" => "Macrame", "Magic" => "Magic", "Metalworking" => "Metalworking", "Model building" => "Model building", "Origami" => "Origami", "Painting" => "Painting", "Pet" => "Pet", "Philately" => "Philately", "Plastic embedding" => "Plastic embedding", "Playing musical instruments" => "Playing musical instruments", "Poi" => "Poi", "Pottery" => "Pottery", "Puzzles" => "Puzzles", "Quilting" => "Quilting", "Scrapbooking" => "Scrapbooking", "Sculpting" => "Sculpting", "Sewing" => "Sewing", "Singing" => "Singing", "Sketching" => "Sketching", "Soapmaking" => "Soapmaking", "Stand-up comedy" => "Stand-up comedy", "Tatting" => "Tatting", "Taxidermy" => "Taxidermy", "Video gaming" => "Video gaming", "Watching movies" => "Watching movies", "Watching television" => "Watching television", "Web surfing" => "Web surfing", "Whittling" => "Whittling", "Wood carving" => "Wood carving", "Woodworking" => "Woodworking", "Worldbuilding" => "Worldbuilding", "Writing" => "Writing", "Yo-yoing" => "Yo-yoing", "Yoga" => "Yoga", "Aircraft" => "Aircraft", "Amateur" => "Amateur", "Bus" => "Bus");
                                                foreach ($interest as $s9032n) {
                                                   echo "<option value=".$s9032n.">$s9032n</option>";
                                                }
                                             ?>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- occupation -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l2">
                                 <div class="caption">
                                    <label>Occupation</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l10 dropdown782">
                                 <div class="row">
                                    <div class="col s12 m12 l12">
                                       <div class="sliding-middle-out anim-area underlined fullwidth width500">
                                          <select id="occupations1" data-fill="y" data-action="occupation" data-selectore="occupationdrp" class="occupationdrp select-dropdown" multiple="multiple" size="4">
                                             <option value="" disabled selected>Choose occupation</option>
                                             <?php
                                                $occupation = array("Actor" => "Actor", "Actuary" => "Actuary", "Administrative worker" => "Administrative worker", "Advertising manager" => "Advertising manager", "Aerial rigger" => "Aerial rigger", "Agriculturaladviser" => "Agriculturaladviser", "Agriculturalmechanic" => "Agriculturalmechanic", "Agronomist" => "Agronomist", "Aircontroller" => "Aircontroller", "Airtechnician" => "Airtechnician", "Aircrafttechnician" => "Aircrafttechnician", "Aircraft mechanic" => "Aircraft mechanic", "Airlineagent" => "Airlineagent", "Ammunitionworker" => "Ammunitionworker", "Animal technician" => "Animal technician", "Animator" => "Animator", "Anthropologist" => "Anthropologist", "Applicationsmanager" => "Applicationsmanager", "Apprentice" => "Apprentice", "Archeologist" => "Archeologist", "Architect" => "Architect", "Architecturalofficer" => "Architecturalofficer", "Arthistorian" => "Arthistorian", "Artglazier" => "Artglazier", "Artlocksmith" => "Artlocksmith", "Art photographer" => "Art photographer", "Art restorer" => "Art restorer", "Articledassistant" => "Articledassistant", "Artificialmaker" => "Artificialmaker", "Artisticmanager" => "Artisticmanager", "Assessorassessor" => "Assessorassessor", "Assistanthelp" => "Assistanthelp", "Assistantworker" => "Assistantworker", "Astrologer" => "Astrologer", "Astronomer" => "Astronomer", "Athlete" => "Athlete", "Auctioneer" => "Auctioneer", "Audiodesigner" => "Audiodesigner", "Auditor" => "Auditor", "Auto-electrician" => "Auto-electrician", "Auxiliaryfiller" => "Auxiliaryfiller", "Auxiliaryworker" => "Auxiliaryworker", "Auxiliaryclothing" => "Auxiliaryclothing", "Auxiliarywoodcutting" => "Auxiliarywoodcutting", "Auxiliarydistribution" => "Auxiliarydistribution", "Auxiliaryworkerproduction" => "Auxiliaryworkerproduction", "Baker" => "Baker", "Bankservice" => "Bankservice", "Bankcredit" => "Bankcredit", "Banking expert" => "Banking expert", "Barber" => "Barber", "Barman/barmaid" => "Barman/barmaid", "Basket-makerweaver" => "Basket-makerweaver", "Beauticiantherapist" => "Beauticiantherapist", "Beekeeper" => "Beekeeper", "Bibliographer" => "Bibliographer", "Biochemist" => "Biochemist", "Biologist" => "Biologist", "Biotechnologist" => "Biotechnologist", "Biscuit maker" => "Biscuit maker", "Blacksmith" => "Blacksmith", "Blastersexpert" => "Blastersexpert", "Blast-furnaceman/woman" => "Blast-furnaceman/woman", "Blastingengineer" => "Blastingengineer", "Boatman/woman" => "Boatman/woman", "Boilerattendant" => "Boilerattendant", "Boilermaker/fitter" => "Boilermaker/fitter", "Bookbinder" => "Bookbinder", "Bookkeeper" => "Bookkeeper", "Bookmaker" => "Bookmaker", "Botanist" => "Botanist", "Brewermaltster" => "Brewermaltster", "Bricklayer" => "Bricklayer", "Broadcaster/announcertelecaster" => "Broadcaster/announcertelecaster", "Brush-makermaker" => "Brush-makermaker", "Builders industry" => "Builders industry", "Buildingmechanic" => "Buildingmechanic", "Building electrician" => "Building electrician", "Buildinglocksmith" => "Buildinglocksmith", "Building inspector" => "Building inspector", "Buildingoperator" => "Buildingoperator", "Buildingoperative" => "Buildingoperative", "Building tinsmith" => "Building tinsmith", "Building/civiltechnician/technologist" => "Building/civiltechnician/technologist", "Butchersausage-maker" => "Butchersausage-maker", "Butler" => "Butler", "Button maker" => "Button maker", "Cab/taxi dispatcher" => "Cab/taxi dispatcher", "Cabinet maker" => "Cabinet maker", "Cabledriver" => "Cabledriver", "Cablemaker" => "Cablemaker", "Camera mechanic" => "Camera mechanic", "Cameracrew" => "Cameracrew", "Canningoperator" => "Canningoperator", "Capitalclerk/officer" => "Capitalclerk/officer", "Captainaircraft" => "Captainaircraft", "Car mechanic" => "Car mechanic", "Car" => "Car", "Careorganiser" => "Careorganiser", "Careerdiplomat" => "Careerdiplomat", "Careeradviser" => "Careeradviser", "Caretaker" => "Caretaker", "Carpenter" => "Carpenter", "Cartographer" => "Cartographer", "Cellulose operator" => "Cellulose operator", "Ceramicmaker" => "Ceramicmaker", "Ceramic" => "Ceramic", "Ceramicist" => "Ceramicist", "Ceramics/pottery maker" => "Ceramics/pottery maker", "Charterbroker" => "Charterbroker", "Cheese maker" => "Cheese maker", "Chemicaloperator" => "Chemicaloperator", "Chemicalmanager" => "Chemicalmanager", "Chemicalassistant" => "Chemicalassistant", "Chemicalproduction" => "Chemicalproduction", "Chemicalindustries" => "Chemicalindustries", "Chemical researcher" => "Chemical researcher", "Chemical technologist" => "Chemical technologist", "Chief/senior" => "Chief/senior", "Chimney sweep" => "Chimney sweep", "Chipboard/fibreboardoperative" => "Chipboard/fibreboardoperative", "Choir master/mistress" => "Choir master/mistress", "Choreographer" => "Choreographer", "Circusperformer" => "Circusperformer", "Cleaner" => "Cleaner", "Clerksystems" => "Clerksystems", "Cloakroom attendant" => "Cloakroom attendant", "Coffee roaster" => "Coffee roaster", "Coffeehouseowner" => "Coffeehouseowner", "Commentator,reporter" => "Commentator,reporter", "Commercial lawyer" => "Commercial lawyer", "Company lawyer" => "Company lawyer", "Composer" => "Composer", "Computerengineer" => "Computerengineer", "Computeroperator" => "Computeroperator", "Computermanager" => "Computermanager", "Computerprogrammer" => "Computerprogrammer", "Concrete" => "Concrete", "Conductor" => "Conductor", "Confectionermaker" => "Confectionermaker", "Conservatorgallery" => "Conservatorgallery", "Construction carpenter/joiner" => "Construction carpenter/joiner", "Construction/buildingmanager" => "Construction/buildingmanager", "Cook" => "Cook", "Corrosionfitter" => "Corrosionfitter", "Court" => "Court", "Craft ceramicist" => "Craft ceramicist", "Craft gilder" => "Craft gilder", "Craftetcher" => "Craftetcher", "Craft glassmaker" => "Craft glassmaker", "Craftchaser" => "Craftchaser", "Craftbrazier" => "Craftbrazier", "Craftmaker" => "Craftmaker", "Craft plasterer" => "Craft plasterer", "Craft stonemason" => "Craft stonemason", "Craft upholsterer" => "Craft upholsterer", "Craneoperator" => "Craneoperator", "Crate maker/cooper" => "Crate maker/cooper", "Criminal investigator" => "Criminal investigator", "Cropoperative" => "Cropoperative", "Croupier" => "Croupier", "Customsinspector" => "Customsinspector", "Cutler" => "Cutler", "Dairyoperator" => "Dairyoperator", "Dance teacher/trainer" => "Dance teacher/trainer", "Dancer" => "Dancer", "Datatechnician" => "Datatechnician", "Debt collector" => "Debt collector", "Decorator-paperhangerdecorator" => "Decorator-paperhangerdecorator", "Dentalnurse" => "Dentalnurse", "Dental technician" => "Dental technician", "Dentistsurgeon" => "Dentistsurgeon", "Developingassistant" => "Developingassistant", "Dietician" => "Dietician", "Digger" => "Digger", "Director" => "Director", "Disc jockey" => "Disc jockey", "Dish washer" => "Dish washer", "Dispatch clerk" => "Dispatch clerk", "Dispatcherquarries" => "Dispatcherquarries", "Dispatcher,power" => "Dispatcher,power", "Diver" => "Diver", "Dog trainer" => "Dog trainer", "Doorkeeper" => "Doorkeeper", "Draughtsperson" => "Draughtsperson", "Dresser" => "Dresser", "Driller" => "Driller", "Drilling" => "Drilling", "Driverdriver" => "Driverdriver", "Driversassistant" => "Driversassistant", "Driving instructor" => "Driving instructor", "Dusttechnician" => "Dusttechnician", "Ecologist" => "Ecologist", "Economistaccountant" => "Economistaccountant", "Editoreditor" => "Editoreditor", "Educationalspecialist" => "Educationalspecialist", "Electricalengineer" => "Electricalengineer", "Electricalappliances" => "Electricalappliances", "Electricalinspector" => "Electricalinspector", "Electricalmechanic" => "Electricalmechanic", "Electrician" => "Electrician", "Electroceramicoperative" => "Electroceramicoperative", "Electronictechnician" => "Electronictechnician", "Electroplating" => "Electroplating", "Employmentofficer" => "Employmentofficer", "Enamel worker" => "Enamel worker", "Engineeringfitter" => "Engineeringfitter", "Engineeringtechnologist" => "Engineeringtechnologist", "Entertainment officer" => "Entertainment officer", "Environmentalinspector" => "Environmentalinspector", "Ergonomist" => "Ergonomist", "Ethnographer" => "Ethnographer", "Exhibitionsmanager" => "Exhibitionsmanager", "Faith healer" => "Faith healer", "Farmlabourer" => "Farmlabourer", "Farmer" => "Farmer", "Fashion designer" => "Fashion designer", "Feedoperator" => "Feedoperator", "Film critic" => "Film critic", "Filmdesigner" => "Filmdesigner", "Filmeditor" => "Filmeditor", "Film projectionist" => "Film projectionist", "Financial analyst" => "Financial analyst", "Financial officer" => "Financial officer", "Fine artist" => "Fine artist", "Fire officer" => "Fire officer", "Fire" => "Fire", "Fireinspector" => "Fireinspector", "Fish farmer" => "Fish farmer", "Fishkeeper/bailiff" => "Fishkeeper/bailiff", "Fisherman" => "Fisherman", "Fitter" => "Fitter", "Fitterinstruments" => "Fitterinstruments", "Fitterfitter" => "Fitterfitter", "Fitterstructures" => "Fitterstructures", "Flightstaff" => "Flightstaff", "Flight engineer" => "Flight engineer", "Floorlayer" => "Floorlayer", "Flower,grower" => "Flower,grower", "Flying instructor" => "Flying instructor", "Foodmanager" => "Foodmanager", "Foodtechnologist" => "Foodtechnologist", "Foreignclerk" => "Foreignclerk", "Forestermanager" => "Forestermanager", "Forester/forest manager" => "Forester/forest manager", "Forestryoperator" => "Forestryoperator", "Forestry worker" => "Forestry worker", "Fortune teller" => "Fortune teller", "Foster parent" => "Foster parent", "Foundry/patternmaker" => "Foundry/patternmaker", "Fringe/trimmings maker" => "Fringe/trimmings maker", "Fruitgrower" => "Fruitgrower", "Funeralworker" => "Funeralworker", "Fursewer" => "Fursewer", "Furnace operator" => "Furnace operator", "Furrier" => "Furrier", "Gardenergardener" => "Gardenergardener", "Gasinspector" => "Gasinspector", "Generalindustry" => "Generalindustry", "Generalproduction" => "Generalproduction", "Generalprocessing" => "Generalprocessing", "Geneticist" => "Geneticist", "Geographer" => "Geographer", "Geologicalmechanic" => "Geologicalmechanic", "Geologist" => "Geologist", "Geomechanic" => "Geomechanic", "Geophysicist" => "Geophysicist", "Glassengraver" => "Glassengraver", "Glassmaker" => "Glassmaker", "Glassoperator" => "Glassoperator", "Glass melter" => "Glass melter", "Glass painter" => "Glass painter", "Glassindustry" => "Glassindustry", "Glasscuttergrinder" => "Glasscuttergrinder", "Glassworkerworker" => "Glassworkerworker", "Glazier" => "Glazier", "Goldsmith" => "Goldsmith", "Governmentofficial" => "Governmentofficial", "Graphicdesigner" => "Graphicdesigner", "Gravedigger" => "Gravedigger", "Guide" => "Guide", "Gunsmith" => "Gunsmith", "Hand embroiderer" => "Hand embroiderer", "Hand lacemaker" => "Hand lacemaker", "Harbour guard" => "Harbour guard", "Hardener" => "Hardener", "Harpooner" => "Harpooner", "Hattermaker" => "Hattermaker", "Heatingfitter" => "Heatingfitter", "Heatingengineer" => "Heatingengineer", "Herbalist" => "Herbalist", "High-rise" => "High-rise", "Historian" => "Historian", "Historicalbuilding" => "Historicalbuilding", "Horsetrainer" => "Horsetrainer", "Host" => "Host", "Hotel porter" => "Hotel porter", "Hotel" => "Hotel", "Hydrologist" => "Hydrologist", "Ice-cream maker" => "Ice-cream maker", "Image consultant" => "Image consultant", "Industrialindustry" => "Industrialindustry", "Informationtransport" => "Informationtransport", "Information assistant/receptionist" => "Information assistant/receptionist", "Inspectortelecommunications" => "Inspectortelecommunications", "Insulation" => "Insulation", "Insurance clerk" => "Insurance clerk", "Insuranceunderwriter" => "Insuranceunderwriter", "Interiordesigner" => "Interiordesigner", "Interpretertranslator" => "Interpretertranslator", "Investment clerk" => "Investment clerk", "Invoice clerk" => "Invoice clerk", "Jewellerdesigner" => "Jewellerdesigner", "Jewellery maker" => "Jewellery maker", "Joiner" => "Joiner", "Judge" => "Judge", "Keeper" => "Keeper", "Keeperanimals" => "Keeperanimals", "Knitter" => "Knitter", "Land surveyor" => "Land surveyor", "Landscape architect" => "Landscape architect", "Laundry" => "Laundry", "Laundryworker" => "Laundryworker", "Lecturereducation" => "Lecturereducation", "Lecturercourses" => "Lecturercourses", "Lecturer/researcherlinguistics" => "Lecturer/researcherlinguistics", "Librarian" => "Librarian", "Lifeguardinstructor" => "Lifeguardinstructor", "Lift attendant" => "Lift attendant", "Lift fitter" => "Lift fitter", "Lighting technician" => "Lighting technician", "Lightningconductors" => "Lightningconductors", "Lithographer" => "Lithographer", "Livestock farmer" => "Livestock farmer", "Lotteryvendor" => "Lotteryvendor", "Machineworks" => "Machineworks", "Machinery inspector" => "Machinery inspector", "Makertextiles" => "Makertextiles", "Make-upwigmaker" => "Make-upwigmaker", "Management accountant" => "Management accountant", "Management consultant" => "Management consultant", "Manager" => "Manager", "Manager/supervisormanager" => "Manager/supervisormanager", "Marineoperator" => "Marineoperator", "Marinemanager" => "Marinemanager", "Marketing manager" => "Marketing manager", "Masseur/masseuse" => "Masseur/masseuse", "Masterceremonies" => "Masterceremonies", "Materials handler" => "Materials handler", "Mathematician" => "Mathematician", "Medicaltechnician" => "Medicaltechnician", "Mechanic" => "Mechanic", "Mechanicmechanic" => "Mechanicmechanic", "Mechanicaldesigner" => "Mechanicaldesigner", "Mechanicalmanager" => "Mechanicalmanager", "Mechanicaltechnologist" => "Mechanicaltechnologist", "Mechatronic engineer" => "Mechatronic engineer", "Metal engraver" => "Metal engraver", "Metal grinder" => "Metal grinder", "Metal refiner" => "Metal refiner", "Metal turner" => "Metal turner", "Metal" => "Metal", "Metallurgisttechnician" => "Metallurgisttechnician", "Metallurgistmetals" => "Metallurgistmetals", "Meteorologist" => "Meteorologist", "Metrologist" => "Metrologist", "Microbiologist" => "Microbiologist", "Midwife" => "Midwife", "Miller" => "Miller", "Milling-machine operator" => "Milling-machine operator", "Minemechanic" => "Minemechanic", "Mineengineer" => "Mineengineer", "Miner" => "Miner", "Miningtechnician" => "Miningtechnician", "Miningequipment" => "Miningequipment", "Miningdresser" => "Miningdresser", "Miningoperator" => "Miningoperator", "Miningfitter" => "Miningfitter", "Miningrescuer" => "Miningrescuer", "Mining/minerals surveyor" => "Mining/minerals surveyor", "Model/model" => "Model/model", "Modellermaker" => "Modellermaker", "Motorbodybuilder/repairer" => "Motorbodybuilder/repairer", "Mountain guide" => "Mountain guide", "Multimedia designer" => "Multimedia designer", "Multimediadesigner" => "Multimediadesigner", "Municipal" => "Municipal", "Municipalworker" => "Municipalworker", "Museum/artcurator" => "Museum/artcurator", "Music director" => "Music director", "Musicaltechnician" => "Musicaltechnician", "Musician" => "Musician", "Musicologist" => "Musicologist", "Nannynurse" => "Nannynurse", "Naturalist/naturewarden" => "Naturalist/naturewarden", "Newspaper" => "Newspaper", "Nuclearoperator" => "Nuclearoperator", "Nurseobstetric" => "Nurseobstetric", "Nurseryteacher" => "Nurseryteacher", "Nutritionist" => "Nutritionist", "Office junior" => "Office junior", "On-lineoperator" => "On-lineoperator", "Operational analyst/researcher" => "Operational analyst/researcher", "Operationselectrician" => "Operationselectrician");
                                                foreach ($occupation as $s9032n) {
                                                   echo "<option value=".$s9032n.">$s9032n</option>";
                                                }
                                             ?>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li>
                     <div class="personal-info fullwidth edit-mode">
                        <div class="right">                                   
                           <a href="javascript:void(0)" class="btn-custom waves-effect" onclick="open_edit_act_bf_cl(this)">Cancel</a>                                    
                           <a href="javascript:void(0)" class="btn-custom waves-effect" onclick="open_edit_act_bf_cl(this)">Save</a>
                        </div>
                     </div>
                  </li>
               </ul>

               <div class="editicon2 new-post-mobile clear">
                  <a class="grey popup-window waves-effect waves-light" href="javascript:void(0)" onclick="open_edit_act_bf(this)"><i class="mdi mdi-pencil"></i></a>
               </div>
            </div>
            <div id="menu-profilepic" class="profilepic-content settings-content">
               <div class="formtitle">
                  <h4 class="border_bt">Profile Picture</h4>
               </div>
               <div class="profpic-settings custome-pic">
                  <div class="row">
                     <div class="col s12 m7 l5 cropping-section setting-crop">
                        <div class="cropper cropper-wrapper">
                           <div class="image-upload">
                              <label for="file-input">
                              <i class="zmdi zmdi-camera-bw"></i>
                              </label>
                              <input id="file-input" type="file" class="js-cropper-upload" value="Select" onclick="$('.js-cropper-result').hide();$('.crop').show();$('.image-upload').hide();"/>
                           </div>
                           <div class="js-cropper-result">
                              <img class="circle" src="images/demo-profile.jpg" />  
                           </div>
                           <div class="crop dis-none">
                              <div class="green-top desktop-view showon">Drag to crop</div>
                              <div class="js-cropping"></div>
                              <i class="js-cropper-result--btn zmdi zmdi-check upload-btn"></i>
                              <i class="mdi mdi-close img-cancel-btn" onclick="$('.js-cropper-result').show();$('.crop').hide();$('.image-upload').show();"></i>
                           </div>
                        </div>
                        <div class="green-top desktop-view">Crop Photo</div>
                        <h2 class="desktop-none">Adel Hasanat</h2>
                     </div>
                     <div class="col s12 m5 l7 setting-crop-small cus6">
                        <div class="uploadProfile-stuff">
                           <p class="grayp">Your photo needs to be 200x200 with Gif or Jpeg format</p>
                           <div class="fakeFileButton main-crop-btn" id="cropContainerHeaderButton11">
                              <img src="images/upload-green.png" /> &nbsp; Upload a photo
                              <div class="form-group">
                                 <input type="hidden" id="cropContainerPreload" value="Upload"  accept="image/*" onChange="showpoinfo1()">
                              </div>
                           </div>
                           <div class="divider"></div>
                           <div class="fakeFileButton">
                              <a href="#change-profile" class="popup-modal"><img src="images/webcam.png" /> &nbsp; Take a photo</a>
                           </div>
                           <div class="divider"></div>
                           <div class="fakeFileButton">
                              <img src="images/fb.png" /> &nbsp; Use a photo from Facebook
                              <div class="form-group">
                                 <input type="hidden" accept="image/*">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="menu-communication" class="communication-content settings-content">
               <div class="formtitle">
                  <h4 class="border_bt">Communication</h4>
               </div>
               <ul class="settings-ul basicinfo-ul">
               <div class="formtitle formtitle mobile-show">
                  <h5>Sound</h5>
               </div>
               <ul class="settings-ul notification-ul">
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s10 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>Play a sound when new message is received</label>
                                 </div>
                              </div>
                              <div class="col s2 m2 l2  right btn-holder">
                                 <div class="right" id="connect">
                                    <!-- Switch -->
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
               </ul>
               <div class="clear"></div>
               <div class="formtitle mobile-show">
                  <h5>Display preview</h5>
               </div>
               <ul class="settings-ul notification-ul">
                  <!-- sound for receiving notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s10 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>Show new message preview</label>
                                 </div>
                              </div>
                              <div class="col s2 m2 l2  right btn-holder">
                                 <div class="right" id="connect">
                                    <!-- Switch -->
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
               </ul>
               <div class="clear"></div>
               <ul class="settings-ul notification-ul">
                  <!-- sound for receiving notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m12 l10 detail-holder">
                                 <div class="info">
                                    <div class="drop-holder">
                                       <div class="sliding-middle-out anim-area underlined fullwidth">
                                          <select>
                                             <option>Get messages from connections</option>
                                             <option>Get messages from connections of connect</option>
                                             <option>Get messages from everyone</option>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m12 l10 detail-holder">
                                 <div class="info">
                                    <div class="drop-holder">
                                       <div class="sliding-middle-out anim-area underlined fullwidth">
                                          <select>
                                             <option>Turn off alerts and sound for</option>
                                             <option>1 hr</option>
                                             <option>1 day</option>
                                             <option>1 week</option>
                                             <option>1 month</option>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- sound for receiving notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <div class="subsetting-area">
                                       <div class="commu_area">
                                          <input id="Show-in-box" type="checkbox">
                                          <label for="Show-in-box">Show me as away when I've been inactive for <input value="10" type="text"> minutes</label>
                                       </div>
                                       <div class="commu_area">
                                          <input id="Enter-in-box" type="checkbox">
                                          <label for="Enter-in-box">Enter to sens the message </label>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
               </ul>
               <div class="clear"></div>
            </div>
            <div id="menu-security" class="security-content settings-content">
               <div class="formtitle">
                  <h4 class="border_bt">Security Settings
                     <span class="right">
                     <a href="javascript:void(0)" class="editiconCircleEffect editicon1 waves-effect waves-theme" onclick="open_edit_act_ss(this)"><i class="mdi mdi-pencil"></i></a>
                     </span>
                  </h4>
               </div>
               <ul class="settings-ul security-ul normal-part">
                  <!-- Security Question -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Security Question</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l6 detail-holder">
                                 <div class="info">
                                    <label>Set your security question</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Lookup Setting -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Lookup Setting</label>
                                 </div>
                              </div>
                              <div class="col s12 m5 l6 detail-holder">
                                 <div class="info">
                                    <label>Who can look me up?</label>
                                 </div>
                              </div>
                              <div class="col s12 m4 l3 btn-holder has-security">
                                 <span class="security-setting">Private</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Connect Request Settings -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Connect Request Setting</label>
                                 </div>
                              </div>
                              <div class="col s12 m5 l6 detail-holder">
                                 <div class="info">
                                    <label>Who can send me connect requests?</label>
                                 </div>
                              </div>
                              <div class="col s12 m4 l3 btn-holder has-security">
                                 <span class="security-setting">Connections of Connections</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Connect List -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Connect List</label>
                                 </div>
                              </div>
                              <div class="col s12 m5 l6 detail-holder">
                                 <div class="info">
                                    <label>Who should see my connect list?</label>
                                 </div>
                              </div>
                              <div class="col s12 m4 l3 btn-holder has-security">
                                 <span class="security-setting">Private</span>
                                 </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Photo Security -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Photo Security</label>
                                 </div>
                              </div>
                              <div class="col s12 m5 l6 detail-holder">
                                 <div class="info">
                                    <label>Who can see my photos?</label>
                                 </div>
                              </div>
                              <div class="col s12 m4 l3 btn-holder has-security">
                                 <span class="security-setting">Private</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Post Security -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Post Security</label>
                                 </div>
                              </div>
                              <div class="col s12 m5 l6 detail-holder">
                                 <div class="info">
                                    <label>Who can see my posts?</label>
                                 </div>
                              </div>
                              <div class="col s12 m4 l3 btn-holder has-security">
                                 <span class="security-setting">Private</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Post on wall -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Post on wall</label>
                                 </div>
                              </div>
                              <div class="col s12 m5 l6 detail-holder">
                                 <div class="info">
                                    <label>Who can add stuff to my public Wall</label>
                                 </div>
                              </div>
                              <div class="col s12 m4 l3 btn-holder has-security">
                                 <span class="security-setting">Private</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Post Review -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Post Review</label>
                                 </div>
                              </div>
                              <div class="col s12 m5 l6 detail-holder">
                                 <div class="info">
                                    <label>Review posts connections tag you in before they appear on your public wall</label>
                                 </div>
                              </div>
                              <div class="col s12 m4 l3 btn-holder has-security">
                                 <span class="security-setting">Enabled</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Tag Reviews -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Tag Reviews</label>
                                 </div>
                              </div>
                              <div class="col s12 m5 l6 detail-holder">
                                 <div class="info">
                                    <label>Review tags people add to your own posts before the tags appear on site</label>
                                 </div>
                              </div>
                              <div class="col s12 m4 l3 btn-holder has-security">
                                 <span class="security-setting">Private</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- View Permission -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>View Permission</label>
                                 </div>
                              </div>
                              <div class="col s12 m5 l6 detail-holder">
                                 <div class="info">
                                    <label>Who can see what others post on your public Wall</label>
                                 </div>
                              </div>
                              <div class="col s12 m4 l3 btn-holder has-security">
                                 <span class="security-setting">Private</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
               </ul>
               <ul class="settings-ul security-ul edit-part dis-none">
                  <!-- Security Question -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Security Question</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l6">
                                 <div class="row">
                                    <div class="col s12 m6 l6">
                                       <div class="sliding-middle-out anim-area underlined fullwidth">
                                          <select>
                                             <option>What is your email on file?</option>
                                             <option>Which city you were born in?</option>
                                             <option>What is your girlconnect's name?</option>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col s12 m6 l6">
                                       <div class="eyeicon">
                                          <div class="sliding-middle-out anim-area underlined fullwidth">
                                             <input type="text" class="title" placeholder="Answer"/>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Lookup Setting -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Lookup Setting</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l6">
                                 <div class="info">
                                    <label>Who can look me up?</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l3 right">
                                 <div class="left">
                                    <a class="dropdown-button nothemecolor lookupsettingprivacylabel" href="javascript:void(0)" data-modeltag="lookupsettingprivacylabel" data-fetch="yes" data-label="lookupsetting" data-activates="privancy1">
                                       <span class="getvalue">Private</span> <span class="caret"></span>
                                    </a> 
                                    <ul id='privancy1' class='dropdown-content'>
                                       <li class="selectore"><a href="javascript:void(0)">Private</a></li>
                                       <li class="selectore"><a href="javascript:void(0)">Connections</a></li>
                                       <li class="selectore"><a href="javascript:void(0)">Public</a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Connect Request Settings -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Connect Request Setting</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l6">
                                 <div class="info">
                                    <label>Who can send me connect requests?</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l3 right">
                                 <div class="left">
                                    <a class="dropdown-button nothemecolor connectrequestsettingprivacylabel" href="javascript:void(0)" data-modeltag="connectrequestsettingprivacylabel" data-fetch="yes" data-label="connect_request" data-activates="privancy5">
                                       <span class="getvalue">Private</span> <span class="caret"></span>
                                    </a> 
                                    <ul id="privancy5" class="dropdown-content ftof">
                                       <li class="selectore"><a href="javascript:void(0)">Private</a></li>
                                       <li class="selectore"><a href="javascript:void(0)">Connections</a></li>
                                       <li class="selectore"><a href="javascript:void(0)">Public</a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Connect List -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Connect List</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l6">
                                 <div class="info">
                                    <label>Who should see my connect list?</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l3 right">
                                 <div class="left">
                                    <a class="dropdown-button nothemecolor connectlistprivacylabel" href="javascript:void(0)" data-modeltag="connectlistprivacylabel" data-fetch="yes" data-label="connect_list" data-activates="privancy12">
                                       <span class="getvalue">Private</span> <span class="caret"></span>
                                    </a>
                                    <ul id="privancy12" class="dropdown-content">
                                       <li class="selectore"><a href="javascript:void(0)">Private</a></li>
                                       <li class="selectore"><a href="javascript:void(0)">Connections</a></li>
                                       <li class="selectore"><a href="javascript:void(0)">Public</a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Photo Security -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Photo Security</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l6">
                                 <div class="info">
                                    <label>Who can see my photos?</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l3 right">
                                 <div class="left">
                                    <a class="dropdown-button nothemecolor photosecurityspan photosecurityprivacylabel" href="javascript:void(0)" data-modeltag="photosecurityprivacylabel" data-fetch="yes" data-label="photosecurity" data-activates="privancy3">
                                       <span class="getvalue">Private</span> <span class="caret"></span>
                                    </a> 
                                    <ul id="privancy3" class="dropdown-content">
                                       <li class="selectore"><a href="javascript:void(0)">Private</a></li>
                                       <li class="selectore"><a href="javascript:void(0)">Connections</a></li>
                                       <li class="selectore customli_modal"><a href="javascript:void(0)">Custom</a></li>
                                       <li class="selectore"><a href="javascript:void(0)">Public</a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Post Security -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Post Security</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l6">
                                 <div class="info">
                                    <label>Who can see my posts?</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l3 right">
                                 <div class="left">
                                    <a class="dropdown-button nothemecolor postsecurityspan postprivacylabel" href="javascript:void(0)" data-modeltag="postprivacylabel" data-fetch="yes" data-label="postprivacy" data-activates="privancy2">
                                       <span class="getvalue">Private</span> <span class="caret"></span>
                                    </a>
                                    <ul id="privancy2" class="dropdown-content">
                                       <li class="selectore"><a href="javascript:void(0)">Private</a></li>
                                       <li class="selectore"><a href="javascript:void(0)">Connections</a></li>
                                       <li class="selectore customli_modal"><a href="javascript:void(0)">Custom</a></li>
                                       <li class="selectore"><a href="javascript:void(0)">Public</a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Post on wall -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Post on wall</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l6">
                                 <div class="info">
                                    <label>Who can add stuff to my public Wall</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l3 right">
                                 <div class="left">
                                    <a class="dropdown-button nothemecolor postonwallprivacylabel" href="javascript:void(0)" data-modeltag="postonwallprivacylabel" data-fetch="yes" data-label="postonwallprivacy" data-activates="privancy6">
                                       <span class="getvalue">Private</span> <span class="caret"></span>
                                    </a>
                                    <ul id="privancy6" class="dropdown-content">
                                       <li class="selectore"><a href="javascript:void(0)">Private</a></li>
                                       <li class="selectore"><a href="javascript:void(0)">Connections</a></li>
                                       <li class="selectore customli_modal"><a href="javascript:void(0)">Custom</a></li>
                                       <li class="selectore"><a href="javascript:void(0)">Public</a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Post Review -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Post Review</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l6">
                                 <div class="info">
                                    <label>Review posts connections tag you in before they appear on your public wall</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l3 right">
                                 <div class="left">
                                    <a class="dropdown-button nothemecolor postreviewprivacylabel" href="javascript:void(0)" data-modeltag="postreviewprivacylabel" data-fetch="yes" data-label="review_posts" data-activates="privancy7">
                                       <span class="getvalue">Enabled</span> <span class="caret"></span>
                                    </a> 
                                    <ul id="privancy7" class="dropdown-content">
                                       <li><a href="javascript:void(0)">Enabled</a></li>
                                       <li><a href="javascript:void(0)">Disabled</a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Tag Reviews -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Tag Reviews</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l6">
                                 <div class="info">
                                    <label>Review tags people add to your own posts before the tags appear on site</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l3 right">
                                 <div class="left">
                                    <a class="dropdown-button nothemecolor tagreviewprivacylabel" href="javascript:void(0)" data-modeltag="tagreviewprivacylabel" data-fetch="yes" data-label="review_tags" data-activates="privancy10">
                                       <span class="getvalue">Private</span> <span class="caret"></span>
                                    </a>
                                    <ul id="privancy10" class="dropdown-content">
                                       <li class="selectore"><a href="javascript:void(0)">Private</a></li>
                                       <li class="selectore"> <a href="javascript:void(0)">Connections</a></li>
                                       <li class="selectore"><a href="javascript:void(0)">Public</a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- View Permission -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>View Permission</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l6">
                                 <div class="info">
                                    <label>Who can see what others post on your public Wall</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l3 right">
                                 <div class="left">
                                    <a class="dropdown-button nothemecolor activitypermissionprivacylabel" href="javascript:void(0)" data-modeltag="activitypermissionprivacylabel" data-fetch="yes" data-label="activitypermissionprivacy" data-activates="privancy121">
                                       <span class="getvalue">Private</span> <span class="caret"></span>
                                    </a>
                                    <ul id="privancy121" class="dropdown-content">
                                       <li class="selectore"><a href="javascript:void(0)">Private</a></li>
                                       <li class="selectore"><a href="javascript:void(0)">Connections</a></li>
                                       <li class="selectore customli_modal"><a href="javascript:void(0)">Custom</a></li>
                                       <li class="selectore"><a href="javascript:void(0)">Public</a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>

                  <li>
                     <div class="personal-info fullwidth edit-mode">
                        <div class="right">                                   
                           <a href="javascript:void(0)" class="btn-custom waves-effect" onclick="open_edit_act_ss_cl(this)">Cancel</a>                                    
                           <a href="javascript:void(0)" class="btn-custom waves-effect" onclick="open_edit_act_ss_cl(this)">Save</a>
                        </div>
                     </div>
                  </li>
               </ul>

               <div class="editicon2 new-post-mobile clear">
                  <a class="grey popup-window waves-effect waves-light" href="javascript:void(0)" onclick="open_edit_act_ss(this)"><i class="mdi mdi-pencil"></i></a>
               </div>
            </div>
            <div id="menu-notification" class="notification-content settings-content">
               <div class="formtitle">
                  <h4 class="border_bt">Notifications</h4>
               </div>
               <div class="formtitle mobile-show">
                  <h5>General</h5>
               </div>
               <ul class="settings-ul notification-ul">
                  <!-- connections activity -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>Get notifications when you get eGift</label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="connect">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- account security and privacy issues -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>When your credits is near tripping point</label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2  right btn-holder">
                                 <div class="right" id="email">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- activity that involves member -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>Get email notifications about your account security and privacy issues</label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2  right btn-holder">
                                 <div class="right" id="member">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
               </ul>
               <div class="clear"></div>
               <div class="formtitle mobile-show">
                  <h5>Posts</h5>
               </div>
               <ul class="settings-ul notification-ul">
                  <!-- like share comment on photo -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>When someone like your post</label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="activity_on_user">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- activities involves your group -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>When someone comment your post</label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="group">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- people start following you -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>When someone share your post</label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="non_connect">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
               </ul>
               <div class="clear"></div>
               <div class="formtitle mobile-show">
                  <h5>Connections</h5>
               </div>
               <ul class="settings-ul notification-ul">
                  <!-- get/confirm connect request -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>Get notifications about your connect’s activities</label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="connect_req">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- get e-card / e-gift -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>Get notifications about your connect’s activities on your post</label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="e_c">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- invitation for meeting -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>Get notifications about your none connect’s activities on your post</label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="meeting">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- ask a question -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>Get notified when a connect request confirmed</label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="question" >
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- credit is near tipping point -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>Play a sound when new notification is received</label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="credit">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- credit is near tipping point -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>Play a sound when a message is received</label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="credit">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
               </ul>
               <div class="clear"></div>
               <div class="formtitle mobile-show">
                  <h5>Collections</h5>
               </div>
               <ul class="settings-ul notification-ul">
                  <!-- new notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>Get notify when someone follows your collection</label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="sound-notification">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- new notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>	Get notify when someone Share your collection</label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="sound-notification">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
               </ul>
               <div class="clear"></div>
               <div class="formtitle mobile-show">
                  <h5>Events</h5>
               </div>
               <ul class="settings-ul notification-ul">
                  <!-- new notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>Get notify when some share your event</label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="sound-notification">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- new notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>When a post is added to your event</label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="sound-notification">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- new notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>When a photo is uploaded to your event</label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="sound-notification">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- new notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>When someone attend your event</label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="sound-notification">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- new notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>When someone invites you to an event</label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="sound-notification">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- new notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>When a member get invited to your event</label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="sound-notification">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
               </ul>
               <div class="clear"></div>
               <div class="formtitle mobile-show">
                  <h5>Groups</h5>
               </div>
               <ul class="settings-ul notification-ul">
                  <!-- new notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>Get notify when some share your group </label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="sound-notification">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- new notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>When someone add post to your group </label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="sound-notification">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- new notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>When someone add photo to your group</label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="sound-notification">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- new notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>When someone become a member of your group </label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="sound-notification">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- new notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>When someone invites you to a group</label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="sound-notification">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- new notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>When a member get invite to your group </label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="sound-notification">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
               </ul>
               <div class="clear"></div>
               <div class="formtitle mobile-show">
                  <h5>Travelbuddy</h5>
               </div>
               <ul class="settings-ul notification-ul">
                  <!-- new notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>Get notify when a new trip posted by a connect </label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="sound-notification">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- new notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>When I get invited to a trip</label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="sound-notification">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
               </ul>
               <div class="clear"></div>
               <div class="formtitle">
                  <h5>Hangout</h5>
               </div>
               <ul class="settings-ul notification-ul">
                  <!-- new notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>Get notify when a new hangout posted by a connect </label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="sound-notification">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- new notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>When I get invited to a hangout </label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="sound-notification">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
               </ul>
               <div class="clear"></div>
               <div class="formtitle">
                  <h5>Weekend escape</h5>
               </div>
               <ul class="settings-ul notification-ul">
                  <!-- new notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>Get notify when a new escape event posted by a connect </label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="sound-notification">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- new notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>When I get invited to a weekend escape </label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="sound-notification">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
               </ul>
               <div class="clear"></div>
               <div class="formtitle">
                  <h5>Hire a guide</h5>
               </div>
               <ul class="settings-ul notification-ul">
                  <!-- new notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>Get notify when a new guide profile added from connect </label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="sound-notification">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- new notification -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m10 l10 detail-holder">
                                 <div class="info">
                                    <label>When I get invited by traveler </label>
                                 </div>
                              </div>
                              <div class="col s12 m2 l2 right btn-holder">
                                 <div class="right" id="sound-notification">
                                    <div class="switch">
                                       <label>
                                       <input type="checkbox">
                                       <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
               </ul>
               <div class="clear"></div>
            </div>
            <div id="menu-block" class="block-content settings-content">
               <div class="formtitle">
                  <h4 class="border_bt">Blocking
                     <span class="right">
                     <a href="javascript:void(0)" class="editiconCircleEffect editicon1 waves-effect waves-theme" onclick="open_edit_act_blocking(this)"><i class="mdi mdi-pencil"></i></a>
                     </span>
                  </h4>
               </div>
               <ul class="settings-ul block-ul normal-part">
                  <!-- Restricted List -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Restricted List</label>
                                 </div>
                              </div>
                              <div class="col s12 m7 l6">
                                 <div class="info">
                                    <label>People on this list cannot see my posts</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Blocked List -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Blocked List</label>
                                 </div>
                              </div>
                              <div class="col s12 m7 l6">
                                 <div class="info">
                                    <label>People on this list cannot see my posts</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Blocked Event Invites -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Blocked Event Invites</label>
                                 </div>
                              </div>
                              <div class="col s12 m7 l6">
                                 <div class="info">
                                    <label>People on this list can not send event invitation</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Message Filtering -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Message Filter</label>
                                 </div>
                              </div>
                              <div class="col s12 m7 l6">
                                 <div class="info">
                                    <label>Whose messages do I want filtered into my Inbox?</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Request Filtering -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Request Filter</label>
                                 </div>
                              </div>
                              <div class="col s12 m7 l6">
                                 <div class="info">
                                    <label>People on this list will not be able to send me requests</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Message Filtering -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Mute travelbuddy user list</label>
                                 </div>
                              </div>
                              <div class="col s12 m7 l6">
                                 <div class="info">
                                    <label>You will not see travelbuddy trips of people in this list</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Message Filtering -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Mute hangout user list</label>
                                 </div>
                              </div>
                              <div class="col s12 m7 l6">
                                 <div class="info">
                                    <label>You will not see hangout events of people in this list</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Message Filtering -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Mute weekend escape user list</label>
                                 </div>
                              </div>
                              <div class="col s12 m7 l6">
                                 <div class="info">
                                    <label>You will not see weekend escape plans of people in this list</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Message Filtering -->
                  <li>
                     <div class="settings-group">
                        <div class="normal-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 caption-holder">
                                 <div class="caption">
                                    <label>Mute local guide user list</label>
                                 </div>
                              </div>
                              <div class="col s12 m7 l6">
                                 <div class="info">
                                    <label>You will not see local guide profiles of people in this list</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>

               </ul>

               <ul class="settings-ul block-ul edit-part dis-none">
                  <!-- Restricted List -->
                  <li> 
                     <div class="settings-group">
                       <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 ">
                                 <div class="caption">
                                    <label>Restricted List</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l6">
                                 <span class="tagged_person_name compose_addpersonAction_as" id="compose_addpersonAction_as">Add people to your restricted list</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Blocked List -->
                  <li>
                     <div class="settings-group">
                       <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 ">
                                 <div class="caption">
                                    <label>Blocked List</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l6">
                                 <span class="tagged_person_name compose_addpersonAction_as" id="compose_addpersonAction_as">Add people to your block list</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Blocked Event Invites -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 ">
                                 <div class="caption">
                                    <label>Blocked Event Invites</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l6">
                                 <span class="tagged_person_name compose_addpersonAction_as" id="compose_addpersonAction_as">Add people to your block list</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Message Filtering -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 ">
                                 <div class="caption">
                                    <label>Message Filter</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l6">
                                 <span class="tagged_person_name compose_addpersonAction_as" id="compose_addpersonAction_as">Add people to your message filter</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Request Filtering -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 ">
                                 <div class="caption">
                                    <label>Message Filtering</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l6">
                                 <span class="tagged_person_name compose_addpersonAction_as" id="compose_addpersonAction_as">Add people to your message filter</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Message Filtering -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 ">
                                 <div class="caption">
                                    <label>Mute travelbuddy user list</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l6">
                                 <span class="tagged_person_name compose_addpersonAction_as" id="compose_addpersonAction_as">Add people to your mute list</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Message Filtering -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 ">
                                 <div class="caption">
                                    <label>Mute hangout user list</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l6">
                                 <span class="tagged_person_name compose_addpersonAction_as" id="compose_addpersonAction_as">Add people to your mute list</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Message Filtering -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 ">
                                 <div class="caption">
                                    <label>Mute weekend escape user list</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l6">
                                 <span class="tagged_person_name compose_addpersonAction_as" id="compose_addpersonAction_as">Add people to your mute list</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- Message Filtering -->
                  <li>
                     <div class="settings-group">
                        <div class="edit-mode">
                           <div class="row">
                              <div class="col s12 m3 l3 ">
                                 <div class="caption">
                                    <label>Mute local guide user list</label>
                                 </div>
                              </div>
                              <div class="col s12 m9 l6">
                                 <span class="tagged_person_name compose_addpersonAction_as" id="compose_addpersonAction_as">Add people to your mute list</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>

                  <li>
                     <div class="personal-info fullwidth edit-mode">
                        <div class="right">                                   
                           <a href="javascript:void(0)" class="btn-custom waves-effect" onclick="open_edit_act_blocking_cl(this)">Cancel</a>                                    
                           <a href="javascript:void(0)" class="btn-custom waves-effect" onclick="open_edit_act_blocking_cl(this)">Save</a>
                        </div>
                     </div>
                  </li>

               </ul>

               <div class="editicon2 new-post-mobile clear">
                  <a class="grey popup-window waves-effect waves-light" href="javascript:void(0)" onclick="open_edit_act_blocking(this)"><i class="mdi mdi-pencil"></i></a>
               </div>
            </div>
            <!-- close account-->
            <div id="menu-close" class="close-content settings-content">
               <div class="formtitle">
                  <h4 class="border_bt">Close Account</h4>
               </div>
               <div class="close-account-change">
                  <div class="top-close-head">
                     <h3>Bhadresh, we're sorry to see you go</h3>
                  </div>
                  <div class="new-closeaccount">
                     <p>Once you close your account? You'll lose your connections, messages, posts and comments</p>
                  </div>
                  <div class="closeaccountsection">
                     <p class="cl-head">Don't lose touch with your 81 connections like Nikhat, NAVLE, and Harikrishna..</p>
                     <ul class="row roe-close">
                        <li class="col m4 s12 l4">
                           <a class="invitelike-connect" href="javascript:void(0)">
                              <span class="imgholder"><img src="images/people-1.png"></span>
                              <span class="descholder">
                                 <h6>Nikhat Mark</h6>
                                 <p>Indore (MP)</p>
                              </span>
                           </a>
                        </li>
                        <li class="col m4 s12 l4">
                           <a class="invitelike-connect" href="javascript:void(0)">
                              <span class="imgholder"><img src="images/people-2.png"></span>
                              <span class="descholder">
                                 <h6>NAVLE Mark</h6>
                                 <p>Indore (MP)</p>
                              </span>
                           </a>
                        </li>
                        <li class="col m4 s12 l4">
                           <a class="invitelike-connect" href="javascript:void(0)">
                              <span class="imgholder"><img src="images/people-3.png"></span>
                              <span class="descholder">
                                 <h6>Harikrishna</h6>
                                 <p>Indore (MP)</p>
                              </span>
                           </a>
                        </li>
                     </ul>
                  </div>
                  <ul class="settings-ul basicinfo-ul">
                     <!-- reason of leaving -->
                     <li>
                        <div class="settings-group">
                           <div class="normal-mode">
                              <div class="row">
                                 <div class="col s12 m3 l2 caption-holder">
                                    <div class="caption">
                                       <label>Reason of Leaving</label>
                                    </div>
                                 </div>
                                 <div class="col s12 m9 l10 detail-holder">
                                    <div class="info">
                                       <div class="radio-holder">
                                          <input name="group1" type="radio" id="test1" />
                                          <label for="test1">This is temporary, I will be back.
                                          </label>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="radio-holder"> 
                                          <input name="group1" type="radio" id="test2" />
                                          <label for="test2">I don't understand how to use Iwasinqatar.
                                          </label>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="radio-holder"> 
                                          <input name="group1" type="radio" id="test3" />
                                          <label for="test3">My account was hacked.
                                          </label>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="radio-holder">
                                          <input name="group1" type="radio" id="test4" />
                                          <label for="test4">I spent too much time using Iwasinqatar.
                                          </label>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="radio-holder">
                                          <input name="group1" type="radio" id="test5" />
                                          <label for="test5">I get too many emails, invitations, and requests from Iwasinqatar.
                                          </label>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="radio-holder">
                                          <input name="group1" type="radio" id="test6" />
                                          <label for="test6">I have a privacy concern.
                                          </label>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="radio-holder">
                                          <input name="group1" type="radio" id="test7" />
                                          <label for="test7">I have another Iwasinqatar account.
                                          </label>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="radio-holder">
                                          <input name="group1" type="radio" id="test8" />
                                          <label for="test8">I don't find Iwasinqatar useful.
                                          </label>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="radio-holder" onclick="showother()">
                                          <input name="group1" type="radio" id="test9" />
                                          <label for="test9">Other
                                          </label>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </li>
                     <!-- action required -->
                     <li class="others-click dis-none">
                        <div class="settings-group">
                           <div class="normal-mode">
                              <div class="row">
                                 <div class="col s12 m3 l2 caption-holder">
                                    <div class="caption">
                                       <label>Action required</label>
                                    </div>
                                 </div>
                                 <div class="col s12 m9 l10 detail-holder">
                                    <div class="info">
                                       <div class="sliding-middle-out anim-area underlined fullwidth">
                                          <textarea class="materialize-textarea mb0 md_textarea descinput"></textarea>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </li>
                     <!-- action required -->
                     <li class="why-cloing">
                        <div class="settings-group">
                           <div class="normal-mode">
                              <div class="row">
                                 <div class="col s12 m12 l12 detail-holder">
                                    <div class="info">
                                       <div class="sliding-middle-custom anim-area underlined fullwidth">
                                          <textarea class="materialize-textarea mb0 md_textarea descinput" placeholder="Tell us why you're closing your account:"></textarea>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </li>
                  </ul>
                  <div class="clear"></div>
                  <div class="subsetting-area">
                     <ul class="settings-ul basicinfo-ul">
                        <li>
                           <div class="btn-holder right">						
                              <a href="javascript:void(0)" class="btn-custom">Cancel</a>	
                              <a href="javascript:void(0)" onclick="generateDiscard('dis_delaccount')" class="btn-custom">Close Account
                              </a>					
                           </div>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
            <!-- close account-->
            <!-- close account 1-->
            <div id="menu-close1" class="close-content settings-content lst-close">
               <div class="formtitle">
                  <h4 class="border_bt">Close Account</h4>
               </div>
               <div class="close-account-change">
                  <div class="top-close-head">
                     <h3>We can't close your account yet</h3>
                  </div>
                  <div class="new-closeaccount">
                     <p>You need to be resolved some issues before we can close your account. Once you've resolved the issue, please retry closing your account from your setting page.</p>
                  </div>
                  <ul class="close-ul">
                     <li>You have an active ad campaign. You'll need to <a href="">deactivate it through advert manager</a></li>
                     <li>
                        <span>You own the following collections. You will need to delete it throight collection console</span>
                        <ul>
                           <li><a href=""><i class="mdi mdi-chevron-right"></i> Piece of Qatar</a></li>
                           <li><a href=""><i class="mdi mdi-chevron-right"></i> Beautiful Flowers</a></li>
                        </ul>
                     </li>
                     <li>
                        <span>You admin the following groups. You will to need delete it or promote another admin throught group console</span>
                        <ul>
                           <li><a href=""><i class="mdi mdi-chevron-right"></i> Amazing Flowers</a></li>
                        </ul>
                     </li>
                     <li>
                        <span>You are orginzed the following events. You will need to delete it or promote another orginizer throught event console</span>
                        <ul>
                           <li><a href=""><i class="mdi mdi-chevron-right"></i> Music Night@ Summer 2016</a></li>
                        </ul>
                     </li>
                     <li>
                        <span>You admin the following pages. You will to need delete it or promote another admin throught page setting</span>
                        <ul>
                           <li><a href=""><i class="mdi mdi-chevron-right"></i> Piece of Qatar</a></li>
                        </ul>
                     </li>
                  </ul>
                  <div class="clear"></div>
               </div>
            </div>
            <!-- close account 1-->
            <div id="choose-theme" class="choose-theme-content settings-content">
               <div class="formtitle">
                  <h4 class="border_bt">Choose Theme</h4>
               </div>
               <div class="settings-theme">
                  <p class="grayp">Please click on a box to choose your theme color:</p>
                  <div class="colorsBox theme-drawer">
                     <div class="boxrow">
                        <a href="javascript:void(0);" body-color="theme-color" class="tm-dark-blue active"></a>
                        <a href="javascript:void(0);" body-color="theme-purple" class="tm-purple"></a>
                        <a href="javascript:void(0);" body-color="theme-light-blue" class="tm-light-blue"></a>
                     </div>
                     <div class="boxrow">
                        <a href="javascript:void(0);" body-color="theme-green" class="tm-green"></a>
                        <a href="javascript:void(0);" body-color="theme-light-red" class="tm-light-red"></a>
                        <a href="javascript:void(0);" body-color="theme-light-purple" class="tm-light-purple"></a>
                     </div>
                     <div class="boxrow">
                        <a href="javascript:void(0);" body-color="theme-black" class="tm-black"></a>
                        <a href="javascript:void(0);" body-color="theme-bright-blue" class="tm-bright-blue"></a>
                        <a href="javascript:void(0);" body-color="theme-emerald" class="tm-emerald"></a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>	
<!--manageadmin-->
<div id="manageadmin-popup" class="modal manage-modal manageadmin-popup">
   <div class="modal_header">
      <button class="close_btn custom_modal_close_btn close_modal waves-effect">
      <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
      </button>
      <h3>Manage Admin</h3>
   </div>
   <div class="custom_modal_content modal_content">
      <div class="main-pcontent spadding">
         <ul class="tabs">
            <li onclick="allmemberforadmin();" class="tab active"><a href="#member-all" data-toggle="tab" aria-expanded="false" class="active">Promote to Admin</a></li>
            <li onclick="memberrequest();" class="tab"><a href="#member-request" data-toggle="tab" aria-expanded="true" class="">Request</a></li>
            <li class="indicator"></li>
         </ul>
         <div class="tab-content">
            <div id="member-all" class="tab-pane in active dis-block" data-id="froup-stab">
               <ul class="manage-members">
                  <div class="post-holder bshadow">
                     <div class="joined-tb">
                        <i class="mdi mdi-file-outline"></i>        
                        <p>No record found</p>
                     </div>
                  </div>
               </ul>
            </div>
            <div id="member-request" class="tab-pane dis-none">
               <ul class="manage-members">
                  <div class="post-holder bshadow">
                     <div class="joined-tb">
                        <i class="mdi mdi-file-outline"></i>        
                        <p>No request found</p>
                     </div>
                  </div>
               </ul>
            </div>
         </div>
      </div>
   </div>
</div>
<!--manageorganizer-->
<div id="manageorganizer-popup" class="modal manage-modal manageadmin-popup" >
   <div class="modal_header">
      <button class="close_btn custom_modal_close_btn close_modal waves-effect">
      <i class="mdi mdi-close mdi-20px material_close"></i>
      </button>
      <h3>Manage Organizer</h3>
   </div>
   <div class="custom_modal_content modal_content">
      <div class="main-pcontent spadding">
         <ul class="tabs">
            <li onclick="allmemberforadmin();" class="tab active"><a href="#organizer-all" data-toggle="tab" aria-expanded="false" class="active">Promote to Organizer</a></li>
            <li onclick="memberrequest();" class="tab"><a href="#organizer-request" data-toggle="tab" aria-expanded="true"class="">Request</a></li>
            <li class="indicator"></li>
         </ul>
         <div class="tab-content">
            <div id="organizer-all" class="tab-pane in active dis-block" data-id="froup-stab">
               <ul class="manage-members">
                  <div class="post-holder bshadow">
                     <div class="joined-tb">
                        <i class="mdi mdi-file-outline"></i>        
                        <p>No record found</p>
                     </div>
                  </div>
               </ul>
            </div>
            <div id="organizer-request" class="tab-pane dis- none">
               <ul class="manage-members">
                  <div class="post-holder bshadow">
                     <div class="joined-tb">
                        <i class="mdi mdi-file-outline"></i>        
                        <p>No request found</p>
                     </div>
                  </div>
               </ul>
            </div>
         </div>
      </div>
   </div>
</div>
<!--manageowner-->
<div id="manageowner-popup" class="modal manage-modal manageadmin-popup" >
   <div class="modal_header">
      <button class="close_btn custom_modal_close_btn close_modal">
      <i class="mdi mdi-close mdi-20px material_close"></i>
      </button>
      <h3>Manage Owner</h3>
   </div>
   <div class="custom_modal_content modal_content">
      <div class="main-pcontent spadding">
         <ul class="tabs">
            <li onclick="allmemberforadmin();" class="tab active"><a href="#manageowner-all" data-toggle="tab" aria-expanded="false" class="active">Promote to Owner</a></li>
            <li onclick="memberrequest();" class="tab"><a href="#manageowner-request" data-toggle="tab" aria-expanded="true">Request</a></li>
            <li class="indicator"></li>
         </ul>
         <div class="tab-content">
            <div id="manageowner-all" class="tab-pane in active dis-block" data-id="froup-stab">
               <ul class="manage-members">
                  <div class="post-holder bshadow">
                     <div class="joined-tb">
                        <i class="mdi mdi-file-outline"></i>        
                        <p>No record found</p>
                     </div>
                  </div>
               </ul>
            </div>
            <div id="manageowner-request" class="tab-pane dis-none">
               <ul class="manage-members">
                  <div class="post-holder bshadow">
                     <div class="joined-tb">
                        <i class="mdi mdi-file-outline"></i>        
                        <p>No request found</p>
                     </div>
                  </div>
               </ul>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="compose_addpersonAction_as_modal" class="modal modalxii_level1" >
   <div class="content_header">
      <button class="close_span waves-effect">
      <i class="mdi mdi-close mdi-20px material_close"></i>
      </button>
      <p class="selected_photo_text">4 users selected</p>
      <a href="javascript:void(0)" id="chk_person_done_ss" class="done_btn action_btn close_modal">Done</a>
   </div>
   <nav class="search_for_tag">
      <div class="nav-wrapper">
         <form>
            <div class="input-field">
               <input id="search_box" required="" type="search">
               <label class="label-icon" for="search_box">
               <i class="zmdi zmdi-search"></i>
               </label>
            </div>
         </form>
      </div>
   </nav>
   <div class="person_box">
      <div class="person_detail_container person_detail_div">
         <span class="person_profile"> <img src="images/Female.jpg"> </span> 
         <div class="person_name_container">
            <p class="person_name" id="5a37a2eaea01e37a5337678a">Neelam Jain</p>
            <p class="user_checkbox"> 
               <input checked="" value="5a37a2eaea01e37a5337678a" class="chk_person" name="chk_person" data-name="Neelam Jain" type="checkbox"> 
               <label for="filled_for_person_5a37a2eaea01e37a5337678a"></label> 
            </p>
         </div>
      </div>
      <div class="person_detail_container person_detail_div">
         <span class="person_profile"> <img src="images/Male.jpg"> 
         </span> 
         <div class="person_name_container">
            <p class="person_name" id="5a21235cea01e3e12adbf273">Sumit Singh</p>
            <p class="user_checkbox"> <input checked="" value="5a21235cea01e3e12adbf273" class="chk_person" name="chk_person" data-name="Sumit Singh" type="checkbox"> <label for="filled_for_person_5a21235cea01e3e12adbf273"></label> </p>
         </div>
      </div>
      <div class="person_detail_container person_detail_div">
         <span class="person_profile"> <img src="images/Female.jpg"> 
         </span> 	
         <div class="person_name_container">
            <p class="person_name" id="5972291bea01e3392ada1143">Daniel Aguado</p>
            <p class="user_checkbox"> <input checked="" value="5972291bea01e3392ada1143" class="chk_person" name="chk_person" data-name="Daniel Aguado" type="checkbox"> <label for="filled_for_person_5972291bea01e3392ada1143"></label> </p>
         </div>
      </div>
      <div class="person_detail_container person_detail_div">
         <span class="person_profile"> <img src="images/Male.jpg"> </span> 
         <div class="person_name_container">
            <p class="person_name" id="57972b76cf926ff93c90909f">Adel Google</p>
            <p class="user_checkbox"> <input checked="" value="57972b76cf926ff93c90909f" class="chk_person" name="chk_person" data-name="Adel Google" type="checkbox"> <label for="filled_for_person_57972b76cf926ff93c90909f"></label> </p>
         </div>
      </div>
      <div class="person_detail_container person_detail_div">
         <span class="person_profile"> <img src="images/Female.jpg"> </span> 
         <div class="person_name_container">
            <p class="person_name" id="579729e4cf926f773c90909f">Adel Hasanat</p>
            <p class="user_checkbox"> 
               <input value="579729e4cf926f773c90909f" class="chk_person" name="chk_person" data-name="Adel Hasanat" type="checkbox"> 
               <label for="filled_for_person_579729e4cf926f773c90909f"></label> 
            </p>
         </div>
      </div>
   </div>
</div>
<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">
   <?php include('common/map_modal.php'); ?>
</div>
<?php include('common/custom_modal.php'); ?>
<?php include('common/privacymodal.php'); ?>

<?php include('common/discard_popup.php'); ?>
<?php include("script.php"); ?>	
</body>
</html>
<script>
	var cropper = $('.cropper');

	if (cropper.length) {
	  $.each(cropper, function(i, val) {
	    var uploadCrop;

	    var readFile = function(input) {
	      if (input.files && input.files[i]) {
	        var reader = new FileReader();

	        reader.onload = function(e) {
	          uploadCrop.croppie('bind', {
	            url: e.target.result
	          });
	        };

	        reader.readAsDataURL(input.files[i]);
	      } else {
	        alert("Sorry - you're browser doesn't support the FileReader API");
	      }
	    };

	    uploadCrop = $('.js-cropping').croppie({ // TODO: fix so its selects right element
	      viewport: {
	        width: 200,
	        height: 200
	      },
	      boundary: {
	        width: 350,
	        height: 325
	      },
	      enableOrientation: true
	    });

	    $('.js-cropper-upload').on('change', function() {
	      $('.crop').show(); // TODO: fix so its selects right element
	      readFile(this);
	    });
	    
	    $('.js-cropper-rotate--btn').on('click', function(ev) {
	      uploadCrop.croppie('rotate', parseInt($(this).data('deg')));
	    });

	    $('.js-cropper-result--btn').on('click', function(ev) {
	      uploadCrop.croppie('result', 'canvas').then(function(resp) {
	        popupResult({
	          src: resp
	        });
	      });
	    });

	    var popupResult = function(result) {
	      var html;

	      if (result.html) {
	        html = result.html;
	      }

	      if (result.src) {
	        html = '<img src="' + result.src + '" />';
	      }
	      $('.js-cropper-result').show();	
	      $('.js-cropper-result').html(html); // TODO: fix so its selects right element
	      $('.crop').hide();
	      $('.image-upload').show();
	    };
	  });
	}
</script>