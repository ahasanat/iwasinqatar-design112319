<?php include("header.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div>
</div>
<div class="clear"></div>
<div class="fixed-layout ipad-mfix">
   <?php include("common/leftmenu.php"); ?>
   <div class="main-content with-lmenu sub-page vip-page main-page">
      <div class="combined-column wide-open">
         <div class="content-box">
            <div class="banner-section text-center">
               <div class="container">
                  <h4>Become a VIP member today to unlock premium feature!</h4>
                  <p>The easiest way to get instant access to the privieges below is to upgrade to VIP membership</p>
                  <div class="vip-rate">
                     <p>For only $9 monthly</p>
                     <!-- <a href="#payment-popup" class="popup-modal btn btn-primary btn-lg green-styled-btn">Join VIP</a>  -->
                     <!-- <a href="#payment-popup" class="waves-effect waves-light btn">Join VIP</a> -->
                     <a class="waves-effect waves-light btn modal-trigger" href="#payment-popup">Join VIP</a>
                  </div>
               </div>
            </div>
            <div class="container">
               <div class="cbox-desc">
                  <div class="vip-content">
                     <ul>
                        <li>
                           <div class="vip-info">
                              <div class="iconholder"><i class="zmdi zmdi-layers zmdi-hc-fw"></i></div>
                              <div class="descholder">
                                 <h6>Earn Points</h6>
                                 <p>"Earn 100 iwasinqatar credit points"</p>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="vip-info">
                              <div class="iconholder"><i class="zmdi zmdi-tag-more zmdi-hc-fw" ></i></div>
                              <div class="descholder">
                                 <h6>Discount on Ads</h6>
                                 <p>"get 20% discount on  ads or sponsored ads"</p>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="vip-info">
                              <div class="iconholder"><i class="zmdi zmdi-share zmdi-hc-fw" ></i></div>
                              <div class="descholder">
                                 <h6>Share Plans</h6>
                                 <p>Automatically share your travel plans with the people who are traveling to the same destination</p>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="vip-info">
                              <div class="iconholder"><i class="zmdi zmdi-refresh-alt"></i></div>
                              <div class="descholder">
                                 <h6>Automatic Updates</h6>
                                 <p>Get updates about people traveling to the same place as you or  people traveling to your hometown.</p>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="vip-info">
                              <div class="iconholder"><i class="zmdi zmdi-badge-check"></i></div>
                              <div class="descholder">
                                 <h6>VIP and Verified Badge</h6>
                                 <p>VIP member photo will display verified badge,  verified people connect much faster</p>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="vip-info">
                              <div class="iconholder"><i class="zmdi zmdi-format-list-bulleted"></i></div>
                              <div class="descholder">
                                 <h6>Get Listed  First</h6>
                                 <p>Your name and photo will be listed first on the many iwasinqatar listing</p>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="vip-info">
                              <div class="iconholder"><i class="zmdi zmdi-mail-send"></i></div>
                              <div class="descholder">
                                 <h6>Send Message</h6>
                                 <p>Send message to none  connect members Inbox</p>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="vip-info">
                              <div class="iconholder"><i class="zmdi zmdi-comment-text-alt"></i></div>
                              <div class="descholder">
                                 <h6>Message Read</h6>
                                 <p>Check if message you sent have been read</p>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="vip-info">
                              <div class="iconholder"><i class="zmdi zmdi-eye"></i></div>
                              <div class="descholder">
                                 <h6>Profile viewer</h6>
                                 <p>Find out who looks at your profile</p>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="vip-info">
                              <div class="iconholder"><i class="zmdi zmdi-case"></i></div>
                              <div class="descholder">
                                 <h6>Business Listed First</h6>
                                 <p>Your business pages or collections will listed first</p>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="vip-info">
                              <div class="iconholder"><i class="zmdi zmdi-headset"></i></div>
                              <div class="descholder">
                                 <h6>Premium Support</h6>
                                 <p>Access VIP only chat and email support</p>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="vip-info onlycap">
                              <div class="iconholder"><i class="zmdi zmdi-money"></i></div>
                              <div class="descholder">
                                 <h6>For only $9 monthly</h6>
                              </div>
                           </div>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php include('common/chat.php'); ?>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>	

<div id="payment-popup" class="modal payment-popup credit-payment-modal fullpopup">
<?php include('common/credit_payment_popup.php'); ?>
</div>

<?php include("script.php"); ?>
</body>
</html>