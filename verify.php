<?php include("header.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div>
</div>
<div class="clear"></div>
<div class="fixed-layout ipad-mfix">
   <?php include("common/leftmenu.php"); ?>
   <div class="main-content with-lmenu sub-page verify-page main-page">
      <div class="combined-column wide-open">
         <div class="content-box">
            <div class="banner-section">
               <div class="container">
                  <h4>Get verified and establish trust</h4>
                  <p>Verified people connect with travellers faster</p>
               </div>
            </div>
            <div class="container">
               <div class="cbox-desc">
                  <div class="row">
                     <div class="col l6 m12 s12">
                        <div class="verify-content">
                           <h5>Why Verify?</h5>
                           <ul>
                              <li>
                                 <div class="verify-info">
                                    <div class="iconholder"><img src="images/connect.png"/></div>
                                    <div class="descholder">
                                       <h6>Connect Faster</h6>
                                       <p>Verified members can connect with others at a faster rate</p>
                                    </div>
                                 </div>
                              </li>
                              <li>
                                 <div class="verify-info">
                                    <div class="iconholder"><img src="images/earnverified.png"/></div>
                                    <div class="descholder">
                                       <h6>Earn Verified check</h6>
                                       <p>Verified will be noticed, travelers look to see if you are verified</p>
                                    </div>
                                 </div>
                              </li>
                              <li>
                                 <div class="verify-info">
                                    <div class="iconholder"><img src="images/verifieduser.png"/></div>
                                    <div class="descholder">
                                       <p class="darktext">"while I was considering travel buddy, verification was the first thing I looked at."</p>
                                       <a href="javascript:void(0)">- Carolina Brain</a>
                                    </div>
                                 </div>
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col l6 m12 s12">
                        <div class="gray-section">
                           <h4>Verify with Iwasinqatar Now</h4>
                           <p>Just $10 a year earns you trust with iwasinqatar travelers</p>
                           <img src="images/verify-icon.png"/>
                           <a class="waves-effect waves-light btn modal-trigger" href="#payment-popup">Get Verified</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php include('common/chat.php'); ?>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>	 

<div id="payment-popup" class="modal payment-popup credit-payment-modal fullpopup">
<?php include('common/credit_payment_popup.php'); ?>
</div>

<?php include("script.php"); ?>
</body>
</html>