
<?php include("header.php"); ?>
<?php include("common/menu.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div>
</div>
<div class="clear"></div> 
<div class="container page_container pages_container">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout ipad-mfix">
      <div class="content-box nbg noboxshadow">
         <div class="hcontent-holder home-section gray-section tours-page tours dine-local dine-inner-pages">
            <div class="container mt-10">
               <div class="tours-section">
                  <div class="row mx-0 valign-wrapper label-head">
                     <div class="py-20 left">
                        <h3 class="heading-inner">HOMESTAY</h3>
                        <p class="para-inner">Experience magic homestay with locals</p>
                     </div>
                     <div class="ml-auto">
                        <a href="javascript:void(0)" class="homestayCreateAction"> 
                           <span class="hidden-sm">BECOME</span> HOST
                        </a>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col l3 m6 s12 wow slideInLeft">
                        <div class="tour-box">
                           <span class="imgholder">
                              <a href="homestay-detail.php">
                                 <img src="images/home-tour1.jpg">
                                 <i class="mdi mdi-delete"></i>
                              </a>
                              <div class="price-tag"><span> €70 </span></div>
                           </span>
                           <span class="descholder">
                              <a href="">
                                 <img src="https://screen-api.eatwith.com/files/478854/-/scale_crop/68x68/center/-/progressive/yes/" alt="">
                              </a>
                              <small class="dine-hosttext">Hosted by <a dir="auto" href="">Tino</a> in Amsterdam</small>
                              <div class="dine-eventtags">
                                 <div class="tag-inner">one room shared bath</div>
                              </div>
                              <a class="dine-eventtitle" dir="auto" href="">Title of the home stay</a>
                              <div class="dine-rating pt-10 center">
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                              </div>
                           </span>
                        </div>
                     </div>
                     <div class="col l3 m6 s12 wow slideInLeft">
                        <div class="tour-box">
                           <span class="imgholder">
                              <a href="homestay-detail.php">
                                 <img src="images/home-tour1.jpg">
                                 <i class="mdi mdi-delete"></i>
                              </a>
                              <div class="price-tag"><span> €70 </span></div>
                           </span>
                           <span class="descholder">
                              <a href="">
                                 <img src="https://screen-api.eatwith.com/files/478854/-/scale_crop/68x68/center/-/progressive/yes/" alt="">
                              </a>
                              <small class="dine-hosttext">Hosted by <a dir="auto" href="">Tino</a> in Amsterdam</small>
                              <div class="dine-eventtags">
                                 <div class="tag-inner">one room shared bath</div>
                              </div>
                              <a class="dine-eventtitle" dir="auto" href="">Title of the home stay</a>
                              <div class="dine-rating pt-10 center">
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                              </div>
                           </span>
                        </div>
                     </div>
                     <div class="col l3 m6 s12 wow slideInLeft">
                        <div class="tour-box">
                           <span class="imgholder">
                              <a href="homestay-detail.php">
                                 <img src="images/home-tour1.jpg">
                                 <i class="mdi mdi-delete"></i>
                              </a>
                              <div class="price-tag"><span> €70 </span></div>
                           </span>
                           <span class="descholder">
                              <a href="">
                                 <img src="https://screen-api.eatwith.com/files/478854/-/scale_crop/68x68/center/-/progressive/yes/" alt="">
                              </a>
                              <small class="dine-hosttext">Hosted by <a dir="auto" href="">Tino</a> in Amsterdam</small>
                              <div class="dine-eventtags">
                                 <div class="tag-inner">one room shared bath</div>
                              </div>
                              <a class="dine-eventtitle" dir="auto" href="">Title of the home stay</a>
                              <div class="dine-rating pt-10 center">
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                              </div>
                           </span>
                        </div>
                     </div>
                     <div class="col l3 m6 s12 wow slideInLeft">
                        <div class="tour-box">
                           <span class="imgholder">
                              <a href="homestay-detail.php">
                                 <img src="images/home-tour1.jpg">
                                 <i class="mdi mdi-delete"></i>
                              </a>
                              <div class="price-tag"><span> €70 </span></div>
                           </span>
                           <span class="descholder">
                              <a href="">
                                 <img src="https://screen-api.eatwith.com/files/478854/-/scale_crop/68x68/center/-/progressive/yes/" alt="">
                              </a>
                              <small class="dine-hosttext">Hosted by <a dir="auto" href="">Tino</a> in Amsterdam</small>
                              <div class="dine-eventtags">
                                 <div class="tag-inner">one room shared bath</div>
                              </div>
                              <a class="dine-eventtitle" dir="auto" href="">Title of the home stay</a>
                              <div class="dine-rating pt-10 center">
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                              </div>
                           </span>
                        </div>
                     </div>
                  </div>
                  <div class="new-post-mobile clear homestayCreateAction">
                     <a href="javascript:void(0)" class="popup-window" ><i class="mdi mdi-account"></i></a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>
<!--add page modal-->
<div id="add-item-popup" class="modal add-item-popup custom_md_modal collec_main_cus add_collection_new dropdownheight145">
   <div class="modal_content_container">
      <div class="modal_content_child modal-content">
         <div class="popup-title ">
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>			 
            <h3>Create a page</h3>
            <a type="button" class="item_done crop_done hidden_close_span close_modal waves-effect" href="javascript:void(0)" >Done</a>
         </div>
         <div class="main-pcontent">
            <form class="add-item-form">
               <div class="frow frowfull">
                  <div class="crop-holder" id="image-cropper">
                     <div class="cropit-preview"></div>
                     <div class="main-img">
                        <img src="images/additem-collections.png" class="ui-corner-all"/>
                     </div>
                     <div class="main-img1">
                        <img id="imageid" draggable="false"/>
                     </div>
                     <div class="btnupload custom_up_load" id="upload_img_action">
                        <div class="fileUpload">
                           <i class="zmdi zmdi-camera"></i>
                           <input type="file" name="filupload" id="crop-file" class="upload cropit-image-input" />
                        </div>
                     </div>
                     <a  href="javascript:void(0)" class="btn btn-save image_save_btn image_save dis-none">
                     <span class="zmdi zmdi-check"></span>
                     </a>
                     <a id="removeimg" href="javascript:void(0)" class="collection_image_trash image_trash">
                     <i class="mdi mdi-close" ></i>
                     </a>
                  </div>
               </div>
               <div class="sidepad">
                  <div class="frow">
                     <input id="page_title" type="text" class="validate item_title" placeholder="Page title" />					
                  </div>
                  <div class="frow dropdown782">
                     <select id="pageCatDrop1" class="pageservices" data-fill="n" data-action="pageservices" data-selectore="pageservices">
                     <?php
                        $page = array("Bags/Luggage" => "Bags/Luggage", "Camera/Photo" => "Camera/Photo", "Cars" => "Cars", "Clothing" => "Clothing", "Entertainment" => "Entertainment", "Professional Services" => "Professional Services", "Sporting Goods" => "Sporting Goods", "Kitchen/Cooking" => "Kitchen/Cooking", "Concert Tour" => "Concert Tour", "Concert Venue" => "Concert Venue", "Food/Beverages" => "Food/Beverages", "Outdoor Gear" => "Outdoor Gear", "Tour Operator" => "Tour Operator", "Travel Agency" => "Travel Agency", "Travel Services" => "Travel Services", "Attractions/Things to Do" => "Attractions/Things to Do", "Event Planning/Event Services" => "Event Planning/Event Services", "Hotel" => "Hotel", "Landmark" => "Landmark", "Movie Theater" => "Movie Theater", "Museum/Art gallery" => "Museum/Art gallery", "Outdoor Gear/Sporting Goods" => "Outdoor Gear/Sporting Goods", "Public Places" => "Public Places", "Travel Site" => "Travel Site", "Travel Destination" => "Travel Destination", "Organization" => "Organization", "Website" => "Website");
                        foreach ($page as $s9032n) {
                          echo "<option value=".$s9032n.">$s9032n</option>";
                        }
                     ?>
                     </select>
                  </div>
                  <div class="frow">
                     <textarea id="Collection_tagline" class="materialize-textarea mb0 md_textarea item_tagline" placeholder="Short description of your page"></textarea>
                     <span class="char-limit">0/80</span>								
                  </div>
                  <div class="frow">
                     <textarea type="text" placeholder="Tell people more about the page" class="materialize-textarea md_textarea item_about"></textarea>
                  </div>
                  <div class="frow">
                     <input type="text" id="compose_mapmodalAction" class="validate item_title compose_mapmodalAction" placeholder="Bussines address 'City/Country'" data-query="all" onfocus="filderMapLocationModal(this)" autocomplete="off">
                  </div>
                  <div class="frow">
                     <input id="extweb" type="text" class="validate item_title" placeholder="List your external website, if you have one" />
                  </div>
                  <div class="frow">
                     <span class="icon-span"><input type="radio" id="agreeemailpage" name="verify-radio"></span>
                     <p>Veryfiy ownership by sending  a text message to following email</p>
                  </div>
                  <div class="frow">
                     <input type="text" placeholder="Your company email address">
                  </div>
                  <div class="frow">
                     <input type="checkbox" id="create_page" />
                     <label for="create_page">I verify that I am the official representative of this entity and have the right to act on behalf of my entity in the creation of this page.</label>						
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="valign-wrapper additem_modal_footer modal-footer">
      <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">Cancel</a>
      <a href="javascript:void(0)" class="btngen-center-align waves-effect">Create</a>
   </div>
</div>

<!-- edit data modal -->
<div id="homestayCreateModal" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose event-detail-modal">
   <div class="modal_content_container">
      <div class="modal_content_child modal-content">
         <div class="popup-title ">
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>         
            <h3>Homestay detail</h3>
            <a type="button" class="item_done crop_done hidden_close_span custom_close waves-effect" href="javascript:void(0)">Done</a>
         </div>
         <div class="custom_modal_content modal_content" id="createpopup">
            <div class="ablum-yours profile-tab">
               <div class="ablum-box detail-box">
                  <div class="content-holder main-holder">
                     <div class="summery">
                        <div class="dsection bborder expandable-holder expanded">
                           <div class="form-area expandable-area">
                              <form class="ablum-form">
                                 <div class="form-box">
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Title</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <input type="text" placeholder="Homestay title: i.e One room for homestay" class="fullwidth locinput "/>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="row">
                                          <div class="col s6">
                                             <div class="frow pr-5">
                                                <div class="caption-holder">
                                                   <label>Property type</label>
                                                </div>
                                                <a class="dropdown_text dropdown-button-left" href="javascript:void(0)" data-activates="homestayProp">
                                                   <span>House</span>
                                                   <i class="zmdi zmdi-caret-down"></i>
                                                </a>
                                                <ul id="homestayProp" class="dropdown-privacy dropdown-content custom_dropdown">
                                                   <li>
                                                      <a href="javascript:void(0)">House</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Apartment</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Condominium</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Farmstay</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Houseboat</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Bed and breakfast</a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder mb-5">
                                             <label>What will guests have?</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="detail-holder inline-radio">
                                                <input name="licensedguy" checked="" type="radio" id="enPl" value="enPl">
                                                <label for="enPl">Entire place</label>
                                                <input name="licensedguy" type="radio" id="prRm" value="prRm">
                                                <label for="prRm">Private room</label>
                                                <input name="licensedguy" type="radio" id="shRm" value="shRm">
                                                <label for="shRm">Shared room</label>     
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv mb-5">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Bath</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="detail-holder inline-radio">
                                                <input name="bath" checked="" type="radio" id="prBt" value="prBt">
                                                <label for="prBt">private</label>
                                                <input name="bath" type="radio" id="shBt" value="shBt">
                                                <label for="shBt">Shared</label>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Welcomed guest</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="fitem">
                                                <div class="h-checkbox entertosend leftbox">
                                                   <input id="males" type="checkbox">
                                                   <label for="males">Males</label>
                                                </div>
                                             </div>
                                             <div class="fitem">
                                                <div class="h-checkbox entertosend leftbox">
                                                   <input id="females" type="checkbox">
                                                   <label for="females">Females</label>
                                                </div>
                                             </div>
                                             <div class="fitem">
                                                <div class="h-checkbox entertosend leftbox">
                                                   <input id="couples" type="checkbox">
                                                   <label for="couples">Couples</label>
                                                </div>
                                             </div>
                                             <div class="fitem">
                                                <div class="h-checkbox entertosend leftbox">
                                                   <input id="families" type="checkbox">
                                                   <label for="families">Families</label>
                                                </div>
                                             </div>
                                             <div class="fitem">
                                                <div class="h-checkbox entertosend leftbox">
                                                   <input id="students" type="checkbox">
                                                   <label for="students">Students</label>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Homestay location</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <input type="text" placeholder="Enter city name" class="fullwidth locinput" data-query="all" onfocus="filderMapLocationModal(this)"id="createlocation"/>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <div class="row">
                                                <div class="col s4">
                                                   <label>Rate per adult guest</label>
                                                </div>
                                                <div class="col s6">
                                                   <div class="detail-holder">
                                                      <div class="input-field">
                                                         <input type="text" placeholder="20" class="fullwidth input-rate" id="createlocation"/>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col s2">
                                                   <a class="dropdown_text dropdown-button currency_drp" href="javascript:void(0)" data-activates="currency_handler">
                                                      <span class="currency_label">USD</span>
                                                      <i class="zmdi zmdi-caret-down"></i>
                                                   </a>
                                                   <ul id="currency_handler" class="dropdown-privacy dropdown-content custom_dropdown">
                                                      <?php
                                                      $fee = array("USD", "EUR", "YEN", "CAD", "AUE");
                                                      foreach ($fee as $s8032n) {
                                                         ?>
                                                         <li> <a href="javascript:void(0)"><?=$s8032n?></a> </li>
                                                         <?php
                                                      }
                                                      ?>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Descibe your homestay</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <input type="text" placeholder="Tell people about your Homestay"/>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Homestay rules</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <input type="text" placeholder="Tell your guest about your rules for homestay" class="fullwidth locinput "/>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder mb-10">
                                             <label>Services</label>
                                          </div>
                                          <div class="detail-holder">
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Waste tank discharge" title="Waste tank discharge" src="images/services-icon/1.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Public lavatory" title="Public lavatory" src="images/services-icon/2.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Walking path" title="Walking path" src="images/services-icon/3.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Swimming pool" title="Swimming pool" src="images/services-icon/4.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Fishing permits" title="Fishing permits" src="images/services-icon/5.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Cooking facilities" title="Cooking facilities" src="images/services-icon/6.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Sports hall" title="Sports hall" src="images/services-icon/7.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Washing machine" title="Washing machine" src="images/services-icon/8.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Hot pot" title="Hot pot" src="images/services-icon/9.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Sports field" title="Sports field" src="images/services-icon/10.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Shower" title="Shower" src="images/services-icon/11.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Golf course" title="Golf course" src="images/services-icon/12.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Sauna" title="Sauna" src="images/services-icon/13.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Play ground" title="Play ground" src="images/services-icon/14.png">
                                             </a>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="frow nomargin new-post">
                                       <div class="caption-holder">
                                          <label>Awesome photos help guests want to join up</label>
                                       </div>
                                       <div class="detail-holder">
                                          <div class="input-field ">
                                             <div class="post-photos new_pic_add">
                                                <div class="img-row">
                                                   <div class="img-box">
                                                      <div class="custom-file addimg-box add-photo ablum-add">
                                                         <span class="icont">+</span><br><span class="">Upload photo</span>
                                                         <div class="addimg-icon">
                                                         </div>
                                                         <input class="upload custom-upload remove-custom-upload" title="Choose a file to upload" required="" data-class=".post-photos .img-row" multiple="true" type="file">
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <p class="photolabelinfo">Please add three cover photos for your homestay profile</p>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="valign-wrapper additem_modal_footer modal-footer">
      <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">Cancel</a>
      <a href="javascript:void(0)" class="btngen-center-align waves-effect">Publish</a>
   </div>
</div>

<?php include('common/discard_popup.php'); ?>
<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">
   <?php include('common/map_modal.php'); ?>
</div>
<?php include("script.php"); ?>			
</body>
</html>