<?php include("header.php"); ?>
<?php include("common/menu.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon">
         <span class="icon-holder ispan">
            <i class="mdi mdi-arrow-up-bold-circle"></i>
         </span>
      </div>
   </div>
</div>
<div class="clear"></div>
<div class="">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout">
      <div class="main-content main-page places-page pb-0 m-t-50">
         <div class="combined-column wide-open main-page full-page">
         <div class="tablist sub-tabs">
            <ul class="tabs tabs-fixed-width text-menu left tabsnew">
               <li class="tab"><a tabname="Wall" href="#places-all"></a></li>
            </ul>
         </div>
         <div class="places-content places-all">
            <div class="container cshfsiput cshfsi">
               <div class="places-column cshfsiput cshfsi m-top">
                  <div class="tab-content">
                     <div id="places-tip" class="placestip-content subtab bottom_tabs">
                        <div class="row cshfsiput cshfsi">
                           <div class="social-section profile-box detailBox">
                              <div class="user-profile">
                                 <div class="avatar-left">
                                    <img src="images/recent-3.png">
                                 </div>
                                 <div class="avatar-middle">
                                    <img src="images/recent-3.png">
                                 </div>
                                 <div class="avatar-right">
                                    <img src="images/recent-3.png">
                                 </div>
                              </div>
                             <div class="item titlelabel user-info">
                               <p class="center-align u-name mb0">Questions for Qatar (<span class="count">10</span>)</p>
                               <p class="user-desg center-align"></p>
                             </div>
                             <div class="item profile-info row mx-0 width-100">
                                 <div class="sub-item">
                                       <h5 class="">Discussion</h5> 
                                       <p class="">20</p>
                                   </div>
                                 <div class="sub-item">
                                    <h5 class="">Tips</h5>
                                    <p class="">20</p>
                                 </div>
                                 <div class="sub-item">
                                    <h5 class="">Reviews</h5>
                                    <p class="">20</p>
                                 </div>
                                 <div class="sub-item">
                                    <h5 class="">Photostream</PB_photosh5>
                                    <p class="">20</p>
                                 </div>
                                 <div class="sub-item">
                                    <h5 class="">Locals</h5>
                                    <p class="">20</p>
                                 </div>
                                 <div class="sub-item">
                                    <h5 class="">Travellers</h5>
                                    <p class="">20</p>
                                 </div>
                              </div>   
                           </div>
                           <div class="postBox">
                              <div class="content-box ">
                                 <div class="new-post base-newpost compose_newask cshfsiput cshfsi">
                                    <div class="npost-content">
                                       <div class="post-mcontent">
                                          <i class="mdi mdi-pencil-box-outline main-icon"></i>
                                          <div class="desc">
                                             <div class="input-field comments_box">
                                                <p>Ask a question</p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="cbox-desc nm-postlist post-list cshfsiput cshfsi">
                                    <div class="post-holder bborder reviewpost-holder">
                                       <div class="post-topbar">
                                          <div class="post-userinfo">
                                             <div class="img-holder">
                                                <div id="profiletip-1" class="profiletipholder">
                                                   <span class="profile-tooltip">
                                                   <img class="circle" src="images/demo-profile.jpg" />
                                                   </span>
                                                   <span class="profiletooltip_content">
                                                      <div class="profile-tip">
                                                         <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                         <div class="profile-tip-avatar">
                                                            <a href="javascript:void(0)">
                                                            <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                            </a>
                                                         </div>
                                                         <div class="profile-tip-info">
                                                            <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                            <div class="cover-headline">
                                                               <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                               Web Designer, Cricketer
                                                            </div>
                                                            <div class="profiletip-bio">
                                                               <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                               Lives in : <span>Gariyadhar</span>
                                                            </div>
                                                            <div class="profiletip-bio">
                                                               <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                               Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                            </div>
                                                         </div>
                                                         <div class="profile-tip-divider"></div>
                                                         <div class="profile-tip-btn">
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                         </div>
                                                      </div>
                                                   </span>
                                                </div>
                                             </div>
                                             <div class="desc-holder">
                                                <a href="javascript:void(0)">Adel Hasanat</a> has a question about <a class="sub-link" href="javascript:void(0)">Qatar</a>
                                                <span class="timestamp">August 31 at 08:45 pm<span class="glyphicon glyphicon-globe"></span></span>
                                             </div>
                                          </div>
                                          <div class="settings-icon">
                                             <div class="dropdown">
                                                <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="dropdown-editques">
                                                <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                </a>
                                                <ul class="dropdown-content custom_dropdown" id="dropdown-editques">
                                                   <li><a href="javascript:void(0)" class="edit_newask">Edit question</a></li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="post-content">
                                          <div class="post-details">
                                             <div class="post-title">Is veg food easily available?</div>
                                             <div class="post-desc">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.?</p>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="post-data">
                                          <div class="post-actions">
                                             <div class="right like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like" data-title="User Name"><i class="zmdi zmdi-thumb-up"></i></a>
                                                <span class="lcount">4</span>
                                                <a href="javascript:void(0)" class="pa-comment"><i class="zmdi zmdi-comment"></i></a>
                                                <span class="comment-lcount">4</span>
                                             </div>
                                          </div>
                                          <div class="comments-section panel">
                                             <div class="comments-area">
                                                <div class="post-more">
                                                   <a href="javascript:void(0)" class="view-morec">View more comments</a>
                                                   <span class="total-comments">3 of 7</span>
                                                </div>
                                                <div class="post-comments">
                                                   <div class="pcomments">
                                                      <div class="pcomment-earlier">
                                                         <div class="pcomment-holder">
                                                            <div class="pcomment main-comment">
                                                               <div class="img-holder">
                                                                  <div id="commentptip-1" class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/demo-profile.jpg" />
                                                                     </span>
                                                                     <span class="profiletooltip_content">
                                                                        <div class="profile-tip">
                                                                           <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                           <div class="profile-tip-avatar">
                                                                              <a href="javascript:void(0)">
                                                                              <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                              </a>
                                                                           </div>
                                                                           <div class="profile-tip-info">
                                                                              <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                              <div class="cover-headline">
                                                                                 <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                                 Web Designer, Cricketer
                                                                              </div>
                                                                              <div class="profiletip-bio">
                                                                                 <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                                 Lives in : <span>Gariyadhar</span>
                                                                              </div>
                                                                              <div class="profiletip-bio">
                                                                                 <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                                 Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                              </div>
                                                                           </div>
                                                                           <div class="profile-tip-divider"></div>
                                                                           <div class="profile-tip-btn">
                                                                              <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                           </div>
                                                                        </div>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <div class="normal-mode">
                                                                     <div class="desc">
                                                                        <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="comment-stuff">
                                                                        <div class="more-opt">
                                                                           <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                           <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                           <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                              <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask1">
                                                                              <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                              </a>
                                                                              <ul class="dropdown-content" id="dropdown-editdeleteask1">
                                                                                 <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                                 <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                              </ul>
                                                                           </div>
                                                                        </div>
                                                                        <div class="less-opt">
                                                                           <div class="timestamp">8h</div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="edit-mode">
                                                                     <div class="desc">
                                                                        <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="clear"></div>
                                                            <div class="comment-reply-holder comment-addreply">
                                                               <div class="addnew-comment valign-wrapper comment-reply">
                                                                  <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                                  <div class="desc-holder">
                                                                     <div class="sliding-middle-custom anim-area">
                                                                        <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="pcomment-holder">
                                                            <div class="pcomment main-comment">
                                                               <div class="img-holder">
                                                                  <div id="commentptip-2" class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/demo-profile.jpg" />
                                                                     </span>
                                                                     <span class="profiletooltip_content">
                                                                        <div class="profile-tip">
                                                                           <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                           <div class="profile-tip-avatar">
                                                                              <a href="javascript:void(0)">
                                                                              <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                              </a>
                                                                           </div>
                                                                           <div class="profile-tip-info">
                                                                              <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                              <div class="cover-headline">
                                                                                 <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                                 Web Designer, Cricketer
                                                                              </div>
                                                                              <div class="profiletip-bio">
                                                                                 <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                                 Lives in : <span>Gariyadhar</span>
                                                                              </div>
                                                                              <div class="profiletip-bio">
                                                                                 <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                                 Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                              </div>
                                                                           </div>
                                                                           <div class="profile-tip-divider"></div>
                                                                           <div class="profile-tip-btn">
                                                                              <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                           </div>
                                                                        </div>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <div class="normal-mode">
                                                                     <div class="desc">
                                                                        <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="comment-stuff">
                                                                        <div class="more-opt">
                                                                           <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                           <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                           <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                              <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask2">
                                                                              <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                              </a>
                                                                              <ul class="dropdown-content" id="dropdown-editdeleteask2">
                                                                                 <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                                 <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                              </ul>
                                                                           </div>
                                                                        </div>
                                                                        <div class="less-opt">
                                                                           <div class="timestamp">8h</div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="edit-mode">
                                                                     <div class="desc">
                                                                        <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="clear"></div>
                                                            <div class="comment-reply-holder comment-addreply">
                                                               <div class="addnew-comment valign-wrapper comment-reply">
                                                                  <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                                  <div class="desc-holder">
                                                                     <div class="sliding-middle-custom anim-area">
                                                                        <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="pcomment-holder has-comments">
                                                         <div class="pcomment main-comment">
                                                            <div class="img-holder">
                                                               <div id="commentptip-3" class="profiletipholder">
                                                                  <span class="profile-tooltip">
                                                                  <img class="circle" src="images/demo-profile.jpg" />
                                                                  </span>
                                                                  <span class="profiletooltip_content">
                                                                     <div class="profile-tip">
                                                                        <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                        <div class="profile-tip-avatar">
                                                                           <a href="javascript:void(0)">
                                                                           <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                           </a>
                                                                        </div>
                                                                        <div class="profile-tip-info">
                                                                           <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                           <div class="cover-headline">
                                                                              <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                              Web Designer, Cricketer
                                                                           </div>
                                                                           <div class="profiletip-bio">
                                                                              <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                              Lives in : <span>Gariyadhar</span>
                                                                           </div>
                                                                           <div class="profiletip-bio">
                                                                              <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                              Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                           </div>
                                                                        </div>
                                                                        <div class="profile-tip-divider"></div>
                                                                        <div class="profile-tip-btn">
                                                                           <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                        </div>
                                                                     </div>
                                                                  </span>
                                                               </div>
                                                            </div>
                                                            <div class="desc-holder">
                                                               <div class="normal-mode">
                                                                  <div class="desc">
                                                                     <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                  </div>
                                                                  <div class="comment-stuff">
                                                                     <div class="more-opt">
                                                                        <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                        <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                        <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                           <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask3">
                                                                           <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                           </a>
                                                                           <ul class="dropdown-content" id="dropdown-editdeleteask3">
                                                                              <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                              <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                           </ul>
                                                                        </div>
                                                                     </div>
                                                                     <div class="less-opt">
                                                                        <div class="timestamp">8h</div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="edit-mode">
                                                                  <div class="desc">
                                                                     <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="clear"></div>
                                                         <div class="comment-reply-holder">
                                                            <div class="pcomment comment-reply">
                                                               <div class="img-holder">
                                                                  <div id="commentptip-5" class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/demo-profile.jpg" />
                                                                     </span>
                                                                     <span class="profiletooltip_content">
                                                                        <div class="profile-tip">
                                                                           <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                           <div class="profile-tip-avatar">
                                                                              <a href="javascript:void(0)">
                                                                              <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                              </a>
                                                                           </div>
                                                                           <div class="profile-tip-info">
                                                                              <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                              <div class="cover-headline">
                                                                                 <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                                 Web Designer, Cricketer
                                                                              </div>
                                                                              <div class="profiletip-bio">
                                                                                 <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                                 Lives in : <span>Gariyadhar</span>
                                                                              </div>
                                                                              <div class="profiletip-bio">
                                                                                 <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                                 Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                              </div>
                                                                           </div>
                                                                           <div class="profile-tip-divider"></div>
                                                                           <div class="profile-tip-btn">
                                                                              <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                           </div>
                                                                        </div>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <div class="normal-mode">
                                                                     <div class="desc">
                                                                        <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="comment-stuff">
                                                                        <div class="more-opt">
                                                                           <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                           <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                           <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                              <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask4">
                                                                              <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                              </a>
                                                                              <ul class="dropdown-content" id="dropdown-editdeleteask4">
                                                                                 <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                                 <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                              </ul>
                                                                           </div>
                                                                        </div>
                                                                        <div class="less-opt">
                                                                           <div class="timestamp">8h</div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="edit-mode">
                                                                     <div class="desc">
                                                                        <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="pcomment comment-reply">
                                                               <div class="img-holder">
                                                                  <div id="commentptip-6" class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/demo-profile.jpg" />
                                                                     </span>
                                                                     <span class="profiletooltip_content">
                                                                        <div class="profile-tip">
                                                                           <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                           <div class="profile-tip-avatar">
                                                                              <a href="javascript:void(0)">
                                                                              <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                              </a>
                                                                           </div>
                                                                           <div class="profile-tip-info">
                                                                              <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                              <div class="cover-headline">
                                                                                 <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                                 Web Designer, Cricketer
                                                                              </div>
                                                                              <div class="profiletip-bio">
                                                                                 <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                                 Lives in : <span>Gariyadhar</span>
                                                                              </div>
                                                                              <div class="profiletip-bio">
                                                                                 <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                                 Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                              </div>
                                                                           </div>
                                                                           <div class="profile-tip-divider"></div>
                                                                           <div class="profile-tip-btn">
                                                                              <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                           </div>
                                                                        </div>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <div class="normal-mode">
                                                                     <div class="desc">
                                                                        <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                                                     </div>
                                                                     <div class="comment-stuff">
                                                                        <div class="more-opt">
                                                                           <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                           <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                           <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                              <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask5">
                                                                              <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                              </a>
                                                                              <ul class="dropdown-content" id="dropdown-editdeleteask5">
                                                                                 <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                                 <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                              </ul>
                                                                           </div>
                                                                        </div>
                                                                        <div class="less-opt">
                                                                           <div class="timestamp">8h</div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="edit-mode">
                                                                     <div class="desc">
                                                                        <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="comment-reply-holder comment-addreply">
                                                            <div class="addnew-comment valign-wrapper comment-reply">
                                                               <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                               <div class="desc-holder">
                                                                  <div class="sliding-middle-custom anim-area">
                                                                     <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="pcomment-holder">
                                                         <div class="pcomment main-comment">
                                                            <div class="img-holder">
                                                               <div id="commentptip-4" class="profiletipholder">
                                                                  <span class="profile-tooltip">
                                                                  <img class="circle" src="images/demo-profile.jpg" />
                                                                  </span>
                                                                  <span class="profiletooltip_content">
                                                                     <div class="profile-tip">
                                                                        <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                        <div class="profile-tip-avatar">
                                                                           <a href="javascript:void(0)">
                                                                           <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                           </a>
                                                                        </div>
                                                                        <div class="profile-tip-info">
                                                                           <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                           <div class="cover-headline">
                                                                              <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                              Web Designer, Cricketer
                                                                           </div>
                                                                           <div class="profiletip-bio">
                                                                              <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                              Lives in : <span>Gariyadhar</span>
                                                                           </div>
                                                                           <div class="profiletip-bio">
                                                                              <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                              Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                           </div>
                                                                        </div>
                                                                        <div class="profile-tip-divider"></div>
                                                                        <div class="profile-tip-btn">
                                                                           <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                        </div>
                                                                     </div>
                                                                  </span>
                                                               </div>
                                                            </div>
                                                            <div class="desc-holder">
                                                               <div class="normal-mode">
                                                                  <div class="desc">
                                                                     <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                  </div>
                                                                  <div class="comment-stuff">
                                                                     <div class="more-opt">
                                                                        <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                        <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                        <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                           <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask6">
                                                                           <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                           </a>
                                                                           <ul class="dropdown-content" id="dropdown-editdeleteask6">
                                                                              <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                              <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                           </ul>
                                                                        </div>
                                                                     </div>
                                                                     <div class="less-opt">
                                                                        <div class="timestamp">8h</div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="edit-mode">
                                                                  <div class="desc">
                                                                     <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="clear"></div>
                                                         <div class="comment-reply-holder comment-addreply">
                                                            <div class="addnew-comment valign-wrapper comment-reply">
                                                               <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                               <div class="desc-holder">
                                                                  <div class="sliding-middle-custom anim-area">
                                                                     <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="addnew-comment valign-wrapper">
                                                      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                      <div class="desc-holder">
                                                         <div class="sliding-middle-out anim-area">
                                                            <textarea>Write a comment</textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="post-holder bborder reviewpost-holder">
                                       <div class="post-topbar">
                                          <div class="post-userinfo">
                                             <div class="img-holder">
                                                <div id="profiletip-1" class="profiletipholder">
                                                   <span class="profile-tooltip">
                                                   <img class="circle" src="images/demo-profile.jpg" />
                                                   </span>
                                                   <span class="profiletooltip_content">
                                                      <div class="profile-tip">
                                                         <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                         <div class="profile-tip-avatar">
                                                            <a href="javascript:void(0)">
                                                            <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                            </a>
                                                         </div>
                                                         <div class="profile-tip-info">
                                                            <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                            <div class="cover-headline">
                                                               <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                               Web Designer, Cricketer
                                                            </div>
                                                            <div class="profiletip-bio">
                                                               <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                               Lives in : <span>Gariyadhar</span>
                                                            </div>
                                                            <div class="profiletip-bio">
                                                               <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                               Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                            </div>
                                                         </div>
                                                         <div class="profile-tip-divider"></div>
                                                         <div class="profile-tip-btn">
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                         </div>
                                                      </div>
                                                   </span>
                                                </div>
                                             </div>
                                             <div class="desc-holder">
                                                <a href="javascript:void(0)">Nimish Parekh</a> has a question about <a class="sub-link" href="javascript:void(0)">Qatar</a>
                                                <span class="timestamp">August 31 at 08:45 pm<span class="glyphicon glyphicon-globe"></span></span>
                                             </div>
                                          </div>
                                          <div class="settings-icon">
                                             <div class="dropdown">
                                                <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="dropdown-editques2">
                                                <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                </a>
                                                <ul class="dropdown-content custom_dropdown" id="dropdown-editques2">
                                                   <li><a href="javascript:void(0)" class="edit_newask">Edit question</a></li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="post-content">
                                          <div class="post-details">
                                             <div class="post-desc">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.<br />Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec?</p>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="post-data">
                                          <div class="post-actions">
                                             <div class="right like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like" data-title="User Name"><i class="zmdi zmdi-thumb-up"></i></a>
                                                <span class="lcount">4</span>
                                                <a href="javascript:void(0)" class="pa-comment"><i class="zmdi zmdi-comment"></i></a>
                                                <span class="comment-lcount">4</span>
                                             </div>
                                          </div>
                                          <div class="comments-section panel">
                                             <div class="comments-area">
                                                <div class="post-more">
                                                   <a href="javascript:void(0)" class="view-morec">View more comments</a>
                                                   <span class="total-comments">3 of 7</span>
                                                </div>
                                                <div class="post-comments">
                                                   <div class="pcomments">
                                                      <div class="pcomment-earlier">
                                                         <div class="pcomment-holder">
                                                            <div class="pcomment main-comment">
                                                               <div class="img-holder">
                                                                  <div id="commentptip-1" class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/demo-profile.jpg" />
                                                                     </span>
                                                                     <span class="profiletooltip_content">
                                                                        <div class="profile-tip">
                                                                           <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                           <div class="profile-tip-avatar">
                                                                              <a href="javascript:void(0)">
                                                                              <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                              </a>
                                                                           </div>
                                                                           <div class="profile-tip-info">
                                                                              <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                              <div class="cover-headline">
                                                                                 <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                                 Web Designer, Cricketer
                                                                              </div>
                                                                              <div class="profiletip-bio">
                                                                                 <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                                 Lives in : <span>Gariyadhar</span>
                                                                              </div>
                                                                              <div class="profiletip-bio">
                                                                                 <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                                 Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                              </div>
                                                                           </div>
                                                                           <div class="profile-tip-divider"></div>
                                                                           <div class="profile-tip-btn">
                                                                              <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                           </div>
                                                                        </div>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <div class="normal-mode">
                                                                     <div class="desc">
                                                                        <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="comment-stuff">
                                                                        <div class="more-opt">
                                                                           <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                           <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                           <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                              <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask7">
                                                                              <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                              </a>
                                                                              <ul class="dropdown-content" id="dropdown-editdeleteask7">
                                                                                 <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                                 <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                              </ul>
                                                                           </div>
                                                                        </div>
                                                                        <div class="less-opt">
                                                                           <div class="timestamp">8h</div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="edit-mode">
                                                                     <div class="desc">
                                                                        <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="clear"></div>
                                                            <div class="comment-reply-holder comment-addreply">
                                                               <div class="addnew-comment valign-wrapper comment-reply">
                                                                  <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                                  <div class="desc-holder">
                                                                     <div class="sliding-middle-custom anim-area">
                                                                        <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="pcomment-holder">
                                                            <div class="pcomment main-comment">
                                                               <div class="img-holder">
                                                                  <div id="commentptip-2" class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/demo-profile.jpg" />
                                                                     </span>
                                                                     <span class="profiletooltip_content">
                                                                        <div class="profile-tip">
                                                                           <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                           <div class="profile-tip-avatar">
                                                                              <a href="javascript:void(0)">
                                                                              <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                              </a>
                                                                           </div>
                                                                           <div class="profile-tip-info">
                                                                              <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                              <div class="cover-headline">
                                                                                 <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                                 Web Designer, Cricketer
                                                                              </div>
                                                                              <div class="profiletip-bio">
                                                                                 <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                                 Lives in : <span>Gariyadhar</span>
                                                                              </div>
                                                                              <div class="profiletip-bio">
                                                                                 <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                                 Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                              </div>
                                                                           </div>
                                                                           <div class="profile-tip-divider"></div>
                                                                           <div class="profile-tip-btn">
                                                                              <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                           </div>
                                                                        </div>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <div class="normal-mode">
                                                                     <div class="desc">
                                                                        <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="comment-stuff">
                                                                        <div class="more-opt">
                                                                           <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                           <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                           <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                              <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask8">
                                                                              <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                              </a>
                                                                              <ul class="dropdown-content" id="dropdown-editdeleteask8">
                                                                                 <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                                 <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                              </ul>
                                                                           </div>
                                                                        </div>
                                                                        <div class="less-opt">
                                                                           <div class="timestamp">8h</div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="edit-mode">
                                                                     <div class="desc">
                                                                        <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="clear"></div>
                                                            <div class="comment-reply-holder comment-addreply">
                                                               <div class="addnew-comment valign-wrapper comment-reply">
                                                                  <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                                  <div class="desc-holder">
                                                                     <div class="sliding-middle-custom anim-area">
                                                                        <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="pcomment-holder has-comments">
                                                         <div class="pcomment main-comment">
                                                            <div class="img-holder">
                                                               <div id="commentptip-3" class="profiletipholder">
                                                                  <span class="profile-tooltip">
                                                                  <img class="circle" src="images/demo-profile.jpg" />
                                                                  </span>
                                                                  <span class="profiletooltip_content">
                                                                     <div class="profile-tip">
                                                                        <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                        <div class="profile-tip-avatar">
                                                                           <a href="javascript:void(0)">
                                                                           <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                           </a>
                                                                        </div>
                                                                        <div class="profile-tip-info">
                                                                           <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                           <div class="cover-headline">
                                                                              <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                              Web Designer, Cricketer
                                                                           </div>
                                                                           <div class="profiletip-bio">
                                                                              <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                              Lives in : <span>Gariyadhar</span>
                                                                           </div>
                                                                           <div class="profiletip-bio">
                                                                              <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                              Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                           </div>
                                                                        </div>
                                                                        <div class="profile-tip-divider"></div>
                                                                        <div class="profile-tip-btn">
                                                                           <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                        </div>
                                                                     </div>
                                                                  </span>
                                                               </div>
                                                            </div>
                                                            <div class="desc-holder">
                                                               <div class="normal-mode">
                                                                  <div class="desc">
                                                                     <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                  </div>
                                                                  <div class="comment-stuff">
                                                                     <div class="more-opt">
                                                                        <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                        <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                        <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                           <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask9">
                                                                           <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                           </a>
                                                                           <ul class="dropdown-content" id="dropdown-editdeleteask9">
                                                                              <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                              <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                           </ul>
                                                                        </div>
                                                                     </div>
                                                                     <div class="less-opt">
                                                                        <div class="timestamp">8h</div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="edit-mode">
                                                                  <div class="desc">
                                                                     <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="clear"></div>
                                                         <div class="comment-reply-holder">
                                                            <div class="pcomment comment-reply">
                                                               <div class="img-holder">
                                                                  <div id="commentptip-5" class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/demo-profile.jpg" />
                                                                     </span>
                                                                     <span class="profiletooltip_content">
                                                                        <div class="profile-tip">
                                                                           <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                           <div class="profile-tip-avatar">
                                                                              <a href="javascript:void(0)">
                                                                              <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                              </a>
                                                                           </div>
                                                                           <div class="profile-tip-info">
                                                                              <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                              <div class="cover-headline">
                                                                                 <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                                 Web Designer, Cricketer
                                                                              </div>
                                                                              <div class="profiletip-bio">
                                                                                 <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                                 Lives in : <span>Gariyadhar</span>
                                                                              </div>
                                                                              <div class="profiletip-bio">
                                                                                 <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                                 Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                              </div>
                                                                           </div>
                                                                           <div class="profile-tip-divider"></div>
                                                                           <div class="profile-tip-btn">
                                                                              <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                           </div>
                                                                        </div>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <div class="normal-mode">
                                                                     <div class="desc">
                                                                        <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="comment-stuff">
                                                                        <div class="more-opt">
                                                                           <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                           <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                           <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                              <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask10">
                                                                              <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                              </a>
                                                                              <ul class="dropdown-content" id="dropdown-editdeleteask10">
                                                                                 <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                                 <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                              </ul>
                                                                           </div>
                                                                        </div>
                                                                        <div class="less-opt">
                                                                           <div class="timestamp">8h</div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="edit-mode">
                                                                     <div class="desc">
                                                                        <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="pcomment comment-reply">
                                                               <div class="img-holder">
                                                                  <div id="commentptip-6" class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/demo-profile.jpg" />
                                                                     </span>
                                                                     <span class="profiletooltip_content">
                                                                        <div class="profile-tip">
                                                                           <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                           <div class="profile-tip-avatar">
                                                                              <a href="javascript:void(0)">
                                                                              <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                              </a>
                                                                           </div>
                                                                           <div class="profile-tip-info">
                                                                              <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                              <div class="cover-headline">
                                                                                 <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                                 Web Designer, Cricketer
                                                                              </div>
                                                                              <div class="profiletip-bio">
                                                                                 <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                                 Lives in : <span>Gariyadhar</span>
                                                                              </div>
                                                                              <div class="profiletip-bio">
                                                                                 <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                                 Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                              </div>
                                                                           </div>
                                                                           <div class="profile-tip-divider"></div>
                                                                           <div class="profile-tip-btn">
                                                                              <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                           </div>
                                                                        </div>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <div class="normal-mode">
                                                                     <div class="desc">
                                                                        <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                                                     </div>
                                                                     <div class="comment-stuff">
                                                                        <div class="more-opt">
                                                                           <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                           <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                           <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                              <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask11">
                                                                              <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                              </a>
                                                                              <ul class="dropdown-content" id="dropdown-editdeleteask11">
                                                                                 <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                                 <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                              </ul>
                                                                           </div>
                                                                        </div>
                                                                        <div class="less-opt">
                                                                           <div class="timestamp">8h</div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="edit-mode">
                                                                     <div class="desc">
                                                                        <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="comment-reply-holder comment-addreply">
                                                            <div class="addnew-comment valign-wrapper comment-reply">
                                                               <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                               <div class="desc-holder">
                                                                  <div class="sliding-middle-custom anim-area">
                                                                     <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="pcomment-holder">
                                                         <div class="pcomment main-comment">
                                                            <div class="img-holder">
                                                               <div id="commentptip-4" class="profiletipholder">
                                                                  <span class="profile-tooltip">
                                                                  <img class="circle" src="images/demo-profile.jpg" />
                                                                  </span>
                                                                  <span class="profiletooltip_content">
                                                                     <div class="profile-tip">
                                                                        <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                        <div class="profile-tip-avatar">
                                                                           <a href="javascript:void(0)">
                                                                           <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                           </a>
                                                                        </div>
                                                                        <div class="profile-tip-info">
                                                                           <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                           <div class="cover-headline">
                                                                              <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                              Web Designer, Cricketer
                                                                           </div>
                                                                           <div class="profiletip-bio">
                                                                              <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                              Lives in : <span>Gariyadhar</span>
                                                                           </div>
                                                                           <div class="profiletip-bio">
                                                                              <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                              Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                           </div>
                                                                        </div>
                                                                        <div class="profile-tip-divider"></div>
                                                                        <div class="profile-tip-btn">
                                                                           <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                        </div>
                                                                     </div>
                                                                  </span>
                                                               </div>
                                                            </div>
                                                            <div class="desc-holder">
                                                               <div class="normal-mode">
                                                                  <div class="desc">
                                                                     <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                  </div>
                                                                  <div class="comment-stuff">
                                                                     <div class="more-opt">
                                                                        <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                        <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                        <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                           <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask12">
                                                                           <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                           </a>
                                                                           <ul class="dropdown-content" id="dropdown-editdeleteask12">
                                                                              <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                              <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                           </ul>
                                                                        </div>
                                                                     </div>
                                                                     <div class="less-opt">
                                                                        <div class="timestamp">8h</div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="edit-mode">
                                                                  <div class="desc">
                                                                     <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="clear"></div>
                                                         <div class="comment-reply-holder comment-addreply">
                                                            <div class="addnew-comment valign-wrapper comment-reply">
                                                               <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                               <div class="desc-holder">
                                                                  <div class="sliding-middle-custom anim-area">
                                                                     <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="addnew-comment valign-wrapper">
                                                      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                      <div class="desc-holder">
                                                         <div class="sliding-middle-out anim-area">
                                                            <textarea>Write a comment</textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="new-post-mobile clear">
                                 <a href="javascript:void(0)" class="popup-window compose_newtrip" ><i class="mdi mdi-pencil"></i></a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <?php include('common/gen_wall_col.php'); ?>
            </div>
         </div>
         </div>
         </div>
         </div>

         </div>

         <!-- trip-->
         <!-- compose tool box modal -->
         <div id="compose_newtrip" class="modal compose_tool_box post-popup custom_modal main_modal new-wall-post set_re_height">
         <div class="hidden_header">
         <div class="content_header">
         <button class="close_span cancel_poup waves-effect">
         <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
         </button>
         <p class="modal_header_xs">Write trip</p>
         <a type="button" class="post_btn action_btn post_btn_xs close_modal waves-effect"  onclick="verify()">Post</a>
         </div>
         </div>
         <div class="modal-content">
         <div class="new-post active">
         <div class="top-stuff">
            <div class="postuser-info">
               <span class="img-holder"><img class="circle" src="images/demo-profile.jpg" /></span>
               <div class="desc-holder">
                  <span class="profile_name">Nimish Parekh</span>
               </div>
            </div>
            <div class="settings-icon">                     
               <a href="javascript:void(0)" onclick="clearPost()">
               <i class="zmdi zmdi-refresh-alt"></i>
               </a>
            </div>
         </div>
         <div class="clear"></div>
         <div class="scroll_div">
            <div class="npost-content">
               <div class="post-mcontent">
                  <div class="npost-title title_post_container">                                    
                     <input type="text" class="title" placeholder=" Help others with your valuable tip ">                                   
                  </div>
                  <div class="clear"></div>
                  <div class="desc">
                     <textarea id="new_post_comment" placeholder="Write your tip" class="materialize-textarea comment_textarea new_post_comment"></textarea>
                  </div>
               </div>
            </div>
         </div>
         </div>
         </div>
         <div class="modal-footer">
         <div class="new-post active">
         <div class="post-bcontent">
            <div class="footer_icon_container">
               <button class="comment_footer_icon waves-effect compose_titleAction" id="compose_titleAction">
               <img src="images/addtitleBl.png">
               </button>
            </div>
            <div class="post-bholder">
               <div class="post-loader"><img src="images/home-loader.gif"/></div>
               <div class="hidden_xs">
                  <a class="btngen-center-align close_modal open_discard_modal waves-effect">cancel</a>
                  <a type="button" class="btngen-center-align waves-effect">Post</a>
               </div>
            </div>
         </div>
         </div>
         </div>
         </div>
         <!-- trip-->
         <!--edit trip-->
         <!-- compose tool box modal -->
         <div id="edit_newtrip" class="modal compose_tool_box post-popup custom_modal main_modal new-wall-post set_re_height">
         <div class="hidden_header">
         <div class="content_header">
         <button class="close_span cancel_poup waves-effect">
         <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
         </button>
         <p class="modal_header_xs">Write Post</p>
         <a type="button" class="post_btn action_btn post_btn_xs close_modal waves-effect"  onclick="verify()">Post</a>
         </div>
         </div>
         <div class="modal-content">
         <div class="new-post active">
         <div class="top-stuff">
            <div class="postuser-info">
               <span class="img-holder"><img class="circle" src="images/demo-profile.jpg" /></span>
               <div class="desc-holder">
                  <span class="profile_name">Nimish Parekh</span>
               </div>
            </div>
            <div class="settings-icon">
               <a href="javascript:void(0)" onclick="clearPost()">
               <i class="zmdi zmdi-refresh-alt"></i>
               </a>
            </div>
         </div>
         <div class="clear"></div>
         <div class="scroll_div">
            <div class="npost-content">
               <div class="post-mcontent">
                  <div class="npost-title title_post_container title_block">                                    
                     <input type="text" class="title" placeholder=""value="trip title">                                 
                  </div>
                  <div class="clear"></div>
                  <div class="desc">
                     <textarea id="new_post_comment" placeholder="Write your tip" class="materialize-textarea comment_textarea new_post_comment">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</textarea>
                  </div>
               </div>
            </div>
         </div>
         </div>
         </div>
         <div class="modal-footer">
         <div class="new-post active">
         <div class="post-bcontent">
            <div class="footer_icon_container">
               <button class="comment_footer_icon waves-effect compose_titleAction" id="compose_titleAction">
               <img src="images/addtitleBl.png">
               </button>
            </div>
            <div class="post-bholder">
               <div class="post-loader"><img src="images/home-loader.gif"/></div>
               <div class="hidden_xs">
                  <a class="btngen-center-align close_modal open_discard_modal waves-effect">cancel</a>
                  <a type="button" class="btngen-center-align waves-effect">Post</a>
               </div>
            </div>
         </div>
         </div>
         </div>
         </div>
         <!--edit trip-->    

         <!-- compose tool box modal -->
         <div id="compose_discus" class="modal compose_tool_box post-popup custom_modal main_modal new-wall-post set_re_height compose_discus_popup">
         <div class="hidden_header">  
         <div class="content_header">
         <button class="close_span cancel_poup waves-effect">
         <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
         </button>
         <p class="modal_header_xs">Write Post</p>
         <a type="button" class="post_btn action_btn post_btn_xs close_modal waves-effect"  onclick="verify()">Post</a>
         </div>
         </div>
         <div class="modal-content">
         <div class="new-post active">
         <div class="top-stuff">
            <!--<div class="side-user">-->
            <div class="postuser-info">
               <span class="img-holder"><img class="circle" src="images/demo-profile.jpg" /></span>
               <div class="desc-holder">
                  <span class="profile_name">Nimish Parekh</span>
                  <label id="tag_person" class="tag_person_new"></label>
                  <div class="public_dropdown_container">
                     <a class="dropdown_text dropdown-button-left normalpostcreateprivacylabel" href="javascript:void(0)" onclick="privacymodal(this)" data-modeltag="normalpostcreateprivacylabel" data-fetch="no" data-label="normalpost">
                        <span id="post_privacy2" class="post_privacy_label">Public</span>
                        <i class="zmdi zmdi-caret-down"></i>
                     </a>
                  </div>
               </div>
            </div>
            <div class="settings-icon">
               <a class="dropdown-button "  href="javascript:void(0)" data-activates="newpost_settings">
               <i class="zmdi zmdi-more"></i>
               </a>
               <ul id="newpost_settings" class="dropdown-content custom_dropdown">
                  <li>
                     <a href="javascript:void(0)">
                     <input type="checkbox" id="toolbox_disable_sharing" />
                     <label for="toolbox_disable_sharing">Disable Sharing</label>
                     </a>
                  </li>
                  <li>
                     <a href="javascript:void(0)">
                     <input type="checkbox" id="toolbox_disable_comments" />
                     <label for="toolbox_disable_comments">Disable Comments</label>
                     </a>
                  </li>
                  <li>
                     <a  onclick="clearPost()">Clear Post</a>
                  </li>
               </ul>
            </div>
         </div>
         <div class="clear"></div>
         <div class="scroll_div">
            <div class="npost-content">
               <div class="post-mcontent">
                  <div class="npost-title title_post_container">                                    
                     <input type="text" class="title" placeholder="Your tip title">                                 
                  </div>
                  <div class="clear"></div>
                  <div class="desc">
                     <textarea id="new_post_comment" placeholder="What's new?" class="materialize-textarea comment_textarea new_post_comment"></textarea>
                  </div>
                  <div class="post-photos">
                     <div class="img-row">
                     </div>
                  </div>
                  <div class="post-tag">
                     <div class="areatitle">With</div>
                     <div class="areadesc">
                        <input type="text" class="ptag" placeholder="Who are you with?"/>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         </div>
         </div>
         <div class="modal-footer">
         <div class="new-post active">
         <div class="post-bcontent">
            <div class="footer_icon_container">
               <button class="comment_footer_icon waves-effect" id="compose_uploadphotomodalAction">
               <i class="zmdi zmdi-camera"></i>
               </button>
               <button class="comment_footer_icon waves-effect" id="compose_addpersonAction">
               <i class="zmdi zmdi-account"></i>
               </button>
               <button class="comment_footer_icon compose_titleAction waves-effect" id="compose_titleAction">
               <img src="images/addtitleBl.png">
               </button>
            </div>
            <div class="public_dropdown_container_xs">
               <a class="dropdown_text dropdown-button-left normalpostcreateprivacylabel" href="javascript:void(0)" onclick="privacymodal(this)" data-modeltag="normalpostcreateprivacylabel" data-fetch="no" data-label="normalpost">
                  <span id="post_privacy2" class="post_privacy_label">Public</span>
                  <i class="zmdi zmdi-caret-down"></i>
               </a>
            </div>
            <div class="post-bholder">
               <div class="post-loader"><img src="images/home-loader.gif"/></div>
               <div class="hidden_xs">
                  <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">cancel</a>
                  <a href="javascript:void(0)" class="btngen-center-align waves-effect btn-flat disabled submit">Post</a>
               </div>
            </div>
         </div>
         </div>
         </div>
         </div>
         <!-- Discuss-->
  
         <div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1 map_modalUniq">
         <?php include('common/map_modal.php'); ?>
         </div>
         <?php include('common/addperson_popup.php'); ?>
         <?php include('common/add_photo_popup.php'); ?>
         <!-- Add Photo-->
         <!-- compose Comment modal -->
         <div id="comment_modal_xs" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose compose_Comment_Action">
         <div class="modal_content_container">
         <div class="modal_content_child modal-content">
         <div class="popup-title ">
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>           
            <h3>All Comments</h3>
            <a type="button" class="item_done crop_done hidden_close_span waves-effect custom_close" href="javascript:void(0)">Done</a>
            <a type="button" class="item_done crop_done comment-close custom_close waves-effect" href="javascript:void(0)"><i class="mdi mdi-close"></i></a>
         </div>
         <div class="custom_modal_content modal_content" id="createpopup">
            <div class="comment-box-tab profile-tab">
               <div class="comment-poup-box detail-box">
                  <div class="content-holder main-holder">
                     <div class="summery">
                        <div class="dsection bborder expandable-holder expanded">
                           <div class="form-area expandable-area post-holder">
                              <div class="post-more">
                                 <a href="javascript:void(0)" class="view-morec">View more comments</a>
                                 <span class="total-comments">3 of 7</span>
                              </div>
                              <div class="post-comments">
                                 <div class="pcomments">
                                    <div class="pcomment-earlier">
                                       <div class="pcomment-holder">
                                          <div class="pcomment main-comment">
                                             <div class="img-holder">
                                                <div class="profiletipholder">
                                                   <span class="profile-tooltip">
                                                   <img class="circle" src="images/demo-profile.jpg"/>
                                                   </span>
                                                   <span class="profiletooltip_content slidingpan-holder">
                                                      <div class="profile-tip dis-none">
                                                         <div class="profile-tip-avatar">
                                                            <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                 
                                                            <div class="sliding-pan location-span">
                                                               <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                            </div>
                                                         </div>
                                                         <div class="profile-tip-name">
                                                            <a href="javascript:void(0)">Adel Hasanat</a>
                                                         </div>
                                                         <div class="profile-tip-info">
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                            </div>
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                            </div>
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </span>
                                                </div>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="normal-mode">
                                                   <div class="desc">
                                                      <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                   </div>
                                                   <div class="comment-stuff">
                                                      <div class="more-opt">
                                                         <span class="likeholder">
                                                         <span class="like-tooltip">
                                                         <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                         <i class="zmdi zmdi-thumb-up"></i>
                                                         </a>
                                                         </span>
                                                         </span>
                                                         <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                         <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit32">                               <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                         </a>
                                                         <ul id="comment_ecit32" class="dropdown-content custom_dropdown">
                                                            <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                            <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                         </ul>
                                                      </div>
                                                      <div class="less-opt">
                                                         <div class="timestamp">8h</div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="edit-mode">
                                                   <div class="desc">
                                                      <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-1">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                      <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="clear"></div>
                                          <div class="comment-reply-holder comment-addreply">
                                             <div class="addnew-comment valign-wrapper comment-reply">
                                                <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                <div class="desc-holder">                                   
                                                   <textarea class="materialize-textarea mb0 md_textarea descinput">Write a reply...</textarea>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="pcomment-holder">
                                          <div class="pcomment main-comment">
                                             <div class="img-holder">
                                                <div class="profiletipholder">
                                                   <span class="profile-tooltip">
                                                   <img class="circle" src="images/demo-profile.jpg"/>
                                                   </span>
                                                   <span class="profiletooltip_content slidingpan-holder">
                                                      <div class="profile-tip dis-none">
                                                         <div class="profile-tip-avatar">
                                                            <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                 
                                                            <div class="sliding-pan location-span">
                                                               <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                            </div>
                                                         </div>
                                                         <div class="profile-tip-name">
                                                            <a href="javascript:void(0)">Adel Hasanat</a>
                                                         </div>
                                                         <div class="profile-tip-info">
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                            </div>
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                            </div>
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </span>
                                                </div>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="normal-mode">
                                                   <div class="desc">
                                                      <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                   </div>
                                                   <div class="comment-stuff">
                                                      <div class="more-opt">
                                                         <span class="likeholder">
                                                         <span class="like-tooltip">
                                                         <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                         <i class="zmdi zmdi-thumb-up"></i>
                                                         </a>
                                                         </span>
                                                         </span>    
                                                         <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                         <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit33">                               <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                         </a>
                                                         <ul id="comment_ecit33" class="dropdown-content custom_dropdown">
                                                            <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                            <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                         </ul>
                                                      </div>
                                                      <div class="less-opt">
                                                         <div class="timestamp">8h</div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="edit-mode">
                                                   <div class="desc">
                                                      <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-2">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                      <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="clear"></div>
                                          <div class="comment-reply-holder comment-addreply">
                                             <div class="addnew-comment valign-wrapper comment-reply">
                                                <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                <div class="desc-holder">                                   
                                                   <textarea class="materialize-textarea mb0 md_textarea descinput">Write a reply...</textarea>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="pcomment-holder has-comments">
                                       <div class="pcomment main-comment">
                                          <div class="img-holder">
                                             <div class="profiletipholder">
                                                <span class="profile-tooltip">
                                                <img class="circle" src="images/demo-profile.jpg"/>
                                                </span>
                                                <span class="profiletooltip_content slidingpan-holder">
                                                   <div class="profile-tip dis-none">
                                                      <div class="profile-tip-avatar">
                                                         <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                    
                                                         <div class="sliding-pan location-span">
                                                            <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                         </div>
                                                      </div>
                                                      <div class="profile-tip-name">
                                                         <a href="javascript:void(0)">Adel Hasanat</a>
                                                      </div>
                                                      <div class="profile-tip-info">
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                         </div>
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                         </div>
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </span>
                                             </div>
                                          </div>
                                          <div class="desc-holder">
                                             <div class="normal-mode">
                                                <div class="desc">
                                                   <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                   <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                </div>
                                                <div class="comment-stuff">
                                                   <div class="more-opt">
                                                      <span class="likeholder">
                                                      <span class="like-tooltip">
                                                      <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                      <i class="zmdi zmdi-thumb-up"></i>
                                                      </a>
                                                      </span>
                                                      </span>
                                                      <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                      <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit34" >                              <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                      </a>
                                                      <ul id="comment_ecit34" class="dropdown-content custom_dropdown">
                                                         <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                         <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                      </ul>
                                                   </div>
                                                   <div class="less-opt">
                                                      <div class="timestamp">8h</div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="edit-mode">
                                                <div class="desc">
                                                   <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                   <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="comment-reply-holder">
                                          <div class="comments-reply-summery">
                                             <a href="javascript:void(0)" onclick="openReplies(this)">
                                             <i class="mdi mdi-share"></i>
                                             2 Replies                                                  
                                             </a>
                                             <i class="mdi mdi-bullseye dot-i"></i>
                                             Just Now
                                          </div>
                                          <div class="comments-reply-details">
                                             <div class="pcomment comment-reply">
                                                <div class="img-holder">
                                                   <div class="profiletipholder">
                                                      <span class="profile-tooltip">
                                                      <img class="circle" src="images/demo-profile.jpg"/>
                                                      </span>
                                                      <span class="profiletooltip_content slidingpan-holder">
                                                         <div class="profile-tip dis-none">
                                                            <div class="profile-tip-avatar">
                                                               <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                  
                                                               <div class="sliding-pan location-span">
                                                                  <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                               </div>
                                                            </div>
                                                            <div class="profile-tip-name">
                                                               <a href="javascript:void(0)">Adel Hasanat</a>
                                                            </div>
                                                            <div class="profile-tip-info">
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </span>
                                                   </div>
                                                </div>
                                                <div class="desc-holder">
                                                   <div class="normal-mode">
                                                      <div class="desc">
                                                         <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                         <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                      </div>
                                                      <div class="comment-stuff">
                                                         <div class="more-opt">
                                                            <span class="likeholder">
                                                            <span class="like-tooltip">
                                                            <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                            <i class="zmdi zmdi-thumb-up"></i>
                                                            </a>
                                                            </span>
                                                            </span>
                                                            <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                            <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit35">                                <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                            </a>
                                                            <ul id="comment_ecit35" class="dropdown-content custom_dropdown">
                                                               <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                               <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                            </ul>
                                                         </div>
                                                         <div class="less-opt">
                                                            <div class="timestamp">8h</div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="edit-mode">
                                                      <div class="desc">
                                                         <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-4">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                         <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pcomment comment-reply">
                                                <div class="img-holder">
                                                   <div class="profiletipholder">
                                                      <span class="profile-tooltip">
                                                      <img class="circle" src="images/demo-profile.jpg"/>
                                                      </span>
                                                      <span class="profiletooltip_content slidingpan-holder">
                                                         <div class="profile-tip dis-none">
                                                            <div class="profile-tip-avatar">
                                                               <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                  
                                                               <div class="sliding-pan location-span">
                                                                  <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                               </div>
                                                            </div>
                                                            <div class="profile-tip-name">
                                                               <a href="javascript:void(0)">Adel Hasanat</a>
                                                            </div>
                                                            <div class="profile-tip-info">
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </span>
                                                   </div>
                                                </div>
                                                <div class="desc-holder">
                                                   <div class="normal-mode">
                                                      <div class="desc">
                                                         <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                         <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                                      </div>
                                                      <div class="comment-stuff">
                                                         <div class="more-opt">
                                                            <span class="likeholder">
                                                            <span class="like-tooltip">
                                                            <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                            <i class="zmdi zmdi-thumb-up"></i>
                                                            </a>
                                                            </span>
                                                            </span> 
                                                            <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                            <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit37">                                <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                            </a>
                                                            <ul id="comment_ecit37" class="dropdown-content custom_dropdown">
                                                               <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                               <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                            </ul>
                                                         </div>
                                                         <div class="less-opt">
                                                            <div class="timestamp">8h</div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="edit-mode">
                                                      <div class="desc">
                                                         <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-5">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                         <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="comment-reply-holder comment-addreply">
                                          <div class="addnew-comment valign-wrapper comment-reply">
                                             <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                             <div class="desc-holder">                                  
                                                <textarea class="materialize-textarea mb0 md_textarea descinput">Write a reply...</textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="pcomment-holder">
                                       <div class="pcomment main-comment">
                                          <div class="img-holder">
                                             <div class="profiletipholder">
                                                <span class="profile-tooltip">
                                                <img class="circle" src="images/demo-profile.jpg"/>
                                                </span>
                                                <span class="profiletooltip_content slidingpan-holder">
                                                   <div class="profile-tip dis-none">
                                                      <div class="profile-tip-avatar">
                                                         <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                    
                                                         <div class="sliding-pan location-span">
                                                            <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                         </div>
                                                      </div>
                                                      <div class="profile-tip-name">
                                                         <a href="javascript:void(0)">Adel Hasanat</a>
                                                      </div>
                                                      <div class="profile-tip-info">
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                         </div>
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                         </div>
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </span>
                                             </div>
                                          </div>
                                          <div class="desc-holder">
                                             <div class="normal-mode">
                                                <div class="desc">
                                                   <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                   <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)"><span class="readlink">Read More</span></a></p>
                                                </div>
                                                <div class="comment-stuff">
                                                   <div class="more-opt">
                                                      <span class="likeholder">
                                                      <span class="like-tooltip">
                                                      <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                      <i class="zmdi zmdi-thumb-up"></i>
                                                      </a>
                                                      </span>
                                                      </span>   
                                                      <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                      <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit36" >                              <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                      </a>
                                                      <ul id="comment_ecit36" class="dropdown-content custom_dropdown">
                                                         <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                         <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                      </ul>
                                                   </div>
                                                   <div class="less-opt">
                                                      <div class="timestamp">8h</div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="edit-mode">
                                                <div class="desc">
                                                   <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-6">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                   <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="comment-reply-holder comment-addreply">
                                          <div class="addnew-comment valign-wrapper comment-reply">
                                             <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                             <div class="desc-holder">                                  
                                                <textarea class="materialize-textarea mb0 md_textarea descinput">Write a reply...</textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         </div>
</div>
<div class="modal-footer">
<div class="new-post active">
<div class="post-bcontent">
   <div class="addnew-comment valign-wrapper">
      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
      <div class="desc-holder">                                    
         <textarea class="materialize-textarea mb0 md_textarea descinput" id="comment_txt_1">Write a reply...</textarea>
      </div>
   </div>
</div>
</div>
</div>
<div class="additem_modal_footer modal-footer">
<div class="">
<div class="btn-holder">
   <div class="addnew-comment valign-wrapper">
      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
      <div class="desc-holder">                                    
         <textarea class="materialize-textarea mb0 md_textarea descinput" id="comment_txt_1">Write a reply...</textarea>
      </div>
   </div>
</div>
</div>
</div>
      <div class="new-post-mobile clear">
         <a class="popup-window composetoolboxAction" href="javascript:void(0)"><i class="mdi mdi-pencil"></i></a>
      </div>
   </div>
</div>
   
</div>   
<!--map modal-->
<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">
   <?php include('common/map_modal.php'); ?>
</div>

<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/wall.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$w = $(window).width();
if ( $w > 739) {      
$(".places-tabs .sub-tabs li a").click(function(){
   $("body").removeClass("remove_scroller");
}); 
$(".tabs.icon-menu.tabsnew li a").click(function(){
   $("body").removeClass("remove_scroller");
}); 
$(".mbl-tabnav").click(function(){
   $("body").removeClass("remove_scroller");
}); 
$(".clicable.viewall-link").click(function(){
   $("body").removeClass("remove_scroller");
}); 
} else {
$(".places-tabs .sub-tabs li a").click(function(){
   $("body").addClass("remove_scroller");
}); 
$(".clicable.viewall-link").click(function(){
   $("body").addClass("remove_scroller");
});         
$(".tabs.icon-menu.tabsnew li a").click(function(){
   $("body").addClass("remove_scroller");
}); 
$(".mbl-tabnav").click(function(){
   $("body").removeClass("remove_scroller");
});
}

$(".header-icon-tabs .tabsnew .tab a").click(function(){
$(".bottom_tabs").hide();
});

$(".places-tabs .tab a").click(function(){
$(".top_tabs").hide();
});

// footer work for places home page only
$('.footer-section').css('left', '0');
$w = $(window).width();
if($w <= 768) {
$('.main-footer').css({
   'width': '100%',
   'left': '0'
});
} else {
var $_I = $('.places-content.places-all').width();
var $__I = $('.places-content.places-all').find('.container').width();

var $half = parseInt($_I) - parseInt($__I);
$half = parseInt($half) / 2;

$('.main-footer').css({
   'width': $_I+'px',
   'left': '-'+$half+'px'
});
}
});

$(window).resize(function() {
// footer work for places home page only
if($('#places-all').hasClass('active')) {
$('.footer-section').css('left', '0');
$w = $(window).width();
if($w <= 768) {
   $('.main-footer').css({
      'width': '100%',
      'left': '0'
   });
} else {
   var $_I = $('.places-content.places-all').width();
   var $__I = $('.places-content.places-all').find('.container').width();

   var $half = parseInt($_I) - parseInt($__I);
   $half = parseInt($half) / 2;

   $('.main-footer').css({
      'width': $_I+'px',
      'left': '-'+$half+'px'
   });
}
}
});

$(document).on('click', '.tablist .tab a', function(e) {
$href = $(this).attr('href');
$href = $href.replace('#', '');

$('.places-content').removeClass().addClass('places-content '+$href);
   $this = $(this);
});
</script>

<?php include('common/discard_popup.php'); ?>
<?php include('common/upload_gallery_popup.php'); ?>
<?php include('common/privacymodal.php'); ?>
<?php include('common/custom_modal.php'); ?>
<?php include('common/addcategories_popup.php'); ?>

<?php include("script.php"); ?>