<?php include("header.php"); ?>
<div class="mobile_back">
   <div class="dirinfo backbtn" data-class="step2" data-ndataclass="step1">
      <a href="advertisement.php" class=""><i class="mdi mdi-arrow-left"></i></a>
   </div>
</div>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div>
</div>
<div class="clear"></div>
<?php include("common/leftmenu.php"); ?>
<div class="fixed-layout ipad-mfix">
   <div class="main-content advert-page manageadvert-page">
      <div class="edit-travad">
         <div class="edit-holder nice-scroll">
            <div class="advertmanager-page">
               <div class="detail-box">
                  <div class="travadvert-section active">
                     <div class="travad-accordion travad-details">
                        <div class="main_edt_title">
                           <h3 class="sub-title">Page Engagement</h3>
                           <div class="dirinfo right backbtn btn_back_cus">
                              <a href="javascript:void(0)"  onclick="closeEditAd()" class="btn btn-primary btn-md left"><i class="zmdi zmdi-chevron-left"></i></a>
                           </div>
                        </div>
                        <div class="main-accContent">
                           <!-- page likes -->									
                           <div class="travad-detailbox pagelikes">
                              <div class="row arrange_row">
                                 <div class="col l5 m5 s12 detail-part">
                                    <div class="frow more_m5">
                                       <label>Select a page to advert</label>
                                       <div class="sliding-middle-custom anim-area underlined fullwidth select_change">
                                          <select class="select2" tabindex="-1"  id="mySelect" onchange="main_title_change()">
                                             <option>Hyatt Hotel</option>
                                             <option>Qatar Trips</option>
                                             <option>Politics</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="frow remove_m10">
                                       <label>Catch Phrase <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom anim-area underlined fullwidth tt-holder th50">
                                          <textarea class="materialize-textarea mb0 md_textarea descinput catch_phase" placeholder="Add a few sentences to catch people's attention to your ad" data-length="70"></textarea>
                                       </div>
                                    </div>
                                    <div class="frow more_m5">
                                       <label>Headline <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom underlined anim-area fullwidth">
                                          <input type="text" class="fullwidth change_head" placeholder="Write a headline of your ad">
                                       </div>
                                    </div>
                                    <div class="frow">
                                       <label>Text <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom anim-area underlined fullwidth tt-holder th70">
                                          <textarea 
                                             class="materialize-textarea mb0 md_textarea descinput sub_title_you_ad" placeholder="Short description of your ad" data-length="140"></textarea>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col l7 m7 s12 preview-part">
                                    <div class="frow top-sec">
                                       <label>Advert Preview</label>
                                       <div class="settings-icon">
                                          <div class="dropdown dropdown-custom dropdown-med resist">
                                             <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="setting_btn2">
                                             <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                             </a>
                                             <ul id="setting_btn2" class="dropdown-content custom_dropdown">
                                                <li class="dmenu-title">Show advert in</li>
                                                <li class="divider"></li>
                                                <li>
                                                   <ul class="echeck-list">
                                                      <li class="selected"><a href="javascript:void(0)"><i class="zmdi zmdi-check"></i>Stream Feeds (Desktop)</a></li>
                                                      <li class="selected"><a href="javascript:void(0)"><i class="zmdi zmdi-check"></i>Stream Feeds (Mobile)</a></li>
                                                      <li class="selected"><a href="javascript:void(0)"><i class="zmdi zmdi-check"></i>Right Column (Desktop)</a></li>
                                                   </ul>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="mbl-preview-part">
                                       <div class="adpreview-holder sfeed-m">
                                          <div class="post-holder travad-box page-travad">
                                             <div class="post-topbar">
                                                <div class="post-userinfo">
                                                   <div class="img-holder">
                                                      <div id="" class="profiletipholder">
                                                         <span class="profile-tooltip">
                                                         <img class="circle" src="images/hyattprofile.jpg"/>
                                                         </span>
                                                      </div>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <a href="javascript:void(0)" class="head_chtitle">Hyatt Hotel</a>
                                                      <span class="timestamp">Sponsored Ad</span>
                                                   </div>
                                                </div>
                                                <div class="settings-icon">
                                                   <div class="dropdown dropdown-custom dropdown-med">
                                                      <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="Sponsored_1">
                                                      <i class="zmdi zmdi-more"></i>
                                                      </a>
                                                      <ul id="Sponsored_1" class="dropdown-content custom_dropdown">
                                                         <li class="nicon"><a href="javascript:void(0)">Hide ad</a></li>
                                                         <li class="nicon"><a href="javascript:void(0)">Save ad</a></li>
                                                         <li class="nicon"><a href="javascript:void(0)">Mute this seller ads</a></li>
                                                         <li class="nicon"><a href="javascript:void(0)">Report ad</a></li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="post-content">
                                                <div class="shared-box shared-category">
                                                   <div class="post-holder">
                                                      <div class="post-content">
                                                         <div class="post-desc">
                                                            <p class="change_ad_word">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                         </div>
                                                         <div class="post-img-holder">
                                                            <div class="post-img one-img">
                                                               <div class="pimg-holder"><img src="images/pagead.jpg"/></div>
                                                            </div>
                                                         </div>
                                                         <div class="share-summery">
                                                            <div class="travad-title">Get a luxurious experience with us!</div>
                                                            <div class="travad-subtitle">We are here to provide you high class services with variety of amenities.</div>
                                                            <div class="travad-info">345 people liked this</div>
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn waves-effect waves-light"><i class="zmdi zmdi-thumb-up"></i>Like</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="preview-accordion">
                                       <ul class="collapsible" data-collapsible="accordion">
                                          <li>
                                             <h4 class="collapsible-header">Right Column (Desktop)</h4>
                                             <div class="collapsible-body">
                                                <div>
                                                   <div class="adpreview-holder rcolumn-d">
                                                      <div class="content-box">
                                                         <div class="cbox-desc">
                                                            <div class="side-travad page-travad">
                                                               <div class="travad-maintitle">
                                                                  <img src="images/hyattprofile.jpg">
                                                                  <h6 class="head_chtitle">Hyatt Hotel</h6>
                                                                  <span>Sponsored</span>
                                                               </div>
                                                               <div class="post-desc">
                                                                  <p class="change_ad_word">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                               </div>
                                                               <div class="imgholder adsection_img">
                                                                  <img src="images/pagead.jpg"/>								
                                                               </div>
                                                               <div class="descholder">
                                                                  <div class="travad-title">Get a luxurious experience with us!</div>
                                                                  <div class="travad-subtitle">We are here to provide you high class services with variety of amenities.</div>
                                                                  <div class="travad-info">345 people liked this</div>
                                                                  <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn waves-effect waves-light"><i class="zmdi zmdi-thumb-up"></i>Like</a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                          <li>
                                             <h4 class="collapsible-header">Stream Feeds (Mobile)</h4>
                                             <div class="collapsible-body">
                                                <div>
                                                   <div class="adpreview-holder sfeed-m">
                                                      <div class="post-holder travad-box page-travad">
                                                         <div class="post-topbar">
                                                            <div class="post-userinfo">
                                                               <div class="img-holder">
                                                                  <div id="profiletip-4" class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/hyattprofile.jpg"/>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <a href="javascript:void(0)" class="head_chtitle">Hyatt Hotel</a>
                                                                  <span class="timestamp">Sponsored Ad</span>
                                                               </div>
                                                            </div>
                                                            <div class="settings-icon">
                                                               <div class="dropdown dropdown-custom dropdown-med">
                                                                  <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="Sponsored_ad">
                                                                  <i class="zmdi zmdi-more"></i>
                                                                  </a>
                                                                  <ul id="Sponsored_ad" class="dropdown-content custom_dropdown">
                                                                     <li class="nicon"><a href="javascript:void(0)">Hide ad</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Save ad</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Mute this seller ads</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Report ad</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="post-content">
                                                            <div class="shared-box shared-category">
                                                               <div class="post-holder">
                                                                  <div class="post-content">
                                                                     <div class="post-desc">
                                                                        <p class="change_ad_word">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="post-img-holder">
                                                                        <div class="post-img one-img">
                                                                           <div class="pimg-holder"><img src="images/pagead.jpg"/></div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="share-summery">
                                                                        <div class="travad-title">Get a luxurious experience with us!</div>
                                                                        <div class="travad-subtitle">We are here to provide you high class services with variety of amenities.</div>
                                                                        <div class="travad-info">345 people liked this</div>
                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn waves-effect waves-light"><i class="zmdi zmdi-thumb-up"></i>Like</a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                          <li>
                                             <h4 class="collapsible-header last-item active">Stream Feeds (Desktop)</h4>
                                             <div class="collapsible-body">
                                                <div>
                                                   <div class="adpreview-holder sfeed-d">
                                                      <div class="post-holder travad-box page-travad">
                                                         <div class="post-topbar">
                                                            <div class="post-userinfo">
                                                               <div class="img-holder">
                                                                  <div id="profiletip-4" class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/hyattprofile.jpg"/>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <a href="javascript:void(0)" class="head_chtitle">Hyatt Hotel</a>
                                                                  <span class="timestamp">Sponsored Ad</span>
                                                               </div>
                                                            </div>
                                                            <div class="settings-icon">
                                                               <div class="dropdown dropdown-custom dropdown-med">
                                                                  <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="Sponsored_ad1">
                                                                  <i class="zmdi zmdi-more"></i>
                                                                  </a>
                                                                  <ul id="Sponsored_ad1" class="dropdown-content custom_dropdown">
                                                                     <li class="nicon"><a href="javascript:void(0)">Hide ad</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Save ad</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Mute this seller ads</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Report ad</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="post-content">
                                                            <div class="shared-box shared-category">
                                                               <div class="post-holder">
                                                                  <div class="post-content">
                                                                     <div class="post-desc">
                                                                        <p class="change_ad_word">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="post-img-holder">
                                                                        <div class="post-img one-img">
                                                                           <div class="pimg-holder"><img src="images/additem-photo.png"/></div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="share-summery">
                                                                        <div class="travad-title">Get a luxurious experience with us!</div>
                                                                        <div class="travad-subtitle">We are here to provide you high class services with variety of amenities.</div>
                                                                        <div class="travad-info">345 people liked this</div>
                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn waves-effect waves-light"><i class="zmdi zmdi-thumb-up"></i>Like</a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- brand awareness -->									
                           <div class="travad-detailbox brandawareness">
                              <div class="row">
                                 <div class="col l5 m5 s12 detail-part">
                                    <div class="frow remove_m10">
                                       <label>Branding Phrase <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom anim-area underlined fullwidth tt-holder">
                                          <input type='text'class="fullwidth aware_head" placeholder="Slogan your brand in a few words" data-length="50"/>
                                       </div>
                                    </div>
                                    <div class="frow remove_m10">
                                       <label>Text <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom anim-area underlined fullwidth tt-holder th70">
                                          <textarea  class="materialize-textarea mb0 md_textarea descinput aware_text" placeholder="Create personalized text that easily identify your brand name or product" data-length="140"></textarea>
                                       </div>
                                    </div>
                                    <div class="frow remove_m10">
                                       <label>Website URL <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom underlined anim-area fullwidth">
                                          <input type="text" class="fullwidth" placeholder="Add your website URL">
                                       </div>
                                    </div>
                                    <div class="frow remove_m10">
                                       <p class="note">For effective branding advert, add an image that show your product or business name</p>
                                    </div>
                                 </div>
                                 <div class="col l7 m7 s12 preview-part">
                                    <div class="frow top-sec">
                                       <label>Advert Preview</label>														
                                       <div class="settings-icon">
                                          <div class="dropdown dropdown-custom dropdown-med resist">
                                             <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="setting_btn3">
                                             <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                             </a>
                                             <ul id="setting_btn3" class="dropdown-content custom_dropdown">
                                                <li class="dmenu-title">Show advert in</li>
                                                <li class="divider"></li>
                                                <li>
                                                   <ul class="echeck-list">
                                                      <li class="selected"><a href="javascript:void(0)"><i class="zmdi zmdi-check"></i>Stream Feeds (Desktop)</a></li>
                                                      <li class="selected"><a href="javascript:void(0)"><i class="zmdi zmdi-check"></i>Stream Feeds (Mobile)</a></li>
                                                      <li class="selected"><a href="javascript:void(0)"><i class="zmdi zmdi-check"></i>Right Column (Desktop)</a></li>
                                                   </ul>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="mbl-preview-part">
                                       <div class="adpreview-holder sfeed-m">
                                          <div class="post-holder travad-box brand-travad">
                                             <div class="post-topbar">
                                                <div class="post-userinfo change_awar_head">
                                                   Best coffee in the world!
                                                </div>
                                                <div class="settings-icon">
                                                   <div class="dropdown dropdown-custom dropdown-med">
                                                      <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="Sponsored_2">
                                                      <i class="zmdi zmdi-more"></i>
                                                      </a>
                                                      <ul id="Sponsored_2" class="dropdown-content custom_dropdown">
                                                         <li class="nicon"><a href="javascript:void(0)">Hide ad</a></li>
                                                         <li class="nicon"><a href="javascript:void(0)">Save ad</a></li>
                                                         <li class="nicon"><a href="javascript:void(0)">Mute this seller ads</a></li>
                                                         <li class="nicon"><a href="javascript:void(0)">Report ad</a></li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="post-content">
                                                <div class="shared-box shared-category">
                                                   <div class="post-holder">
                                                      <div class="post-content">
                                                         <div class="post-img-holder">
                                                            <div class="post-img one-img">
                                                               <div class="pimg-holder"><img src="images/brand-p.jpg"/></div>
                                                            </div>
                                                         </div>
                                                         <div class="share-summery">
                                                            <div class="travad-subtitle change_awar_text">We just get new starbucks coffee that is double in caffine that everybody is calling it a boost!</div>
                                                            <a href="javascript:void(0)" target="_blank" class="btn btn-primary btn-sm adbtn waves-effect waves-light">Explore</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="preview-accordion">
                                       <ul class="collapsible" data-collapsible="accordion">
                                          <li>
                                             <h4 class="collapsible-header">Right Column (Desktop)</h4>
                                             <div class="collapsible-body">
                                                <div>
                                                   <div class="adpreview-holder rcolumn-d">
                                                      <div class="content-box">
                                                         <div class="cbox-desc">
                                                            <div class="side-travad brand-travad">
                                                               <div class="travad-maintitle change_awar_head">Best coffee in the world!</div>
                                                               <div class="imgholder">
                                                                  <img src="images/brand-p.jpg">
                                                               </div>
                                                               <div class="descholder">
                                                                  <div class="travad-subtitle change_awar_text">We just get new starbucks coffee that is double in caffine that everybody is calling it a boost!</div>
                                                                  <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn waves-effect waves-light">Explore</a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                          <li>
                                             <h4 class="collapsible-header">Stream Feeds (Mobile)</h4>
                                             <div class="collapsible-body">
                                                <div>
                                                   <div class="adpreview-holder sfeed-m">
                                                      <div class="post-holder travad-box brand-travad">
                                                         <div class="post-topbar">
                                                            <div class="post-userinfo change_awar_head">
                                                               Best coffee in the world!
                                                            </div>
                                                            <div class="settings-icon">
                                                               <div class="dropdown dropdown-custom dropdown-med">
                                                                  <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="Sponsored_3">
                                                                  <i class="zmdi zmdi-more"></i>
                                                                  </a>
                                                                  <ul id="Sponsored_3" class="dropdown-content custom_dropdown">
                                                                     <li class="nicon"><a href="javascript:void(0)">Hide ad</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Save ad</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Mute this seller ads</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Report ad</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="post-content">
                                                            <div class="shared-box shared-category">
                                                               <div class="post-holder">
                                                                  <div class="post-content">
                                                                     <div class="post-img-holder">
                                                                        <div class="post-img one-img">
                                                                           <div class="pimg-holder"><img src="images/brand-p.jpg"/></div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="share-summery">
                                                                        <div class="travad-subtitle change_awar_text">We just get new starbucks coffee that is double in caffine that everybody is calling it a boost!</div>
                                                                        <a href="javascript:void(0)" target="_blank" class="btn btn-primary btn-sm adbtn waves-effect waves-light">Explore</a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                          <li>
                                             <h4 class="collapsible-header active last-item">Stream Feeds (Desktop)</h4>
                                             <div class="collapsible-body">
                                                <div>
                                                   <div class="adpreview-holder sfeed-d">
                                                      <div class="post-holder travad-box brand-travad">
                                                         <div class="post-topbar">
                                                            <div class="post-userinfo change_awar_head">
                                                               Best coffee in the world!
                                                            </div>
                                                            <div class="settings-icon">
                                                               <div class="dropdown dropdown-custom dropdown-med">
                                                                  <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="Sponsored_4">
                                                                  <i class="zmdi zmdi-more"></i>
                                                                  </a>
                                                                  <ul id="Sponsored_4" class="dropdown-content custom_dropdown">
                                                                     <li class="nicon"><a href="javascript:void(0)">Hide ad</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Save ad</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Mute this seller ads</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Report ad</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="post-content">
                                                            <div class="shared-box shared-category">
                                                               <div class="post-holder">
                                                                  <div class="post-content">
                                                                     <div class="post-img-holder">
                                                                        <div class="post-img one-img">
                                                                           <div class="pimg-holder"><img src="images/additem-photo.png"/></div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="share-summery">
                                                                        <div class="travad-subtitle change_awar_text">We just get new starbucks coffee that is double in caffine that everybody is calling it a boost!</div>
                                                                        <a href="javascript:void(0)" target="_blank" class="btn btn-primary btn-sm adbtn waves-effect waves-light">Explore</a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- website leads -->									
                           <div class="travad-detailbox websiteleads">
                              <div class="row">
                                 <div class="col l5 m5 s12 detail-part">
                                    <div class="frow">
                                       <label>Advert Title <a href="javascript:void(0)" class="simple-tooltip " title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom underlined anim-area fullwidth">
                                          <input type="text" class="fullwidth advert_title" placeholder="Add your advert title">
                                       </div>
                                    </div>
                                    <div class="frow">
                                       <label>Logo Image <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="img-holder logoimg-holder">
                                          <img src="images/adimg-placeholder-logo.png"/>
                                          <div class="overlay">
                                             <span class="overlayUploader">
                                             <i class="mdi mdi-plus"></i>
                                             <input type="file" id="uploadWebsiteLeads" onchange="adPhotoInputChange(this,event,'adImageLogo')"/><a href="javascript:void(0);" class="popup-modal">&nbsp;</a>
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="frow">
                                       <label>Catch Phrase <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom anim-area underlined fullwidth tt-holder th50">
                                          <textarea  class="materialize-textarea mb0 md_textarea descinput advert_phrase" placeholder="Slogan your brand in a few words" data-length="70"></textarea>
                                       </div>
                                    </div>
                                    <div class="frow">
                                       <label>Headline <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom underlined anim-area fullwidth">
                                          <input type="text" class="fullwidth advert_head" placeholder="Write a headline of your ad">
                                       </div>
                                    </div>
                                    <div class="frow">
                                       <label>Text <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom anim-area underlined fullwidth tt-holder th70">
                                          <textarea class="materialize-textarea mb0 md_textarea descinput advert_text" placeholder="Create personalized text that easily identify your brand name or product" data-length="140"></textarea>
                                       </div>
                                    </div>
                                    <div class="frow">
                                       <label>Website URL<a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom underlined anim-area fullwidth">
                                          <input type="text" class="fullwidth advert_url" placeholder="Add your website URL">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col l7 m7 s12 preview-part">
                                    <div class="frow top-sec">
                                       <label>Advert Preview</label>
                                       <div class="settings-icon">
                                          <div class="dropdown dropdown-custom dropdown-med resist">
                                             <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="setting_btn4"><i class="zmdi zmdi-more zmdi-hc-2x"></i></a>
                                             <ul id="setting_btn4" class="dropdown-content custom_dropdown">
                                                <li class="dmenu-title">Show advert in</li>
                                                <li class="divider"></li>
                                                <li>
                                                   <ul class="echeck-list">
                                                      <li class="selected"><a href="javascript:void(0)"><i class="zmdi zmdi-check"></i>Stream Feeds (Desktop)</a></li>
                                                      <li class="selected"><a href="javascript:void(0)"><i class="zmdi zmdi-check"></i>Stream Feeds (Mobile)</a></li>
                                                      <li class="selected"><a href="javascript:void(0)"><i class="zmdi zmdi-check"></i>Right Column (Desktop)</a></li>
                                                   </ul>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="mbl-preview-part">
                                       <div class="adpreview-holder sfeed-m">
                                          <div class="post-holder travad-box weblink-travad">
                                             <div class="post-topbar">
                                                <div class="post-userinfo">
                                                   <div class="img-holder">
                                                      <div id="" class="profiletipholder">
                                                         <span class="profile-tooltip">
                                                         <img class="circle" src="images/adimg-food.jpg"/>
                                                         </span>
                                                      </div>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="travad-maintitle lead_title">Avida Food Hunt</div>
                                                      <span class="timestamp">Sponsored Ad</span>
                                                   </div>
                                                </div>
                                                <div class="settings-icon">
                                                   <div class="dropdown dropdown-custom dropdown-med">
                                                      <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="Sponsored_5">
                                                      <i class="zmdi zmdi-more"></i>
                                                      </a>
                                                      <ul id="Sponsored_5" class="dropdown-content custom_dropdown">
                                                         <li class="nicon"><a href="javascript:void(0)">Hide ad</a></li>
                                                         <li class="nicon"><a href="javascript:void(0)">Save ad</a></li>
                                                         <li class="nicon"><a href="javascript:void(0)">Mute this seller ads</a></li>
                                                         <li class="nicon"><a href="javascript:void(0)">Report ad</a></li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="post-content">
                                                <div class="shared-box shared-category">
                                                   <div class="post-holder">
                                                      <div class="post-content">
                                                         <div class="post-desc lead_phrase">
                                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                         </div>
                                                         <div class="post-img-holder">
                                                            <div class="post-img one-img">
                                                               <div class="pimg-holder"><img src="images/admain-food.jpg"/></div>
                                                            </div>
                                                         </div>
                                                         <div class="share-summery">
                                                            <div class="travad-title lead_head">30% off on special pizza this weekend!</div>
                                                            <div class="travad-subtitle lead_text">We bring you with flat 30% off on Avida special pizza this festive weekend.</div>
                                                            <a href="javascript:void(0)" class="adlink lead_url"><i class="mdi mdi-earth"></i>www.avidafoodhunt.com</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="preview-accordion">
                                       <ul class="collapsible" data-collapsible="accordion">
                                          <li>
                                             <h4 class="collapsible-header">Right Column (Desktop)</h4>
                                             <div class="collapsible-body">
                                                <div>
                                                   <div class="adpreview-holder rcolumn-d">
                                                      <div class="content-box">
                                                         <div class="cbox-desc">
                                                            <div class="side-travad weblink-travad">
                                                               <div class="travad-maintitle">
                                                                  <img src="images/adimg-food.jpg">
                                                                  <h6 class="lead_title">Avida Food Hunt</h6>
                                                                  <span>Sponsored</span>
                                                               </div>
                                                               <div class="imgholder">
                                                                  <img src="images/admain-food.jpg"/>								
                                                               </div>
                                                               <div class="descholder">
                                                                  <div class="travad-title lead_head">30% off on special pizza this weekend!</div>
                                                                  <div class="travad-subtitle lead_text">We bring you with flat 30% off on Avida special pizza this festive weekend.</div>
                                                                  <a href="javascript:void(0)" class="adlink lead_url"><i class="mdi mdi-earth"></i><span>www.avidafoodhunt.com</span></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                          <li>
                                             <h4 class="collapsible-header">Stream Feeds (Mobile)</h4>
                                             <div class="collapsible-body">
                                                <div>
                                                   <div class="adpreview-holder sfeed-m">
                                                      <div class="post-holder travad-box weblink-travad">
                                                         <div class="post-topbar">
                                                            <div class="post-userinfo">
                                                               <div class="img-holder">
                                                                  <div id="" class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/adimg-food.jpg"/>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <div class="travad-maintitle lead_title">Avida Food Hunt</div>
                                                                  <span class="timestamp">Sponsored Ad</span>
                                                               </div>
                                                            </div>
                                                            <div class="settings-icon">
                                                               <div class="dropdown dropdown-custom dropdown-med">
                                                                  <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="Sponsored_6">
                                                                  <i class="zmdi zmdi-more"></i>
                                                                  </a>
                                                                  <ul id="Sponsored_6" class="dropdown-content custom_dropdown">
                                                                     <li class="nicon"><a href="javascript:void(0)">Hide ad</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Save ad</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Mute this seller ads</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Report ad</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="post-content">
                                                            <div class="shared-box shared-category">
                                                               <div class="post-holder">
                                                                  <div class="post-content">
                                                                     <div class="post-desc">
                                                                        <p class="lead_phrase">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="post-img-holder">
                                                                        <div class="post-img one-img">
                                                                           <div class="pimg-holder"><img src="images/admain-food.jpg"/></div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="share-summery">
                                                                        <div class="travad-title lead_head">30% off on special pizza this weekend!</div>
                                                                        <div class="travad-subtitle lead_text">We bring you with flat 30% off on Avida special pizza this festive weekend.</div>
                                                                        <a href="javascript:void(0)" class="adlink lead_url"><i class="mdi mdi-earth"></i>www.avidafoodhunt.com</a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                          <li>
                                             <h4 class="collapsible-header active last-item">Stream Feeds (Desktop)</h4>
                                             <div class="collapsible-body">
                                                <div>
                                                   <div class="adpreview-holder sfeed-d">
                                                      <div class="post-holder travad-box weblink-travad">
                                                         <div class="post-topbar">
                                                            <div class="post-userinfo">
                                                               <div class="img-holder">
                                                                  <div id="" class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/adimg-food.jpg"/>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <div class="travad-maintitle lead_title">Avida Food Hunt</div>
                                                                  <span class="timestamp">Sponsored Ad</span>
                                                               </div>
                                                            </div>
                                                            <div class="settings-icon">
                                                               <div class="dropdown dropdown-custom dropdown-med">
                                                                  <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="Sponsored_7">
                                                                  <i class="zmdi zmdi-more"></i>
                                                                  </a>
                                                                  <ul id="Sponsored_7" class="dropdown-content custom_dropdown">
                                                                     <li class="nicon"><a href="javascript:void(0)">Hide ad</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Save ad</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Mute this seller ads</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Report ad</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="post-content">
                                                            <div class="shared-box shared-category">
                                                               <div class="post-holder">
                                                                  <div class="post-content">
                                                                     <div class="post-desc">
                                                                        <p class="lead_phrase">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="post-img-holder">
                                                                        <div class="post-img one-img">
                                                                           <div class="pimg-holder"><img src="images/additem-photo.png"/></div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="share-summery">
                                                                        <div class="travad-title lead_head">30% off on special pizza this weekend!</div>
                                                                        <div class="travad-subtitle lead_text">We bring you with flat 30% off on Avida special pizza this festive weekend.</div>
                                                                        <a href="javascript:void(0)" class="adlink lead_url"><i class="mdi mdi-earth"></i>www.avidafoodhunt.com</a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- Website Conversion -->									
                           <div class="travad-detailbox websiteconversion">
                              <div class="row">
                                 <div class="col l5 m5 s12 detail-part">
                                    <div class="frow more_m5">
                                       <label>Advert Title <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom underlined anim-area fullwidth">
                                          <input type="text" class="fullwidth conversion_title" placeholder="Add your advert title">
                                       </div>
                                    </div>
                                    <div class="frow">
                                       <label>Logo Image <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="img-holder logoimg-holder">
                                          <img src="images/adimg-placeholder-logo.png"/>
                                          <div class="overlay">
                                             <span class="overlayUploader">
                                             <i class="mdi mdi-plus"></i>
                                             <input type="file" id="uploadWebsiteLeads" onchange="adPhotoInputChange(this,event,'adImageLogo')"/><a href="javascript:void(0);" class="popup-modal">&nbsp;</a>
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="frow remove_m10">
                                       <label>Catch Phrase <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom anim-area underlined fullwidth tt-holder th50">
                                          <textarea  class="materialize-textarea mb0 md_textarea descinput conversion_phrase" placeholder="Slogan your brand in a few words" data-length="70"></textarea>
                                       </div>
                                    </div>
                                    <div class="frow more_m5">
                                       <label>Headline <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom underlined anim-area fullwidth">
                                          <input type="text" class="fullwidth conversion_head" placeholder="Write a headline of your ad">
                                       </div>
                                    </div>
                                    <div class="frow remove_m10">
                                       <label>Text <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom anim-area underlined fullwidth tt-holder th70">
                                          <textarea class="materialize-textarea mb0 md_textarea descinput conversion_text" placeholder="Create personalized text that easily identify your brand name or product" data-length="140"></textarea>
                                       </div>
                                    </div>
                                    <div class="frow">
                                       <label>Select action button</label>
                                       <div class="btndrop">
                                          <select id="change_button" class="select" tabindex="-1"  onchange="main_change_button(this.value)">
                                             <option>Book Now</option>
                                             <option>Shop Now</option>
                                             <option>Explore</option>
                                             <option>Know More</option>
                                             <option>Contact Us</option>
                                             <option>Sign Up</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="frow remove_m10">
                                       <label>Website URL <a href="javascript:void(0)" class="simple-tooltip tooltipstered"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom underlined anim-area fullwidth">
                                          <input class="fullwidth" placeholder="Add your website URL" type="text">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col l7 m7 s12 preview-part">
                                    <div class="frow top-sec">
                                       <label>Advert Preview</label>														
                                       <div class="settings-icon">
                                          <div class="dropdown dropdown-custom dropdown-med resist">
                                             <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="setting_btn5"><i class="zmdi zmdi-more zmdi-hc-2x"></i></a>
                                             <ul id="setting_btn5" class="dropdown-content custom_dropdown">
                                                <li class="dmenu-title">Show advert in</li>
                                                <li class="divider"></li>
                                                <li>
                                                   <ul class="echeck-list">
                                                      <li class="selected"><a href="javascript:void(0)"><i class="zmdi zmdi-check"></i>Stream Feeds (Desktop)</a></li>
                                                      <li class="selected"><a href="javascript:void(0)"><i class="zmdi zmdi-check"></i>Stream Feeds (Mobile)</a></li>
                                                      <li class="selected"><a href="javascript:void(0)"><i class="zmdi zmdi-check"></i>Right Column (Desktop)</a></li>
                                                   </ul>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="mbl-preview-part">
                                       <div class="adpreview-holder sfeed-m">
                                          <div class="post-holder travad-box actionlink-travad">
                                             <div class="post-topbar">
                                                <div class="post-userinfo">
                                                   <div class="img-holder">
                                                      <div id="profiletip-4" class="profiletipholder">
                                                         <span class="profile-tooltip">
                                                         <img src="images/adimg-flight.jpg"/>
                                                         </span>
                                                      </div>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="travad-maintitle con_create_title">Jet Airways</div>
                                                      <span class="timestamp">Sponsored Ad</span>
                                                   </div>
                                                </div>
                                                <div class="settings-icon">
                                                   <div class="dropdown dropdown-custom dropdown-med">
                                                      <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="Sponsored_8">
                                                      <i class="zmdi zmdi-more"></i>
                                                      </a>
                                                      <ul id="Sponsored_8" class="dropdown-content custom_dropdown">
                                                         <li class="nicon"><a href="javascript:void(0)">Hide ad</a></li>
                                                         <li class="nicon"><a href="javascript:void(0)">Save ad</a></li>
                                                         <li class="nicon"><a href="javascript:void(0)">Mute this seller ads</a></li>
                                                         <li class="nicon"><a href="javascript:void(0)">Report ad</a></li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="post-content">
                                                <div class="shared-box shared-category">
                                                   <div class="post-holder">
                                                      <div class="post-content">
                                                         <div class="post-desc">
                                                            <p class="con_create_phrase">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                         </div>
                                                         <div class="post-img-holder">
                                                            <div class="post-img one-img">
                                                               <div class="pimg-holder"><img src="images/admain-flight.jpg"/></div>
                                                            </div>
                                                         </div>
                                                         <div class="share-summery">
                                                            <div class="travad-title con_create_head">Daily flight to London</div>
                                                            <div class="travad-subtitle con_create_text">Now we introduce a daily flight to London from the major cities of your country.</div>
                                                            <a href="javascript:void(0)" class="btn btn-primary adbtn waves-effect waves-light con_create_button">Book Now</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="preview-accordion">
                                       <ul class="collapsible" data-collapsible="accordion">
                                          <li>
                                             <h4 class="collapsible-header">Right Column (Desktop)</h4>
                                             <div class="collapsible-body">
                                                <div>
                                                   <div class="adpreview-holder rcolumn-d">
                                                      <div class="content-box">
                                                         <div class="cbox-desc">
                                                            <div class="side-travad actionlink-travad">
                                                               <div class="travad-maintitle">
                                                                  <img src="images/adimg-flight.jpg">
                                                                  <h6 class="con_create_title">Jet Airways</h6>
                                                                  <span>Sponsored</span>
                                                               </div>
                                                               <div class="imgholder">
                                                                  <img src="images/admain-flight.jpg"/>								
                                                               </div>
                                                               <div class="descholder">
                                                                  <div class="travad-title con_create_head">Daily flight to London</div>
                                                                  <div class="travad-subtitle con_create_text">Now we introduce a daily flight to London from the major cities of your country.</div>
                                                                  <a href="javascript:void(0)" class="btn btn-primary adbtn waves-effect waves-light con_create_button">Book Now</a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                          <li>
                                             <h4 class="collapsible-header">Stream Feeds (Mobile)</h4>
                                             <div class="collapsible-body">
                                                <div>
                                                   <div class="adpreview-holder sfeed-m">
                                                      <div class="post-holder travad-box actionlink-travad">
                                                         <div class="post-topbar">
                                                            <div class="post-userinfo">
                                                               <div class="img-holder">
                                                                  <div id="profiletip-4" class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img src="images/adimg-flight.jpg"/>
                                                                     </span>																						
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <div class="travad-maintitle con_create_title">Jet Airways</div>
                                                                  <span class="timestamp">Sponsored Ad</span>
                                                               </div>
                                                            </div>
                                                            <div class="settings-icon">
                                                               <div class="dropdown dropdown-custom dropdown-med">
                                                                  <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="Sponsored_9">
                                                                  <i class="zmdi zmdi-more"></i>
                                                                  </a>
                                                                  <ul id="Sponsored_9" class="dropdown-content custom_dropdown">
                                                                     <li class="nicon"><a href="javascript:void(0)">Hide ad</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Save ad</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Mute this seller ads</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Report ad</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="post-content">
                                                            <div class="shared-box shared-category">
                                                               <div class="post-holder">
                                                                  <div class="post-content">
                                                                     <div class="post-desc">
                                                                        <p class="con_create_phrase">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="post-img-holder">
                                                                        <div class="post-img one-img">
                                                                           <div class="pimg-holder"><img src="images/admain-flight.jpg"/></div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="share-summery">
                                                                        <div class="travad-title con_create_head">Daily flight to London</div>
                                                                        <div class="travad-subtitle con_create_text">Now we introduce a daily flight to London from the major cities of your country.</div>
                                                                        <a href="javascript:void(0)" class="btn btn-primary adbtn waves-effect waves-light con_create_button">Book Now</a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                          <li>
                                             <h4 class="collapsible-header active last-item">Stream Feeds (Desktop)</h4>
                                             <div class="collapsible-body">
                                                <div>
                                                   <div class="adpreview-holder sfeed-d">
                                                      <div class="post-holder travad-box actionlink-travad">
                                                         <div class="post-topbar">
                                                            <div class="post-userinfo">
                                                               <div class="img-holder">
                                                                  <div id="profiletip-4" class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img src="images/adimg-flight.jpg"/>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <div class="travad-maintitle con_create_title">Jet Airways</div>
                                                                  <span class="timestamp">Sponsored Ad</span>
                                                               </div>
                                                            </div>
                                                            <div class="settings-icon">
                                                               <div class="dropdown dropdown-custom dropdown-med">
                                                                  <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="Sponsored_10">
                                                                  <i class="zmdi zmdi-more"></i>
                                                                  </a>
                                                                  <ul id="Sponsored_10" class="dropdown-content custom_dropdown">
                                                                     <li class="nicon"><a href="javascript:void(0)">Hide ad</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Save ad</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Mute this seller ads</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Report ad</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="post-content">
                                                            <div class="shared-box shared-category">
                                                               <div class="post-holder">
                                                                  <div class="post-content">
                                                                     <div class="post-desc">
                                                                        <p class="con_create_phrase">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="post-img-holder">
                                                                        <div class="post-img one-img">
                                                                           <div class="pimg-holder"><img src="images/additem-photo.png"/></div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="share-summery">
                                                                        <div class="travad-title con_create_head">Daily flight to London</div>
                                                                        <div class="travad-subtitle con_create_text">Now we introduce a daily flight to London from the major cities of your country.</div>
                                                                        <a href="javascript:void(0)" class="btn btn-primary adbtn waves-effect waves-light con_create_button">Book Now</a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- inbox highlight -->									
                           <div class="travad-detailbox inboxhighlight">
                              <div class="row">
                                 <div class="col l5 m5 s12 detail-part">
                                    <div class="frow more_m5">
                                       <label>Advert Title <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom underlined anim-area fullwidth">
                                          <input type="text" class="fullwidth inbox_title" placeholder="Add your advert title">
                                       </div>
                                    </div>
                                    <div class="frow">
                                       <label>Logo Image <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="img-holder logoimg-holder">
                                          <img src="images/adimg-placeholder-logo.png"/>
                                          <div class="overlay">
                                             <span class="overlayUploader">
                                             <i class="mdi mdi-plus"></i>
                                             <input type="file" id="uploadWebsiteLeads" onchange="adPhotoInputChange(this,event,'adImageLogo')"/><a href="javascript:void(0);" class="popup-modal">&nbsp;</a>
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="frow remove_m10">
                                       <label>Main Catch Phrase <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom anim-area underlined fullwidth tt-holder">
                                          <input type='text' class="fullwidth inbox_main_catch" placeholder="Add your main catch phrase" data-length="30"/>
                                       </div>
                                    </div>
                                    <div class="frow remove_m10">
                                       <label>Sub Catch Phrase <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom anim-area underlined fullwidth tt-holder">
                                          <input type='text' class="fullwidth inbox_sub_catch" placeholder="Add your sub catch phrase" data-length="35">
                                       </div>
                                    </div>
                                    <div class="frow more_m5">
                                       <label>Headline <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom underlined anim-area fullwidth">
                                          <input type="text" class="fullwidth inbox_head" placeholder="Write a headline of your ad">
                                       </div>
                                    </div>
                                    <div class="frow remove_m10">
                                       <label>Text <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom anim-area underlined fullwidth tt-holder th70">
                                          <textarea class="materialize-textarea mb0 md_textarea descinput inbox_text" placeholder="Create personalized text that easily identify your brand name or product" data-length="140"></textarea>
                                       </div>
                                    </div>
                                    <div class="frow">
                                       <label>Select action button</label>
                                       <div class="btndrop">
                                          <select class="select2" tabindex="-1"  onchange="main_inbox_button(this.value)">
                                             <option>Shop Now</option>
                                             <option>Book Now</option>
                                             <option>Explore</option>
                                             <option>Know More</option>
                                             <option>Contact Us</option>
                                             <option>Sign Up</option>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col l7 m7 s12 preview-part">
                                    <div class="frow top-sec">
                                       <label>Advert Preview</label>														
                                       <div class="settings-icon">
                                          <div class="dropdown dropdown-custom dropdown-med resist">
                                             <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="setting_btn6"><i class="zmdi zmdi-more zmdi-hc-2x"></i></a>
                                             <ul id="setting_btn6" class="dropdown-content custom_dropdown">
                                                <li class="dmenu-title">Show advert in</li>
                                                <li class="divider"></li>
                                                <li>
                                                   <ul class="echeck-list">
                                                      <li class="selected"><a href="javascript:void(0)"><i class="zmdi zmdi-check"></i>Stream Feeds (Desktop)</a></li>
                                                      <li class="selected"><a href="javascript:void(0)"><i class="zmdi zmdi-check"></i>Stream Feeds (Mobile)</a></li>
                                                      <li class="selected"><a href="javascript:void(0)"><i class="zmdi zmdi-check"></i>Right Column (Desktop)</a></li>
                                                   </ul>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="mbl-preview-part">
                                       <div class="adpreview-holder sfeed-d inboxad-preview">
                                          <div class="summery-adpreview">
                                             <h5>Summery Ad Preview</h5>
                                             <ul>
                                                <li class="inbox-travad">
                                                   <a href="javascript:void(0)" onclick="openMessage(this)" id="ad">
                                                      <span class="muser-holder">
                                                         <span class="imgholder"><img src="images/inboxad-logo.jpg"/></span>
                                                         <span class="descholder">
                                                            <h6 class="inbox_create_main">20% discount on shoes</h6>
                                                            <p class="inbox_create_sub">on this christmas</p>
                                                            <span class="adspan"><span>Ad</span></span>
                                                         </span>
                                                      </span>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="clear"></div>
                                          <div class="detail-adpreview">
                                             <h5>Detail Ad Preview</h5>
                                             <div class="right-section inbox-advert">
                                                <div class="topstuff">
                                                   <div class="msgwindow-name">
                                                      <h4><label class="inbox_create_title">Puma Shoes</label> <span>Sponsored Ad</span></h4>
                                                   </div>
                                                </div>
                                                <div class="main-msgwindow">
                                                   <div class="allmsgs-holder">
                                                      <ul class="current-messages">
                                                         <li class="mainli active" id="li-travad">
                                                            <div class="msgdetail-list nice-scroll" tabindex="10">
                                                               <div class="inbox-travad">
                                                                  <img src="images/inboxad-img.png">
                                                                  <h6 class="inbox_create_head">Special discount this christmas</h6>
                                                                  <p class="inbox_create_head">On this christmas, we are bringing you flat 20% discount on our latest shoe ranges! Enjoy your christmas shopping with Puma.</p>
                                                                  <a href="javascript:void(0)" class="btn btn-primary btn-sm right inbox_create_button waves-effect waves-light">Shop Now</a>
                                                               </div>
                                                            </div>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="preview-accordion" id="ih-accrodion">
                                       <ul class="collapsible" data-collapsible="accordion">
                                          <li>
                                             <h4 class="collapsible-header">Stream Feeds (Mobile)</h4>
                                             <div class="collapsible-body">
                                                <div>
                                                   <div class="adpreview-holder sfeed-m inboxad-preview">
                                                      <div class="summery-adpreview">
                                                         <h5>Summery Ad Preview</h5>
                                                         <ul>
                                                            <li class="inbox-travad">
                                                               <a href="javascript:void(0)" onclick="openMessage(this)" id="ad">
                                                                  <span class="muser-holder">
                                                                     <span class="imgholder"><img src="images/inboxad-logo.jpg"/></span>
                                                                     <span class="descholder">
                                                                        <h6 class="inbox_create_main">20% discount on shoes</h6>
                                                                        <p class="inbox_create_sub">on this christmas</p>
                                                                        <span class="adspan"><span>Ad</span></span>
                                                                     </span>
                                                                  </span>
                                                               </a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                      <div class="clear"></div>
                                                      <div class="detail-adpreview">
                                                         <h5>Detail Ad Preview</h5>
                                                         <div class="right-section inbox-advert">
                                                            <div class="topstuff">
                                                               <div class="msgwindow-name">
                                                                  <a href="javascript:void(0)" class="backMessageList" onclick="closeMessage()"><i class="mdi mdi-menu-left"></i></a>
                                                                  <h4><label class="inbox_create_title">Puma Shoes</label> <span>Sponsored Ad</span></h4>
                                                               </div>
                                                            </div>
                                                            <div class="main-msgwindow">
                                                               <div class="allmsgs-holder">
                                                                  <ul class="current-messages">
                                                                     <li class="mainli active" id="li-travad">
                                                                        <div class="msgdetail-list nice-scroll" tabindex="10">
                                                                           <div class="inbox-travad">
                                                                              <img src="images/inboxad-img.png">
                                                                              <h6 class="inbox_create_head">Special discount this christmas</h6>
                                                                              <p class="inbox_create_text">On this christmas, we are bringing you flat 20% discount on our latest shoe ranges! Enjoy your christmas shopping with Puma.</p>
                                                                              <a href="javascript:void(0)" class="btn btn-primary btn-sm right inbox_create_button waves-effect waves-light">Shop Now</a>
                                                                           </div>
                                                                        </div>
                                                                     </li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                          <li>
                                             <h4 class="collapsible-header active last-item">Stream Feeds (Desktop)</h4>
                                             <div class="collapsible-body">
                                                <div>
                                                   <div class="adpreview-holder sfeed-d inboxad-preview">
                                                      <div class="summery-adpreview">
                                                         <h5>Summery Ad Preview</h5>
                                                         <ul>
                                                            <li class="inbox-travad">
                                                               <a href="javascript:void(0)" onclick="openMessage(this)" id="ad">
                                                                  <span class="muser-holder">
                                                                     <span class="imgholder"><img src="images/inboxad-logo.jpg"/></span>
                                                                     <span class="descholder">
                                                                        <h6 class="inbox_create_main">20% discount on shoes</h6>
                                                                        <p class="inbox_create_sub">on this christmas</p>
                                                                        <span class="adspan"><span>Ad</span></span>
                                                                     </span>
                                                                  </span>
                                                               </a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                      <div class="clear"></div>
                                                      <div class="detail-adpreview">
                                                         <h5>Detail Ad Preview</h5>
                                                         <div class="right-section inbox-advert">
                                                            <div class="topstuff">
                                                               <div class="msgwindow-name">
                                                                  <h4><label class="inbox_create_title">Puma Shoes</label> <span>Sponsored Ad</span></h4>
                                                               </div>
                                                            </div>
                                                            <div class="main-msgwindow">
                                                               <div class="allmsgs-holder">
                                                                  <ul class="current-messages">
                                                                     <li class="mainli active" id="li-travad">
                                                                        <div class="msgdetail-list nice-scroll" tabindex="10">
                                                                           <div class="inbox-travad">
                                                                              <img src="images/additem-photo.png">
                                                                              <h6 class="inbox_create_head">Special discount this christmas</h6>
                                                                              <p class="inbox_create_text">On this christmas, we are bringing you flat 20% discount on our latest shoe ranges! Enjoy your christmas shopping with Puma.</p>
                                                                              <a href="javascript:void(0)" class="btn btn-primary btn-sm right inbox_create_button waves-effect waves-light">Shop Now</a>
                                                                           </div>
                                                                        </div>
                                                                     </li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- page endorsement -->									
                           <div class="travad-detailbox pageendorse">
                              <div class="row">
                                 <div class="col l5 m5 s12 detail-part">
                                    <div class="frow">
                                       <label>Select a page to advert</label>
                                       <div class="sliding-middle-custom anim-area underlined fullwidth">
                                          <select class="select2" tabindex="-1"  onchange="pageendorse_change(this.value)">
                                             <option>Hyatt Hotel</option>
                                             <option>Qatar Trips</option>
                                             <option>Politics</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="frow">
                                       <label>Catch Phrase <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom anim-area underlined fullwidth tt-holder th50">
                                          <textarea class="materialize-textarea mb0 md_textarea descinput endorsement_phrase" placeholder="Slogan your brand in a few words" data-length="70"></textarea>
                                       </div>
                                    </div>
                                    <div class="frow">
                                       <label>Headline <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom underlined anim-area fullwidth">
                                          <input type="text" class="fullwidth endorsement_head" placeholder="Write a headline of your ad">
                                       </div>
                                    </div>
                                    <div class="frow">
                                       <label>Text <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       <div class="sliding-middle-custom anim-area underlined fullwidth tt-holder th70">
                                          <textarea class="materialize-textarea mb0 md_textarea descinput endorsement_text" placeholder="Create personalized text that easily identify your brand name or product" data-length="140"></textarea>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col l7 m7 s12 preview-part">
                                    <div class="frow top-sec">
                                       <label>Advert Preview</label>														
                                       <div class="settings-icon">
                                          <div class="dropdown dropdown-custom dropdown-med resist">
                                             <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="setting_btn7"><i class="zmdi zmdi-more zmdi-hc-2x"></i></a>
                                             <ul id="setting_btn7" class="dropdown-content custom_dropdown">
                                                <li class="dmenu-title">Show advert in</li>
                                                <li class="divider"></li>
                                                <li>
                                                   <ul class="echeck-list">
                                                      <li class="selected"><a href="javascript:void(0)"><i class="zmdi zmdi-check"></i>Stream Feeds (Desktop)</a></li>
                                                      <li class="selected"><a href="javascript:void(0)"><i class="zmdi zmdi-check"></i>Stream Feeds (Mobile)</a></li>
                                                      <li class="selected"><a href="javascript:void(0)"><i class="zmdi zmdi-check"></i>Right Column (Desktop)</a></li>
                                                   </ul>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="mbl-preview-part">
                                       <div class="adpreview-holder sfeed-m">
                                          <div class="post-holder travad-box page-travad">
                                             <div class="post-topbar">
                                                <div class="post-userinfo">
                                                   <div class="img-holder">
                                                      <div id="profiletip-4" class="profiletipholder">
                                                         <span class="profile-tooltip">
                                                         <img class="circle" src="images/hyattprofile.jpg"/>
                                                         </span>
                                                      </div>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <a href="javascript:void(0)" class="endorsement_create_value">Hyatt Hotel</a>
                                                      <span class="timestamp">Sponsored Ad</span>
                                                   </div>
                                                </div>
                                                <div class="settings-icon">
                                                   <div class="dropdown dropdown-custom dropdown-med">
                                                      <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="Sponsored_11">
                                                      <i class="zmdi zmdi-more"></i>
                                                      </a>
                                                      <ul id="Sponsored_11" class="dropdown-content custom_dropdown">
                                                         <li class="nicon"><a href="javascript:void(0)">Hide ad</a></li>
                                                         <li class="nicon"><a href="javascript:void(0)">Save ad</a></li>
                                                         <li class="nicon"><a href="javascript:void(0)">Mute this seller ads</a></li>
                                                         <li class="nicon"><a href="javascript:void(0)">Report ad</a></li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="post-content">
                                                <div class="shared-box shared-category">
                                                   <div class="post-holder">
                                                      <div class="post-content">
                                                         <div class="post-desc">
                                                            <p class="endorsement_create_phrase">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                         </div>
                                                         <div class="post-img-holder">
                                                            <div class="post-img one-img">
                                                               <div class="pimg-holder"><img src="images/pagead-endorse.jpg"/></div>
                                                            </div>
                                                         </div>
                                                         <div class="share-summery">
                                                            <div class="travad-title endorsement_create_head">Best facilites you ever found!</div>
                                                            <div class="travad-subtitle endorsement_create_text">Endorse us for the best hospitality services we provide.</div>
                                                            <div class="travad-info">78 people endorsed this page</div>
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn waves-effect waves-light">Endorse</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="preview-accordion">
                                       <ul class="collapsible" data-collapsible="accordion">
                                          <li>
                                             <h4 class="collapsible-header">Right Column (Desktop)</h4>
                                             <div class="collapsible-body">
                                                <div>
                                                   <div class="adpreview-holder rcolumn-d">
                                                      <div class="content-box bshadow">
                                                         <div class="cbox-desc">
                                                            <div class="side-travad page-travad">
                                                               <div class="travad-maintitle">
                                                                  <img src="images/hyattprofile.jpg">
                                                                  <h6 class="head_chtitle endorsement_create_value">Hyatt Hotel</h6>
                                                                  <span>Sponsored</span>
                                                               </div>
                                                               <div class="imgholder">
                                                                  <img src="images/pagead-endorse.jpg"/>								
                                                               </div>
                                                               <div class="descholder">
                                                                  <div class="travad-title endorsement_create_main endorsement_create_head">Best facilites you ever found!</div>
                                                                  <div class="travad-subtitle endorsement_create_text">Endorse us for the best hospitality services we provide.</div>
                                                                  <div class="travad-info">78 people endorsed this page</div>
                                                                  <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn waves-effect waves-light endorsement_create_button">Endorse</a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                          <li>
                                             <h4 class="collapsible-header">Stream Feeds (Mobile)</h4>
                                             <div class="collapsible-body">
                                                <div>
                                                   <div class="adpreview-holder sfeed-m">
                                                      <div class="post-holder travad-box page-travad">
                                                         <div class="post-topbar">
                                                            <div class="post-userinfo">
                                                               <div class="img-holder">
                                                                  <div id="profiletip-4" class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/hyattprofile.jpg"/>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <a href="javascript:void(0)" class="endorsement_create_value">Hyatt Hotel</a>
                                                                  <span class="timestamp">Sponsored Ad</span>
                                                               </div>
                                                            </div>
                                                            <div class="settings-icon">
                                                               <div class="dropdown dropdown-custom dropdown-med">
                                                                  <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="Sponsored_12">
                                                                  <i class="zmdi zmdi-more"></i>
                                                                  </a>
                                                                  <ul id="Sponsored_12" class="dropdown-content custom_dropdown">
                                                                     <li class="nicon"><a href="javascript:void(0)">Hide ad</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Save ad</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Mute this seller ads</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Report ad</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="post-content">
                                                            <div class="shared-box shared-category">
                                                               <div class="post-holder">
                                                                  <div class="post-content">
                                                                     <div class="post-desc">
                                                                        <p class="endorsement_create_phrase">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="post-img-holder">
                                                                        <div class="post-img one-img">
                                                                           <div class="pimg-holder"><img src="images/pagead-endorse.jpg"/></div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="share-summery">
                                                                        <div class="travad-title endorsement_create_head">Best facilites you ever found!</div>
                                                                        <div class="travad-subtitle endorsement_create_text">Endorse us for the best hospitality services we provide.</div>
                                                                        <div class="travad-info">78 people endorsed this page</div>
                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn waves-effect waves-light">Endorse</a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                          <li>
                                             <h4 class="collapsible-header active last-item">Stream Feeds (Desktop)</h4>
                                             <div class="collapsible-body">
                                                <div>
                                                   <div class="adpreview-holder sfeed-d">
                                                      <div class="post-holder travad-box page-travad">
                                                         <div class="post-topbar">
                                                            <div class="post-userinfo">
                                                               <div class="img-holder">
                                                                  <div id="profiletip-4" class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/hyattprofile.jpg"/>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <a href="javascript:void(0)" class="endorsement_create_value">Hyatt Hotel</a>
                                                                  <span class="timestamp">Sponsored Ad</span>
                                                               </div>
                                                            </div>
                                                            <div class="settings-icon">
                                                               <div class="dropdown dropdown-custom dropdown-med">
                                                                  <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="Sponsored_13">
                                                                  <i class="zmdi zmdi-more"></i>
                                                                  </a>
                                                                  <ul id="Sponsored_13" class="dropdown-content custom_dropdown">
                                                                     <li class="nicon"><a href="javascript:void(0)">Hide ad</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Save ad</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Mute this seller ads</a></li>
                                                                     <li class="nicon"><a href="javascript:void(0)">Report ad</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="post-content">
                                                            <div class="shared-box shared-category">
                                                               <div class="post-holder">
                                                                  <div class="post-content">
                                                                     <div class="post-desc">
                                                                        <p class="endorsement_create_phrase">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="post-img-holder">
                                                                        <div class="post-img one-img">
                                                                           <div class="pimg-holder"><img src="images/additem-photo.png"/></div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="share-summery">
                                                                        <div class="travad-title endorsement_create_head">Best facilites you ever found!</div>
                                                                        <div class="travad-subtitle endorsement_create_text">Endorse us for the best hospitality services we provide.</div>
                                                                        <div class="travad-info">78 people endorsed this page</div>
                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn waves-effect waves-light">Endorse</a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="adstep-holder hasDivider">
                        <div class="adsec-divider">
                        </div>
                        <div class="adstep-title">
                           <img src="images/adstep-audience.png"/>
                           <h6>Target your audience</h6>
                           <p>Create your audience details to achieve the desired daily reach</p>
                        </div>
                        <div class="adstep-details">
                           <div class="row">
                              <div class="col l7 m6 s12"> 
                                 <div class="audience-form">
                                    <div class="frow">
                                       <div class="caption-holder">
                                          <label>Locations <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       </div>
                                       <div class="detail-holder">
                                          <div class="sliding-middle-out anim-area underlined fullwidth dropdown782">
                                             <select data-fill="y" data-selectore="country123" data-action="country" class="country123 select2" multiple ='multiple' id='ads_loc' tabindex="-1">
                                                <option value="" disabled selected>Choose location</option>
                                                <?php
                                                  $country = array("AFGHANISTAN" => "AFGHANISTAN", "ALBANIA" => "ALBANIA", "ALGERIA" => "ALGERIA", "AMERICAN SAMOA" => "AMERICAN SAMOA", "ANDORRA" => "ANDORRA", "ANGOLA" => "ANGOLA", "ANGUILLA" => "ANGUILLA", "ANTARCTICA" => "ANTARCTICA", "ANTIGUA AND BARBUDA" => "ANTIGUA AND BARBUDA", "ARGENTINA" => "ARGENTINA", "ARMENIA" => "ARMENIA", "ARUBA" => "ARUBA", "AUSTRALIA" => "AUSTRALIA", "AUSTRIA" => "AUSTRIA", "AZERBAIJAN" => "AZERBAIJAN", "BAHAMAS" => "BAHAMAS", "BAHRAIN" => "BAHRAIN", "BANGLADESH" => "BANGLADESH", "BARBADOS" => "BARBADOS", "BELARUS" => "BELARUS", "BELGIUM" => "BELGIUM", "BELIZE" => "BELIZE", "BENIN" => "BENIN", "BERMUDA" => "BERMUDA", "BHUTAN" => "BHUTAN", "BOLIVIA" => "BOLIVIA", "BOSNIA AND HERZEGOVINA" => "BOSNIA AND HERZEGOVINA", "BOTSWANA" => "BOTSWANA", "BOUVET ISLAND" => "BOUVET ISLAND", "BRAZIL" => "BRAZIL", "BRITISH INDIAN OCEAN TERRITORY" => "BRITISH INDIAN OCEAN TERRITORY", "BRUNEI DARUSSALAM" => "BRUNEI DARUSSALAM", "BULGARIA" => "BULGARIA", "BURKINA FASO" => "BURKINA FASO", "BURUNDI" => "BURUNDI", "CAMBODIA" => "CAMBODIA", "CAMEROON" => "CAMEROON", "CANADA" => "CANADA", "CAPE VERDE" => "CAPE VERDE", "CAYMAN ISLANDS" => "CAYMAN ISLANDS", "CENTRAL AFRICAN REPUBLIC" => "CENTRAL AFRICAN REPUBLIC", "CHAD" => "CHAD", "CHILE" => "CHILE", "CHINA" => "CHINA", "CHRISTMAS ISLAND" => "CHRISTMAS ISLAND", "COCOS (KEELING) ISLANDS" => "COCOS (KEELING) ISLANDS", "COLOMBIA" => "COLOMBIA", "COMOROS" => "COMOROS", "CONGO" => "CONGO", "CONGO, THE DEMOCRATIC REPUBLIC OF THE" => "CONGO, THE DEMOCRATIC REPUBLIC OF THE", "COOK ISLANDS" => "COOK ISLANDS", "COSTA RICA" => "COSTA RICA", "COTE D'IVOIRE" => "COTE D'IVOIRE", "CROATIA" => "CROATIA", "CUBA" => "CUBA", "CYPRUS" => "CYPRUS", "CZECH REPUBLIC" => "CZECH REPUBLIC", "DENMARK" => "DENMARK", "DJIBOUTI" => "DJIBOUTI", "DOMINICA" => "DOMINICA", "DOMINICAN REPUBLIC" => "DOMINICAN REPUBLIC", "ECUADOR" => "ECUADOR", "EGYPT" => "EGYPT", "EL SALVADOR" => "EL SALVADOR", "EQUATORIAL GUINEA" => "EQUATORIAL GUINEA", "ERITREA" => "ERITREA", "ESTONIA" => "ESTONIA", "ETHIOPIA" => "ETHIOPIA", "FALKLAND ISLANDS (MALVINAS)" => "FALKLAND ISLANDS (MALVINAS)", "FAROE ISLANDS" => "FAROE ISLANDS", "FIJI" => "FIJI", "FINLAND" => "FINLAND", "FRANCE" => "FRANCE", "FRENCH GUIANA" => "FRENCH GUIANA", "FRENCH POLYNESIA" => "FRENCH POLYNESIA", "FRENCH SOUTHERN TERRITORIES" => "FRENCH SOUTHERN TERRITORIES", "GABON" => "GABON", "GAMBIA" => "GAMBIA", "GEORGIA" => "GEORGIA", "GERMANY" => "GERMANY", "GHANA" => "GHANA", "GIBRALTAR" => "GIBRALTAR", "GREECE" => "GREECE", "GREENLAND" => "GREENLAND", "GRENADA" => "GRENADA", "GUADELOUPE" => "GUADELOUPE", "GUAM" => "GUAM", "GUATEMALA" => "GUATEMALA", "GUINEA" => "GUINEA", "GUINEA-BISSAU" => "GUINEA-BISSAU", "GUYANA" => "GUYANA", "HAITI" => "HAITI", "HEARD ISLAND AND MCDONALD ISLANDS" => "HEARD ISLAND AND MCDONALD ISLANDS", "HOLY SEE (VATICAN CITY STATE)" => "HOLY SEE (VATICAN CITY STATE)", "HONDURAS" => "HONDURAS", "HONG KONG" => "HONG KONG", "HUNGARY" => "HUNGARY", "ICELAND" => "ICELAND", "INDIA" => "INDIA", "INDONESIA" => "INDONESIA", "IRAN, ISLAMIC REPUBLIC OF" => "IRAN, ISLAMIC REPUBLIC OF", "IRAQ" => "IRAQ", "IRELAND" => "IRELAND", "ISRAEL" => "ISRAEL", "ITALY" => "ITALY", "JAMAICA" => "JAMAICA", "JAPAN" => "JAPAN", "JORDAN" => "JORDAN", "KAZAKHSTAN" => "KAZAKHSTAN", "KENYA" => "KENYA", "KIRIBATI" => "KIRIBATI", "KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF" => "KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF", "KOREA, REPUBLIC OF" => "KOREA, REPUBLIC OF", "KUWAIT" => "KUWAIT", "KYRGYZSTAN" => "KYRGYZSTAN", "LAO PEOPLE'S DEMOCRATIC REPUBLIC" => "LAO PEOPLE'S DEMOCRATIC REPUBLIC", "LATVIA" => "LATVIA", "LEBANON" => "LEBANON", "LESOTHO" => "LESOTHO", "LIBERIA" => "LIBERIA", "LIBYAN ARAB JAMAHIRIYA" => "LIBYAN ARAB JAMAHIRIYA", "LIECHTENSTEIN" => "LIECHTENSTEIN", "LITHUANIA" => "LITHUANIA", "LUXEMBOURG" => "LUXEMBOURG", "MACAO" => "MACAO", "MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF" => "MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF", "MADAGASCAR" => "MADAGASCAR", "MALAWI" => "MALAWI", "MALAYSIA" => "MALAYSIA", "MALDIVES" => "MALDIVES", "MALI" => "MALI", "MALTA" => "MALTA", "MARSHALL ISLANDS" => "MARSHALL ISLANDS", "MARTINIQUE" => "MARTINIQUE", "MAURITANIA" => "MAURITANIA", "MAURITIUS" => "MAURITIUS", "MAYOTTE" => "MAYOTTE", "MEXICO" => "MEXICO", "MICRONESIA, FEDERATED STATES OF" => "MICRONESIA, FEDERATED STATES OF", "MOLDOVA, REPUBLIC OF" => "MOLDOVA, REPUBLIC OF", "MONACO" => "MONACO", "MONGOLIA" => "MONGOLIA", "MONTSERRAT" => "MONTSERRAT", "MOROCCO" => "MOROCCO", "MOZAMBIQUE" => "MOZAMBIQUE", "MYANMAR" => "MYANMAR", "NAMIBIA" => "NAMIBIA", "NAURU" => "NAURU", "NEPAL" => "NEPAL", "NETHERLANDS" => "NETHERLANDS", "NETHERLANDS ANTILLES" => "NETHERLANDS ANTILLES", "NEW CALEDONIA" => "NEW CALEDONIA", "NEW ZEALAND" => "NEW ZEALAND", "NICARAGUA" => "NICARAGUA", "NIGER" => "NIGER", "NIGERIA" => "NIGERIA", "NIUE" => "NIUE", "NORFOLK ISLAND" => "NORFOLK ISLAND", "NORTHERN MARIANA ISLANDS" => "NORTHERN MARIANA ISLANDS", "NORWAY" => "NORWAY", "OMAN" => "OMAN", "PAKISTAN" => "PAKISTAN", "PALAU" => "PALAU", "PALESTINIAN TERRITORY, OCCUPIED" => "PALESTINIAN TERRITORY, OCCUPIED", "PANAMA" => "PANAMA", "PAPUA NEW GUINEA" => "PAPUA NEW GUINEA", "PARAGUAY" => "PARAGUAY", "PERU" => "PERU", "PHILIPPINES" => "PHILIPPINES", "PITCAIRN" => "PITCAIRN", "POLAND" => "POLAND", "PORTUGAL" => "PORTUGAL", "PUERTO RICO" => "PUERTO RICO", "QATAR" => "QATAR", "REUNION" => "REUNION", "ROMANIA" => "ROMANIA", "RUSSIAN FEDERATION" => "RUSSIAN FEDERATION", "RWANDA" => "RWANDA", "SAINT HELENA" => "SAINT HELENA", "SAINT KITTS AND NEVIS" => "SAINT KITTS AND NEVIS", "SAINT LUCIA" => "SAINT LUCIA", "SAINT PIERRE AND MIQUELON" => "SAINT PIERRE AND MIQUELON", "SAINT VINCENT AND THE GRENADINES" => "SAINT VINCENT AND THE GRENADINES", "SAMOA" => "SAMOA", "SAN MARINO" => "SAN MARINO", "SAO TOME AND PRINCIPE" => "SAO TOME AND PRINCIPE", "SAUDI ARABIA" => "SAUDI ARABIA", "SENEGAL" => "SENEGAL", "SERBIA AND MONTENEGRO" => "SERBIA AND MONTENEGRO", "SEYCHELLES" => "SEYCHELLES", "SIERRA LEONE" => "SIERRA LEONE", "SINGAPORE" => "SINGAPORE", "SLOVAKIA" => "SLOVAKIA", "SLOVENIA" => "SLOVENIA", "SOLOMON ISLANDS" => "SOLOMON ISLANDS", "SOMALIA" => "SOMALIA", "SOUTH AFRICA" => "SOUTH AFRICA", "SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS" => "SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS", "SPAIN" => "SPAIN", "SRI LANKA" => "SRI LANKA", "SUDAN" => "SUDAN", "SURINAME" => "SURINAME", "SVALBARD AND JAN MAYEN" => "SVALBARD AND JAN MAYEN", "SWAZILAND" => "SWAZILAND", "SWEDEN" => "SWEDEN", "SWITZERLAND" => "SWITZERLAND", "SYRIAN ARAB REPUBLIC" => "SYRIAN ARAB REPUBLIC", "TAIWAN, PROVINCE OF CHINA" => "TAIWAN, PROVINCE OF CHINA", "TAJIKISTAN" => "TAJIKISTAN", "TANZANIA, UNITED REPUBLIC OF" => "TANZANIA, UNITED REPUBLIC OF", "THAILAND" => "THAILAND", "TIMOR-LESTE" => "TIMOR-LESTE", "TOGO" => "TOGO", "TOKELAU" => "TOKELAU", "TONGA" => "TONGA", "TRINIDAD AND TOBAGO" => "TRINIDAD AND TOBAGO", "TUNISIA" => "TUNISIA", "TURKEY" => "TURKEY", "TURKMENISTAN" => "TURKMENISTAN", "TURKS AND CAICOS ISLANDS" => "TURKS AND CAICOS ISLANDS", "TUVALU" => "TUVALU", "UGANDA" => "UGANDA", "UKRAINE" => "UKRAINE", "UNITED ARAB EMIRATES" => "UNITED ARAB EMIRATES", "UNITED KINGDOM" => "UNITED KINGDOM", "UNITED STATES" => "UNITED STATES", "UNITED STATES MINOR OUTLYING ISLANDS" => "UNITED STATES MINOR OUTLYING ISLANDS", "URUGUAY" => "URUGUAY", "UZBEKISTAN" => "UZBEKISTAN", "VANUATU" => "VANUATU", "VENEZUELA" => "VENEZUELA", "VIET NAM" => "VIET NAM", "VIRGIN ISLANDS, BRITISH" => "VIRGIN ISLANDS, BRITISH", "VIRGIN ISLANDS, U.S." => "VIRGIN ISLANDS, U.S.", "WALLIS AND FUTUNA" => "WALLIS AND FUTUNA", "WESTERN SAHARA" => "WESTERN SAHARA", "YEMEN" => "YEMEN", "ZAMBIA" => "ZAMBIA", "ZIMBABWE" => "ZIMBABWE");
                                                   
                                                   foreach ($country as $scountry) {
                                                      echo "<option value=".$scountry.">$scountry</option>";
                                                   }
                                                ?>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="frow">
                                       <div class="caption-holder">
                                          <label>Age <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       </div>
                                       <div class="detail-holder">
                                          <div class="range-slider range-slider2">
                                             <input type="text" class="amount" readonly>
                                             <div id="test-slider1"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="frow">
                                       <div class="caption-holder">
                                          <label>Language <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       </div>
                                       <div class="detail-holder">
                                          <div class="sliding-middle-out anim-area underlined fullwidth dropdown782">
                                             <select data-fill="y" data-selectore="language123" data-action="language" class="language123 select2" multiple ='multiple' id='ads_lang' tabindex="-1">
                                                <option value="" disabled selected>Choose language</option>
                                                <?php
                                                  $language = array("English" => "English", "Afar" => "Afar", "Abkhazian" => "Abkhazian", "Afrikaans" => "Afrikaans", "Amharic" => "Amharic", "Arabic" => "Arabic", "Assamese" => "Assamese", "Aymara" => "Aymara", "Azerbaijani" => "Azerbaijani", "Bashkir" => "Bashkir", "Belarusian" => "Belarusian", "Bulgarian" => "Bulgarian", "Bihari" => "Bihari", "Bislama" => "Bislama", "Bengali/Bangla" => "Bengali/Bangla", "Tibetan" => "Tibetan", "Breton" => "Breton", "Catalan" => "Catalan", "Corsican" => "Corsican", "Czech" => "Czech", "Welsh" => "Welsh", "Danish" => "Danish", "German" => "German", "Bhutani" => "Bhutani", "Greek" => "Greek", "Esperanto" => "Esperanto", "Spanish" => "Spanish", "Estonian" => "Estonian", "Basque" => "Basque", "Persian" => "Persian", "Finnish" => "Finnish", "Fiji" => "Fiji", "Faeroese" => "Faeroese", "French" => "French", "Frisian" => "Frisian", "Irish" => "Irish", "Scots/Gaelic" => "Scots/Gaelic", "Galician" => "Galician", "Guarani" => "Guarani", "Gujarati" => "Gujarati", "Hausa" => "Hausa", "Hindi" => "Hindi", "Croatian" => "Croatian", "Hungarian" => "Hungarian", "Armenian" => "Armenian", "Interlingua" => "Interlingua", "Interlingue" => "Interlingue", "Inupiak" => "Inupiak", "Indonesian" => "Indonesian", "Icelandic" => "Icelandic", "Italian" => "Italian", "Hebrew" => "Hebrew", "Japanese" => "Japanese", "Yiddish" => "Yiddish", "Javanese" => "Javanese", "Georgian" => "Georgian", "Kazakh" => "Kazakh", "Greenlandic" => "Greenlandic", "Cambodian" => "Cambodian", "Kannada" => "Kannada", "Korean" => "Korean", "Kashmiri" => "Kashmiri", "Kurdish" => "Kurdish", "Kirghiz" => "Kirghiz", "Latin" => "Latin", "Lingala" => "Lingala", "Laothian" => "Laothian", "Lithuanian" => "Lithuanian", "Latvian/Lettis" => "Latvian/Lettis", "Malagasy" => "Malagasy", "Maori" => "Maori", "Macedonian" => "Macedonian", "Malayalam" => "Malayalam", "Mongolian" => "Mongolian", "Moldavian" => "Moldavian", "Marathi" => "Marathi", "Malay" => "Malay", "Maltese" => "Maltese", "Burmese" => "Burmese", "Nauru" => "Nauru", "Nepali" => "Nepali", "Dutch" => "Dutch", "Norwegian" => "Norwegian", "Occitan" => "Occitan", "Afan" => "Afan", "Punjabi" => "Punjabi", "Polish" => "Polish", "Pashto/Pushto" => "Pashto/Pushto", "Portuguese" => "Portuguese", "Quechua" => "Quechua", "Rhaeto-Romance" => "Rhaeto-Romance", "Kirundi" => "Kirundi", "Romanian" => "Romanian", "Russian" => "Russian", "Kinyarwanda" => "Kinyarwanda", "Sanskrit" => "Sanskrit", "Sindhi" => "Sindhi", "Sangro" => "Sangro", "Serbo-Croatian" => "Serbo-Croatian", "Singhalese" => "Singhalese", "Slovak" => "Slovak", "Slovenian" => "Slovenian", "Samoan" => "Samoan", "Shona" => "Shona", "Somali" => "Somali", "Albanian" => "Albanian", "Serbian" => "Serbian", "Siswati" => "Siswati", "Sesotho" => "Sesotho", "Sundanese" => "Sundanese", "Swedish" => "Swedish", "Swahili" => "Swahili", "Tamil" => "Tamil", "Telugu" => "Telugu", "Tajik" => "Tajik", "Thai" => "Thai", "Tigrinya" => "Tigrinya", "Turkmen" => "Turkmen", "Tagalog" => "Tagalog", "Setswana" => "Setswana", "Tonga" => "Tonga", "Turkish" => "Turkish", "Tsonga" => "Tsonga", "Tatar" => "Tatar", "Twi" => "Twi", "Ukrainian" => "Ukrainian", "Urdu" => "Urdu", "Uzbek" => "Uzbek", "Vietnamese" => "Vietnamese", "Volapuk" => "Volapuk", "Wolof" => "Wolof", "Xhosa" => "Xhosa", "Yoruba" => "Yoruba", "Chinese" => "Chinese", "Zulu" => "Zulu");
                                                   
                                                   foreach ($language as $s9032n) {
                                                      echo "<option value=".$s9032n.">$s9032n</option>";
                                                   }
                                                ?>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="frow">
                                       <div class="caption-holder">
                                          <label>Gender <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       </div>
                                       <div class="detail-holder">
                                          <ul class="checkul">
                                             <li>
                                                <div class="h-checkbox entertosend leftbox">
                                                   <input type="checkbox" id="test1">
                                                   <label>Male</label>
                                                </div>
                                             </li>
                                             <li>
                                                <div class="h-checkbox entertosend leftbox">
                                                   <input type="checkbox" id="test1">
                                                   <label>Female</label>
                                                </div>
                                             </li>
                                             <li>
                                                <div class="h-checkbox entertosend leftbox">
                                                   <input type="checkbox" id="test1">
                                                   <label>Several People</label>
                                                </div>
                                             </li>
                                          </ul>
                                       </div>
                                    </div>
                                    <div class="frow">
                                       <div class="caption-holder">
                                          <label>Proficient <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       </div>
                                       <div class="detail-holder">
                                          <div class="sliding-middle-out anim-area underlined fullwidth dropdown782">
                                             <select data-fill="y" data-selectore="proficient123" data-action="proficient" class="proficient123 select2" multiple ='multiple' id='ads_pro' tabindex="-1">
                                                <option value="" disabled selected>Choose proficient</option>
                                                <?php
                                                  $proficient = array("Actor" => "Actor", "Actuary" => "Actuary", "Administrative worker" => "Administrative worker", "Advertising manager" => "Advertising manager", "Aerial rigger" => "Aerial rigger", "Agriculturaladviser" => "Agriculturaladviser", "Agriculturalmechanic" => "Agriculturalmechanic", "Agronomist" => "Agronomist", "Aircontroller" => "Aircontroller", "Airtechnician" => "Airtechnician", "Aircrafttechnician" => "Aircrafttechnician", "Aircraft mechanic" => "Aircraft mechanic", "Airlineagent" => "Airlineagent", "Ammunitionworker" => "Ammunitionworker", "Animal technician" => "Animal technician", "Animator" => "Animator", "Anthropologist" => "Anthropologist", "Applicationsmanager" => "Applicationsmanager", "Apprentice" => "Apprentice", "Archeologist" => "Archeologist", "Architect" => "Architect", "Architecturalofficer" => "Architecturalofficer", "Arthistorian" => "Arthistorian", "Artglazier" => "Artglazier", "Artlocksmith" => "Artlocksmith", "Art photographer" => "Art photographer", "Art restorer" => "Art restorer", "Articledassistant" => "Articledassistant", "Artificialmaker" => "Artificialmaker", "Artisticmanager" => "Artisticmanager", "Assessorassessor" => "Assessorassessor", "Assistanthelp" => "Assistanthelp", "Assistantworker" => "Assistantworker", "Astrologer" => "Astrologer", "Astronomer" => "Astronomer", "Athlete" => "Athlete", "Auctioneer" => "Auctioneer", "Audiodesigner" => "Audiodesigner", "Auditor" => "Auditor", "Auto-electrician" => "Auto-electrician", "Auxiliaryfiller" => "Auxiliaryfiller", "Auxiliaryworker" => "Auxiliaryworker", "Auxiliaryclothing" => "Auxiliaryclothing", "Auxiliarywoodcutting" => "Auxiliarywoodcutting", "Auxiliarydistribution" => "Auxiliarydistribution", "Auxiliaryworkerproduction" => "Auxiliaryworkerproduction", "Baker" => "Baker", "Bankservice" => "Bankservice", "Bankcredit" => "Bankcredit", "Banking expert" => "Banking expert", "Barber" => "Barber", "Barman/barmaid" => "Barman/barmaid", "Basket-makerweaver" => "Basket-makerweaver", "Beauticiantherapist" => "Beauticiantherapist", "Beekeeper" => "Beekeeper", "Bibliographer" => "Bibliographer", "Biochemist" => "Biochemist", "Biologist" => "Biologist", "Biotechnologist" => "Biotechnologist", "Biscuit maker" => "Biscuit maker", "Blacksmith" => "Blacksmith", "Blastersexpert" => "Blastersexpert", "Blast-furnaceman/woman" => "Blast-furnaceman/woman", "Blastingengineer" => "Blastingengineer", "Boatman/woman" => "Boatman/woman", "Boilerattendant" => "Boilerattendant", "Boilermaker/fitter" => "Boilermaker/fitter", "Bookbinder" => "Bookbinder", "Bookkeeper" => "Bookkeeper", "Bookmaker" => "Bookmaker", "Botanist" => "Botanist", "Brewermaltster" => "Brewermaltster", "Bricklayer" => "Bricklayer", "Broadcaster/announcertelecaster" => "Broadcaster/announcertelecaster", "Brush-makermaker" => "Brush-makermaker", "Builders industry" => "Builders industry", "Buildingmechanic" => "Buildingmechanic", "Building electrician" => "Building electrician", "Buildinglocksmith" => "Buildinglocksmith", "Building inspector" => "Building inspector", "Buildingoperator" => "Buildingoperator", "Buildingoperative" => "Buildingoperative", "Building tinsmith" => "Building tinsmith", "Building/civiltechnician/technologist" => "Building/civiltechnician/technologist", "Butchersausage-maker" => "Butchersausage-maker", "Butler" => "Butler", "Button maker" => "Button maker", "Cab/taxi dispatcher" => "Cab/taxi dispatcher", "Cabinet maker" => "Cabinet maker", "Cabledriver" => "Cabledriver", "Cablemaker" => "Cablemaker", "Camera mechanic" => "Camera mechanic", "Cameracrew" => "Cameracrew", "Canningoperator" => "Canningoperator", "Capitalclerk/officer" => "Capitalclerk/officer", "Captainaircraft" => "Captainaircraft", "Car mechanic" => "Car mechanic", "Car" => "Car", "Careorganiser" => "Careorganiser", "Careerdiplomat" => "Careerdiplomat", "Careeradviser" => "Careeradviser", "Caretaker" => "Caretaker", "Carpenter" => "Carpenter", "Cartographer" => "Cartographer", "Cellulose operator" => "Cellulose operator", "Ceramicmaker" => "Ceramicmaker", "Ceramic" => "Ceramic", "Ceramicist" => "Ceramicist", "Ceramics/pottery maker" => "Ceramics/pottery maker", "Charterbroker" => "Charterbroker", "Cheese maker" => "Cheese maker", "Chemicaloperator" => "Chemicaloperator", "Chemicalmanager" => "Chemicalmanager", "Chemicalassistant" => "Chemicalassistant", "Chemicalproduction" => "Chemicalproduction", "Chemicalindustries" => "Chemicalindustries", "Chemical researcher" => "Chemical researcher", "Chemical technologist" => "Chemical technologist", "Chief/senior" => "Chief/senior", "Chimney sweep" => "Chimney sweep", "Chipboard/fibreboardoperative" => "Chipboard/fibreboardoperative", "Choir master/mistress" => "Choir master/mistress", "Choreographer" => "Choreographer", "Circusperformer" => "Circusperformer", "Cleaner" => "Cleaner", "Clerksystems" => "Clerksystems", "Cloakroom attendant" => "Cloakroom attendant", "Coffee roaster" => "Coffee roaster", "Coffeehouseowner" => "Coffeehouseowner", "Commentator,reporter" => "Commentator,reporter", "Commercial lawyer" => "Commercial lawyer", "Company lawyer" => "Company lawyer", "Composer" => "Composer", "Computerengineer" => "Computerengineer", "Computeroperator" => "Computeroperator", "Computermanager" => "Computermanager", "Computerprogrammer" => "Computerprogrammer", "Concrete" => "Concrete", "Conductor" => "Conductor", "Confectionermaker" => "Confectionermaker", "Conservatorgallery" => "Conservatorgallery", "Construction carpenter/joiner" => "Construction carpenter/joiner", "Construction/buildingmanager" => "Construction/buildingmanager", "Cook" => "Cook", "Corrosionfitter" => "Corrosionfitter", "Court" => "Court", "Craft ceramicist" => "Craft ceramicist", "Craft gilder" => "Craft gilder", "Craftetcher" => "Craftetcher", "Craft glassmaker" => "Craft glassmaker", "Craftchaser" => "Craftchaser", "Craftbrazier" => "Craftbrazier", "Craftmaker" => "Craftmaker", "Craft plasterer" => "Craft plasterer", "Craft stonemason" => "Craft stonemason", "Craft upholsterer" => "Craft upholsterer", "Craneoperator" => "Craneoperator", "Crate maker/cooper" => "Crate maker/cooper", "Criminal investigator" => "Criminal investigator", "Cropoperative" => "Cropoperative", "Croupier" => "Croupier", "Customsinspector" => "Customsinspector", "Cutler" => "Cutler", "Dairyoperator" => "Dairyoperator", "Dance teacher/trainer" => "Dance teacher/trainer", "Dancer" => "Dancer", "Datatechnician" => "Datatechnician", "Debt collector" => "Debt collector", "Decorator-paperhangerdecorator" => "Decorator-paperhangerdecorator", "Dentalnurse" => "Dentalnurse", "Dental technician" => "Dental technician", "Dentistsurgeon" => "Dentistsurgeon", "Developingassistant" => "Developingassistant", "Dietician" => "Dietician", "Digger" => "Digger", "Director" => "Director", "Disc jockey" => "Disc jockey", "Dish washer" => "Dish washer", "Dispatch clerk" => "Dispatch clerk", "Dispatcherquarries" => "Dispatcherquarries", "Dispatcher,power" => "Dispatcher,power", "Diver" => "Diver", "Dog trainer" => "Dog trainer", "Doorkeeper" => "Doorkeeper", "Draughtsperson" => "Draughtsperson", "Dresser" => "Dresser", "Driller" => "Driller", "Drilling" => "Drilling", "Driverdriver" => "Driverdriver", "Driversassistant" => "Driversassistant", "Driving instructor" => "Driving instructor", "Dusttechnician" => "Dusttechnician", "Ecologist" => "Ecologist", "Economistaccountant" => "Economistaccountant", "Editoreditor" => "Editoreditor", "Educationalspecialist" => "Educationalspecialist", "Electricalengineer" => "Electricalengineer", "Electricalappliances" => "Electricalappliances", "Electricalinspector" => "Electricalinspector", "Electricalmechanic" => "Electricalmechanic", "Electrician" => "Electrician", "Electroceramicoperative" => "Electroceramicoperative", "Electronictechnician" => "Electronictechnician", "Electroplating" => "Electroplating", "Employmentofficer" => "Employmentofficer", "Enamel worker" => "Enamel worker", "Engineeringfitter" => "Engineeringfitter", "Engineeringtechnologist" => "Engineeringtechnologist", "Entertainment officer" => "Entertainment officer", "Environmentalinspector" => "Environmentalinspector", "Ergonomist" => "Ergonomist", "Ethnographer" => "Ethnographer", "Exhibitionsmanager" => "Exhibitionsmanager", "Faith healer" => "Faith healer", "Farmlabourer" => "Farmlabourer", "Farmer" => "Farmer", "Fashion designer" => "Fashion designer", "Feedoperator" => "Feedoperator", "Film critic" => "Film critic", "Filmdesigner" => "Filmdesigner", "Filmeditor" => "Filmeditor", "Film projectionist" => "Film projectionist", "Financial analyst" => "Financial analyst", "Financial officer" => "Financial officer", "Fine artist" => "Fine artist", "Fire officer" => "Fire officer", "Fire" => "Fire", "Fireinspector" => "Fireinspector", "Fish farmer" => "Fish farmer", "Fishkeeper/bailiff" => "Fishkeeper/bailiff", "Fisherman" => "Fisherman", "Fitter" => "Fitter", "Fitterinstruments" => "Fitterinstruments", "Fitterfitter" => "Fitterfitter", "Fitterstructures" => "Fitterstructures", "Flightstaff" => "Flightstaff", "Flight engineer" => "Flight engineer", "Floorlayer" => "Floorlayer", "Flower,grower" => "Flower,grower", "Flying instructor" => "Flying instructor", "Foodmanager" => "Foodmanager", "Foodtechnologist" => "Foodtechnologist", "Foreignclerk" => "Foreignclerk", "Forestermanager" => "Forestermanager", "Forester/forest manager" => "Forester/forest manager", "Forestryoperator" => "Forestryoperator", "Forestry worker" => "Forestry worker", "Fortune teller" => "Fortune teller", "Foster parent" => "Foster parent", "Foundry/patternmaker" => "Foundry/patternmaker", "Fringe/trimmings maker" => "Fringe/trimmings maker", "Fruitgrower" => "Fruitgrower", "Funeralworker" => "Funeralworker", "Fursewer" => "Fursewer", "Furnace operator" => "Furnace operator", "Furrier" => "Furrier", "Gardenergardener" => "Gardenergardener", "Gasinspector" => "Gasinspector", "Generalindustry" => "Generalindustry", "Generalproduction" => "Generalproduction", "Generalprocessing" => "Generalprocessing", "Geneticist" => "Geneticist", "Geographer" => "Geographer", "Geologicalmechanic" => "Geologicalmechanic", "Geologist" => "Geologist", "Geomechanic" => "Geomechanic", "Geophysicist" => "Geophysicist", "Glassengraver" => "Glassengraver", "Glassmaker" => "Glassmaker", "Glassoperator" => "Glassoperator", "Glass melter" => "Glass melter", "Glass painter" => "Glass painter", "Glassindustry" => "Glassindustry", "Glasscuttergrinder" => "Glasscuttergrinder", "Glassworkerworker" => "Glassworkerworker", "Glazier" => "Glazier", "Goldsmith" => "Goldsmith", "Governmentofficial" => "Governmentofficial", "Graphicdesigner" => "Graphicdesigner", "Gravedigger" => "Gravedigger", "Guide" => "Guide", "Gunsmith" => "Gunsmith", "Hand embroiderer" => "Hand embroiderer", "Hand lacemaker" => "Hand lacemaker", "Harbour guard" => "Harbour guard", "Hardener" => "Hardener", "Harpooner" => "Harpooner", "Hattermaker" => "Hattermaker", "Heatingfitter" => "Heatingfitter", "Heatingengineer" => "Heatingengineer", "Herbalist" => "Herbalist", "High-rise" => "High-rise", "Historian" => "Historian", "Historicalbuilding" => "Historicalbuilding", "Horsetrainer" => "Horsetrainer", "Host" => "Host", "Hotel porter" => "Hotel porter", "Hotel" => "Hotel", "Hydrologist" => "Hydrologist", "Ice-cream maker" => "Ice-cream maker", "Image consultant" => "Image consultant", "Industrialindustry" => "Industrialindustry", "Informationtransport" => "Informationtransport", "Information assistant/receptionist" => "Information assistant/receptionist", "Inspectortelecommunications" => "Inspectortelecommunications", "Insulation" => "Insulation", "Insurance clerk" => "Insurance clerk", "Insuranceunderwriter" => "Insuranceunderwriter", "Interiordesigner" => "Interiordesigner", "Interpretertranslator" => "Interpretertranslator", "Investment clerk" => "Investment clerk", "Invoice clerk" => "Invoice clerk", "Jewellerdesigner" => "Jewellerdesigner", "Jewellery maker" => "Jewellery maker", "Joiner" => "Joiner", "Judge" => "Judge", "Keeper" => "Keeper", "Keeperanimals" => "Keeperanimals", "Knitter" => "Knitter", "Land surveyor" => "Land surveyor", "Landscape architect" => "Landscape architect", "Laundry" => "Laundry", "Laundryworker" => "Laundryworker", "Lecturereducation" => "Lecturereducation", "Lecturercourses" => "Lecturercourses", "Lecturer/researcherlinguistics" => "Lecturer/researcherlinguistics", "Librarian" => "Librarian", "Lifeguardinstructor" => "Lifeguardinstructor", "Lift attendant" => "Lift attendant", "Lift fitter" => "Lift fitter", "Lighting technician" => "Lighting technician", "Lightningconductors" => "Lightningconductors", "Lithographer" => "Lithographer", "Livestock farmer" => "Livestock farmer", "Lotteryvendor" => "Lotteryvendor", "Machineworks" => "Machineworks", "Machinery inspector" => "Machinery inspector", "Makertextiles" => "Makertextiles", "Make-upwigmaker" => "Make-upwigmaker", "Management accountant" => "Management accountant", "Management consultant" => "Management consultant", "Manager" => "Manager", "Manager/supervisormanager" => "Manager/supervisormanager", "Marineoperator" => "Marineoperator", "Marinemanager" => "Marinemanager", "Marketing manager" => "Marketing manager", "Masseur/masseuse" => "Masseur/masseuse", "Masterceremonies" => "Masterceremonies", "Materials handler" => "Materials handler", "Mathematician" => "Mathematician", "Medicaltechnician" => "Medicaltechnician", "Mechanic" => "Mechanic", "Mechanicmechanic" => "Mechanicmechanic", "Mechanicaldesigner" => "Mechanicaldesigner", "Mechanicalmanager" => "Mechanicalmanager", "Mechanicaltechnologist" => "Mechanicaltechnologist", "Mechatronic engineer" => "Mechatronic engineer", "Metal engraver" => "Metal engraver", "Metal grinder" => "Metal grinder", "Metal refiner" => "Metal refiner", "Metal turner" => "Metal turner", "Metal" => "Metal", "Metallurgisttechnician" => "Metallurgisttechnician", "Metallurgistmetals" => "Metallurgistmetals", "Meteorologist" => "Meteorologist", "Metrologist" => "Metrologist", "Microbiologist" => "Microbiologist", "Midwife" => "Midwife", "Miller" => "Miller", "Milling-machine operator" => "Milling-machine operator", "Minemechanic" => "Minemechanic", "Mineengineer" => "Mineengineer", "Miner" => "Miner", "Miningtechnician" => "Miningtechnician", "Miningequipment" => "Miningequipment", "Miningdresser" => "Miningdresser", "Miningoperator" => "Miningoperator", "Miningfitter" => "Miningfitter", "Miningrescuer" => "Miningrescuer", "Mining/minerals surveyor" => "Mining/minerals surveyor", "Model/model" => "Model/model", "Modellermaker" => "Modellermaker", "Motorbodybuilder/repairer" => "Motorbodybuilder/repairer", "Mountain guide" => "Mountain guide", "Multimedia designer" => "Multimedia designer", "Multimediadesigner" => "Multimediadesigner", "Municipal" => "Municipal", "Municipalworker" => "Municipalworker", "Museum/artcurator" => "Museum/artcurator", "Music director" => "Music director", "Musicaltechnician" => "Musicaltechnician", "Musician" => "Musician", "Musicologist" => "Musicologist", "Nannynurse" => "Nannynurse", "Naturalist/naturewarden" => "Naturalist/naturewarden", "Newspaper" => "Newspaper", "Nuclearoperator" => "Nuclearoperator", "Nurseobstetric" => "Nurseobstetric", "Nurseryteacher" => "Nurseryteacher", "Nutritionist" => "Nutritionist", "Office junior" => "Office junior", "On-lineoperator" => "On-lineoperator", "Operational analyst/researcher" => "Operational analyst/researcher", "Operationselectrician" => "Operationselectrician");
                                                   
                                                   foreach ($proficient as $s9032n) {
                                                      echo "<option value=".$s9032n.">$s9032n</option>";
                                                   }
                                                ?>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="frow">
                                       <div class="caption-holder">
                                          <label>Interest <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                       </div>
                                       <div class="detail-holder">
                                          <div class="sliding-middle-out anim-area underlined fullwidth dropdown782">
                                             <select data-fill="y" data-selectore="interest123" data-action="interest" class="interest123 select2" multiple ='multiple' id='ads_int' tabindex="-1">
                                                <option value="" disabled selected>Choose interest</option>
                                                <?php
                                                   $interest = array("Aircraft spotting" => "Aircraft spotting", "Amateur astronomy" => "Amateur astronomy", "Astrology" => "Astrology", "Birdwatching" => "Birdwatching", "Bus spotting" => "Bus spotting", "Geocaching" => "Geocaching", "Gongoozling" => "Gongoozling", "Herping" => "Herping", "Meteorology" => "Meteorology", "Photography" => "Photography", "Trainspotting" => "Trainspotting", "Traveling" => "Traveling", "Hiking/backpacking" => "Hiking/backpacking", "Whale watching" => "Whale watching", "Satellite watching" => "Satellite watching", "Fishkeeping" => "Fishkeeping", "Learning" => "Learning", "Microscopy" => "Microscopy", "Reading" => "Reading", "Shortwave listening" => "Shortwave listening", "Videophilia" => "Videophilia", "Airsoft" => "Airsoft", "American football" => "American football", "Archery" => "Archery", "Association football" => "Association football", "Australian rules football" => "Australian rules football", "Auto racing" => "Auto racing", "Baseball" => "Baseball", "Beach Volleyball" => "Beach Volleyball", "Breakdancing" => "Breakdancing", "Climbing" => "Climbing", "Cricket" => "Cricket", "Cycling" => "Cycling", "Disc golf" => "Disc golf", "Dog sport" => "Dog sport", "Equestrianism" => "Equestrianism", "Exhibition drill" => "Exhibition drill", "Field hockey" => "Field hockey", "Figure skating" => "Figure skating", "Fishing" => "Fishing", "Footbag" => "Footbag", "Golfing" => "Golfing", "Handball" => "Handball", "Ice hockey" => "Ice hockey", "Judo" => "Judo", "Jukskei" => "Jukskei", "Kart racing" => "Kart racing", "Knife throwing" => "Knife throwing", "Lacrosse" => "Lacrosse", "Model aircraft" => "Model aircraft", "Marching Band" => "Marching Band", "Racquetball" => "Racquetball", "Radio-controlled car racing" => "Radio-controlled car racing", "Roller derby" => "Roller derby", "Rugby league football" => "Rugby league football", "Sculling or Rowing" => "Sculling or Rowing", "Shooting sport" => "Shooting sport", "Skateboarding" => "Skateboarding", "Speed skating" => "Speed skating", "Squash" => "Squash", "Surfing" => "Surfing", "Swimming" => "Swimming", "Table tennis" => "Table tennis", "Tennis" => "Tennis", "Tour skating" => "Tour skating", "Triathlon" => "Triathlon", "Ultimate Frisbee" => "Ultimate Frisbee", "Water Polo" => "Water Polo", "Volleyball" => "Volleyball", "Horseback riding" => "Horseback riding", "Animal fancy" => "Animal fancy", "Badminton" => "Badminton", "Baton Twirling" => "Baton Twirling", "Billiards" => "Billiards", "Bowling" => "Bowling", "Boxing" => "Boxing", "Bridge" => "Bridge", "Cheerleading" => "Cheerleading", "Chess" => "Chess", "Color guard" => "Color guard", "Curling" => "Curling", "Dancing" => "Dancing", "Darts" => "Darts", "Debate" => "Debate", "Fencing" => "Fencing", "Go" => "Go", "Gymnastics" => "Gymnastics", "Ice skating" => "Ice skating", "Kabaddi" => "Kabaddi", "Laser tag" => "Laser tag", "Marbles" => "Marbles", "Martial arts" => "Martial arts", "Mahjong" => "Mahjong", "Poker" => "Poker", "Slot car racing" => "Slot car racing", "Speedcubing" => "Speedcubing", "Sport stacking" => "Sport stacking", "Table football" => "Table football", "Weightlifting" => "Weightlifting", "Action figure" => "Action figure", "Antiquing" => "Antiquing", "Art collecting" => "Art collecting", "Book collecting" => "Book collecting", "Card collecting" => "Card collecting", "Coin collecting" => "Coin collecting", "Comic book collecting" => "Comic book collecting", "Deltiology" => "Deltiology", "Die-cast toy" => "Die-cast toy", "Element collecting" => "Element collecting", "Movie and movie memorabilia collecting" => "Movie and movie memorabilia collecting", "Record collecting" => "Record collecting", "Stamp collecting" => "Stamp collecting", "Video game collecting" => "Video game collecting", "Vintage cars" => "Vintage cars", "Antiquities" => "Antiquities", "Auto audiophilia" => "Auto audiophilia", "Flower collecting and pressing" => "Flower collecting and pressing", "Fossil hunting" => "Fossil hunting", "Insect collecting" => "Insect collecting", "Magnet fishing" => "Magnet fishing", "Metal detecting" => "Metal detecting", "Stone collecting" => "Stone collecting", "Mineral collecting" => "Mineral collecting", "Rock balancing" => "Rock balancing", "Sea glass collecting" => "Sea glass collecting", "Seashell collecting" => "Seashell collecting", "Air sports" => "Air sports", "Astronomy" => "Astronomy", "BASE jumping" => "BASE jumping", "Basketball" => "Basketball", "Beekeeping" => "Beekeeping", "Bird watching" => "Bird watching", "Blacksmithing" => "Blacksmithing", "Board sports" => "Board sports", "Bodybuilding" => "Bodybuilding", "Brazilian jiu-jitsu" => "Brazilian jiu-jitsu", "Camping" => "Camping", "Canyoning" => "Canyoning", "Dowsing" => "Dowsing", "Driving" => "Driving", "Flag football" => "Flag football", "Flying" => "Flying", "Flying disc" => "Flying disc", "Foraging" => "Foraging", "Freestyle football" => "Freestyle football", "Gardening" => "Gardening", "Ghost hunting" => "Ghost hunting", "Graffiti" => "Graffiti", "High-power rocketry" => "High-power rocketry", "Hiking" => "Hiking", "Hooping" => "Hooping", "Hunting" => "Hunting", "Inline skating" => "Inline skating", "Jogging" => "Jogging", "Kayaking" => "Kayaking", "Kite flying" => "Kite flying", "Kitesurfing" => "Kitesurfing", "LARPing" => "LARPing", "Letterboxing" => "Letterboxing", "Motor sports" => "Motor sports", "Mountain biking" => "Mountain biking", "Mountaineering" => "Mountaineering", "Mushroom hunting/Mycology" => "Mushroom hunting/Mycology", "Netball" => "Netball", "Nordic skating" => "Nordic skating", "Orienteering" => "Orienteering", "Paintball" => "Paintball", "Parkour" => "Parkour", "Polo" => "Polo", "Powerlifting" => "Powerlifting", "Rafting" => "Rafting", "Rappelling" => "Rappelling", "Road biking" => "Road biking", "Rock climbing" => "Rock climbing", "Roller skating" => "Roller skating", "Rugby" => "Rugby", "Running" => "Running", "Sailing" => "Sailing", "Sand art" => "Sand art", "Scouting" => "Scouting", "Scuba diving" => "Scuba diving", "Topiary" => "Topiary", "Shooting" => "Shooting", "Shopping" => "Shopping", "Skiing" => "Skiing", "Skimboarding" => "Skimboarding", "Skydiving" => "Skydiving", "Slacklining" => "Slacklining", "Snowboarding" => "Snowboarding", "Stone skipping" => "Stone skipping", "Travel" => "Travel", "Taekwondo" => "Taekwondo", "Tai chi" => "Tai chi", "Urban exploration" => "Urban exploration", "Vacation" => "Vacation", "Vehicle restoration" => "Vehicle restoration", "Walking" => "Walking", "Water sports" => "Water sports", "3D printing" => "3D printing", "Acting" => "Acting", "Amateur radio" => "Amateur radio", "Baton twirling" => "Baton twirling", "Board/Tabletop games" => "Board/Tabletop games", "Book restoration" => "Book restoration", "Cabaret" => "Cabaret", "Calligraphy" => "Calligraphy", "Candle making" => "Candle making", "Coffee roasting" => "Coffee roasting", "Coloring" => "Coloring", "Computer programming" => "Computer programming", "Cooking" => "Cooking", "Cosplaying" => "Cosplaying", "Couponing" => "Couponing", "Creative writing" => "Creative writing", "Crocheting" => "Crocheting", "Cross-stitch" => "Cross-stitch", "Crossword puzzles" => "Crossword puzzles", "Cryptography" => "Cryptography", "Dance" => "Dance", "Digital arts" => "Digital arts", "Do it yourself" => "Do it yourself", "Drama" => "Drama", "Drawing" => "Drawing", "Electronics" => "Electronics", "Embroidery" => "Embroidery", "Fantasy Sports" => "Fantasy Sports", "Fashion" => "Fashion", "Flower arranging" => "Flower arranging", "Foreign interests learning" => "Foreign interests learning", "Genealogy" => "Genealogy", "Glassblowing" => "Glassblowing", "Gunsmithing" => "Gunsmithing", "Herp keeping" => "Herp keeping", "Homebrewing" => "Homebrewing", "Hydroponics" => "Hydroponics", "Jewelry making" => "Jewelry making", "Jigsaw puzzles" => "Jigsaw puzzles", "Juggling" => "Juggling", "Knapping" => "Knapping", "Knife making" => "Knife making", "Knitting" => "Knitting", "Kombucha Brewing" => "Kombucha Brewing", "Lacemaking" => "Lacemaking", "Lapidary" => "Lapidary", "Leather crafting" => "Leather crafting", "Lego building" => "Lego building", "Listening to music" => "Listening to music", "Machining" => "Machining", "Macrame" => "Macrame", "Magic" => "Magic", "Metalworking" => "Metalworking", "Model building" => "Model building", "Origami" => "Origami", "Painting" => "Painting", "Pet" => "Pet", "Philately" => "Philately", "Plastic embedding" => "Plastic embedding", "Playing musical instruments" => "Playing musical instruments", "Poi" => "Poi", "Pottery" => "Pottery", "Puzzles" => "Puzzles", "Quilting" => "Quilting", "Scrapbooking" => "Scrapbooking", "Sculpting" => "Sculpting", "Sewing" => "Sewing", "Singing" => "Singing", "Sketching" => "Sketching", "Soapmaking" => "Soapmaking", "Stand-up comedy" => "Stand-up comedy", "Tatting" => "Tatting", "Taxidermy" => "Taxidermy", "Video gaming" => "Video gaming", "Watching movies" => "Watching movies", "Watching television" => "Watching television", "Web surfing" => "Web surfing", "Whittling" => "Whittling", "Wood carving" => "Wood carving", "Woodworking" => "Woodworking", "Worldbuilding" => "Worldbuilding", "Writing" => "Writing", "Yo-yoing" => "Yo-yoing", "Yoga" => "Yoga", "Aircraft" => "Aircraft", "Amateur" => "Amateur", "Bus" => "Bus"); 
                                                   foreach ($interest as $s9032n) {
                                                      echo "<option value=".$s9032n.">$s9032n</option>";
                                                   }
                                                ?>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col l5 m6 s12 right">
                                 <div class="estimate-box">
                                    <h5>Estimated Target Audience</h5>
                                    <div class="databox">
                                       <div class="meter-holder">
                                          <div class="gauge2 audience-meter"></div>
                                       </div>
                                       <p>Audience selection is <span>fairly broad</span></p>
                                    </div>
                                    <div class="databox">
                                       <h6>Audience Details</h6>
                                       <div class="frow">
                                          <div class="caption-holder"><label>Location</label></div>
                                          <div class="detail-holder">
                                             <p>Tibet, Combodia</p>
                                          </div>
                                       </div>
                                       <div class="frow">
                                          <div class="caption-holder"><label>Age</label></div>
                                          <div class="detail-holder">
                                             <p>15 - 60</p>
                                          </div>
                                       </div>
                                       <div class="frow">
                                          <div class="caption-holder"><label>Language</label></div>
                                          <div class="detail-holder">
                                             <p>English</p>
                                          </div>
                                       </div>
                                       <div class="frow">
                                          <div class="caption-holder"><label>Gender</label></div>
                                          <div class="detail-holder">
                                             <p>Male, Female</p>
                                          </div>
                                       </div>
                                    </div>
                                    <h6>Estimated Daily Reach</h6>
                                    <div class="frow">
                                       <div class="range-slider range-slider1">
                                          <input type="text" class="amount" readonly>
                                          <div id="test-slider"></div>
                                       </div>
                                       <label class="max-amount" readonly>6000 people on Iwasinqatar</label>
                                    </div>
                                    <p class="note">This is just an estimate daily reach. Actual one may vary based on members response to your ad</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="adstep-holder hasDivider">
                        <div class="adsec-divider">
                        </div>
                        <div class="adstep-title">
                           <img src="images/adstep-pricing.png"/>
                           <h6>Budget and Pricing</h6>
                           <p>Adjust your budget and set an affordable pricing for your ads</p>
                        </div>
                        <div class="adstep-details">
                           <div class="pricing-form">
                              <div class="frow">
                                 <div class="caption-holder">
                                    <label>Average Cost <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                 </div>
                                 <div class="detail-holder">
                                    <ul class="adcost-ul">
                                       <li>
                                          <div class="details">
                                             <h6>Cost per click (CPC)</h6>
                                             <p>Pay when your ad is clicked through to your website</p>
                                          </div>
                                          <div class="costs">																
                                             <span class="cost">$0.80</span>
                                          </div>
                                       </li>
                                       <li>
                                          <div class="details">
                                             <h6>Cost per engagement (CPE)</h6>
                                             <p>Pay when your ad is liked or shared</p>
                                          </div>
                                          <div class="costs">																
                                             <span class="cost">$0.40</span>
                                          </div>
                                       </li>
                                       <li>
                                          <div class="details">
                                             <h6>Cost per impressions (CPM)</h6>
                                             <p>Cost per 1000 user views</p>
                                          </div>
                                          <div class="costs">																
                                             <span class="cost">$0.40</span>
                                          </div>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="frow">
                                 <div class="caption-holder">
                                    <label>Daily Budget <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                 </div>
                                 <div class="detail-holder">
                                    <div class="sliding-middle-out anim-area underlined">
                                       <input type="text" class="budget-text" placeholder="$10.00">
                                    </div>
                                    <p class="note">Minimum budget : $10.00</p>
                                 </div>
                              </div>
                              <div class="frow">
                                 <div class="caption-holder">
                                    <label>Schedule <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                                 </div>
                                 <div class="detail-holder">
                                    <ul class="radioul">
                                       <li>
                                          <div class="radio-holder">
                                             <label class="control control--radio">
                                                &nbsp;
                                                <input type="radio" name="radio1" checked value="startdate"/>
                                                <div class="control__indicator"></div>
                                             </label>
                                             <p>Run my advert set contineously starting today</p>
                                          </div>
                                       </li>
                                       <li>
                                          <div class="radio-holder">
                                             <label class="control control--radio">
                                                &nbsp;
                                                <input type="radio" name="radio1" value="startenddate"/>
                                                <div class="control__indicator"></div>
                                             </label>
                                             <p>Set a starting date</p>
                                             <div class="sliding-middle-out anim-area underlined adddate">
                                                <input type="text" placeholder="Start date" data-toggle="datepicker" class="datepickerinput form-control" data-query="M" readonly>
                                             </div>
                                             <p>and end date</p>
                                             <div class="sliding-middle-out anim-area underlined adddate">
                                                <input type="text" placeholder="End date" data-toggle="datepicker" class="datepickerinput form-control" data-query="M" readonly>
                                             </div>
                                          </div>
                                       </li>
                                    </ul>
                                    <p>Campaigns will end at midnight ( UTC Timezone ) on the date selected.</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="btn-holder">
                        <div class="dirinfo right">
                           <a href="javascript:void(0)"  onclick="closeEditAd()" id="goPayment" class="btn-custom waves-effect">Save</a>
                           <a href="javascript:void(0)" onclick="closeEditAd()" class="btn-custom waves-effect">Back</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="combined-column wide-open main-page full-page">
         <div class="travadvert-banner">
            <div class="overlay"></div>
            <div class="banner-section">
               <div class="container">
                  <h4>Advertise on Iwasinqatar</h4>
                  <p>Create self service advert using our Advert Manager</p>
               </div>
            </div>
         </div>
         <div class="manageadvert-content">
            <div class="container">
               <div class="fullwidth">
                  <div class="col l12">
                     <div class="left">
                        <h5 class="pad_algin">Account <span>Adel Hasanat</span></h5>
                     </div>
                     <div class="create-btn right">
                        <a href="ad-manager.php" class="btn-custom waves-effect"><i class="mdi mdi-plus"></i> Advert</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="manageadvert-details text-left">
               <div class="container">
                  <div class="row">
                     <div class="col l12 m12 width100">
                        <h4 class="text-left adman_head">Your Advertisements</h4>
                        <div class="scroll_table">
                           <div class="table-responsive">
                              <table class="table-bordered">
                                 <thead>
                                    <tr>
                                       <th>Status</th>
                                       <th class="text-left">Ad sets</th>
                                       <th>Ad Schedule</th>
                                       <th>Ad Budget</th>
                                       <th>Spent</th>
                                       <th>Edit</th>
                                       <th>Delete</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <div class="switch">
                                             <label>
                                             <input type="checkbox">
                                             <span class="lever"></span>
                                             </label>
                                          </div>
                                       </td>
                                       <td><a href="javascript:void(0)" onclick="openManageAdDetails(this)" class="adlink">Testing FH<span>page likes</span></a></td>
                                       <td>20 Nov 2016 - 26 Nov 2016</td>
                                       <th>$120.00</th>
                                       <th>$23.89</th>
                                       <td><a href="javascript:void(0)" onclick="openEditAd()"><i class="zmdi zmdi-edit zmdi-18"></i></a></td>
                                       <td><a href="javascript:void(0)"><i class="zmdi zmdi-delete zmdi-hc-fw zmdi-18"></i></a></td>
                                    </tr>
                                    <tr>
                                       <td>
                                          <div class="switch">
                                             <label>
                                             <input type="checkbox">
                                             <span class="lever"></span>
                                             </label>
                                          </div>
                                       </td>
                                       <td><a href="javascript:void(0)" onclick="openManageAdDetails(this)"  class="adlink">Testing FH<span>page likes</span></a></td>
                                       <td>20 Nov 2016 - 26 Nov 2016</td>
                                       <th>$80.00</th>
                                       <th>$10.00</th>
                                       <td><a href="javascript:void(0)" onclick="openEditAd()"><i class="zmdi zmdi-edit zmdi-18"></i></a></td>
                                       <td><a href="javascript:void(0)"><i class="zmdi zmdi-delete zmdi-hc-fw zmdi-18"></i></a></td>
                                    </tr>
                                    <tr>
                                       <td>
                                          <div class="switch">
                                             <label>
                                             <input type="checkbox">
                                             <span class="lever"></span>
                                             </label>
                                          </div>
                                       </td>
                                       <td><a href="javascript:void(0)" onclick="openManageAdDetails(this)"  class="adlink">Testing FH<span>page likes</span></a></td>
                                       <td>20 Nov 2016 - 26 Nov 2016</td>
                                       <th>$200.00</th>
                                       <th>$43.56</th>
                                       <td><a href="javascript:void(0)" onclick="openEditAd()"><i class="zmdi zmdi-edit zmdi-18"></i></a></td>
                                       <td><a href="javascript:void(0)"><i class="zmdi zmdi-delete zmdi-hc-fw zmdi-18"></i></a></td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="sub-content">
                  <div class="title">
                     <div class="container">
                        <div class="left">
                           <h5>Detail info - Testing FH <span>page likes</span></h5>
                        </div>
                        <div class="right">
                           <h6>Jan 17, 2017 - Ongoing</h6>
                        </div>
                     </div>
                  </div>
                  <div class="details">
                     <div class="container">
                        <div class="travad-states">
                           <div class="state-tab">
                              <h4><img src="images/adgraph-icon.png"/>Spending</h4>
                              <div class="bordered-tab">
                                 <ul class="tabs">
                                    <li class="tab"><a href="#spending-budget">Budget</a></li>
                                    <li class="tab"><a href="#spending-spent">Spent</a></li>
                                    <li class="tab"><a href="#spending-schedule">Schedule</a></li>
                                 </ul>
                                 <div class="tab-content">
                                    <div id="spending-budget">
                                       <p>Your Starting Budget</p>
                                       <h5>$120.00</h5>
                                    </div>
                                    <div id="spending-spent" >
                                       <p>Total Spent</p>
                                       <h5>$16.00</h5>
                                    </div>
                                    <div id="spending-schedule">
                                       <p>Advertisement Schedule</p>
                                       <h5>17 Jan, 2017 - Today</h5>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="state-graph">
                              <div class="state-summery dis_n">
                                 <ul>
                                    <li>
                                       <span>label-1</span>
                                       23230
                                    </li>
                                    <li>
                                       <span>label-2</span>
                                       304
                                    </li>
                                    <li>
                                       <span>label-3</span>
                                       1204
                                    </li>
                                 </ul>
                              </div>
                              <div class="graph-holder">
                                 <img src="images/ad-graph.png"/>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="details graybg">
                     <div class="container">
                        <div class="travad-states">
                           <div class="state-tab">
                              <h4><img src="images/adgraph-icon.png"/>Results</h4>
                              <div class="bordered-tab">
                                 <ul class="tabs link-menu">
                                    <li class="tab"><a href="#results-impression">Impression</a></li>
                                    <li class="tab"><a href="#results-action">Action</a></li>
                                    <li class="tab"><a href="#results-click">Click</a></li>
                                 </ul>
                                 <div class="tab-content">
                                    <div id="results-impression" class="tab-pane fade active in">
                                       <p>Nov 1, 2015 - Nov 23, 2015</p>
                                       <h5>230</h5>
                                    </div>
                                    <div id="results-action" class="tab-pane fade">
                                       <p>Nov 20, 2015 - Nov 23, 2015</p>
                                       <h5>56</h5>
                                    </div>
                                    <div id="results-click" class="tab-pane fade">
                                       <p>Nov 23, 2015</p>
                                       <h5>12</h5>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="state-graph">
                              <div class="state-summery dis_n">
                                 <ul class="graylabel">
                                    <li>
                                       <span>label-1</span>
                                       23230
                                    </li>
                                    <li>
                                       <span>label-2</span>
                                       304
                                    </li>
                                    <li>
                                       <span>label-3</span>
                                       1204
                                    </li>
                                 </ul>
                              </div>
                              <div class="graph-holder">
                                 <img src="images/ad-graph.png"/>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="details">
                     <div class="container">
                        <div class="travad-states">
                           <div class="state-tab">
                              <h4><img src="images/adgraph-icon.png"/>Conversions</h4>
                              <div class="bordered-tab">
                                 <ul class="tabs link-menu">
                                    <li class="tab"><a href="#conversion-lifetime">Lifetime</a></li>
                                    <li class="tab"><a href="#conversion-7days">7 Days</a></li>
                                    <li class="tab"><a href="#conversion-1day">1 Day</a></li>
                                 </ul>
                                 <div class="tab-content">
                                    <div id="conversion-lifetime" class="tab-pane fade active in">
                                       <p>Nov 1, 2015 - Nov 23, 2015</p>
                                       <h5>230</h5>
                                    </div>
                                    <div id="conversion-7days" class="tab-pane fade">
                                       <p>Nov 20, 2015 - Nov 23, 2015</p>
                                       <h5>56</h5>
                                    </div>
                                    <div id="conversion-1day" class="tab-pane fade">
                                       <p>Nov 23, 2015</p>
                                       <h5>12</h5>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="state-graph">
                              <div class="state-summery dis_n">
                                 <ul>
                                    <li>
                                       <span>label-1</span>
                                       23230
                                    </li>
                                    <li>
                                       <span>label-2</span>
                                       304
                                    </li>
                                    <li>
                                       <span>label-3</span>
                                       1204
                                    </li>
                                 </ul>
                              </div>
                              <div class="graph-holder">
                                 <img src="images/ad-graph.png"/>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>

<?php include('common/datepicker.php'); ?>
<?php include("script.php"); ?>