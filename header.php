<?php
   $file = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);
   $file = str_replace('.php','',$file);
?> 
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" href="images/favicon.ico">
      <title>I was in Qatar</title>
      <link href="css/materialize.css" rel="stylesheet">
      <link href="css/animate.css" rel="stylesheet">
      <link href="css/nouislider.css" rel="stylesheet">
      <link href="css/material-design-iconic-font.css" rel="stylesheet">
      <link href="css/materialdesignicons.min.css" rel="stylesheet">    
      <link href="css/tooltipster.bundle.min.css" rel="stylesheet">
      <link href="css/tooltipster-sideTip-borderless.min.css" rel="stylesheet">
      <link href="css/emoticons.css" rel="stylesheet">
      <link href="css/emostickers.css" rel="stylesheet">
      <link href="css/demo-cover.css" type="text/css" media="screen" rel="stylesheet" />
      <link href="css/jquery-gauge.css" type="text/css" rel="stylesheet">
      <link href="css/font-awesome.css" rel="stylesheet">
      <link href="css/template.css" rel="stylesheet">
      <link href="css/themes.css" rel="stylesheet">
      <link href="css/all-ie-only.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="css/master-responsive.css" rel="stylesheet">
      <link href="css/custom-croppie.css" rel="stylesheet">
      <link href="css/justifiedGallery.css" rel="stylesheet" >
      <link href="css/lightgallery.css" rel="stylesheet">
      <link href="css/lg-transitions.css" rel="stylesheet">
      <link href="css/daterangepicker.css" rel="stylesheet">
      <script src="js/jquery.min.js"></script>
   </head>
   <?php
      $bodyClass="theme-color ";
      $custom_wrapper="page-wrapper ";
      $search_holder="search-holder main-sholder ";
      
      if($file == "ad-manager" || $file == "advertisement" || $file == "manage-ad")
      {
         $custom_wrapper .= "hidemenu-wrapper adpage full-wrapper noopened-search";  
      }
      else if($file == "collections-detail" || $file == "community-events-detail" || $file == "groups-detail" || $file == "channels-detail")
      {
      $custom_wrapper .= "menutransheader-wrapper menuhideicons-wrapper";  	
      }
      else if($file == "flight" || $file == "hotels-new")
      {
      $custom_wrapper .= "hidemenu-wrapper full-wrapper transheader-wrapper noopened-search transheadereffect transheadereffectall JIS3829";  	
      }
      else if($file == "hotels" || $file == "hotelsmap")
      {
      $custom_wrapper .= "hidemenu-wrapper full-wrapper noopened-search JIS3829 hotellist-page";    
      }
      else if($file == "hotelslist")
      {
      $custom_wrapper .= "hidemenu-wrapper full-wrapper noopened-search JIS3829";    
      }
      else if($file == "tours")
      {
      $custom_wrapper .= "hidemenu-wrapper full-wrapper noopened-search JIS3829 show-sidebar tourlist-page";    
      }
      else if($file == "restaurants")
      {
      $custom_wrapper .= "hidemenu-wrapper full-wrapper noopened-search JIS3829 restaurants-page";    
      }
      else if($file == "attractions")
      {
      $custom_wrapper .= "hidemenu-wrapper full-wrapper noopened-search JIS3829 attractions-page attractionlist-page";    
      }
      else if($file == "credit" || $file == "credit-transfer" || $file == "credit-update")
      {
      $custom_wrapper .= "hidemenu-wrapper full-wrapper noopened-search creditpage";  	
      
         if($file == "credit-transfer" || $file == "credit-update")
         {
            $custom_wrapper .= " white-wrapper";  	
         }
      }
      else if($file == "vip-member" || $file == "vip-package" || $file == "billing-info" || $file == "verify")
      {
      $custom_wrapper .= "hidemenu-wrapper full-wrapper white-wrapper noopened-search vippage";  
      }
      else if($file == "wall" || $file == "wall-new")
      {
      $custom_wrapper .= "wallpage subpage-wrapper hidemenu-wrapper noopened-search menutransheader-wrapper transheadereffect userwall";  
      }
      else if($file == "business-page" || $file == "business-page-new" || $file == "business-page-detail")
      {
      $custom_wrapper .= "wallpage businesspage subpage-wrapper hidemenu-wrapper noopened-search menutransheader-wrapper show-sidebar transheadereffect";  
      }
      else if($file == "business-pages")
      {
         $custom_wrapper .= " hidemenu-wrapper show-sidebar ";
      }
      else if($file == "places")
      {
      $custom_wrapper .= "place-wrapper full-wrapper hidemenu-wrapper transheader-wrapper transheadereffect transheadereffectall JIS3829";
      $search_holder .="keepopen";
      }
      else if($file == "trip")
      {
      $custom_wrapper .= "trip-wrapper full-wrapper hidemenu-wrapper";
      } 
      else if($file == "business-pages" || $file == "groups" || $file == "community-events" || $file == "who-is-around" || $file == "travelbuddy" || $file == "hangout" || $file == "weekendescape" || $file == "trip-experience")
      {
         $custom_wrapper .= " gen-pages hidemenu-wrapper show-sidebar ";
      } 
      else if($file == "messages-new" || $file == "messages")
      {
      $custom_wrapper .= "messages-wrapper full-wrapper noopened-search hidemenu-wrapper menutransheader-wrapper";
      }
      else if($file == "newpost")
      {
      $custom_wrapper .= "post-pages mobile-screen";
      }
      else if($file == "index")
      {
      $custom_wrapper .= "home-index place-wrapper mainfeed-page";
      } 
      else if($file == "notifications")
      {
         $custom_wrapper .= " hidemenu-wrapper notifications-page hidemenu-wrapper show-sidebar ";
      }
      else if($file == "camping")
      {
         $custom_wrapper .= " place-wrapper mainfeed-page camping-page ";
      }
      else if($file == "camping-detail")
      {
         $custom_wrapper .= " place-wrapper mainfeed-page camping-detail-page ";
      }
      else if($file == "homestay")
      {
         $custom_wrapper .= " place-wrapper mainfeed-page homestay-page ";
      }
      else if($file == "homestay-detail")
      {
         $custom_wrapper .= " place-wrapper mainfeed-page homestay-detail-page ";
      }
      else if($file == "localdine")
      {
         $custom_wrapper .= " place-wrapper mainfeed-page localdine-page ";
      }
      else if($file == "localdinedetail")
      {
         $custom_wrapper .= " place-wrapper mainfeed-page localdine-detail-page ";
      }
      else{
       $custom_wrapper .= "place-wrapper mainfeed-page";
      }
      
      if($file == "places" || $file == "trip" || $file == "flight" || $file == "hotels" || $file == "hotels-new" || $file == "credit" || $file == "credit-transfer" || $file == "credit-update" || $file == "vip-member" || $file == "vip-package" || $file == "verify" || $file == "billing-info" || $file == "settings" || $file == "advertisement" || $file == "manage-ad" || $file == "ad-manager" || $file == "wall" || $file == "wall-new" || $file == "business-page" || $file == "business-page-new" || $file == "messages" || $file == "messages-new")
      {
         $custom_wrapper .= " show-sidebar hotellist-page ";
      }
      
      if($file == "settings")
      {
         $custom_wrapper .= "settings-page settings-wrapper subpage-wrapper hidemenu-wrapper white-wrapper noopened-search";  
      }
      if($file == "complete-profile")
      {
         $custom_wrapper .= "completeprof-wrapper complete-profile";
      }
      if($file == "messages")
      {
         $custom_wrapper .= "whitebg";
      }
      if($file == "questions")
      {
         $custom_wrapper .= "place-wrapper mainfeed-page";
      }

      if($file == "blog")
      {
         $custom_wrapper .= " blog-page";
      }
      if($file == "business-pages")
      {
         $custom_wrapper .= " gen-pages ";
      }
      if($file == "messages-new" || $file == "messages")
      {
      $custom_wrapper .= " messages-wrapper full-wrapper noopened-search hidemenu-wrapper menutransheader-wrapper";
      }
      ?>
   <body class="<?=$bodyClass?>">
      <div class="ienotice">
         <div class="notice-holder">
            <h5>Iwasinqatar can be best viewed in IE 9 or greater</h5>
            <br />
            <a href="https://www.microsoft.com/en-in/download/internet-explorer.aspx">Update your browser here</a>
         </div>
      </div> 
      <div class="<?php echo $custom_wrapper; ?>">
      <?php if($file == "newpost"){ ?>
         <!-- position: relative;
             left: -72px;
             bottom: -105px;
             color: white; -->
      <div class="header-section">
         <div class="header-themebar">
            <div class="gotohome">
               <a href="index.php"><i class="mdi mdi-arrow-left"></i></a>				
            </div>
            <div class="logo-holder">
               <a href="javascript:void(0)" class="desk-logo"><img src="images/black-logo.png"/></a>
               <a href="javascript:void(0)" class="mbl-logo page-name">Write Post</a>
            </div>
            <div class="post-btns">
               <a href="index.php">Post</a>
            </div>
            
         </div>
      </div>
      <?php } else { ?>
      <div class="header-section">
         <div class="header-themebar">
            <div class="container">
               <div class="header-nav">
                  <div class="mobile-menu topicon">
                     <a href="javascript:void(0)" class="mbl-menuicon1 waves-effect"><i class="mdi mdi-menu"></i></a>
                     <?php if($file == "settings") { ?>
                     <div class="gotohome">
                        <a href="javascript:void(0)" onclick="resetInnerPage('settings','show')"><i class="mdi mdi-arrow-left"></i></a>
                     </div>
                     <?php } ?>
                     <?php if($file == "wall" || $file == "wall-new" || $file == "business-page" || $file == "business-page-detail" || $file == "business-page-new" || $file == "business-pages"){ ?>
                     <div class="gotohome">
                        <a href="javascript:void(0)" onclick="resetInnerPage('wall','show')"><i class="mdi mdi-arrow-left"></i></a>
                     </div>
                     <?php } ?>
                     <?php if($file == "places"){ ?>
                     <div class="gotohome">
                        <a href="javascript:void(0)" onclick="resetPlacesTab()"><i class="mdi mdi-arrow-left"></i></a>
                     </div>
                     <?php } ?>
                  </div>
                  <div class="middle-wrapper">
                     <div class="logo-holder">
                        <?php if($file == "places" || $file == "trip" || $file == "tours" || $file == "flight" || $file == "hotels" || $file == "hotels-new" || $file == "credit" || $file == "credit-transfer" || $file == "credit-update" || $file == "vip-member" || $file == "vip-package" || $file == "verify" || $file == "billing-info" || $file == "settings" || $file == "advertisement" || $file == "manage-ad" || $file == "ad-manager" || $file == "wall" || $file == "wall-new" || $file == "business-page" || $file == "business-page-new" || $file == "business-pages" || $file == "business-page-detail" || $file == "messages" || $file == "messages-new" || $file == "notifications" || $file == "local-guide" || $file == "local-driver" || $file == "travellers" || $file == "locals" || $file == "collections" || $file == "reviews" || $file == "tripstory" || $file == "blog" || $file == "questions" || $file == "tips" || $file == "photostream" || $file == "discussion" || $file == "index" || $file == "homestay" || $file == "localdine" || $file == "camping" || $file == "localdinedetail"  || $file == "guide-detail"|| $file == "driver-detail"){ ?>
                        <div class="mobile-menu">
                           <a href="javascript:void(0)" class="waves-effect"><i class="mdi mdi-menu"></i></a>												
                        </div>
                        <?php } ?>
                        <a href="index.php" class="desk-logo"><img src="images/black-logo.png"/></a>
                     </div>
                     <div class="page-name mainpage-name">
                        <?php 
                        if($file == 'business-pages') {
                           echo 'pages';
                        } else  {
                           echo $file;
                        }
                        ?>
                           
                     </div>
                     <div class="page-name innerpage-name" >
                        <?php if($file == "settings"){ ?>Basic Information<?php } ?>
                        <?php if($file == "wall" || $file == "wall-new" || $file == "business-page" || $file == "business-page-new"){ ?>Wall
                        <?php } ?>							
                     </div>
                     <div class="was-in-country">
                        <a class="dropdown-button wasin-country" href="javascript:void(0)" data-activates="wasinCountry"><i class="zmdi zmdi-chevron-down mdi-22px"></i></a>
                        <ul id="wasinCountry" class="dropdown-content">
                           <li><a href="https://www.iwasinpetra.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I was in Petra</a></li>
                           <li><a href="https://www.iwasinjordan.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I was in Jordan</a></li>
                           <li><a href="https://www.iwasinmadain.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I was in Madain</a></li>
                           <li><a href="https://www.iwasinsaudi.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I was in Saudi</a></li>
                           <li><a href="https://www.iwasinfrance.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I was in France</a></li>
                           <li><a href="https://www.iwasindubai.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I was in Dubai</a></li>
                           <li><a href="https://www.iwasinspain.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I was in Spain</a></li>
                           <!-- <li><a href="https://www.iwasinqatar.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I was in Qatar</a></li> -->
                           <li><a href="https://www.iwasinbahrain.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I was in Bahrain</a></li>
                           <li><a href="https://www.iwasinabudhabi.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I was in Abu Dhabi</a></li>
                           <li><a href="#!" class="collection-item">I was in USA</a></li>
                           <li><a href="#!" class="collection-item">I was in Italy</a></li>
                           <li><a href="#!" class="collection-item">I was in Mexico</a></li>
                           <li><a href="#!" class="collection-item">I was in UK</a></li>
                           <li><a href="#!" class="collection-item">I was in Istanbul</a></li>
                           <li><a href="#!" class="collection-item">I was in China</a></li>
                           <li><a href="#!" class="collection-item">I was in Germany</a></li>
                           <li><a href="#!" class="collection-item">I was in Thailand</a></li>
                           <li><a href="#!" class="collection-item">I was in Austria</a></li>
                           <li><a href="#!" class="collection-item">I was in Hong Kong</a></li>
                           <li><a href="#!" class="collection-item">I was in Greece</a></li>
                           <li><a href="#!" class="collection-item">I was in Russia</a></li>
                           <li><a href="#!" class="collection-item">I was in Petra</a></li>
                           <li><a href="#!" class="collection-item">I was in Japan</a></li>
                           <li><a href="#!" class="collection-item">I was in Cyprus</a></li>
                           <li><a href="#!" class="collection-item">I was in Jamaica</a></li>
                           <li><a href="#!" class="collection-item">I was in Poland</a></li>
                           <li><a href="#!" class="collection-item">I was in Hungary</a></li>
                           <li><a href="#!" class="collection-item">I was in Zimbabwe</a></li>
                           <li><a href="#!" class="collection-item">I was in Ukraine</a></li>
                           <li><a href="#!" class="collection-item">I was in Netherlands</a></li>
                           <li><a href="#!" class="collection-item">I was in India</a></li>
                           <li><a href="#!" class="collection-item">I was in Singapore</a></li>
                           <li><a href="#!" class="collection-item">I was in Morocco</a></li>
                           <li><a href="#!" class="collection-item">I was in Egypt</a></li>
                           <li><a href="#!" class="collection-item">I was in Brazil</a></li>
                           <li><a href="#!" class="collection-item">I was in Australia</a></li>
                           <li><a href="#!" class="collection-item">I was in Malaysia</a></li>
                           <li><a href="#!" class="collection-item">I was in Indonesia</a></li>
                           <li><a href="#!" class="collection-item">I was in Argentina</a></li>
                           <li><a href="#!" class="collection-item">I was in Zimbabwe</a></li>
                           <li><a href="#!" class="collection-item">I was in Macao</a></li>
                           <li><a href="#!" class="collection-item">I was in Croatia</a></li>
                           <li><a href="#!" class="collection-item">I was in Southkorea</a></li>
                        </ul>
                     </div>
                  </div>
                  <?php if($file == "messages" || $file == "messages-new" || $file == "wall" || $file == "wall-new"){ ?>
                  <div class="mbl-innerhead">
                     <div class="gotohome">
                        <a href="javascript:void(0)" onclick="closeAddNewMsg()"><i class="mdi mdi-arrow-left"></i></a>
                     </div>
                     <div class="logo-holder">
                        <span class="top_img">
                        <img src="images/whoisaround-img.png"/>
                        </span>
                        <a href="javascript:void(0)" class="mbl-logo page-name" onclick="contactInfo()">Vipul Patel</a>
                        <div class="top_message_status">
                           <span class="">last seen 1hr &nbsp; | </span>
                           <!--<span class="userstatus">&nbsp; i like nonsense it wakes up the brain cells</span>-->
                           <span class=""> 12:57 PM </span>
                           <span class="">| INDIA </span>
                        </div>
                     </div>
                  </div>
                  <?php } ?>
                  <?php if($file == "business-page" || $file == "business-page-detail" || $file == "business-page-new"){ ?>
                  <div class="mbl-innerhead">
                     <div class="gotohome">
                        <a href="javascript:void(0)" onclick="backToMain('businesspage')"><i class="mdi mdi-arrow-left"></i></a>
                     </div>
                     <div class="logo-holder">					
                        <a href="javascript:void(0)" class="mbl-logo page-name">Vipul Patel</a>
                     </div>
                  </div>
                  <?php } ?>
                  <?php if($file == "messages" || $file == "messages-new" || $file == "wall" || $file == "wall-new"){ ?>
                  <div class="mbl-innerhead">
                     <div class="gotohome">
                        <a href="javascript:void(0)" onclick="closeAddNewMsg()"><i class="mdi mdi-arrow-left"></i></a>
                     </div>
                     <div class="logo-holder">
                        <span class="top_img">
                        <img src="images/whoisaround-img.png"/>
                        </span>
                        <a href="javascript:void(0)" class="mbl-logo page-name" onclick="contactInfo()">Vipul Patel</a>
                        <div class="top_message_status">
                           <span class="">last seen 1hr &nbsp; | </span>
                           <!--<span class="userstatus">&nbsp; i like nonsense it wakes up the brain cells</span>-->
                           <span class=""> 12:57 PM </span>
                           <span class="">| INDIA </span>
                        </div>
                     </div>
                  </div>
                  <?php } ?>
                  <?php if($file == "business-page" || $file == "business-page-new"){ ?>
                  <div class="mbl-innerhead">
                     <div class="gotohome">
                        <a href="javascript:void(0)" onclick="backToMain('businesspage')"><i class="mdi mdi-arrow-left"></i></a>
                     </div>
                     <div class="logo-holder">					
                        <a href="javascript:void(0)" class="mbl-logo page-name">Vipul Patel</a>
                     </div>
                  </div>
                  <?php } ?>
                  <div class="profile-top">
                     <a href="wall.php" class="profile-info">
                     <img  class="circle" src="images/demo-profile.jpg"/>
                     <span class="user-name">Nimish</span>
                     </a>
                     <div class="header_add_btn">
                        <a class='dropdown-button add_btn' href='javascript:void(0)' data-activates='add_btn'><i class="mdi mdi-plus"></i></a>
                        <ul id='add_btn' class='dropdown-content custom_dropdown account_custom_app '>
                           <li>
                              <a href="hotels.php" title="Hotels"><i class="mdi mdi-hospital-building"></i>Hotels</a>
                           </li>
                           <li>
                              <a href="homestay.php" title="Homestay"><i class="mdi mdi-hotel blue"></i>Homestay</a>
                           </li>
                           <li>
                              <a href="hotels.php" title="Restaurant"><i class="mdi mdi-silverware"></i>Restaurant</a>
                           </li>
                           <li>
                              <a href="localdine.php" title="Local Dine"><i class="mdi mdi-basecamp"></i>Local Dine</a>
                           </li>
                           <li>
                              <a href="tours.php" title="Tours"><i class="mdi mdi-ticket" ></i>Tours</a>
                           </li>
                           <li>
                              <a href="camping.php" title="Camping"><i class="mdi mdi-terrain"></i>Camping</a>
                           </li>
                           <li>
                              <a href="advertisement.php" title="Trip"><i class="mdi mdi-plus"></i>Advert</a>
                           </li>
                           <li>
                              <a href="javascript:void(0)" onclick="generateDiscard('dis_logout')" title="Logout"><i class="mdi mdi-logout"></i>Logout</a>
                           </li>
                        </ul>
                     </div>
                     <!--<a class="account_btn login_account" href="javascript:void(0)"><i class="mdi mdi-lock"></i></a>-->
                     <a class='dropdown-button account_btn waves-effect' href='#' data-activates='account_setting'><i class="zmdi zmdi-more-vert"></i></a>
                     <!-- Dropdown Structure -->
                     <ul id='account_setting' class='dropdown-content custom_dropdown account_custom_app'>
                        <li><a href="settings.php">Account Settings</a></li>
                        <li><a href="vip-member.php">VIP Member</a></li>
                        <li><a href="credit.php">Credits</a></li>
                        <li><a href="verify.php">Verification</a></li>
                        <li><a href="advertisement.php">Advertising Manager</a></li>
                        <li><a href="billing-info.php">Billing Information</a></li>
                     </ul>
                  </div>
                  <div class="mobile-profile-top">
                     <a href="wall.php">
                        <img class="circle" src="images/demo-profile.jpg">
                     </a>
                  </div>
                  <div class="not-icons desktop">
                     <div class="not-messages noticon">
                        <div class="dropdown dropdown-custom ">
                           <!-- Dropdown Trigger -->
                           <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='not_msg'>
                              <?php if($file == "tours" || $file == "flight" || $file == "hotels" || $file == "hotels-new" || $file == "places"){?>
                                 <img src="images/chat-white.png">
                              <?php } else { ?>
                                 <img src="images/chat-black.png">
                              <?php } ?>
                           </a>
                           <!-- Dropdown Structure -->
                           <ul id='not_msg' class='dropdown-content custom_dropdown message_ul dropdown-menu'>
                              <li>
                                 <div class="msg-list not-area nopad">
                                    <span class="not-title pull-left">Messages</span>
                                    <span class="not-title right right-align">Show all as read</span>
                                    <div class="not-resultlist nice-scroll no-listcontent">
                                       <ul class="msg-listing">
                                          <li class="norecord">
                                             <div class="valign-wrapper">
                                                   <h6 class="center-align">No messages found.</h6>
                                             </div>
                                          </li>
                                          <li class="read mainli">
                                             <div class="msg-holder">
                                                <a href="javascript:void(0)" onclick="openChatbox(this);">
                                                   <span class="img-holder">
                                                      <img class="img-responsive" src="images/demo-profile.jpg">
                                                   </span>
                                                   <span class="desc-holder">
                                                      <span class="uname">Adel Google</span>
                                                      <span class="desc">sdsd</span>
                                                      <span class="time-stamp">11:40 pm</span>
                                                   </span>
                                                </a>
                                                <a class="readicon" onclick="setReadUnread(this),markRead(this);" title="Mark as read" href="javascript:void(0)"></a>
                                                <div class="clear"></div>
                                             </div>
                                          </li>
                                          <li class="read mainli">
                                             <div class="msg-holder">
                                                <a href="javascript:void(0)" onclick="openChatbox(this);">
                                                   <span class="img-holder">
                                                      <img class="img-responsive" src="images/demo-profile.jpg">
                                                   </span>
                                                   <span class="desc-holder">
                                                      <span class="uname">Adel Google</span>
                                                      <span class="desc">sdsd</span>
                                                      <span class="time-stamp">11:40 pm</span>
                                                   </span>
                                                </a>
                                                <a class="readicon" onclick="setReadUnread(this),markRead(this);" title="Mark as read" href="javascript:void(0)"></a>
                                                <div class="clear"></div>
                                             </div>
                                          </li>
                                          <li class="read mainli">
                                             <div class="msg-holder">
                                                <a href="javascript:void(0)" onclick="openChatbox(this);">
                                                   <span class="img-holder">
                                                      <img class="img-responsive" src="images/demo-profile.jpg">
                                                   </span>
                                                   <span class="desc-holder">
                                                      <span class="uname">Adel Google</span>
                                                      <span class="desc">sdsd</span>
                                                      <span class="time-stamp">11:40 pm</span>
                                                   </span>
                                                </a>
                                                <a class="readicon" onclick="setReadUnread(this),markRead(this);" title="Mark as read" href="javascript:void(0)"></a>
                                                <div class="clear"></div>
                                             </div>
                                          </li>
                                          <li class="read mainli">
                                             <div class="msg-holder">
                                                <a href="javascript:void(0)" onclick="openChatbox(this);">
                                                   <span class="img-holder">
                                                      <img class="img-responsive" src="images/demo-profile.jpg">
                                                   </span>
                                                   <span class="desc-holder">
                                                      <span class="uname">Adel Google</span>
                                                      <span class="desc">sdsd</span>
                                                      <span class="time-stamp">11:40 pm</span>
                                                   </span>
                                                </a>
                                                <a class="readicon" onclick="setReadUnread(this),markRead(this);" title="Mark as read" href="javascript:void(0)"></a>
                                                <div class="clear"></div>
                                             </div>
                                          </li>
                                          <li class="read mainli">
                                             <div class="msg-holder">
                                                <a href="javascript:void(0)" onclick="openChatbox(this);">
                                                   <span class="img-holder">
                                                      <img class="img-responsive" src="images/demo-profile.jpg">
                                                   </span>
                                                   <span class="desc-holder">
                                                      <span class="uname">Adel Google</span>
                                                      <span class="desc">sdsd</span>
                                                      <span class="time-stamp">11:40 pm</span>
                                                   </span>
                                                </a>
                                                <a class="readicon" onclick="setReadUnread(this),markRead(this);" title="Mark as read" href="javascript:void(0)"></a>
                                                <div class="clear"></div>
                                             </div>
                                          </li>
                                       </ul>
                                       <span class="not-result bshadow left-align"><a href="javascript:void(0)">Show all messages <i class="mdi mdi-menu-right"></i></a></span>
                                    </div>
                                 </div>
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="not-notification noticon">
                        <div class="dropdown dropdown-custom ">
                           <!-- Dropdown Trigger -->
                           <a class='dropdown-button more_btn' href='#' data-activates='not_notify'>
                              <i class="mdi mdi-bell-outline"></i>
                              <span class="new-notification">10</span>
                           </a>
                           
                           <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='not_frndreq' id="frdrqtdebugger"></a>

                           <!-- Dropdown Structure -->
                           <ul id='not_frndreq' class='dropdown-content request_dropdown '>
                              <li>
                                 <div class="fr-list not-area">
                                    <span class="not-title">Connect Requests</span>
                                    <div class="not-resultlist nice-scroll">
                                       <ul class="fr-listing">
                                          <li>
                                             <form>
                                                <div class="fr-holder">
                                                   <div class="img-holder">
                                                      <a href="javascript:void(0)"><img class="img-responsive" src="images/demo-profile.jpg"></a>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="desc">
                                                         <a href="javascript:void(0)">Abc Def</a>
                                                         <span class="mf-info"></span>
                                                      </div>
                                                      <div class="fr-btn-holder">
                                                         <button class="btn btn-primary btn-sm">Confirm</button>
                                                         <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                      </div>
                                                   </div>
                                                </div>
                                             </form>
                                          </li>
                                          <li>
                                             <form>
                                                <div class="fr-holder">
                                                   <div class="img-holder">
                                                      <a href="javascript:void(0)"><img class="img-responsive" src="images/male.png"></a>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="desc">
                                                         <a href="javascript:void(0)">Abc Def</a>
                                                         <span class="mf-info"></span>
                                                      </div>
                                                      <div class="fr-btn-holder">
                                                         <button class="btn btn-primary btn-sm">Confirm</button>
                                                         <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                      </div>
                                                   </div>
                                                </div>
                                             </form>
                                          </li>
                                          <li>
                                             <form>
                                                <div class="fr-holder">
                                                   <div class="img-holder">
                                                      <a href="javascript:void(0)"><img class="img-responsive" src="images/demo-profile.jpg"></a>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="desc">
                                                         <a href="javascript:void(0)">Abc Def</a>
                                                         <span class="mf-info"></span>
                                                      </div>
                                                      <div class="fr-btn-holder">
                                                         <button class="btn btn-primary btn-sm">Confirm</button>
                                                         <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                      </div>
                                                   </div>
                                                </div>
                                             </form>
                                          </li>
                                          <li>
                                             <form>
                                                <div class="fr-holder">
                                                   <div class="img-holder">
                                                      <a href="javascript:void(0)"><img class="img-responsive" src="images/male.png"></a>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="desc">
                                                         <a href="javascript:void(0)">Abc Def</a>
                                                         <span class="mf-info"></span>
                                                      </div>
                                                      <div class="fr-btn-holder">
                                                         <button class="btn btn-primary btn-sm">Confirm</button>
                                                         <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                      </div>
                                                   </div>
                                                </div>
                                             </form>
                                          </li>
                                          <li>
                                             <form>
                                                <div class="fr-holder">
                                                   <div class="img-holder">
                                                      <a href="javascript:void(0)"><img class="img-responsive" src="images/demo-profile.jpg"></a>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="desc">
                                                         <a href="javascript:void(0)">Abc Def</a>
                                                         <span class="mf-info"></span>
                                                      </div>
                                                      <div class="fr-btn-holder">
                                                         <button class="btn btn-primary btn-sm">Confirm</button>
                                                         <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                      </div>
                                                   </div>
                                                </div>
                                             </form>
                                          </li>
                                          <li>
                                             <form>
                                                <div class="fr-holder">
                                                   <div class="img-holder">
                                                      <a href="javascript:void(0)"><img class="img-responsive" src="images/male.png"></a>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="desc">
                                                         <a href="javascript:void(0)">Abc Def</a>
                                                         <span class="mf-info"></span>
                                                      </div>
                                                      <div class="fr-btn-holder">
                                                         <button class="btn btn-primary btn-sm">Confirm</button>
                                                         <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                      </div>
                                                   </div>
                                                </div>
                                             </form>
                                          </li>
                                          <li>
                                             <form>
                                                <div class="fr-holder">
                                                   <div class="img-holder">
                                                      <a href="javascript:void(0)"><img class="img-responsive" src="images/demo-profile.jpg"></a>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="desc">
                                                         <a href="javascript:void(0)">Abc Def</a>
                                                         <span class="mf-info"></span>
                                                      </div>
                                                      <div class="fr-btn-holder">
                                                         <button class="btn btn-primary btn-sm">Confirm</button>
                                                         <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                      </div>
                                                   </div>
                                                </div>
                                             </form>
                                          </li>
                                          <li>
                                             <form>
                                                <div class="fr-holder">
                                                   <div class="img-holder">
                                                      <a href="javascript:void(0)"><img class="img-responsive" src="images/male.png"></a>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="desc">
                                                         <a href="javascript:void(0)">Abc Def</a>
                                                         <span class="mf-info"></span>
                                                      </div>
                                                      <div class="fr-btn-holder">
                                                         <button class="btn btn-primary btn-sm">Confirm</button>
                                                         <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                      </div>
                                                   </div>
                                                </div>
                                             </form>
                                          </li>
                                          <li>
                                             <form>
                                                <div class="fr-holder">
                                                   <div class="img-holder">
                                                      <a href="javascript:void(0)"><img class="img-responsive" src="images/demo-profile.jpg"></a>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="desc">
                                                         <a href="javascript:void(0)">Abc Def</a>
                                                         <span class="mf-info"></span>
                                                      </div>
                                                      <div class="fr-btn-holder">
                                                         <button class="btn btn-primary btn-sm">Confirm</button>
                                                         <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                      </div>
                                                   </div>
                                                </div>
                                             </form>
                                          </li>
                                          <li>
                                             <form>
                                                <div class="fr-holder">
                                                   <div class="img-holder">
                                                      <a href="javascript:void(0)"><img class="img-responsive" src="images/male.png"></a>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="desc">
                                                         <a href="javascript:void(0)">Abc Def</a>
                                                         <span class="mf-info"></span>
                                                      </div>
                                                      <div class="fr-btn-holder">
                                                         <button class="btn btn-primary btn-sm">Confirm</button>
                                                         <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                      </div>
                                                   </div>
                                                </div>
                                             </form>
                                          </li>
                                          <li>
                                             <form>
                                                <div class="fr-holder">
                                                   <div class="img-holder">
                                                      <a href="javascript:void(0)"><img class="img-responsive" src="images/demo-profile.jpg"></a>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="desc">
                                                         <a href="javascript:void(0)">Abc Def</a>
                                                         <span class="mf-info"></span>
                                                      </div>
                                                      <div class="fr-btn-holder">
                                                         <button class="btn btn-primary btn-sm">Confirm</button>
                                                         <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                      </div>
                                                   </div>
                                                </div>
                                             </form>
                                          </li>
                                          <li>
                                             <form>
                                                <div class="fr-holder">
                                                   <div class="img-holder">
                                                      <a href="javascript:void(0)"><img class="img-responsive" src="images/male.png"></a>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="desc">
                                                         <a href="javascript:void(0)">Abc Def</a>
                                                         <span class="mf-info"></span>
                                                      </div>
                                                      <div class="fr-btn-holder">
                                                         <button class="btn btn-primary btn-sm">Confirm</button>
                                                         <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                      </div>
                                                   </div>
                                                </div>
                                             </form>
                                          </li>
                                          <li>
                                             <form>
                                                <div class="fr-holder">
                                                   <div class="img-holder">
                                                      <a href="javascript:void(0)"><img class="img-responsive" src="images/demo-profile.jpg"></a>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="desc">
                                                         <a href="javascript:void(0)">Abc Def</a>
                                                         <span class="mf-info"></span>
                                                      </div>
                                                      <div class="fr-btn-holder">
                                                         <button class="btn btn-primary btn-sm">Confirm</button>
                                                         <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                      </div>
                                                   </div>
                                                </div>
                                             </form>
                                          </li>
                                          <li>
                                             <form>
                                                <div class="fr-holder">
                                                   <div class="img-holder">
                                                      <a href="javascript:void(0)"><img class="img-responsive" src="images/male.png"></a>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="desc">
                                                         <a href="javascript:void(0)">Abc Def</a>
                                                         <span class="mf-info"></span>
                                                      </div>
                                                      <div class="fr-btn-holder">
                                                         <button class="btn btn-primary btn-sm">Confirm</button>
                                                         <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                      </div>
                                                   </div>
                                                </div>
                                             </form>
                                          </li>
                                       </ul>
                                    </div>
                                    <span class="not-result bshadow"><a href="javascript:void(0)">View All</a></span>
                                 </div>
                              </li>
                           </ul>

                           <!-- Dropdown Structure -->
                           <ul id='not_notify' class='dropdown-content request_dropdown'>
                              <li>
                                 <div class="noti-list not-area">
                                    <span class="not-title">Notifications</span>
                                    <div class="not-resultlist nice-scroll">
                                       <ul class="noti-listing">
                                          <li>
                                             <div class="noti-holder">
                                                <a href="javascript:void(0)">
                                                   <span class="img-holder">
                                                   <img class="img-responsive" src="images/demo-profile.jpg">
                                                   </span>
                                                   <span class="desc-holder">
                                                      <span class="desc">
                                                      <span class="btext">Adel Hasanat</span> wants you to be <span class="btext">Admin</span> for the group <span class="btext">PHP Developers</span>
                                                      </span>
                                                      <div class="btn-holder">
                                                         <button class="btn btn-primary btn-sm">Confirm</button>
                                                         <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                      </div>
                                                      <span class="time-stamp">
                                                      <i class="mdi mdi-earth"></i> Just Now
                                                      </span>
                                                   </span>
                                                </a>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="noti-holder">
                                                <a href="javascript:void(0)">
                                                <span class="img-holder">
                                                <img class="img-responsive" src="images/demo-profile.jpg">
                                                </span>
                                                <span class="desc-holder">
                                                <span class="desc">
                                                <span class="btext">Abc Def</span> replied on your comment:
                                                </span>
                                                <span class="time-stamp">
                                                <i class="mdi mdi-earth"></i> Just Now
                                                </span>
                                                </span>
                                                </a>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="noti-holder">
                                                <a href="javascript:void(0)">
                                                <span class="img-holder">
                                                <img class="img-responsive" src="images/demo-profile.jpg">
                                                </span>
                                                <span class="desc-holder">
                                                <span class="desc">
                                                <span class="btext">Abc Def</span> added a photo
                                                </span>
                                                <span class="time-stamp">
                                                <i class="mdi mdi-account"></i> 20 mins ago
                                                </span>
                                                </span>
                                                </a>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="noti-holder">
                                                <a href="javascript:void(0)">
                                                <span class="img-holder">
                                                <img class="img-responsive" src="images/demo-profile.jpg">
                                                </span>
                                                <span class="desc-holder">
                                                <span class="desc">
                                                <span class="btext">Abc Def</span> replied on your comment:
                                                </span>
                                                <span class="time-stamp">
                                                <i class="mdi mdi-earth"></i> Just Now
                                                </span>
                                                </span>
                                                </a>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="noti-holder">
                                                <a href="javascript:void(0)">
                                                <span class="img-holder">
                                                <img class="img-responsive" src="images/demo-profile.jpg">
                                                </span>
                                                <span class="desc-holder">
                                                <span class="desc">
                                                <span class="btext">Abc Def</span> added a photo
                                                </span>
                                                <span class="time-stamp">
                                                <i class="mdi mdi-account"></i> 20 mins ago
                                                </span>
                                                </span>
                                                </a>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="noti-holder">
                                                <a href="javascript:void(0)">
                                                <span class="img-holder">
                                                <img class="img-responsive" src="images/demo-profile.jpg">
                                                </span>
                                                <span class="desc-holder">
                                                <span class="desc">
                                                <span class="btext">Abc Def</span> replied on your comment:
                                                </span>
                                                <span class="time-stamp">
                                                <i class="mdi mdi-earth"></i> Just Now
                                                </span>
                                                </span>
                                                </a>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="noti-holder">
                                                <a href="javascript:void(0)">
                                                <span class="img-holder">
                                                <img class="img-responsive" src="images/demo-profile.jpg">
                                                </span>
                                                <span class="desc-holder">
                                                <span class="desc">
                                                <span class="btext">Abc Def</span> added a photo
                                                </span>
                                                <span class="time-stamp">
                                                <i class="mdi mdi-account"></i> 20 mins ago
                                                </span>
                                                </span>
                                                </a>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="noti-holder">
                                                <a href="javascript:void(0)">
                                                <span class="img-holder">
                                                <img class="img-responsive" src="images/demo-profile.jpg">
                                                </span>
                                                <span class="desc-holder">
                                                <span class="desc">
                                                <span class="btext">Abc Def</span> replied on your comment:
                                                </span>
                                                <span class="time-stamp">
                                                <i class="mdi mdi-earth"></i> Just Now
                                                </span>
                                                </span>
                                                </a>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="noti-holder">
                                                <a href="javascript:void(0)">
                                                <span class="img-holder">
                                                <img class="img-responsive" src="images/demo-profile.jpg">
                                                </span>
                                                <span class="desc-holder">
                                                <span class="desc">
                                                <span class="btext">Abc Def</span> added a photo
                                                </span>
                                                <span class="time-stamp">
                                                <i class="mdi mdi-account"></i> 20 mins ago
                                                </span>
                                                </span>
                                                </a>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="noti-holder">
                                                <a href="javascript:void(0)">
                                                <span class="img-holder">
                                                <img class="img-responsive" src="images/demo-profile.jpg">
                                                </span>
                                                <span class="desc-holder">
                                                <span class="desc">
                                                <span class="btext">Abc Def</span> replied on your comment:
                                                </span>
                                                <span class="time-stamp">
                                                <i class="mdi mdi-earth"></i> Just Now
                                                </span>
                                                </span>
                                                </a>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="noti-holder">
                                                <a href="javascript:void(0)">
                                                <span class="img-holder">
                                                <img class="img-responsive" src="images/demo-profile.jpg">
                                                </span>
                                                <span class="desc-holder">
                                                <span class="desc">
                                                <span class="btext">Abc Def</span> added a photo
                                                </span>
                                                <span class="time-stamp">
                                                <i class="mdi mdi-account"></i> 20 mins ago
                                                </span>
                                                </span>
                                                </a>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="noti-holder">
                                                <a href="javascript:void(0)">
                                                <span class="img-holder">
                                                <img class="img-responsive" src="images/demo-profile.jpg">
                                                </span>
                                                <span class="desc-holder">
                                                <span class="desc">
                                                <span class="btext">Abc Def</span> replied on your comment:
                                                </span>
                                                <span class="time-stamp">
                                                <i class="mdi mdi-earth"></i> Just Now
                                                </span>
                                                </span>
                                                </a>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="noti-holder">
                                                <a href="javascript:void(0)">
                                                <span class="img-holder">
                                                <img class="img-responsive" src="images/demo-profile.jpg">
                                                </span>
                                                <span class="desc-holder">
                                                <span class="desc">
                                                <span class="btext">Abc Def</span> added a photo
                                                </span>
                                                <span class="time-stamp">
                                                <i class="mdi mdi-account"></i> 20 mins ago
                                                </span>
                                                </span>
                                                </a>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="noti-holder">
                                                <a href="javascript:void(0)">
                                                <span class="img-holder">
                                                <img class="img-responsive" src="images/demo-profile.jpg">
                                                </span>
                                                <span class="desc-holder">
                                                <span class="desc">
                                                <span class="btext">Abc Def</span> replied on your comment:
                                                </span>
                                                <span class="time-stamp">
                                                <i class="mdi mdi-earth"></i> Just Now
                                                </span>
                                                </span>
                                                </a>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="noti-holder">
                                                <a href="javascript:void(0)">
                                                <span class="img-holder">
                                                <img class="img-responsive" src="images/demo-profile.jpg">
                                                </span>
                                                <span class="desc-holder">
                                                <span class="desc">
                                                <span class="btext">Abc Def</span> added a photo
                                                </span>
                                                <span class="time-stamp">
                                                <i class="mdi mdi-account"></i> 20 mins ago
                                                </span>
                                                </span>
                                                </a>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="noti-holder">
                                                <a href="javascript:void(0)">
                                                <span class="img-holder">
                                                <img class="img-responsive" src="images/demo-profile.jpg">
                                                </span>
                                                <span class="desc-holder">
                                                <span class="desc">
                                                <span class="btext">Abc Def</span> replied on your comment:
                                                </span>
                                                <span class="time-stamp">
                                                <i class="mdi mdi-earth"></i> Just Now
                                                </span>
                                                </span>
                                                </a>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="noti-holder">
                                                <a href="javascript:void(0)">
                                                <span class="img-holder">
                                                <img class="img-responsive" src="images/demo-profile.jpg">
                                                </span>
                                                <span class="desc-holder">
                                                <span class="desc">
                                                <span class="btext">Abc Def</span> added a photo
                                                </span>
                                                <span class="time-stamp">
                                                <i class="mdi mdi-account"></i> 20 mins ago
                                                </span>
                                                </span>
                                                </a>
                                             </div>
                                          </li>
                                       </ul>
                                       <span class="not-result notificationfrdrqt bshadow left-align valign-wrapper">
                                          <a href="javascript:void(0)">
                                             <i class="zmdi zmdi-account-o mdi-36px"></i>&nbsp;&nbsp;&nbsp;
                                             <span class="label">Connection Request</span> &nbsp;&nbsp;&nbsp;
                                             <i class="zmdi zmdi-chevron-right mdi-30px"></i>
                                          </a>
                                       </span>
                                       <span class="not-result bshadow"><a href="javascript:void(0)">View All</a></span>
                                    </div>
                                 </div>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php } ?>
      <!-- Login Popup -->
      <div id="login_modal" class="modal login-popup home-page custom_login">
         <div class="popup-title ">			
            <a class="close_span waves-effect" href="javascript:void(0)">
            <i class="mdi mdi-close mdi-20px"></i>
            </a>
         </div>
         <div class="popup-content">
            <div class="home-content">
               <div class="login-part homel-part">
                  <div class="homebox login-box">
                     <div class="sociallink-area">
                        <a href="/iwasinqatarcode/frontend/web?r=site/auth&amp;authclient=facebook" class="fb-btn"><span><i class="mdi mdi-facebook"></i></span>Connect with Facebook</a>
                     </div>
                     <div class="home-divider">
                        <span class="div-or">or</span>
                     </div>
                     <div class="box-content">
                        <div class="login-notice">
                           <span class="success-note">Successfully Logged in!</span>
                           <span class="info-note">Fill in the mandatory fields.</span>
                           <span class="error-note">Please Enter Email address and Password</span>
                        </div>
                        <div class="bc-row">
                           <div class="bc-component">
                              <div class="sliding-middle-out anim-area underlined">
                                 <input type="text" placeholder="Email Address">
                              </div>
                           </div>
                        </div>
                        <div class="bc-row">
                           <div class="bc-component">
                              <div class="sliding-middle-out anim-area underlined">
                                 <input type="password" placeholder="Password">
                              </div>
                           </div>
                        </div>
                        <div class="clear"></div>
                     </div>
                     <div class="nextholder">
                        <a class="fp-link" href="javascript:void(0)" onclick="setForgotPassStep();flipSectionTo('forgot');">Forgot Password?</a>
                        <a href="javascript:void(0)" class="homebtn">Log in</a>
                     </div>
                     <div class="btn-holder">
                        <p>Do not have an account?
                           <a onclick="flipSectionTo('signup');" href="javascript:void(0)">Sign Up</a>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="signup-part homes-part">
                  <div class="homebox signup-box" id="create-account">
                     <div class="box-content">
                        <div class="signup-notice">
                           <span class="success-note">Successfully Logged in!</span>
                           <span class="info-note">Fill in the mandatory fields.</span>
                           <span class="error-note">Please Enter Email address and Password</span>
                        </div>
                        <h5>Create Account</h5>
                        <div class="bc-row">
                           <div class="bc-component">
                              <div class="sliding-middle-out anim-area underlined">
                                 <input type="text" placeholder="First Name">
                              </div>
                           </div>
                        </div>
                        <div class="bc-row">
                           <div class="bc-component">
                              <div class="sliding-middle-out anim-area underlined">
                                 <input type="text" placeholder="Last Name">
                              </div>
                           </div>
                        </div>
                        <div class="bc-row">
                           <div class="bc-component">
                              <div class="sliding-middle-out anim-area underlined">
                                 <input type="text" placeholder="Email Address">
                              </div>
                           </div>
                        </div>
                        <div class="bc-row">
                           <div class="bc-component">
                              <div class="sliding-middle-out anim-area underlined">
                                 <input type="password" placeholder="Password">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="nextholder">
                        <a href="javascript:void(0)" class="homebtn su-nextbtn" data-class="profile-setting" onclick="signupNavigation(this)">Next</a>								
                     </div>
                     <div class="btn-holder">
                        <p>Have an account?
                           <a onclick="flipSectionTo('login');" href="javascript:void(0)">Login</a>
                        </p>
                     </div>
                  </div>
                  <div class="homebox signup-box profile-setting-new" id="profile-setting">
                     <div class="box-content">
                        <div class="signup-notice">
                           <span class="success-note">Successfully Logged in!</span>
                           <span class="info-note">Fill in the mandatory fields.</span>
                           <span class="error-note">Please Enter Email address and Password</span>
                        </div>
                        <h5>Profile Setting</h5>
                        <div class="bc-row">
                           <div class="bc-component">
                              <div class="sliding-middle-out anim-area underlined">
                                 <input type="text" placeholder="City">
                              </div>
                           </div>
                        </div>
                        <div class="bc-row">
                           <div class="bc-component">
                              <div class="sliding-middle-out anim-area underlined">
                                 <input type="text" placeholder="Country">
                              </div>
                           </div>
                        </div>
                        <div class="bc-row">
                           <div class="bc-component">
                              <div class="sliding-middle-out anim-area underlined">
                                 <select id="genderDrop" class="select2 genderDrop" >
                                    <option></option>
                                    <option>Male</option>
                                    <option>Female</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="bc-row">
                           <div class="bc-component">
                              <div class="sliding-middle-out anim-area underlined">
                                 <input type="text" onkeydown="return false;" placeholder="Birthdate" name="LoginForm[birth_date]" class="form-control datetime-picker datepickerinput" data-query="M" data-toggle="datepicker" id="datepicker" readonly>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="nextholder">
                        <a href="javascript:void(0)" class="homebtn su-nextbtn" data-class="security-check" onclick="signupNavigation(this)">Next</a>
                     </div>
                     <div class="btn-holder">
                        <p>Have an account?
                           <a onclick="flipSectionTo('login');" href="javascript:void(0)">Login</a>
                        </p>
                     </div>
                  </div>
                  <div class="homebox signup-box" id="security-check">
                     <div class="box-content">
                        <div class="signup-notice">
                           <span class="success-note">Successfully Logged in!</span>
                           <span class="info-note">Fill in the mandatory fields.</span>
                           <span class="error-note">Please Enter Email address and Password</span>
                        </div>
                        <h5>Security Check</h5>
                        <div class="security-box">
                           <p>To guard against automated systems and robots, please add the two numbers and enter your answer below</p>
                           <div class="bc-row">
                              <div class="bc-label"><label class="blabel">1 + 8 =</label></div>
                              <div class="bc-component">
                                 <div class="sliding-middle-out anim-area underlined">
                                    <input type="text" placeholder="Enter Answer">
                                 </div>
                              </div>
                           </div>
                           <div class="h-checkbox">
                              <input type="checkbox" id="test1">
                              <label>Agreed to the terms and conditions</label>
                           </div>
                        </div>
                     </div>
                     <div class="nextholder">
                        <a href="javascript:void(0)" class="homebtn su-nextbtn" data-class="upload-photo" onclick="signupNavigation(this)">Next</a>
                     </div>
                     <div class="btn-holder">
                        <p>Have an account?
                           <a onclick="flipSectionTo('login');" href="javascript:void(0)">Login</a>
                        </p>
                     </div>
                  </div>
                  <div class="homebox signup-box upload-photo" id="upload-photo">
                     <div class="box-content">
                        <div class="signup-notice">
                           <span class="success-note">Successfully Logged in!</span>
                           <span class="info-note">Fill in the mandatory fields.</span>
                           <span class="error-note">Please Enter Email address and Password</span>
                        </div>
                        <div class="upload-area">
                           <div class="crop-sec">
                              <div class="crop-holder">
                                 <div id="cropContainerPreload"></div>
                                 <div class="custom-file">
                                    <div class="title"><span class="glyphicon glyphicon-camera"></span></div>
                                    <input type="file" class="upload custom-upload" title="Choose a file to upload"/>
                                 </div>
                              </div>
                           </div>
                           <div class="desc-sec">
                              <h4>Adel Hasanat</h4>
                              <h6 class="white-text">Qatar</h6>
                              <p class="note">
                                 Your photo must be with jpeg, gif or jpg format and must be of 150x150 pixels.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="nextholder">
                        <a href="javascript:void(0)" class="homebtn su-skipbtn" data-class="confirm-email" onclick="signupNavigation(this)">Skip</a>
                        <a href="javascript:void(0)" class="homebtn su-nextbtn" data-class="confirm-email" onclick="signupNavigation(this)">Next</a>
                     </div>
                     <div class="btn-holder">
                        <p>Have an account?
                           <a onclick="flipSectionTo('login');" href="javascript:void(0)">Login</a>
                        </p>
                     </div>
                  </div>
                  <div class="homebox signup-box" id="confirm-email">
                     <div class="box-content">
                        <div class="signup-notice">
                           <span class="success-note">Successfully Logged in!</span>
                           <span class="info-note">Fill in the mandatory fields.</span>
                           <span class="error-note">Please Enter Email address and Password</span>
                        </div>
                        <h5>You’re almost done, just confirm your email</h5>
                        <div class="text-center fullwidth">
                           <img src="images/confirm-msg.png" class="confirm-img"/>									
                        </div>
                        <h6 class="white-text">Please confirm your email to have full access to your account</h6>
                     </div>
                     <div class="nextholder">
                        <a class="homebtn autow" href="javascript:void(0)">Confirm Email</a>
                     </div>
                  </div>
               </div>
               <div class="forgot-part homes-part">
                  <div class="homebox forgot-box" id="fp-step-1">
                     <div class="box-content">
                        <div class="fphome-notice">
                           <span class="success-note">Successfully Logged in!</span>
                           <span class="info-note">Fill in the mandatory fields.</span>
                           <span class="error-note">Please Enter Email address and Password</span>
                        </div>
                        <h5>Change Your Password</h5>
                        <div class="fp-box">
                           <p class="text-center">Let's find your account</p>
                           <div class="bc-row mt25">
                              <div class="sliding-middle-out anim-area underlined">
                                 <input type="text" placeholder="Email address or alternate email">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="nextholder">
                        <a href="javascript:void(0)" class="homebtn" data-class="fp-step-2" onclick="forgotPassNavigation(this)">Next</a>
                     </div>
                  </div>
                  <div class="homebox forgot-box" id="fp-step-2">
                     <div class="box-content">
                        <h5>We've sent a link to change your password</h5>
                        <div class="fp-box">
                           <p>check your email and follow the link to quickly reset your password</p>
                        </div>
                     </div>
                     <div class="nextholder">
                        <a href="javascript:void(0)" class="homebtn autow checke-link">Check Email</a>
                        <a href="javascript:void(0)" class="homebtn" data-class="fp-step-3" onclick="forgotPassNavigation(this)">Next</a>
                     </div>
                  </div>
                  <div class="homebox forgot-box" id="fp-step-3">
                     <div class="box-content">
                        <div class="fphome-notice">
                           <span class="success-note">Successfully Logged in!</span>
                           <span class="info-note">Fill in the mandatory fields.</span>
                           <span class="error-note">Please Enter Email address and Password</span>
                        </div>
                        <h5>Choose New Password</h5>
                        <div class="bc-row">
                           <div class="bc-component">
                              <div class="sliding-middle-out anim-area underlined">
                                 <input type="text" placeholder="Type your new password">
                              </div>
                           </div>
                        </div>
                        <div class="bc-row">
                           <div class="bc-component">
                              <div class="sliding-middle-out anim-area underlined">
                                 <input type="text" placeholder="Confirm your new password">
                              </div>
                           </div>
                        </div>
                        <p class="note">Passwords are case sensitive, must be at least 6 characters</p>
                        <a href="javascript:void(0)" class="homebtn" data-class="fp-step-4" onclick="forgotPassNavigation(this)">Continue</a>
                     </div>
                  </div>
                  <div class="homebox forgot-box" id="fp-step-4">
                     <div class="box-content">
                        <h5>Your password has been reset</h5>
                        <div class="fp-box">
                           <p class="text-center">Now you can login with your new password!</p>
                        </div>
                        <a href="javascript:void(0)" onclick="flipSectionTo('login');" class="homebtn">Log in</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Login Popup -->

      