
<?php include("header.php"); ?> 
<?php include("common/menu.php"); ?>
<div class="index-page"><?php include("common/menu.php"); ?></div>
<div class="menu-wrap">
   <div class="menu-sec h-search">
      <img src="images/home-promo1.jpg" alt="header img">
      <div class="search-box">
           <div class="box-content">
               <div class="bc-row home-search">
                  <div class="row mx-0"></div>
               </div>
           </div>
        </div> 
   </div>
</div>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon">
         <span class="icon-holder ispan">
            <i class="mdi mdi-arrow-up-bold-circle"></i>
         </span>
      </div>
   </div>
</div>
<div class="clear"></div>
<div class="container page_container">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout">
      <div class="main-content main-page places-page qatar-page pb-0 mt-0">
         <div class="combined-column wide-open main-page full-page">
         <div class="places-content places-all">
            <h4 class="label_sentence">Enjoy your local dinning</h4>
            <div class="container cshfsiput">
               <div class="places-column cshfsiput m-top">
                  <div class="">
                     <div id="places-dine" class="placesdine-content subtab places-discussion-main top_tabs">
                        <div class="content-box">
                           <div class="mbl-tabnav">
                              <a href="javascript:void(0)"><i class="mdi mdi-arrow-left"></i></a> 
                              <h6>Restaurants</h6>
                           </div>
                           <div class="placesection redsection">
                              <div class="cbox-desc hotels-page np">
                                 <div class="tcontent-holder moreinfo-outer">
                                    <div class="top-stuff top-graybg" id="all-restaurant">
                                       <div class="more-actions">
                                          <div style="display: inline-block;"><a href="restaurantmap.php"><i class="zmdi zmdi-pin"></i>&nbsp;&nbsp;Map</a></div>
                                       </div>
                                       <h6>345 restaurants found in <span>Qatar</span></h6>
                                    </div>
                                    <div class="places-content-holder">
                                       <div class="map-holder">
                                          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3110.3465133386144!2d-9.167423685010494!3d38.77868997958898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd193295d5b45545%3A0x3f9e7b6a5f00e12c!2sPerta!5e0!3m2!1sen!2sin!4v1481089901870" width="600" height="450" frameborder="0" allowfullscreen></iframe>
                                          <a href="javascript:void(0)" class="overlay" onclick="expandMap(this,'#all-restaurant')"></a>
                                          <a href="javascript:void(0)" class="closelink" onclick="shrinkMap(this)"><i class="mdi mdi-close"></i> Close</a>
                                       </div>
                                       <div class="list-holder">
                                          <div class="hotel-list fullw-list">
                                             <ul>
                                                <li>
                                                   <div class="hotel-li expandable-holder dealli mobilelist">
                                                      <div class="summery-info">
                                                         <div class="imgholder himg-box"><img src="images/restaurant1.png" class="himg" /></div>
                                                         <div class="descholder">
                                                            <a href="javascript:void(0)" class="expand-link" onclick="mng_expandable(this,'hasClose')">
                                                               <h4>Hashim Restaurant</h4>
                                                               <div class="clear"></div>
                                                               <div class="reviews-link">
                                                                  <span class="review-count">30 Reviews</span>
                                                                  <span class="checks-holder">
                                                                  <i class="mdi mdi-star active"></i>
                                                                  <i class="mdi mdi-star active"></i>
                                                                  <i class="mdi mdi-star active"></i>
                                                                  <i class="mdi mdi-star active"></i>
                                                                  <i class="mdi mdi-star"></i>
                                                                  <label>Very Good - 70/100</label>
                                                                  </span>
                                                               </div>
                                                               <span class="address">Middle Eastem & African, Mediterranean</span>
                                                               <span class="distance-info"><i class="mdi mdi-phone"></i> 999-999-9999</span>
                                                               <div class="more-holder">
                                                                  <div class="tagging" onclick="explandTags(this)">
                                                                     Popular with:
                                                                     <span>Budget</span>
                                                                  </div>
                                                               </div>
                                                            </a>
                                                         </div>
                                                      </div>
                                                      <div class="expandable-area">
                                                         <a href="javascript:void(0)" class="shrink-link" onclick="mng_expandable(this,'closeIt')"><i class="mdi mdi-close"></i> Close</a>
                                                         <div class="clear"></div>
                                                         <div class="explandable-tabs">
                                                            <ul class="tabs tabsnew subtab-menu">
                                                               <li class="active tab"><a  href="#subtab-reviews2">Reviews</a></li>
                                                               <li class="tab"><a data-which="photo" href="#subtab-photos2" data-tab="subtab-photos">Photos</a></li>
                                                               <li><a aria-expanded="false" data-toggle="tab" href="#subtab-details2">Details</a></li>
                                                            </ul>
                                                            <div class="tab-content">
                                                               <div id="subtab-reviews2" class="tab-pane fade active in">
                                                                  <div class="reviews-summery">
                                                                     <div class="reviews-people">
                                                                        <ul>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-3.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>Kelly Mark <span>about 2 weeks ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>We enjoyed the lounge and bar at the Ritz where you are offered many choices for drinks and some pretty elaborate looking dishes of food as well.</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-2.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>John Davior <span>about 8 months ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>If you want a fancy London experience than The Ritz is where you need to go! At least budget for High Tea!</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-1.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>Joe Doe <span>about 11 months ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>I am not at all sure this is the best hotel in London, but it does deserve the reputation as one of the most glamourous.</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div id="subtab-photos2" class="tab-pane fade subtab-photos">
                                                                  <div class="photo-gallery">
                                                                     <div class="img-preview">
                                                                        <img src="images/post-img1.jpg" />
                                                                     </div>
                                                                     <div class="thumbs-img">
                                                                        <ul>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                        </ul>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div id="subtab-details2" class="tab-pane fade">
                                                                  <div class="subdetail-box">
                                                                     <div class="infoholder">
                                                                        <div class="descholder">
                                                                           <div class="more-holder">
                                                                              <ul class="infoul">
                                                                                 <li>
                                                                                    <i class="zmdi zmdi-pin"></i>
                                                                                    132 Brick Lane | E1 6RU, Qatar E1 6RU, Qatar
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-phone"></i>
                                                                                    +44 20 7247 8210
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-earth"></i>
                                                                                    http://www.yourwebsite.com
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-clock-outline"></i>
                                                                                    Mon-Fri : 12:00 PM - 10:00 AM
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-certificate "></i>
                                                                                    Ranked #1 in Qatar Hotels
                                                                                 </li>
                                                                              </ul>
                                                                              <div class="tagging" onclick="explandTags(this)">
                                                                                 Popular with:
                                                                                 <span>point of interest</span>
                                                                                 <span>establishment</span>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li>
                                                   <div class="hotel-li expandable-holder">
                                                      <div class="summery-info">
                                                         <div class="imgholder himg-box"><img src="images/restaurant2.png" class="himg" /></div>
                                                         <div class="descholder">
                                                            <h4>Emirates Grand</h4>
                                                            <div class="clear"></div>
                                                            <div class="reviews-link">
                                                               <span class="checks-holder">
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star"></i>
                                                               <label>34 Reviews</label>
                                                               </span>
                                                            </div>
                                                            <span class="distance-info">Middle Eastem & African, Mediterranean</span>
                                                            <div class="more-holder">
                                                               <div class="tagging" onclick="explandTags(this)">
                                                                  Popular with:
                                                                  <span>Family</span>
                                                                  <span>Luxury</span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="expandable-area drawer-holder">
                                                         <div class="expanded-details">
                                                            Some details goes here...Some details goes here...Some details goes here...Some details goes here...Some details goes here...Some details goes here...Some details goes here...
                                                         </div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li>
                                                   <div class="hotel-li expandable-holder">
                                                      <div class="summery-info">
                                                         <div class="imgholder himg-box"><img src="images/restaurant3.png" class="himg" /></div>
                                                         <div class="descholder">
                                                            <h4>Al-Quds</h4>
                                                            <div class="clear"></div>
                                                            <div class="reviews-link">
                                                               <span class="checks-holder">
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star"></i>
                                                               <label>34 Reviews</label>
                                                               </span>
                                                            </div>
                                                            <span class="distance-info">Middle Eastem & African, Mediterranean</span>
                                                            <div class="more-holder">
                                                               <div class="tagging" onclick="explandTags(this)">
                                                                  Popular with:
                                                                  <span>Luxury</span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="expandable-area drawer-holder">
                                                         <div class="expanded-details">
                                                            Some details goes here...Some details goes here...Some details goes here...Some details goes here...Some details goes here...Some details goes here...Some details goes here...
                                                         </div>
                                                      </div>
                                                   </div>
                                                </li>
                                             </ul>
                                             <div class="pagination">
                                                <div class="link-holder">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-arrow-left-bold-circle"></i> Prev</a>
                                                </div>
                                                <div class="link-holder">
                                                   <a href="javascript:void(0)">Next <i class="mdi mdi-arrow-right-bold-circle"></i></a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="moreinfo-box">
                                       <a href="javascript:void(0)" onclick="closePlacesMoreInfo(this)" class="backarrow"><i class="mdi mdi-arrow-left-bold-circle"></i></a>
                                       <div class="infoholder nice-scroll">
                                          <div class="imgholder"><img src="images/hotel1.png" /></div>
                                          <div class="descholder wid_0">
                                             <h4>The Guest House</h4>
                                             <div class="clear"></div>
                                             <div class="reviews-link">
                                                <span class="checks-holder">
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star"></i>
                                                <label>34 Reviews</label>
                                                </span>
                                             </div>
                                             <span class="distance-info">Middle Eastem &amp; African, Mediterranean</span>
                                             <div class="clear"></div>
                                             <div class="more-holder">
                                                <ul class="infoul">
                                                   <li>
                                                      <i class="zmdi zmdi-pin"></i>
                                                      132 Brick Lane | E1 6RU, Qatar E1 6RU, Qatar
                                                   </li>
                                                   <li>
                                                      <i class="mdi mdi-phone"></i>
                                                      +44 20 7247 8210
                                                   </li>
                                                   <li>
                                                      <i class="mdi mdi-earth"></i>
                                                      http://www.yourwebsite.com
                                                   </li>
                                                   <li>
                                                      <i class="mdi mdi-clock-outline"></i>
                                                      Today, 12:00 PM - 12:00 AM
                                                   </li>
                                                   <li>
                                                      <i class="mdi mdi-certificate "></i>
                                                      Ranked #1 in Qatar Hotels
                                                   </li>
                                                </ul>
                                                <div class="tagging" onclick="explandTags(this)">
                                                   Popular with:
                                                   <span>Budget</span>
                                                   <span>Foodies</span>
                                                   <span>Family</span>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <?php include('common/gen_wall_col.php'); ?>
            </div>
         </div>
         </div>
         </div>
         </div>

         </div>

         <!-- compose tool box modal -->
         <div id="compose_discus" class="modal compose_tool_box post-popup custom_modal main_modal new-wall-post set_re_height compose_discus_popup">
          <div class="hidden_header">
              <div class="content_header">
                  <button class="close_span cancel_poup waves-effect">
                  <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
                  </button>
                  <p class="modal_header_xs">Write Post</p>
                  <a type="button" class="post_btn action_btn post_btn_xs close_modal waves-effect"  onclick="verify()">Post</a>
              </div>
          </div>
          <div class="modal-content">
              <div class="new-post active">
                  <div class="top-stuff">
                      <!--<div class="side-user">-->
                      <div class="postuser-info">
                          <span class="img-holder"><img class="circle" src="images/demo-profile.jpg" /></span>
                          <div class="desc-holder">
                              <span class="profile_name">Nimish Parekh</span>
                              <label id="tag_person" class="tag_person_new"></label>
                              <div class="public_dropdown_container">
                                  <a class="dropdown_text dropdown-button-left normalpostcreateprivacylabel" href="javascript:void(0)" onclick="privacymodal(this)" data-modeltag="normalpostcreateprivacylabel" data-fetch="no" data-label="normalpost">
                                  <span id="post_privacy2" class="post_privacy_label">Public</span>
                                  <i class="zmdi zmdi-caret-down"></i>
                                  </a>
                              </div>
                          </div>
                      </div>
                      <div class="settings-icon">
                          <a class="dropdown-button "  href="javascript:void(0)" data-activates="newpost_settings">
                          <i class="zmdi zmdi-more"></i>
                          </a>
                          <ul id="newpost_settings" class="dropdown-content custom_dropdown">
                              <li>
                                  <a href="javascript:void(0)">
                                  <input type="checkbox" id="toolbox_disable_sharing" />
                                  <label for="toolbox_disable_sharing">Disable Sharing</label>
                                  </a>
                              </li>
                              <li>
                                  <a href="javascript:void(0)">
                                  <input type="checkbox" id="toolbox_disable_comments" />
                                  <label for="toolbox_disable_comments">Disable Comments</label>
                                  </a>
                              </li>
                              <li>
                                  <a  onclick="clearPost()">Clear Post</a>
                              </li>
                          </ul>
                      </div>
                  </div>
                  <div class="clear"></div>
                  <div class="scroll_div">
                      <div class="npost-content">
                          <div class="post-mcontent">
                              <div class="npost-title title_post_container">                                    
                                  <input type="text" class="title" placeholder="Your tip title">                                 
                              </div>
                              <div class="clear"></div>
                              <div class="desc">
                                  <textarea id="new_post_comment" placeholder="What's new?" class="materialize-textarea comment_textarea new_post_comment"></textarea>
                              </div>
                              <div class="post-photos">
                                  <div class="img-row">
                                  </div>
                              </div>
                              <div class="post-tag">
                                  <div class="areatitle">With</div>
                                  <div class="areadesc">
                                      <input type="text" class="ptag" placeholder="Who are you with?"/>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="modal-footer">
              <div class="new-post active">
                  <div class="post-bcontent">
                      <div class="footer_icon_container">
                          <button class="comment_footer_icon waves-effect" id="compose_uploadphotomodalAction">
                          <i class="zmdi zmdi-camera"></i>
                          </button>
                          <button class="comment_footer_icon waves-effect" id="compose_addpersonAction">
                          <i class="zmdi zmdi-account"></i>
                          </button>
                          <button class="comment_footer_icon compose_titleAction waves-effect" id="compose_titleAction">
                          <img src="images/addtitleBl.png">
                          </button>
                      </div>
                      <div class="public_dropdown_container_xs">
                          <a class="dropdown_text dropdown-button-left normalpostcreateprivacylabel" href="javascript:void(0)" onclick="privacymodal(this)" data-modeltag="normalpostcreateprivacylabel" data-fetch="no" data-label="normalpost">
                          <span id="post_privacy2" class="post_privacy_label">Public</span>
                          <i class="zmdi zmdi-caret-down"></i>
                          </a>
                      </div>
                      <div class="post-bholder">
                          <div class="post-loader"><img src="images/home-loader.gif"/></div>
                          <div class="hidden_xs">
                              <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">cancel</a>
                              <a href="javascript:void(0)" class="btngen-center-align waves-effect btn-flat disabled submit">Post</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
         </div>
         <!-- Discuss-->
  
         <div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1 map_modalUniq">
         <?php include('common/map_modal.php'); ?>
         </div>
         <?php include('common/addperson_popup.php'); ?>
         <?php include('common/add_photo_popup.php'); ?>
         <!-- Add Photo-->
         <!-- compose Comment modal -->
         <div id="comment_modal_xs" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose compose_Comment_Action">
         <div class="modal_content_container">
         <div class="modal_content_child modal-content">
         <div class="popup-title ">
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>           
            <h3>All Comments</h3>
            <a type="button" class="item_done crop_done hidden_close_span waves-effect custom_close" href="javascript:void(0)">Done</a>
            <a type="button" class="item_done crop_done comment-close custom_close waves-effect" href="javascript:void(0)"><i class="mdi mdi-close"></i></a>
         </div>
         <div class="custom_modal_content modal_content" id="createpopup">
            <div class="comment-box-tab profile-tab">
               <div class="comment-poup-box detail-box">
                  <div class="content-holder main-holder">
                     <div class="summery">
                        <div class="dsection bborder expandable-holder expanded">
                           <div class="form-area expandable-area post-holder">
                              <div class="post-more">
                                 <a href="javascript:void(0)" class="view-morec">View more comments</a>
                                 <span class="total-comments">3 of 7</span>
                              </div>
                              <div class="post-comments">
                                 <div class="pcomments">
                                    <div class="pcomment-earlier">
                                       <div class="pcomment-holder">
                                          <div class="pcomment main-comment">
                                             <div class="img-holder">
                                                <div class="profiletipholder">
                                                   <span class="profile-tooltip">
                                                   <img class="circle" src="images/demo-profile.jpg"/>
                                                   </span>
                                                   <span class="profiletooltip_content slidingpan-holder">
                                                      <div class="profile-tip dis-none">
                                                         <div class="profile-tip-avatar">
                                                            <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                 
                                                            <div class="sliding-pan location-span">
                                                               <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                            </div>
                                                         </div>
                                                         <div class="profile-tip-name">
                                                            <a href="javascript:void(0)">Adel Hasanat</a>
                                                         </div>
                                                         <div class="profile-tip-info">
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                            </div>
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                            </div>
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </span>
                                                </div>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="normal-mode">
                                                   <div class="desc">
                                                      <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                   </div>
                                                   <div class="comment-stuff">
                                                      <div class="more-opt">
                                                         <span class="likeholder">
                                                         <span class="like-tooltip">
                                                         <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                         <i class="zmdi zmdi-thumb-up"></i>
                                                         </a>
                                                         </span>
                                                         </span>
                                                         <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                         <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit32">                               <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                         </a>
                                                         <ul id="comment_ecit32" class="dropdown-content custom_dropdown">
                                                            <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                            <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                         </ul>
                                                      </div>
                                                      <div class="less-opt">
                                                         <div class="timestamp">8h</div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="edit-mode">
                                                   <div class="desc">
                                                      <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-1">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                      <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="clear"></div>
                                          <div class="comment-reply-holder comment-addreply">
                                             <div class="addnew-comment valign-wrapper comment-reply">
                                                <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                <div class="desc-holder">                                   
                                                   <textarea class="materialize-textarea mb0 md_textarea descinput">Write a reply...</textarea>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="pcomment-holder">
                                          <div class="pcomment main-comment">
                                             <div class="img-holder">
                                                <div class="profiletipholder">
                                                   <span class="profile-tooltip">
                                                   <img class="circle" src="images/demo-profile.jpg"/>
                                                   </span>
                                                   <span class="profiletooltip_content slidingpan-holder">
                                                      <div class="profile-tip dis-none">
                                                         <div class="profile-tip-avatar">
                                                            <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                 
                                                            <div class="sliding-pan location-span">
                                                               <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                            </div>
                                                         </div>
                                                         <div class="profile-tip-name">
                                                            <a href="javascript:void(0)">Adel Hasanat</a>
                                                         </div>
                                                         <div class="profile-tip-info">
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                            </div>
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                            </div>
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </span>
                                                </div>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="normal-mode">
                                                   <div class="desc">
                                                      <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                   </div>
                                                   <div class="comment-stuff">
                                                      <div class="more-opt">
                                                         <span class="likeholder">
                                                         <span class="like-tooltip">
                                                         <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                         <i class="zmdi zmdi-thumb-up"></i>
                                                         </a>
                                                         </span>
                                                         </span>    
                                                         <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                         <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit33">                               <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                         </a>
                                                         <ul id="comment_ecit33" class="dropdown-content custom_dropdown">
                                                            <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                            <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                         </ul>
                                                      </div>
                                                      <div class="less-opt">
                                                         <div class="timestamp">8h</div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="edit-mode">
                                                   <div class="desc">
                                                      <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-2">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                      <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="clear"></div>
                                          <div class="comment-reply-holder comment-addreply">
                                             <div class="addnew-comment valign-wrapper comment-reply">
                                                <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                <div class="desc-holder">                                   
                                                   <textarea class="materialize-textarea mb0 md_textarea descinput">Write a reply...</textarea>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="pcomment-holder has-comments">
                                       <div class="pcomment main-comment">
                                          <div class="img-holder">
                                             <div class="profiletipholder">
                                                <span class="profile-tooltip">
                                                <img class="circle" src="images/demo-profile.jpg"/>
                                                </span>
                                                <span class="profiletooltip_content slidingpan-holder">
                                                   <div class="profile-tip dis-none">
                                                      <div class="profile-tip-avatar">
                                                         <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                    
                                                         <div class="sliding-pan location-span">
                                                            <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                         </div>
                                                      </div>
                                                      <div class="profile-tip-name">
                                                         <a href="javascript:void(0)">Adel Hasanat</a>
                                                      </div>
                                                      <div class="profile-tip-info">
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                         </div>
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                         </div>
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </span>
                                             </div>
                                          </div>
                                          <div class="desc-holder">
                                             <div class="normal-mode">
                                                <div class="desc">
                                                   <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                   <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                </div>
                                                <div class="comment-stuff">
                                                   <div class="more-opt">
                                                      <span class="likeholder">
                                                      <span class="like-tooltip">
                                                      <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                      <i class="zmdi zmdi-thumb-up"></i>
                                                      </a>
                                                      </span>
                                                      </span>
                                                      <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                      <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit34" >                              <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                      </a>
                                                      <ul id="comment_ecit34" class="dropdown-content custom_dropdown">
                                                         <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                         <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                      </ul>
                                                   </div>
                                                   <div class="less-opt">
                                                      <div class="timestamp">8h</div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="edit-mode">
                                                <div class="desc">
                                                   <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                   <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="comment-reply-holder">
                                          <div class="comments-reply-summery">
                                             <a href="javascript:void(0)" onclick="openReplies(this)">
                                             <i class="mdi mdi-share"></i>
                                             2 Replies                                                  
                                             </a>
                                             <i class="mdi mdi-bullseye dot-i"></i>
                                             Just Now
                                          </div>
                                          <div class="comments-reply-details">
                                             <div class="pcomment comment-reply">
                                                <div class="img-holder">
                                                   <div class="profiletipholder">
                                                      <span class="profile-tooltip">
                                                      <img class="circle" src="images/demo-profile.jpg"/>
                                                      </span>
                                                      <span class="profiletooltip_content slidingpan-holder">
                                                         <div class="profile-tip dis-none">
                                                            <div class="profile-tip-avatar">
                                                               <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                  
                                                               <div class="sliding-pan location-span">
                                                                  <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                               </div>
                                                            </div>
                                                            <div class="profile-tip-name">
                                                               <a href="javascript:void(0)">Adel Hasanat</a>
                                                            </div>
                                                            <div class="profile-tip-info">
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </span>
                                                   </div>
                                                </div>
                                                <div class="desc-holder">
                                                   <div class="normal-mode">
                                                      <div class="desc">
                                                         <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                         <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                      </div>
                                                      <div class="comment-stuff">
                                                         <div class="more-opt">
                                                            <span class="likeholder">
                                                            <span class="like-tooltip">
                                                            <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                            <i class="zmdi zmdi-thumb-up"></i>
                                                            </a>
                                                            </span>
                                                            </span>
                                                            <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                            <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit35">                                <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                            </a>
                                                            <ul id="comment_ecit35" class="dropdown-content custom_dropdown">
                                                               <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                               <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                            </ul>
                                                         </div>
                                                         <div class="less-opt">
                                                            <div class="timestamp">8h</div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="edit-mode">
                                                      <div class="desc">
                                                         <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-4">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                         <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pcomment comment-reply">
                                                <div class="img-holder">
                                                   <div class="profiletipholder">
                                                      <span class="profile-tooltip">
                                                      <img class="circle" src="images/demo-profile.jpg"/>
                                                      </span>
                                                      <span class="profiletooltip_content slidingpan-holder">
                                                         <div class="profile-tip dis-none">
                                                            <div class="profile-tip-avatar">
                                                               <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                  
                                                               <div class="sliding-pan location-span">
                                                                  <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                               </div>
                                                            </div>
                                                            <div class="profile-tip-name">
                                                               <a href="javascript:void(0)">Adel Hasanat</a>
                                                            </div>
                                                            <div class="profile-tip-info">
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </span>
                                                   </div>
                                                </div>
                                                <div class="desc-holder">
                                                   <div class="normal-mode">
                                                      <div class="desc">
                                                         <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                         <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                                      </div>
                                                      <div class="comment-stuff">
                                                         <div class="more-opt">
                                                            <span class="likeholder">
                                                            <span class="like-tooltip">
                                                            <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                            <i class="zmdi zmdi-thumb-up"></i>
                                                            </a>
                                                            </span>
                                                            </span> 
                                                            <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                            <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit37">                                <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                            </a>
                                                            <ul id="comment_ecit37" class="dropdown-content custom_dropdown">
                                                               <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                               <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                            </ul>
                                                         </div>
                                                         <div class="less-opt">
                                                            <div class="timestamp">8h</div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="edit-mode">
                                                      <div class="desc">
                                                         <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-5">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                         <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="comment-reply-holder comment-addreply">
                                          <div class="addnew-comment valign-wrapper comment-reply">
                                             <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                             <div class="desc-holder">                                  
                                                <textarea class="materialize-textarea mb0 md_textarea descinput">Write a reply...</textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="pcomment-holder">
                                       <div class="pcomment main-comment">
                                          <div class="img-holder">
                                             <div class="profiletipholder">
                                                <span class="profile-tooltip">
                                                <img class="circle" src="images/demo-profile.jpg"/>
                                                </span>
                                                <span class="profiletooltip_content slidingpan-holder">
                                                   <div class="profile-tip dis-none">
                                                      <div class="profile-tip-avatar">
                                                         <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                    
                                                         <div class="sliding-pan location-span">
                                                            <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                         </div>
                                                      </div>
                                                      <div class="profile-tip-name">
                                                         <a href="javascript:void(0)">Adel Hasanat</a>
                                                      </div>
                                                      <div class="profile-tip-info">
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                         </div>
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                         </div>
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </span>
                                             </div>
                                          </div>
                                          <div class="desc-holder">
                                             <div class="normal-mode">
                                                <div class="desc">
                                                   <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                   <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)"><span class="readlink">Read More</span></a></p>
                                                </div>
                                                <div class="comment-stuff">
                                                   <div class="more-opt">
                                                      <span class="likeholder">
                                                      <span class="like-tooltip">
                                                      <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                      <i class="zmdi zmdi-thumb-up"></i>
                                                      </a>
                                                      </span>
                                                      </span>   
                                                      <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                      <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit36" >                              <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                      </a>
                                                      <ul id="comment_ecit36" class="dropdown-content custom_dropdown">
                                                         <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                         <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                      </ul>
                                                   </div>
                                                   <div class="less-opt">
                                                      <div class="timestamp">8h</div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="edit-mode">
                                                <div class="desc">
                                                   <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-6">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                   <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="comment-reply-holder comment-addreply">
                                          <div class="addnew-comment valign-wrapper comment-reply">
                                             <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                             <div class="desc-holder">                                  
                                                <textarea class="materialize-textarea mb0 md_textarea descinput">Write a reply...</textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         </div>
</div>
      <div class="modal-footer">
         <div class="new-post active">
            <div class="post-bcontent">
               <div class="addnew-comment valign-wrapper">
                  <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                  <div class="desc-holder">                                    
                     <textarea class="materialize-textarea mb0 md_textarea descinput" id="comment_txt_1">Write a reply...</textarea>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="additem_modal_footer modal-footer">
         <div class="">
            <div class="btn-holder">
               <div class="addnew-comment valign-wrapper">
                  <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                  <div class="desc-holder">                                    
                     <textarea class="materialize-textarea mb0 md_textarea descinput" id="comment_txt_1">Write a reply...</textarea>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="new-post-mobile clear">
         <a class="popup-window composetoolboxAction" href="javascript:void(0)"><i class="mdi mdi-pencil"></i></a>
      </div>
   </div>
</div>
   
</div>   

<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/wall.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$w = $(window).width();
if ( $w > 739) {      
$(".places-tabs .sub-tabs li a").click(function(){
   $("body").removeClass("remove_scroller");
}); 
$(".tabs.icon-menu.tabsnew li a").click(function(){
   $("body").removeClass("remove_scroller");
}); 
$(".mbl-tabnav").click(function(){
   $("body").removeClass("remove_scroller");
}); 
$(".clicable.viewall-link").click(function(){
   $("body").removeClass("remove_scroller");
}); 
} else {
$(".places-tabs .sub-tabs li a").click(function(){
   $("body").addClass("remove_scroller");
}); 
$(".clicable.viewall-link").click(function(){
   $("body").addClass("remove_scroller");
});         
$(".tabs.icon-menu.tabsnew li a").click(function(){
   $("body").addClass("remove_scroller");
}); 
$(".mbl-tabnav").click(function(){
   $("body").removeClass("remove_scroller");
});
}

$(".header-icon-tabs .tabsnew .tab a").click(function(){
$(".bottom_tabs").hide();
});

$(".places-tabs .tab a").click(function(){
$(".top_tabs").hide();
});

// footer work for places home page only
$('.footer-section').css('left', '0');
$w = $(window).width();
if($w <= 768) {
$('.main-footer').css({
   'width': '100%',
   'left': '0'
});
} else {
var $_I = $('.places-content.places-all').width();
var $__I = $('.places-content.places-all').find('.container').width();

var $half = parseInt($_I) - parseInt($__I);
$half = parseInt($half) / 2;

$('.main-footer').css({
   'width': $_I+'px',
   'left': '-'+$half+'px'
});
}
});

$(window).resize(function() {
// footer work for places home page only
if($('#places-all').hasClass('active')) {
$('.footer-section').css('left', '0');
$w = $(window).width();
if($w <= 768) {
   $('.main-footer').css({
      'width': '100%',
      'left': '0'
   });
} else {
   var $_I = $('.places-content.places-all').width();
   var $__I = $('.places-content.places-all').find('.container').width();

   var $half = parseInt($_I) - parseInt($__I);
   $half = parseInt($half) / 2;

   $('.main-footer').css({
      'width': $_I+'px',
      'left': '-'+$half+'px'
   });
}
}
});

$(document).on('click', '.tablist .tab a', function(e) {
$href = $(this).attr('href');
$href = $href.replace('#', '');

$('.places-content').removeClass().addClass('places-content '+$href);
   $this = $(this);
});

   // js to initialize justified gallery
   setTimeout(
     function() 
     {
         $('.home-justified-gallery').justifiedGallery({
            lastRow: 'nojustify',
            rowHeight: 220,
            maxRowHeight: 220,
            margins: 10,
            sizeRangeSuffixes: {
                 lt100: '_t',
                 lt240: '_m',
                 lt320: '_n',
                 lt500: '',
                 lt640: '_z',
                 lt1024: '_b'
            }
         });
     }, 8000);

</script>

<?php include('common/discard_popup.php'); ?>
<?php include('common/upload_gallery_popup.php'); ?>
<?php include('common/privacymodal.php'); ?>
<?php include('common/custom_modal.php'); ?>
<?php include('common/addcategories_popup.php'); ?>

<?php include("script.php"); ?>