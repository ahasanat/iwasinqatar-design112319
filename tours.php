<?php include("header.php"); ?>
<?php include("common/menu.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div> 
</div>
<div class="clear"></div>
<?php include("common/leftmenu.php"); ?>
<div class="fixed-layout">
   <div class="main-content with-lmenu transheader-page tours-page main-page places-page pb-0">
      <div class="combined-column wide-open">
         <div class="content-box">
            <div class="banner-section">
               <h4>Tours, sighting, activities and thing to do</h4>
            </div>
            <div class="places-content places-all">
               <div class="container cshfsiput">
                  <div class="places-column cshfsiput m-top">
                     <div id="places-lodge" class="placeslodge-content subtab places-discussion-main top_tabs">
                        <div class="content-box">
                           <div class="mbl-tabnav">
                              <a href="javascript:void(0)" onclick="openDirectTab('places-all')"><i class="mdi mdi-arrow-left"></i></a> 
                              <h6>Hotels</h6>
                           </div>
                           <div class="placesection redsection">
                              <div class="cbox-desc hotels-page np">
                                 <div class="tcontent-holder moreinfo-outer">
                                    <div class="top-stuff top-graybg" id="all-hotels">
                                       <h6>Qatar</h6>
                                    </div>
                                    <div class="places-content-holder">
                                       <div class="map-holder">
                                          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3110.3465133386144!2d-9.167423685010494!3d38.77868997958898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd193295d5b45545%3A0x3f9e7b6a5f00e12c!2sPerta!5e0!3m2!1sen!2sin!4v1481089901870" width="600" height="450" frameborder="0" allowfullscreen></iframe>
                                          <a href="javascript:void(0)" class="overlay" onclick="expandMap(this,'#all-hotels')"></a>
                                          <a href="javascript:void(0)" class="closelink" onclick="shrinkMap(this)"><i class="mdi mdi-close"></i> Close</a>
                                       </div>
                                       <div class="list-holder">
                                          <div class="hotel-list">
                                             <ul>
                                                <li>
                                                   <div class="hotel-li expandable-holder dealli mobilelist">
                                                      <div class="summery-info">
                                                         <div class="imgholder"><img src="images/hotel-demo.png" /></div>
                                                         <div class="descholder">
                                                            <a href="javascript:void(0)" class="expand-link" onclick="mng_expandable(this,'hasClose')">
                                                               <h4>Hyatt Regency Dubai Creek
                                                               </h4>
                                                               <div class="clear"></div>
                                                               <div class="reviews-link">
                                                                  <span class="review-count">54 reviews</span>
                                                                  <span class="checks-holder">
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <label>Excellent - 88/100</label>
                                                                  </span>
                                                               </div>
                                                               <span class="address">Dubai, Dubai(Emirates), United Arab Emirates</span>
                                                               <span class="distance-info">2.2 miles to City center</span>
                                                               <div class="more-holder">
                                                                  <div class="tagging" onclick="explandTags(this)">
                                                                     Popular with:
                                                                     <span>point of interest</span>
                                                                     <span>establishment</span>
                                                                  </div>
                                                               </div>
                                                            </a>
                                                            <div class="info-action">
                                                               <span class="stars-holder">
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               </span>
                                                               <div class="clear"></div>
                                                               <span class="sitename">booking.com</span>
                                                               <div class="clear"></div>
                                                               <span class="price">JOD 184*</span>
                                                               <div class="clear"></div>
                                                               <a href="javascript:void(0)" class="deal-btn waves-effect waves-light">Book Now <i class="mdi mdi-chevron-right"></i></a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="expandable-area">
                                                         <a href="javascript:void(0)" class="shrink-link" onclick="mng_expandable(this,'closeIt')"><i class="mdi mdi-close"></i> Close</a>
                                                         <div class="clear"></div>
                                                         <div class="explandable-tabs">
                                                            <ul class="tabs tabsnew subtab-menu">
                                                               <li class="tab"><a class="" href="#subtab-details">Details</a></li>
                                                               <li class="tab"><a href="#subtab-review">Reviews</a></li>
                                                               <li class="tab"><a data-which="photo" href="#subtab-photos" data-tab="subtab-photos">Photos</a></li>
                                                               <!--<li class="tab"><a data-toggle="tab" href="#subtab-amenities">Amenities</a></li>-->
                                                            </ul>
                                                            <div class="tab-content">
                                                               <div id="subtab-details" class="">
                                                                  <div class="subdetail-box">
                                                                     <div class="infoholder">
                                                                        <div class="descholder">
                                                                           <div class="more-holder">
                                                                              <ul class="infoul">
                                                                                 <li>
                                                                                    <i class="zmdi zmdi-pin"></i>
                                                                                    132 Brick Lane | E1 6RU, Qatar E1 6RU, Qatar
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-phone"></i>
                                                                                    +44 20 7247 8210
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-earth"></i>
                                                                                    http://www.yourwebsite.com
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-clock-outline"></i>
                                                                                    Mon-Fri : 12:00 PM - 10:00 AM
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-certificate "></i>
                                                                                    Ranked #1 in Qatar Hotels
                                                                                 </li>
                                                                              </ul>
                                                                              <div class="tagging" onclick="explandTags(this)">
                                                                                 Popular with:
                                                                                 <span>point of interest</span>
                                                                                 <span>establishment</span>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div id="subtab-review" class="">
                                                                  <div class="reviews-summery">
                                                                     <div class="reviews-people">
                                                                        <ul>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-3.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>Kelly Mark <span>about 2 weeks ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>We enjoyed the lounge and bar at the Ritz where you are offered many choices for drinks and some pretty elaborate looking dishes of food as well.</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-2.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>John Davior <span>about 8 months ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>If you want a fancy London experience than The Ritz is where you need to go! At least budget for High Tea!</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-1.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>Joe Doe <span>about 11 months ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>I am not at all sure this is the best hotel in London, but it does deserve the reputation as one of the most glamourous.</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div id="subtab-photos" class=" subtab-photos">
                                                                  <div class="photo-gallery">
                                                                     <div class="img-preview">
                                                                        <img src="images/post-img1.jpg" />
                                                                     </div>
                                                                     <div class="thumbs-img">
                                                                        <ul>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                        </ul>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li>
                                                   <div class="hotel-li expandable-holder dealli">
                                                      <div class="summery-info">
                                                         <div class="imgholder"><img src="images/hotel-demo.png" /></div>
                                                         <div class="descholder">
                                                            <a href="javascript:void(0)" class="expand-link" onclick="mng_expandable(this,'hasClose')">
                                                               <h4>Hyatt Regency Dubai Creek
                                                               </h4>
                                                               <div class="clear"></div>
                                                               <div class="reviews-link">
                                                                  <span class="review-count">54 reviews</span>
                                                                  <span class="checks-holder">
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <label>Excellent - 88/100</label>
                                                                  </span>
                                                               </div>
                                                               <span class="address">Dubai, Dubai(Emirates), United Arab Emirates</span>
                                                               <span class="distance-info">2.2 miles to City center</span>
                                                               <div class="more-holder">
                                                                  <div class="tagging" onclick="explandTags(this)">
                                                                     Popular with:
                                                                     <span>point of interest</span>
                                                                     <span>establishment</span>
                                                                  </div>
                                                               </div>
                                                            </a>
                                                            <div class="info-action">
                                                               <span class="stars-holder">
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               </span>
                                                               <div class="clear"></div>
                                                               <span class="sitename">booking.com</span>
                                                               <div class="clear"></div>
                                                               <span class="price">JOD 184*</span>
                                                               <div class="clear"></div>
                                                               <a href="javascript:void(0)" class="deal-btn">Book Now <i class="mdi mdi-chevron-right"></i></a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="expandable-area">
                                                         <a href="javascript:void(0)" class="shrink-link" onclick="mng_expandable(this,'closeIt')"><i class="mdi mdi-close"></i> Close</a>
                                                         <div class="clear"></div>
                                                         <div class="explandable-tabs">
                                                            <ul class="tabs tabsnew subtab-menu">
                                                               <li class="active tab"><a href="#subtab-details">Details</a></li>
                                                               <li class="tab"><a href="#subtab-reviews">Reviews</a></li>
                                                               <li><a aria-expanded="false" data-toggle="tab" data-which="photo" href="#subtab-photos" data-tab="subtab-photos">Photos</a></li>
                                                               <li><a aria-expanded="false" data-toggle="tab" href="#subtab-amenities">Amenities</a></li>
                                                            </ul>
                                                            <div class="tab-content">
                                                               <div id="subtab-details" class="tab-pane fade active in">
                                                                  <div class="subdetail-box">
                                                                     <div class="infoholder">
                                                                        <div class="descholder">
                                                                           <div class="more-holder">
                                                                              <ul class="infoul">
                                                                                 <li>
                                                                                    <i class="zmdi zmdi-pin"></i>
                                                                                    132 Brick Lane | E1 6RU, Qatar E1 6RU, Qatar
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-phone"></i>
                                                                                    +44 20 7247 8210
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-earth"></i>
                                                                                    http://www.yourwebsite.com
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-clock-outline"></i>
                                                                                    Mon-Fri : 12:00 PM - 10:00 AM
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-certificate "></i>
                                                                                    Ranked #1 in Qatar Hotels
                                                                                 </li>
                                                                              </ul>
                                                                              <div class="tagging" onclick="explandTags(this)">
                                                                                 Popular with:
                                                                                 <span>point of interest</span>
                                                                                 <span>establishment</span>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div id="subtab-reviews" class="tab-pane fade">
                                                                  <div class="reviews-summery">
                                                                     <div class="reviews-people">
                                                                        <ul>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-3.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>Kelly Mark <span>about 2 weeks ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>We enjoyed the lounge and bar at the Ritz where you are offered many choices for drinks and some pretty elaborate looking dishes of food as well.</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-2.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>John Davior <span>about 8 months ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>If you want a fancy London experience than The Ritz is where you need to go! At least budget for High Tea!</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-1.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>Joe Doe <span>about 11 months ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>I am not at all sure this is the best hotel in London, but it does deserve the reputation as one of the most glamourous.</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div id="subtab-photos" class="tab-pane fade subtab-photos">
                                                                  <div class="photo-gallery">
                                                                     <div class="img-preview">
                                                                        <img src="images/post-img1.jpg" />
                                                                     </div>
                                                                     <div class="thumbs-img">
                                                                        <ul>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                        </ul>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div id="subtab-amenities" class="tab-pane fade">
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li>
                                                   <div class="hotel-li expandable-holder dealli">
                                                      <div class="summery-info">
                                                         <div class="imgholder"><img src="images/hotel-demo.png" /></div>
                                                         <div class="descholder">
                                                            <a href="javascript:void(0)" class="expand-link" onclick="mng_expandable(this,'hasClose')">
                                                               <h4>Hyatt Regency Dubai Creek
                                                               </h4>
                                                               <div class="clear"></div>
                                                               <div class="reviews-link">
                                                                  <span class="review-count">54 reviews</span>
                                                                  <span class="checks-holder">
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <label>Excellent - 88/100</label>
                                                                  </span>
                                                               </div>
                                                               <span class="address">Dubai, Dubai(Emirates), United Arab Emirates</span>
                                                               <span class="distance-info">2.2 miles to City center</span>
                                                               <div class="more-holder">
                                                                  <div class="tagging" onclick="explandTags(this)">
                                                                     Popular with:
                                                                     <span>point of interest</span>
                                                                     <span>establishment</span>
                                                                  </div>
                                                               </div>
                                                            </a>
                                                            <div class="info-action">
                                                               <span class="stars-holder">
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               </span>
                                                               <div class="clear"></div>
                                                               <span class="sitename">booking.com</span>
                                                               <div class="clear"></div>
                                                               <span class="price">JOD 184*</span>
                                                               <div class="clear"></div>
                                                               <a href="javascript:void(0)" class="deal-btn">Book Now <i class="mdi mdi-chevron-right"></i></a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="expandable-area">
                                                         <a href="javascript:void(0)" class="shrink-link" onclick="mng_expandable(this,'closeIt')"><i class="mdi mdi-close"></i> Close</a>
                                                         <div class="clear"></div>
                                                         <div class="explandable-tabs">
                                                            <ul class="tabs tabsnew subtab-menu">
                                                               <li class="active tab"><a href="#subtab-details">Details</a></li>
                                                               <li class="tab"><a href="#subtab-reviews">Reviews</a></li>
                                                               <li><a aria-expanded="false" data-toggle="tab" data-which="photo" href="#subtab-photos" data-tab="subtab-photos">Photos</a></li>
                                                               <li><a aria-expanded="false" data-toggle="tab" href="#subtab-amenities">Amenities</a></li>
                                                            </ul>
                                                            <div class="tab-content">
                                                               <div id="subtab-details" class="tab-pane fade active in">
                                                                  <div class="subdetail-box">
                                                                     <div class="infoholder">
                                                                        <div class="descholder">
                                                                           <div class="more-holder">
                                                                              <ul class="infoul">
                                                                                 <li>
                                                                                    <i class="zmdi zmdi-pin"></i>
                                                                                    132 Brick Lane | E1 6RU, Qatar E1 6RU, Qatar
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-phone"></i>
                                                                                    +44 20 7247 8210
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-earth"></i>
                                                                                    http://www.yourwebsite.com
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-clock-outline"></i>
                                                                                    Mon-Fri : 12:00 PM - 10:00 AM
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-certificate "></i>
                                                                                    Ranked #1 in Qatar Hotels
                                                                                 </li>
                                                                              </ul>
                                                                              <div class="tagging" onclick="explandTags(this)">
                                                                                 Popular with:
                                                                                 <span>point of interest</span>
                                                                                 <span>establishment</span>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div id="subtab-reviews" class="tab-pane fade">
                                                                  <div class="reviews-summery">
                                                                     <div class="reviews-people">
                                                                        <ul>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-3.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>Kelly Mark <span>about 2 weeks ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>We enjoyed the lounge and bar at the Ritz where you are offered many choices for drinks and some pretty elaborate looking dishes of food as well.</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-2.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>John Davior <span>about 8 months ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>If you want a fancy London experience than The Ritz is where you need to go! At least budget for High Tea!</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-1.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>Joe Doe <span>about 11 months ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>I am not at all sure this is the best hotel in London, but it does deserve the reputation as one of the most glamourous.</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div id="subtab-photos" class="tab-pane fade subtab-photos">
                                                                  <div class="photo-gallery">
                                                                     <div class="img-preview">
                                                                        <img src="images/post-img1.jpg" />
                                                                     </div>
                                                                     <div class="thumbs-img">
                                                                        <ul>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                        </ul>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div id="subtab-amenities" class="tab-pane fade">
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="moreinfo-box">
                                       <a href="javascript:void(0)" onclick="closePlacesMoreInfo(this)" class="backarrow"><i class="mdi mdi-arrow-left-bold-circle"></i></a>
                                       <div class="infoholder nice-scroll">
                                          <div class="imgholder"><img src="images/hotel1.png" /></div>
                                          <div class="descholder">
                                             <h4>The Guest House</h4>
                                             <div class="clear"></div>
                                             <div class="reviews-link">
                                                <span class="checks-holder">
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star"></i>
                                                <label>34 Reviews</label>
                                                </span>
                                             </div>
                                             <span class="distance-info">Middle Eastem &amp; African, Mediterranean</span>
                                             <div class="clear"></div>
                                             <div class="more-holder">
                                                <ul class="infoul">
                                                   <li>
                                                      <i class="zmdi zmdi-pin"></i>
                                                      132 Brick Lane | E1 6RU, Qatar E1 6RU, Qatar
                                                   </li>
                                                   <li>
                                                      <i class="mdi mdi-phone"></i>
                                                      +44 20 7247 8210
                                                   </li>
                                                   <li>
                                                      <i class="mdi mdi-earth"></i>
                                                      http://www.yourwebsite.com
                                                   </li>
                                                   <li>
                                                      <i class="mdi mdi-clock-outline"></i>
                                                      Today, 12:00 PM - 12:00 AM
                                                   </li>
                                                   <li>
                                                      <i class="mdi mdi-certificate "></i>
                                                      Ranked #1 in Qatar Hotels
                                                   </li>
                                                </ul>
                                                <div class="tagging" onclick="explandTags(this)">
                                                   Popular with:
                                                   <span>Budget</span>
                                                   <span>Foodies</span>
                                                   <span>Family</span>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php include('common/gen_wall_col.php'); ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>

<?php include('common/datepicker.php'); ?>

<?php include("script.php"); ?>
</body>
</html>
<script>
</script>