<?php include("header.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div>
</div>
<div class="clear"></div>
<?php include("common/leftmenu.php"); ?>
<div class="fixed-layout ipad-mfix">
   <div class="main-content trip-page p-0">
      <div class="trip-mapview">
         <div class="tripmenu">
            <ul>
               <li class="active"><a href="javascript:void(0)" onclick="showLayer(this,'alltrip')">Trips</a></li>
               <li><a href="javascript:void(0)">Print</a></li>
               <li><a href="javascript:void(0)">Email</a></li>
            </ul>
         </div>
         <div class="notice-holder">
            <div class="settings-notice">
               <span class="success-note"></span>
               <span class="error-note"></span>
               <span class="info-note"></span>
            </div>
         </div>
         <div class="desc-box">
            <div class="side-section">
               <div class="section-layer front" id="trip-list">
                  <div class="section-holder nice-scroll">
                     <div class="sectitle">
                        Your Trips
                     </div>
                     <div class="secdesc">
                        <ul class="triplist">
                           <li>
                              <div class="tripitem">
                                 <div class="itemicon"><i class="mdi mdi-airplane"></i></div>
                                 <h6><a href="javascript:void(0)" onclick="showLayer(this,'viewtrip')">Germany</a></h6>
                                 <span class="sub">223 km / 189 miles</span>
                                 <div class="settings-icon">
                                    <div dropdown-sm">
                                       <a href='#' class="dropdown-button more_btn" data-activates='dropdown-trips1'>			
                                       <i class="zmdi zmdi-hc-2x zmdi-more"></i>
                                       </a>
                                       <ul id='dropdown-trips1' class='dropdown-content custom_dropdown'>
                                          <li><a href="javascript:void(0)" onclick="showLayer(this,'edittrip')">Edit</a></li>
                                          <li><a href="javascript:void(0)">Delete</a></li>
                                          <li><a class="sharepostmodalAction " href="javascript:void(0)" class="">Share</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="tripitem">
                                 <div class="itemicon"><i class="mdi mdi-airplane"></i></div>
                                 <h6><a href="javascript:void(0)" onclick="showLayer(this,'viewtrip')">Singapore</a></h6>
                                 <span class="sub">223 km / 189 miles</span>
                                 <div class="settings-icon">
                                    <div class="dropdown dropdown-custom dropdown-sm">
                                       <a href='#' class="dropdown-button more_btn" data-activates='dropdown-trips2'>			
                                       <i class="zmdi zmdi-hc-2x zmdi-more"></i>
                                       </a>
                                       <ul id='dropdown-trips2' class='dropdown-content custom_dropdown'>
                                          <li><a href="javascript:void(0)" onclick="showLayer(this,'edittrip')">Edit</a></li>
                                          <li><a href="javascript:void(0)">Delete</a></li>
                                          <li><a class="sharepostmodalAction " href="javascript:void(0)" class="">Share</a></li>
                                       </ul>
                                    </div>
                                 </div>
                                 <p>
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat...
                                 </p>
                              </div>
                           </li>
                           <li>
                              <div class="tripitem">
                                 <div class="itemicon"><i class="mdi mdi-airplane"></i></div>
                                 <h6><a href="javascript:void(0)" onclick="showLayer(this,'viewtrip')">London</a></h6>
                                 <span class="sub">223 km / 189 miles</span>
                                 <div class="settings-icon">
                                    <div class="dropdown dropdown-custom dropdown-sm">
                                       <a href='#' class="dropdown-button more_btn" data-activates='dropdown-trips4'>			
                                       <i class="zmdi zmdi-hc-2x zmdi-more"></i>
                                       </a>
                                       <ul id='dropdown-trips4' class='dropdown-content custom_dropdown'>
                                          <li><a href="javascript:void(0)" onclick="showLayer(this,'edittrip')">Edit</a></li>
                                          <li><a href="javascript:void(0)">Delete</a></li>
                                          <li><a class="sharepostmodalAction " href="javascript:void(0)" class="">Share</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="tripitem">
                                 <div class="itemicon"><i class="mdi mdi-airplane"></i></div>
                                 <h6><a href="javascript:void(0)" onclick="showLayer(this,'viewtrip')">Paris</a></h6>
                                 <span class="sub">223 km / 189 miles</span>
                                 <div class="settings-icon">
                                    <div class="dropdown dropdown-custom dropdown-sm">
                                       <a href='#' class="dropdown-button more_btn" data-activates='dropdown-trips5'>			
                                       <i class="zmdi zmdi-hc-2x zmdi-more"></i>
                                       </a>
                                       <ul id='dropdown-trips5' class='dropdown-content custom_dropdown'>
                                          <li><a href="javascript:void(0)" onclick="showLayer(this,'edittrip')">Edit</a></li>
                                          <li><a href="javascript:void(0)">Delete</a></li>
                                          <li><a class="sharepostmodalAction " href="javascript:void(0)" class="">Share</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="tripitem">
                                 <div class="itemicon"><i class="mdi mdi-airplane"></i></div>
                                 <h6><a href="javascript:void(0)" onclick="showLayer(this,'viewtrip')">Germany</a></h6>
                                 <span class="sub">223 km / 189 miles</span>
                                 <div class="settings-icon">
                                    <div class="dropdown dropdown-custom dropdown-sm">
                                       <a href='#' class="dropdown-button more_btn" data-activates='dropdown-trips6'>			
                                       <i class="zmdi zmdi-hc-2x zmdi-more"></i>
                                       </a>
                                       <ul id='dropdown-trips6' class='dropdown-content custom_dropdown'>
                                          <li><a href="javascript:void(0)" onclick="showLayer(this,'edittrip')">Edit</a></li>
                                          <li><a href="javascript:void(0)">Delete</a></li>
                                          <li><a class="sharepostmodalAction " href="javascript:void(0)" class="">Share</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="tripitem">
                                 <div class="itemicon"><i class="mdi mdi-airplane"></i></div>
                                 <h6><a href="javascript:void(0)" onclick="showLayer(this,'viewtrip')">Singapore</a></h6>
                                 <span class="sub">223 km / 189 miles</span>
                                 <div class="settings-icon">
                                    <div class="dropdown dropdown-custom dropdown-sm">
                                       <a href='#' class="dropdown-button more_btn" data-activates='dropdown-trips7'>			
                                       <i class="zmdi zmdi-hc-2x zmdi-more"></i>
                                       </a>
                                       <ul id='dropdown-trips7' class='dropdown-content custom_dropdown'>
                                          <li><a href="javascript:void(0)" onclick="showLayer(this,'edittrip')">Edit</a></li>
                                          <li><a href="javascript:void(0)">Delete</a></li>
                                          <li><a class="sharepostmodalAction " href="javascript:void(0)" class="">Share</a></li>
                                       </ul>
                                    </div>
                                 </div>
                                 <p>
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat...
                                 </p>
                              </div>
                           </li>
                           <li>
                              <div class="tripitem">
                                 <div class="itemicon"><i class="mdi mdi-airplane"></i></div>
                                 <h6><a href="javascript:void(0)" onclick="showLayer(this,'viewtrip')">London</a></h6>
                                 <span class="sub">223 km / 189 miles</span>
                                 <div class="settings-icon">
                                    <div class="dropdown dropdown-custom dropdown-sm">
                                       <a href='#' class="dropdown-button more_btn" data-activates='dropdown-trips8'>			
                                       <i class="zmdi zmdi-hc-2x zmdi-more"></i>
                                       </a>
                                       <ul id='dropdown-trips8' class='dropdown-content custom_dropdown'>
                                          <li><a href="javascript:void(0)" onclick="showLayer(this,'edittrip')">Edit</a></li>
                                          <li><a href="javascript:void(0)">Delete</a></li>
                                          <li><a class="sharepostmodalAction " href="javascript:void(0)" class="">Share</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="tripitem">
                                 <div class="itemicon"><i class="mdi mdi-airplane"></i></div>
                                 <h6><a href="javascript:void(0)" onclick="showLayer(this,'viewtrip')">Paris</a></h6>
                                 <span class="sub">223 km / 189 miles</span>
                                 <div class="settings-icon">
                                    <div class="dropdown dropdown-custom dropdown-sm">
                                       <a href='#' class="dropdown-button more_btn" data-activates='dropdown-trips9'>			
                                       <i class="zmdi zmdi-hc-2x zmdi-more"></i>
                                       </a>
                                       <ul id='dropdown-trips9' class='dropdown-content custom_dropdown'>
                                          <li><a href="javascript:void(0)" onclick="showLayer(this,'edittrip')">Edit</a></li>
                                          <li><a href="javascript:void(0)">Delete</a></li>
                                          <li><a class="sharepostmodalAction " href="javascript:void(0)" class="">Share</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </li>
                        </ul>
                        <a href="javascript:void(0)" class="waves-effect waves-light btn modal-trigger btn-trip pull-trip right bottom-btn" onclick="showLayer(this,'newtrip')">Add New Trip</a>
                     </div>
                  </div>
               </div>
               <div class="section-layer" id="trip-view">
                  <div class="section-holder nice-scroll">
                     <div class="secopt">
                        <a href="javascript:void(0)" class="backarrow" onclick="showLayer(this,'alltrip')"><i class="mdi mdi-chevron-left"></i></a>
                        <a href="javascript:void(0)" class="left iconlink editlink" onclick="showLayer(this,'edittrip')"><i class="mdi mdi-pencil-box-outline"></i> Edit Trip</a>
                        <a href="javascript:void(0)" class="right iconlink deletelink" onclick="showLayer(this,'alltrip')"><i class="zmdi zmdi-delete"></i> Delete Trip</a>
                     </div>
                     <div class="secdesc">
                        <div class="trip-summery">
                           <div class="triptitle">
                              <div class="itemicon"><i class="mdi mdi-airplane"></i></div>
                              <h6>Germany</h6>
                              <span class="sub">Covering 223 km / 189 miles</span>
                           </div>
                           <div class="map-thumb">
                              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3110.3465133386144!2d-9.167423685010494!3d38.77868997958898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd193295d5b45545%3A0x3f9e7b6a5f00e12c!2sPerta!5e0!3m2!1sen!2sin!4v1481089901870" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                              <a href="javascript:void(0)" onclick="openDetailTripMap(this)">&nbsp;</a>
                           </div>
                           <div class="tripdetail">
                              <div class="starttrip">
                                 <span>Trip starts on -</span> 21-11-2016
                              </div>
                              <ul class="tripstops-list">
                                 <li>
                                    <div class="tripstop">
                                       <div class="title">
                                          <span class="numbering">1</span>
                                          <h5>Nuremberg</h5>
                                          <span class="date">11-22-2016</span>
                                       </div>
                                       <div class="morelinks">
                                          <a href="javascript:void(0)" class="left" onclick="openAccommodations(this,'nuremburg')">View accommodations on map</a>
                                          <a href="javascript:void(0)" onclick="showSubLayer(this,'bookmarks','editmode')" class="bookmark-link active"><i class="mdi mdi-bookmark-o"></i></a>
                                       </div>
                                    </div>
                                 </li>
                                 <li>
                                    <div class="tripstop">
                                       <div class="title">
                                          <span class="numbering">2</span>
                                          <h5>Frankfurt</h5>
                                          <span class="date">11-22-2016</span>
                                       </div>
                                       <div class="morelinks">
                                          <a href="javascript:void(0)" class="left" onclick="openAccommodations(this,'frankfurt')">View accommodations on map</a>
                                          <a href="javascript:void(0)" onclick="showSubLayer(this,'bookmarks','editmode')" class="bookmark-link"><i class="mdi mdi-bookmark-o"></i></a>
                                       </div>
                                    </div>
                                 </li>
                                 <li>
                                    <div class="tripstop">
                                       <div class="title">
                                          <span class="numbering">3</span>
                                          <h5>Hanover</h5>
                                          <span class="date">11-22-2016</span>
                                       </div>
                                       <div class="morelinks">
                                          <a href="javascript:void(0)" class="left" onclick="openAccommodations(this,'hanover')">View accommodations on map</a>
                                          <a href="javascript:void(0)" onclick="showSubLayer(this,'bookmarks','editmode')" class="bookmark-link"><i class="mdi mdi-bookmark-o"></i></a>
                                       </div>
                                    </div>
                                 </li>
                              </ul>
                              <div class="dividing-line"></div>
                              <div class="drow">
                                 <label>Your trip summery</label>
                                 <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper.</p>
                              </div>
                              <div class="drow half">
                                 <div class="cap-holder">
                                    <label>Track Path</label>
                                 </div>
                                 <div class="comp-holder">
                                    <p>Lines</p>
                                 </div>
                              </div>
                              <div class="drow half">
                                 <div class="cap-holder">
                                    <label>Path Color</label>
                                 </div>
                                 <div class="comp-holder">
                                    <div class="right">
                                       <ul class="color-palette">
                                          <li><a href="javascript:void(0)" class="colordot bluedot"></a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                              <div class="drow half">
                                 <div class="cap-holder">
                                    <label>Your trip notes
                                    <span class="notetext">
                                    <a href="javascript:void(0)" onclick="showSubLayer(this,'notes','viewmode')">2 notes added</a>
                                    </span>
                                    </label>
                                 </div>
                                 <div class="comp-holder">
                                    <a href="javascript:void(0)" class="right" onclick="showSubLayer(this,'notes','viewmode')">View Notes</a>
                                 </div>
                              </div>
                              <div class="drow half">
                                 <div class="cap-holder">
                                    <label>Who can view your trip details</label>
                                 </div>
                                 <div class="comp-holder">
                                    <div class="security-text">
                                       <i class="mdi mdi-lock"></i> Private
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="section-layer" id="trip-edit">
                  <div class="section-holder nice-scroll">
                     <div class="sectitle">
                        Edit Trip
                        <a href="javascript:void(0)" class="right deletelink redicon" onclick="showLayer(this,'alltrip')"><i class="mdi mdi-close"></i> Cancel</a>
                     </div>
                     <div class="secdesc">
                        <div class="trip-summery">
                           <div class="triptitle edit">
                              <div class="itemicon"><i class="mdi mdi-airplane"></i></div>
                              <div class="sliding-middle-out anim-area underlined fullwidth">
                                 <input type="text" placeholder="Name your trip" value="Germany">
                              </div>
                              <span class="sub">Covering 223 km / 189 miles</span>
                           </div>
                           <div class="map-thumb">
                              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3110.3465133386144!2d-9.167423685010494!3d38.77868997958898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd193295d5b45545%3A0x3f9e7b6a5f00e12c!2sPerta!5e0!3m2!1sen!2sin!4v1481089901870" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                              <a href="javascript:void(0)" onclick="openDetailTripMap(this)">&nbsp;</a>
                           </div>
                           <div class="tripdetail">
                              <div class="starttrip">
                                 <span>Trip starts on -</span> 21-11-2016
                              </div>
                              <ul class="tripstops-list">
                                 <li>
                                    <div class="tripstop hasdelete">
                                       <div class="title">
                                          <span class="numbering">1</span>
                                          <h5>Nuremberg</h5>
                                          <span class="date">
                                             <div class="sliding-middle-out anim-area underlined">
                                                <input type="text" onkeydown="return false;" placeholder="Add Date" value="11-22-2016" class="form-control datetime-picker datepickerinput" data-toggle="datepicker" data-query="M" readonly>
                                             </div>
                                          </span>
                                       </div>
                                       <div class="deletebtn">
                                          <a href="javascript:void(0)" class="right deletelink redicon"><i class="zmdi zmdi-delete"></i></a>
                                       </div>
                                    </div>
                                 </li>
                                 <li>
                                    <div class="tripstop hasdelete">
                                       <div class="title">
                                          <span class="numbering">2</span>
                                          <h5>Frankfurt</h5>
                                          <span class="date">
                                             <div class="sliding-middle-out anim-area underlined">
                                                <input type="text" onkeydown="return false;" placeholder="Add Date" value="11-22-2016" class="form-control datetime-picker datepickerinput" data-toggle="datepicker"  data-query="M" readonly>
                                             </div>
                                          </span>
                                       </div>
                                       <div class="deletebtn">
                                          <a href="javascript:void(0)" class="right deletelink redicon"><i class="zmdi zmdi-delete"></i></a>
                                       </div>
                                    </div>
                                 </li>
                                 <li>
                                    <div class="tripstop hasdelete">
                                       <div class="title">
                                          <span class="numbering">3</span>
                                          <h5>Hanover</h5>
                                          <span class="date">
                                             <div class="sliding-middle-out anim-area underlined">
                                                <input type="text" onkeydown="return false;" placeholder="Add Date" value="11-22-2016" class="form-control datetime-picker datepickerinput" data-toggle="datepicker" data-query="M" readonly>
                                             </div>
                                          </span>
                                       </div>
                                       <div class="deletebtn">
                                          <a href="javascript:void(0)" class="right deletelink redicon"><i class="zmdi zmdi-delete"></i></a>
                                       </div>
                                    </div>
                                 </li>
                              </ul>
                              <div class="drow">
                                 <div class="add-newstop">
                                    <div class="newstop opened">
                                       <div class="sliding-middle-out anim-area underlined fullwidth locinput">
                                          <input type="text" placeholder="Where you want to go next" data-query="M" onfocus="filderMapLocationModal(this)" autocomplete="off">
                                       </div>
                                       <!-- <div class="row mb-0 mt-10 trip-select">
                                          <div class="input-field col s12">
                                              <select>
                                                <option value="1">Before Numemburg</option>
                                                <option value="2">After Numemburg</option>
                                                <option value="3">After Hanover</option>
                                              </select>
                                              <label>Add</label>
                                            </div>
                                          </div> -->
                                       <div class="stop-spec trip-select">
                                          <label>Add:</label>
                                          <select class="select2" tabindex="-1" >
                                             <option>Before Numemburg</option>
                                             <option>After Numemburg</option>
                                             <option>After Frankfurt</option>
                                             <option>After Hanover</option>
                                          </select>
                                       </div>
                                       <a href="javascript:void(0)" class="waves-effect waves-light btn modal-trigger btn-trip pull-trip right mt-10 mb-10">Add Stop</a>
                                    </div>
                                 </div>
                              </div>
                              <div class="dividing-line"></div>
                              <div class="drow">
                                 <div class="row mb-0">
                                    <div class="input-field col s12">
                                       <textarea id="textarea1" class="materialize-textarea"></textarea>
                                       <label for="textarea1" class="active">Your trip summery</label>
                                    </div>
                                 </div>
                              </div>
                              <div class="drow half">
                                 <div class="cap-holder">
                                    <label>Track Path</label>
                                 </div>
                                 <div class="comp-holder">
                                    <div class="right">
                                       <div class="radio-holder">
                                          <label class="control control--radio">
                                             Lines
                                             <input type="radio" name="radio1" checked value="lines"/>
                                             <div class="control__indicator"></div>
                                          </label>
                                       </div>
                                       <div class="radio-holder">
                                          <label class="control control--radio">
                                             Curves
                                             <input type="radio" name="radio1" value="curves"/>
                                             <div class="control__indicator"></div>
                                          </label>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="drow half">
                                 <div class="cap-holder">
                                    <label>Path Color</label>
                                 </div>
                                 <div class="comp-holder">
                                    <div class="right">
                                       <ul class="color-palette">
                                          <li class="active"><a href="javascript:void(0)" class="colordot bluedot"></a></li>
                                          <li><a href="javascript:void(0)" class="colordot purpledot"></a></li>
                                          <li><a href="javascript:void(0)" class="colordot greendot"></a></li>
                                          <li><a href="javascript:void(0)" class="colordot reddot"></a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                              <div class="drow half">
                                 <div class="cap-holder">
                                    <label>Your trip notes
                                    <span class="notetext">
                                    <a href="javascript:void(0)" onclick="showSubLayer(this,'notes','addmode')">2 notes added</a>
                                    </span>
                                    </label>
                                 </div>
                                 <div class="comp-holder">
                                    <a href="javascript:void(0)" class="right" onclick="showSubLayer(this,'notes','addmode')"><i class="mdi mdi-plus"></i> Add Note</a>
                                 </div>
                              </div>
                              <div class="drow half">
                                 <div class="cap-holder">
                                    <label>Who can view your trip details</label>
                                 </div>
                                 <div class="comp-holder category-list">
                                    <div class="custom-drop">
                                       <div class="dropdown dropdown-custom dropdown-xsmall">
                                          <a href="javascript:void(0)" class="dropdown-button more_btn" data-activates="dropdown-category-edit">			
                                          Public <span class="mdi mdi-menu-down"></span>
                                          </a>
                                          <ul id="dropdown-category-edit" class="dropdown-content custom_dropdown">
                                             <li><a href="javascript:void(0)">Private</a></li> 
                                             <li><a href="javascript:void(0)">Connections</a></li> 
                                             <li><a href="javascript:void(0)">Custom</a></li>
                                             <li><a href="#sharepost-popup" class="popup-modal pa-share">Public</a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="drow">
                                 <a href="javascript:void(0)" class="waves-effect waves-light btn modal-trigger btn-trip pull-trip right">Save Trip</a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="section-layer" id="trip-new">
                  <div class="section-holder nice-scroll">
                     <div class="sectitle">
                        Add a new trip
                        <a href="javascript:void(0)" class="right deletelink redicon" onclick="showLayer(this,'alltrip')"><i class="mdi mdi-close"></i> Cancel</a>
                     </div>
                     <div class="secdesc">
                        <div class="trip-summery">
                           <div class="tripdetail">
                              <div class="drow">
                                 <div class="sliding-middle-out anim-area underlined fullwidth">
                                    <input type="text" placeholder="Name your trip">
                                 </div>
                              </div>
                              <div class="starttrip">
                                 <span>Trip starts on -</span>
                                 <span>
                                    <div class="sliding-middle-out anim-area underlined">
                                       <input type="text" onkeydown="return false;" placeholder="Add Date" data-toggle="datepicker" class="datepickerinput" data-query="M" readonly>
                                    </div>
                                 </span>
                              </div>
                              <div class="drow">
                                 <div class="sliding-middle-out anim-area underlined fullwidth">
                                    <input type="text" placeholder="Where your trip starts from">
                                 </div>
                                 <div class="leftbox colored location-chkbox mt-10">
                                    <p>
                                       <input type="checkbox" id="homelocation" />
                                       <label for="homelocation">Start from my home location</label>
                                    </p>
                                 </div>
                              </div>
                              <ul class="tripstops-list">
                                 <li>
                                    <div class="tripstop hasdelete">
                                       <div class="title">
                                          <span class="numbering">1</span>
                                          <h5>Nuremberg</h5>
                                          <span class="date">11-22-2016</span>
                                       </div>
                                       <div class="deletebtn">
                                          <a href="javascript:void(0)" class="right deletelink redicon"><i class="zmdi zmdi-delete"></i></a>
                                       </div>
                                    </div>
                                 </li>
                              </ul>
                              <div class="drow">
                                 <div class="add-newstop">
                                    <div class="newstop opened">
                                       <div class="sliding-middle-out anim-area underlined fullwidth locinput">
                                          <input type="text" placeholder="Where you want to go next" data-query="M" onfocus="filderMapLocationModal(this)" autocomplete="off">
                                       </div>
                                       <div class="stop-spec trip-select">
                                          <label>Add:</label>
                                          <select class="select2" tabindex="-1" >
                                             <option>Before Numemburg</option>
                                             <option>After Numemburg</option>
                                             <option>After Frankfurt</option>
                                             <option>After Hanover</option>
                                          </select>
                                       </div>
                                       <a href="javascript:void(0)" class="waves-effect waves-light btn modal-trigger btn-trip pull-trip right mt-10 ">Add Stop</a>
                                    </div>
                                 </div>
                              </div>
                              <div class="dividing-line"></div>
                              <div class="row">
                                 <div class="input-field col s12">
                                    <textarea id="textarea1" class="materialize-textarea"></textarea>
                                    <label for="textarea1" class="active">Your trip summery</label>
                                 </div>
                              </div>
                              <div class="drow half">
                                 <div class="cap-holder">
                                    <label>Track Path</label>
                                 </div>
                                 <div class="comp-holder">
                                    <div class="right">
                                       <div class="radio-holder">
                                          <label class="control control--radio">
                                             Lines
                                             <input type="radio" name="radio1" checked value="lines"/>
                                             <div class="control__indicator"></div>
                                          </label>
                                       </div>
                                       <div class="radio-holder">
                                          <label class="control control--radio">
                                             Curves
                                             <input type="radio" name="radio1" value="curves"/>
                                             <div class="control__indicator"></div>
                                          </label>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="drow half">
                                 <div class="cap-holder">
                                    <label>Path Color</label>
                                 </div>
                                 <div class="comp-holder">
                                    <div class="right">
                                       <ul class="color-palette">
                                          <li class="active"><a href="javascript:void(0)" class="colordot bluedot"></a></li>
                                          <li><a href="javascript:void(0)" class="colordot purpledot"></a></li>
                                          <li><a href="javascript:void(0)" class="colordot greendot"></a></li>
                                          <li><a href="javascript:void(0)" class="colordot reddot"></a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                              <div class="drow half">
                                 <div class="cap-holder">
                                    <label>Your trip notes
                                    <span class="notetext">
                                    <a href="javascript:void(0)" onclick="showSubLayer(this,'notes','addmode')">2 notes added</a>
                                    </span>
                                    </label>
                                 </div>
                                 <div class="comp-holder">
                                    <a href="javascript:void(0)" class="right" onclick="showSubLayer(this,'notes','addmode')"><i class="mdi mdi-plus"></i> Add Note</a>
                                 </div>
                              </div>
                              <div class="drow half">
                                 <div class="cap-holder">
                                    <label>Who can view your trip details</label>
                                 </div>
                                 <div class="comp-holder category-list">
                                    <div class="custom-drop">
                                       <div class="dropdown dropdown-custom dropdown-xsmall">
                                          <a href="javascript:void(0)" class="dropdown-button more_btn" data-activates="dropdown-category">			
                                          Public <span class="mdi mdi-menu-down"></span>
                                          </a>
                                          <ul id="dropdown-category" class="dropdown-content custom_dropdown">
                                             <li><a href="javascript:void(0)">Private</a></li>
                                             <li><a href="javascript:void(0)">Connections</a></li>
                                             <li><a href="javascript:void(0)">Custom</a></li>
                                             <li><a href="#sharepost-popup" class="popup-modal pa-share">Public</a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="drow">
                                 <a href="javascript:void(0)" class="waves-effect waves-light btn modal-trigger btn-trip pull-trip right">Add New Trip</a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="section-layer" id="trip-bookmark">
                  <div class="section-holder moreinfo-outer nice-scroll">
                     <div class="moreinfo-box">
                        <a href="javascript:void(0)" onclick="hideBookmarkDetail(this)" class="backarrow"><i class="mdi mdi-arrow-left-bold-circle"></i></a>
                        <div class="infoholder nice-scroll">
                           <div class="imgholder"><img src="images/hotel1.png"/></div>
                           <div class="descholder">
                              <h4>The Guest House</h4>
                              <div class="clear"></div>
                              <div class="reviews-link">
                                 <span class="checks-holder">
                                 <i class="mdi mdi-star active"></i>
                                 <i class="mdi mdi-star active"></i>
                                 <i class="mdi mdi-star active"></i>
                                 <i class="mdi mdi-star active"></i>
                                 <i class="mdi mdi-star"></i>
                                 <label>34 Reviews</label>
                                 </span>
                              </div>
                              <span class="distance-info">Middle Eastem &amp; African, Mediterranean</span>
                              <div class="clear"></div>
                              <div class="more-holder">
                                 <ul class="infoul">
                                    <li>
                                       <i class="zmdi zmdi-pin"></i>
                                       132 Brick Lane | E1 6RU, Qatar E1 6RU, Qatar
                                    </li>
                                    <li>
                                       <i class="mdi mdi-phone"></i>
                                       +44 20 7247 8210
                                    </li>
                                    <li>
                                       <i class="mdi mdi-earth"></i>
                                       http://www.yourwebsite.com
                                    </li>
                                    <li>
                                       <i class="mdi mdi-clock-outline"></i>
                                       Today, 12:00 PM - 12:00 AM
                                    </li>
                                    <li>
                                       <i class="mdi mdi-certificate "></i>
                                       Ranked #1 in Qatar Hotels
                                    </li>
                                 </ul>
                                 <div class="tagging" onclick="explandTags(this)">
                                    Popular with:
                                    <span>Budget</span>
                                    <span>Foodies</span>
                                    <span>Family</span>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="section-sublayer" id="notes">
                  <div class="section-holder nice-scroll">
                     <div class="sectitle">
                        Notes
                        <span class="count">2 notes</span>
                        <a href="javascript:void(0)" class="right iconlink redicon" onclick="hideSubLayer(this)"><i class="mdi mdi-close"></i> Close</a>
                     </div>
                     <div class="addnote-section">
                        <div class="note-holder">
                           <div class="detail-mode">
                              <div class="editable">
                                 <form>
                                    <div class="notetitle">
                                       <div class="sliding-middle-out anim-area underlined fullwidth">
                                          <input type="text" placeholder="Note Title">
                                       </div>
                                    </div>
                                    <div class="note-tt fullwidth">
                                       <div class="sliding-middle-out anim-area underlined fullwidth tt-holder">
                                          <textarea placeholder="Your trip note" class="materialize-textarea"></textarea>
                                       </div>
                                    </div>
                                    <a href="javascript:void(0)" class="waves-effect waves-light btn btn-xs modal-trigger btn-trip right mt-10">Add Note</a>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="secdesc">
                        <div class="notes-list">
                           <ul>
                              <li>
                                 <div class="note-holder mode-holder">
                                    <div class="normal-mode">
                                       <div class="editable">
                                          <h6 onclick="open_detail(this)">Hotel Bookings</h6>
                                          <span class="sub" onclick="open_detail(this)">added on 10-02-2016</span>
                                          <div class="actionbtns">
                                             <a href="javascript:void(0)" onclick="open_detail(this)" class="editlink">
                                             <i class="zmdi zmdi-edit zmdi-hc-2x"></i>
                                             </a>
                                             <a href="javascript:void(0)" class="deletelink">
                                             <i class="zmdi zmdi-delete zmdi-hc-2x"></i>
                                             </a>
                                          </div>
                                       </div>
                                       <div class="viewable">
                                          <h6><a href="javascript:void(0)" onclick="open_detail(this)">Hotel Bookings</a></h6>
                                          <span class="sub">added on 10-02-2016</span>
                                       </div>
                                    </div>
                                    <div class="detail-mode">
                                       <div class="editable">
                                          <form>
                                             <div class="notetitle">
                                                <div class="row mb-0">
                                                   <div class="input-field col s12">
                                                      <input type="text" value="Hotel Bookings">
                                                      <label for="trip-name" class="">Note Title</label>
                                                   </div>
                                                </div>
                                                <a href="javascript:void(0)" class="right iconlink  redicon" onclick="close_detail(this)"><i class="mdi mdi-close"></i> Cancel</a>
                                             </div>
                                             <div class="note-tt fullwidth">
                                                <div class="sliding-middle-out anim-area underlined fullwidth tt-holder">
                                                   <textarea placeholder="Your trip note" class="materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</textarea>
                                                </div>
                                             </div>
                                             <a href="javascript:void(0)" class="btn btn-primary btn-sm right bottombtn redicon" onclick="close_detail(this)">Save Note</a>
                                          </form>
                                       </div>
                                       <div class="viewable">
                                          <form>
                                             <div class="notetitle">
                                                Hotel Bookings
                                                <a href="javascript:void(0)" class="right iconlink  redicon" onclick="close_detail(this)"><i class="mdi mdi-close"></i> Close</a>
                                             </div>
                                             <div class="note-tt fullwidth">
                                                <p>
                                                   Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                                                </p>
                                             </div>
                                          </form>
                                       </div>
                                    </div>
                                 </div>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="section-sublayer" id="bookmarks">
                  <div class="section-holder nice-scroll">
                     <div class="sectitle">
                        Bookmarked Points
                        <span class="count">2 bookmarks</span>
                        <a href="javascript:void(0)" class="right iconlink redicon" onclick="hideSubLayer(this)"><i class="mdi mdi-close"></i> Close</a>
                     </div>
                     <div class="secdesc">
                        <div class="notes-list">
                           <ul>
                              <li>
                                 <div class="note-holder mode-holder">
                                    <div class="normal-mode">
                                       <div class="editable">
                                          <h6><a href="javascript:void(0)" onclick="showBookmarkDetail(this)">Silver Leaf Hotel</a></h6>
                                          <div class="actionbtns">
                                             <a href="javascript:void(0)" class="deletelink"><i class="zmdi zmdi-delete"></i></a>
                                          </div>
                                       </div>
                                       <div class="viewable">
                                          <h6><a href="javascript:void(0)" onclick="open_detail(this)">Hotel Bookings</a></h6>
                                          <span class="sub">added on 10-02-2016</span>
                                       </div>
                                    </div>
                                 </div>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="map-box">
            <div class="map-container">
               <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3110.3465133386144!2d-9.167423685010494!3d38.77868997958898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd193295d5b45545%3A0x3f9e7b6a5f00e12c!2sPerta!5e0!3m2!1sen!2sin!4v1481089901870" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="map-filter">
               <div class="map-icons">
                  <a href="javascript:void(0)" title="Lodge"><span class="icon lodge-icon"><img src="images/lodge-icon.png"></span></a>
                  <a href="javascript:void(0)" title="Dine"><span class="icon dine-icon"><img src="images/dine-icon.png"></span></a>
                  <a href="javascript:void(0)" title="Todo"><span class="icon todo-icon"><img src="images/todo-icon.png"></span></a>
               </div>
               <a href="javascript:void(0)" class="backroute" onclick="BackTripRouteMap(this)">Back to trip route</a>
               <div class="map-drop">
                  <div class="sliding-middle-out anim-area underlined">
                     <select class="select2" tabindex="-1" >
                        <option>Numemburg</option>
                        <option>Frankfurt</option>
                        <option>Hanover</option>
                     </select>
                  </div>
               </div>
            </div>
            <div class="mobile-comp">
               <a href="javascript:void(0)" onclick="closeDetailTripMap(this)">Back to detailview</a>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>
<div id="modal1" class="modal">
   <div class="modal-content">
      <h4>Modal Header</h4>
      <p>A bunch of text</p>
   </div>
   <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
   </div>
</div>

<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">  
   <?php include('common/map_modal.php'); ?>
</div>

<?php include('common/datepicker.php'); ?>

<?php  include('common/share_popup.php'); ?>

<?php include("script.php"); ?>
</body>
</html>