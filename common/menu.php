<div class="sticky-nav">
	<div class="top-nav">
		<div class="row mx-0">
			<div class="col l12 s12">
				<div class="nav-links">
					<ul>
						<li class="<?=($file=="index")?' active' :'';?>"><a href="index.php">Home</a></li>
						<li class="<?=($file=="discussion")?' active' :'';?>"><a href="discussion.php">Discussion</a></li>
						<li class="<?=($file=="photostream")?' active' :'';?>"><a href="photostream.php">Photostream</a></li>
						<li class="<?=($file=="tips")?' active' :'';?>"><a href="tips.php">Tips</a></li>
						<li class="<?=($file=="questions")?' active' :'';?>"><a href="questions.php">Questions</a></li>
						<li class="<?=($file=="blog")?' active' :'';?>"><a href="blog.php">Blog</a></li>
						<li>
							<a class="dropdown-button" href="#" data-activates="moreTopLinks"><i class="zmdi zmdi-more"></i></a>
							<ul id="moreTopLinks" class="dropdown-content custom_dropdown">
								<li class="<?=($file=="tripstory")?' active' :'';?>"><a href="tripstory.php">Trip Story</a></li>
		                        <li class="<?=($file=="reviews")?' active' :'';?>"><a href="reviews.php">Reviews</a></li>
		                        <li class="<?=($file=="collections")?' active' :'';?>"><a href="collections.php">Photo Collections</a></li>
		                        <li class="<?=($file=="travellers")?' active' :'';?>"><a href="locals.php">Qatar Locals</a></li>
		                        <li class="<?=($file=="travellers")?' active' :'';?>"><a href="travellers.php">People travelling to Qatar</a></li>
		                        <li class="<?=($file=="local-guide")?' active' :'';?>"><a href="local-guide.php">Local Guide</a></li>
		                        <li class="<?=($file=="local-driver")?' active' :'';?>"><a href="local-driver.php">Local Driver</a></li>
		                        <li class="<?=($file=="")?' active' :'';?>"><a href="local-guide.php">City Guide</a></li>
		                        <li class="<?=($file=="business-pages" || $file =="business-pages-detail")?' active' :'';?>"><a href="business-pages.php">Business pages</a></li>
		                    </ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

