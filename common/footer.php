<div class="footer-section">
   <div class="mobile-footer-arrow">
      <div class="mobile-footer">
         <div class="dropdown dropdown-custom mmenu text-center">
            <a href="index.php" class="dropdown-toggle"  role="button" aria-haspopup="true" aria-expanded="false">
            <img src="images/foot-home.png" />
            Home
            </a>					
         </div>
         <div class="dropdown-button more_btn footer-menu dropdown-custom mmenu text-center footer_booking">
            <a href="javascript:void(0)" class="dropdown-toggle"  data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <i class="mdi mdi-plus-circle mdi-23px" style="
                width: 22px;
                display: block;
                opacity: 0.6;
                margin: 3px auto;
            "></i>   
            Booking
            </a>
            <ul class="dropdown-menu large-menuicons">
               <!-- <li><a href="hotels.php"><span class="icon lodge-icon"><img src="images/lodge-icon.png"></span>Hotels</a></li>
               <li><a href="javascript:void(0)"><span class="icon flight-icon"><img src="images/flight-icon.png"></span>Flights</a></li>
               <li><a href="tours.php"><span class="icon tour-icon"><img src="images/tour-icon.png"></span>Tours</a></li>
               <li><a href="trip.php"><span class="icon addtrip-icon"><img src="images/addtrip-icon.png"></span>Trip</a></li> -->

               <li>
                  <a href="hotels.php" title="Hotels">
                     <i class="mdi mdi-hospital-building"></i>Hotels
                  </a>
               </li>
               <li>
                  <a href="homestay.php" title="Homestay">
                     <i class="mdi mdi-hotel blue"></i>Homestay
                  </a>
               </li>
               <li>
                  <a href="hotels.php" title="Restaurant">
                     <i class="mdi mdi-silverware"></i>Restaurant
                  </a>
               </li>
               <li>
                  <a href="localdine.php" title="Local Dine">
                     <i class="mdi mdi-basecamp"></i>Local Dine
                  </a>
               </li>
               <li>
                  <a href="tours.php" title="Tours">
                     <i class="mdi mdi-ticket" ></i>Tours
                  </a>
               </li>
               <li>
                  <a href="camping.php" title="Camping">
                     <i class="mdi mdi-terrain"></i>Camping
                  </a>
               </li>
            </ul>
         </div>
         <div class="dropdown dropdown-custom mmenu text-center footer_local">
            <a href="locals.php" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">
            <img src="images/foot-places.png" />
            Local
            </a>					
         </div>
         <div class="dropdown-button more_btn footer-menu dropdown-custom mmenu text-center footer_alert" onclick="openalert()">
            <a href="javascript:void(0)" class="dropdown-toggle">
               <img src="images/foot-more.png" />
               Alert
            </a>
         </div>
      </div>
      <div class="master_alert">
         <div class="littlemaster_alert">
            <a href="people-you-know.php">
               <span class="icon default-icon">
                  <img src="images/connectreq-icon.png">
               </span>Connect Requests
            </a>
         </div>
         <div class="littlemaster_alert">
            <a href="messages-new.php">
               <span class="icon default-icon">
                  <img src="images/message-icon.png">
               </span>Messages
            </a>
         </div>
         <div class="littlemaster_alert">
            <a href="notifications.php">
               <span class="icon default-icon">
                  <img src="images/notification-icon.png">
               </span>Notifications
            </a>
         </div>
      </div>
   </div>
   <div class="main-footer">
      <ul class="center-align">
         <li><a href="javascript:void(0)">Home</a></li>
         <li><a href="javascript:void(0)">Booking</a></li>
         <li><a href="javascript:void(0)">Local</a></li>
         <li><a href="javascript:void(0)">Alert</a></li>
      </ul>
   </div>
</div>