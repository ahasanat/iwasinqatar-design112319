<!--attachment modal-->
<div id="compose_visitcountry" class="modal compose_inner_modal modalxii_level1 visit-country">
   <div class="content_header">
      <button class="close_span waves-effect">
      <i class="mdi mdi-close mdi-20px material_close"></i>
      </button>
      <p class="selected_person_text">Select Country</p>
   </div>
   <div class="person_box">
      <div class="collection visit-country-list">
         <a href="http://www.visitpetra.jo" target="_blank" class="collection-item">Visit Petra</a>
         <a href="http://www.visitjordan.com" target="_blank" class="collection-item">Visit Jordan</a>
         <a href="https://rcu.gov.sa/" target="_blank" class="collection-item">Visit Madain</a>
         <a href="https://www.visitsaudi.com" target="_blank" class="collection-item">Visit Saudi</a>
         <a href="http://ee.france.fr" target="_blank" class="collection-item">Visit France</a>
         <a href="https://www.visitdubai.com" target="_blank" class="collection-item">Visit Dubai</a>
         <a href="http://www.tourspain.org" target="_blank" class="collection-item">Visit Spain</a>
         <a href="https://www.visitqatar.qa" target="_blank" class="collection-item">Visit Qatar</a>
         <a href="https://visitbahrain.bh" target="_blank" class="collection-item">Visit Bahrain</a>
         <a href="https://visitabudhabi.ae" target="_blank" class="collection-item">Visit Abu Dhabi</a>
         <a href="https://www.visittheusa.com" target="_blank" class="collection-item">Visit USA</a>
         <a href="https://www.visititaly.eu" target="_blank" class="collection-item">Visit Italy</a>
         <a href="https://www.visitmexico.com" target="_blank" class="collection-item">Visit Mexico</a>
         <a href="" target="_blank" class="collection-item">Visit UK</a>
         <a href="https://visit.istanbul target="_blank" class="collection-item">Visit Istanbul</a>
         <a href="https://chinatour.net" target="_blank" class="collection-item">Visit China</a>
         <a href="https://www.germany.travel" target="_blank" class="collection-item">Visit Germany</a>
         <a href="https://www.tourismthailand.org" target="_blank" class="collection-item">Visit Thailand</a>
         <a href="https://www.austria.info" target="_blank" class="collection-item">Visit Austria</a>
         <a href="https://www.visithongkong.gov.hk" target="_blank" class="collection-item">Visit Hong Kong</a>
         <a href="http://www.visitgreece.gr" target="_blank" class="collection-item">Visit Greece</a>
         <a href="https://www.visitrussia.com" target="_blank" class="collection-item">Visit Russia</a>
         <a href="https://us.jnto.go.jp" target="_blank" class="collection-item">Visit Japan</a>
         <a href="https://www.visitcyprus.com" target="_blank" class="collection-item">Visit Cyprus</a>
         <a href="https://www.visitjamaica.com" target="_blank" class="collection-item">Visit Jamaica</a>
         <a href="https://www.visitpoland.com" target="_blank" class="collection-item">Visit Poland</a>
         <a href="http://visit-hungary.com/budapest" target="_blank" class="collection-item">Visit Hungary</a>
         <a href="https://www.zimbabwetourism.net" target="_blank" class="collection-item">Visit Zimbabwe</a>
         <a href="http://visittoukraine.com/en" target="_blank" class="collection-item">Visit Ukraine</a>
         <a href="https://www.holland.com/" target="_blank" class="collection-item">Visit Netherlands</a>
         <a href="https://www.indianvisit.com/" target="_blank" class="collection-item">Visit India</a>
         <a href="https://www.visitsingapore.com/en/" target="_blank" class="collection-item">Visit Singapore</a>
         <a href="http://www.tourism.gov.pk/" target="_blank" class="collection-item">Visit Pakistan</a>
         <a href="https://www.visitmorocco.com/en" target="_blank" class="collection-item">Visit Morocco</a>
         <a href="http://www.egypt.travel/" target="_blank" class="collection-item">Visit Egypt</a>
         <a href="https://www.visitbrasil.com/" target="_blank" class="collection-item">Visit Brazil</a>
         <a href="https://www.australia.com/en" target="_blank" class="collection-item">Visit Australia</a>
         <a href="http://www.visit-malaysia.com/" target="_blank" class="collection-item">Visit Malaysia</a>
         <a href="https://www.indonesia.travel/gb/en/home" target="_blank" class="collection-item">Visit Indonesia</a>
         <a href="https://www.argentina.travel/" target="_blank" class="collection-item">Visit Argentina</a>
         <a href="https://www.zimbabwetourism.net/" target="_blank" class="collection-item">Visit Zimbabwe</a>
         <a href="https://www.visitmacao.com.au/" target="_blank" class="collection-item">Visit Macao</a>
         <a href="https://www.visit-croatia.co.uk/" target="_blank" class="collection-item">Visit Croatia</a>
         <a href="http://english.visitkorea.or.kr/enu/index.kto" target="_blank" class="collection-item">Visit Southkorea</a>
         <a href="https://www.visitiran.ir/" target="_blank" class="collection-item">Visit Iran</a>
      </div>
   </div>
</div>
