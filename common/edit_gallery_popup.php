<div id="edit-gallery-popup" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose upload-gallery-popup">
    <div class="modal_content_container">
        <div class="modal_content_child modal-content">
            <div class="popup-title">
                <button class="hidden_close_span close_span waves-effect">
                    <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
                </button>
                <h3>Edit photo details</h3>
                <a type="button" class="item_done crop_done hidden_close_span custom_close waves-effect" href="javascript:void(0)">Done</a>
            </div>

            <div class="custom_modal_content modal_content" id="createpopup">
                <div class="ablum-yours profile-tab">
                    <div class="ablum-box detail-box">
                        <div class="content-holder main-holder">
                            <div class="summery">
                                <div class="dsection bborder expandable-holder expanded">
                                    <div class="form-area expandable-area">
                                        <form class="ablum-form">
                                            <div class="form-box">
                                                <div class="fulldiv">
                                                    <div class="half">
                                                        <div class="frow">
                                                            <div class="caption-holder">
                                                                <label>Photo title</label>
                                                            </div>
                                                            <div class="detail-holder">
                                                                <div class="input-field">
                                                                    <input type="text" placeholder="Photo title" class="fullwidth locinput" value="awesome photo" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="fulldiv w-100">
                                                    <div class="half">
                                                        <div class="frow">
                                                            <div class="caption-holder">
                                                                <label>Say something about it</label>
                                                            </div>
                                                            <div class="detail-holder">
                                                                <div class="input-field">
                                                                    <textarea id="Collection_tagline" class="materialize-textarea mb0 md_textarea item_tagline" placeholder="Tell people about the photo">this is very interesting photo...</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="fulldiv">
                                                    <div class="half">
                                                        <div class="frow">
                                                            <div class="caption-holder">
                                                                <label>Location</label>
                                                            </div>
                                                            <div class="detail-holder">
                                                                <div class="input-field">
                                                                    <input type="text" placeholder="Where was it taken?" class="fullwidth locinput" data-query="M" onfocus="filderMapLocationModal(this)" id="createlocation3" value="Qatar" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

												<div class="fulldiv">
													<div class="half">
														<div class="frow">
															<div class="caption-holder">
																<label>Tagged connections</label>
															</div>
															<div class="detail-holder">
																<div class="input-field">
																	<input type="text" placeholder="Connections with you in the photo" class="fullwidth" id="photoTags" value="Muhammad Ammad, Saad Ahasanat" />
																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="fulldiv w-100 visibleto">
													<div class="half">
														<div class="frow">
															<div class="caption-holder">
																<label>Visible to</label>
																<div class="right mt-5">
											                        <a class="dropdown-button collectioncreateprivacylabel" href="javascript:void(0)" onclick="privacymodal(this)">
											                           <span class="mdi-14px"> Privacy </span>
											                           <i class="zmdi zmdi-caret-down"></i>
											                        </a>
											                    </div>
															</div>
														</div>
													</div>
												</div>

												<div class="fulldiv">
													<div class="half">
														<div class="frow">
															<div class="caption-holder">
																<label>Photo categories</label>
															</div>
															<div class="detail-holder">
																<div class="input-field">
																	<input type="text" placeholder="add categories" class="fullwidth " id="photoCategories" value="Nature" />
																</div>
															</div>
														</div>
													</div>
												</div>

                                                <div class="frow nomargin new-post">
                                                    <div class="caption-holder">
                                                        <label>Add photos</label>
                                                    </div>
                                                    <div class="detail-holder">
                                                        <div class="input-field ">
                                                            <div class="post-photos new_pic_add">
                                                                <div class="img-row">
                                                                    <div class="img-box"><img src="images/post-img.jpg" class="thumb-image undefined"><a href="javascript:void(0)" class="removePhotoFile" data-code="1498812224457"><i class="mdi mdi-close"></i></a></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="valign-wrapper additem_modal_footer modal-footer">
        <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">Cancel</a>
        <a href="javascript:void(0)" class="btngen-center-align waves-effect">Save</a>
    </div>
</div>