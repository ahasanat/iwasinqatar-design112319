<?php
	$mhidecls = '';
	$mHidePageBulks = array('places', 'discussion', 'trip','credit-transfer','credit-update','credit','verify','vip-member','billing-info','settings','ad-manager','advertisement','manage-ad','hotels','tours','wall','pages','messages-org','messages','business-page', 'business-pages', 'business-page-detail', 'notifications', 'local-guide', 'local-driver','travellers','locals','collections','reviews','tripstory','blog','questions','tips','photostream','index','homestay','homestay-detail','localdine','localdinedetail','camping','guide-detail','driver-detail');	
	if(in_array($file, $mHidePageBulks)) {
		$mhidecls = 'm-hide';
	}
?>
<div class="sidemenu-holder <?=$mhidecls?>">
   <div class="sidemenu nice-scroll">
      <a href="javascript:void(0)" class="closemenu waves-theme waves-effect"><i class="mdi mdi-close"></i></a>
      <div class="side-user-cover">
         <img src="images/wgallery3.jpg">
      </div>
      <div class="side-user">
         <span class="img-holder"><img class="circle" src="images/demo-profile.jpg" /></span>
         <a href="wall"><span class="desc-holder">Nimish Parekh</span></a>
      </div>
      <div class="sidemenu-ul">
		<ul>
			<li class="lm-home<?=($file=="index")?' active' :'';?>"><a href="index.php">Home</a></li>
			<li class="lm-discussion<?=($file=="discussion")?' active' :'';?>"><a href="discussion.php">Discussion</a></li>
			<li class="lm-pages<?=($file=="photostream")?' active' :'';?>"><a href="photostream.php">Photostream</a></li>
			<li class="lm-channels<?=($file=="tips")?' active' :'';?>"><a href="tips.php">Tips</a></li>
			<li class="lm-questions<?=($file=="questions")?' active' :'';?>"><a href="questions.php">Questions</a></li>
			<li class="lm-commeve<?=($file=="blog")?' active' :'';?>"><a href="blog.php">Blog</a></li>
			<li class="lm-waround<?=($file=="")?' active' :'';?>"><a href="tripstory.php">Trip Story</a></li>
			<li class="lm-tbuddy<?=($file=="reviews")?' active' :'';?>"><a href="reviews.php">Reviews</a></li>
			<li class="lm-lbuddy<?=($file=="collections")?' active' :'';?>"><a href="collections.php">Photo Collections</a></li>
			<li class="lm-lbuddy<?=($file=="locals")?' active' :'';?>"><a href="locals.php">Qatar Locals</a></li>
			<li class="<?=($file=="travellers")?' active' :'';?>"><a href="travellers.php">People travelling to Qatar</a></li>
			<li class="lm-localguide<?=($file=="local-guide")?' active' :'';?>"><a href="local-guide.php">Local Guide</a></li>						
			<li class="lm-texp<?=($file=="local-driver")?' active' :'';?>"><a href="local-driver.php">Local Driver</a></li>
			<li class="lm-groups<?=($file=="" || $file=="groups-detail")?' active' :'';?>"><a href="local-guide.php">City Guide</a></li>
			<li class="lm-pages<?=($file=="business-pages" || $file=="business-pages-detail")?' active' :'';?>"><a href="business-pages.php">Business pages</a></li>
		</ul>
	</div>
   </div>
   <div class="mobile-menu">
      <a href="javascript:void(0)"><i class="mdi mdi-menu"></i></a>
   </div>
</div>