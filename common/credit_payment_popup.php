<?php
   $file = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);
   $file = str_replace('.php','',$file);
   if($file == "credit-update" || $file == "credit-transfer" || $file == "credit") {
      $file = "credit";
   }

   if($file == "ad-manager") {
      $file = "ads";
   }
   
   if($file == 'credit') {
      $h3 = 'Iwasinqatar Credits';
      $h6 = 'Virtual currency which can be used to buy various services on Iwasinqatar';
      $rightLaps = array('<li> <i class="zmdi zmdi-check"></i> First in search list 100 credits/day </li>', '<li> <i class="zmdi zmdi-check"></i> Send eGift for 100 credits </li>', '<li> <i class="zmdi zmdi-check"></i> Transfer credits to your connections </li>', '<li> <i class="zmdi zmdi-check"></i> Boost my popularity 100 credits/day </li>');
      $h62 = 'Top up now - The more credits you buy, the cheaper they are :';
   } else if($file == 'verify') {
      $h3 = 'Get verified and establish trust';
      $h6 = 'Verified people connect with travellers faster';  
      $rightLaps = array('<li> <img src="images/connect.png"><br/>Connect Faster </li>', '<li> <img src="images/earnverified.png"><br/>Earn Verified Check </li>');
      $h62 = 'Choose the package that suits you best';
   } else if ($file == 'vip-member') {
      $h3 = 'VIP Membership';
      $h6 = 'Enjoy various exclusive perks and boost your popularity';
      $rightLaps = array('<li> <div class="vip-info"> <div class="iconholder"><i class="zmdi zmdi-check"></i></div> <div class="descholder"> <h6>Earn Points</h6></div> </div> </li>', '<li> <div class="vip-info"> <div class="iconholder"><i class="zmdi zmdi-check"></i></div> <div class="descholder"> <h6>Discount on Ads</h6></div> </div> </li>', '<li> <div class="vip-info"> <div class="iconholder"><i class="zmdi zmdi-check"></i></div> <div class="descholder"> <h6>Share Plans</h6></div> </div> </li>', '<li> <div class="vip-info"> <div class="iconholder"><i class="zmdi zmdi-check"></i></div> <div class="descholder"> <h6>Automatic Updates</h6></div> </div> </li>', '<li> <div class="vip-info"> <div class="iconholder"><i class="zmdi zmdi-check"></i></div> <div class="descholder"> <h6>VIP and Verified Badge</h6></div> </div> </li>', '<li> <div class="vip-info"> <div class="iconholder"><i class="zmdi zmdi-check"></i></div> <div class="descholder"> <h6>Get Listed  First</h6></div> </div> </li>', '<li> <div class="vip-info"> <div class="iconholder"><i class="zmdi zmdi-check"></i></div> <div class="descholder"> <h6>Send Message</h6></div> </div> </li>', '<li> <div class="vip-info"> <div class="iconholder"><i class="zmdi zmdi-check"></i></div> <div class="descholder"> <h6>Message Read</h6></div> </div> </li>', '<li> <div class="vip-info"> <div class="iconholder"><i class="zmdi zmdi-check"></i></div> <div class="descholder"> <h6>Profile viewer</h6></div> </div> </li>', '<li> <div class="vip-info"> <div class="iconholder"><i class="zmdi zmdi-check"></i></div> <div class="descholder"> <h6>Business Listed First</h6></div> </div> </li>', '<li> <div class="vip-info"> <div class="iconholder"><i class="zmdi zmdi-check"></i></div> <div class="descholder"> <h6>Premium Support</h6></div> </div> </li>', '<li> <div class="vip-info onlycap"> <div class="iconholder"><i class="zmdi zmdi-check"></i></div> <div class="descholder"> <h6>For only $9 monthly</h6> </div> </div> </li>');
      $h62 = 'Become a VIP member today to unlock premium feature!';
   } else {
      $rightLaps = array();
      $h6 = 'Please select your preferred payment method';
      $h3 = 'Advert Payment';
   }
 
?>
<div class="credit-modal-content wow zoomIn  animated" data-wow-duration="1200ms" data-wow-delay="500ms">
   <div class="popup-title credit <?=$file?>"> 
      <h3><?=$h3?></h3>
      <h6><?=$h6?></h6>
      <a class="close-popup modal-action modal-close waves-effect" href="javascript:void(0)">
      <i class="mdi mdi-close"></i>
      </a>
   </div>

   <div class="popup-content">
      <div class="payment" id="tabs">
         <div class="payment-tab"> 
            <ul class="tabs">
               <li class="tab"><a href="#payment-paypal"><span class="icon"><img src="images/payment-paypal.png"/></span>Paypal</a></li>
               <li class="tab"><a href="#payment-creditcard"><span class="icon"><img src="images/payment-creditcard.png"/></span>Credit Card</a></li>
               <?php if($file != 'ads') { ?>
               <li class="tab"><a href="#payment-mobile"><span class="icon"><img src="images/payment-mobile.png"/></span>Mobile Pay</a></li>
               <?php } ?>
            </ul>
         </div>
         <div class="payment-info">
            <div class="tab-content">
               <?php if($file != 'ads') { ?>
               <div class="module-info">
                  <ul class="checkul"> 
                     <?php
                        foreach ($rightLaps as $srightLaps) {
                           echo $srightLaps;
                        }
                     ?>
                  </ul>
               </div>
               <?php } ?>
               <div class="common-info">
                  <?php if($file == 'ads') { ?>
                     <h4>Your Cart</h4>
                     <div class="frow">
                        <div class="caption-holder">
                           <label>Ad Name</label>
                        </div>
                        <div class="detail-holder">
                           <p>Your ad name</p>
                        </div>
                     </div>
                     <div class="frow">
                        <div class="caption-holder">
                           <label>Ad Duration <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                        </div>
                        <div class="detail-holder">
                           <p>12 Day</p>
                           <span class="adend">End date is <span class="b">13 Apr 2017</span></span>
                        </div>
                     </div>
                     <div class="frow">
                        <div class="caption-holder">
                           <label>Daily Budget <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                        </div>
                        <div class="detail-holder">
                           <p>$12.00</p>
                        </div>
                     </div>
                     <div class="clear"></div>
                     <div class="divider"></div>
                     <div class="frow">
                        <div class="caption-holder">
                           <label>Total Cost <a href="javascript:void(0)" class="simple-tooltip" title="some intro text goes here<br />some intro text goes here"><i class="zmdi zmdi-help"></i></a></label>
                        </div>
                        <div class="detail-holder">
                           <span class="green-t">$144.00</span>
                        </div>
                     </div>
                  <?php } else { ?>
                     <h6><?=$h62?></h6>
                     <div class="vip-page">
                        <div class="vip-package">
                           <ul>
                              <li>
                                 <div class="month-radio">
                                    <label class="control control--radio">
                                       200 Credits
                                       <input type="radio" name="radio"/>
                                       <div class="control__indicator"></div>
                                    </label>
                                 </div>
                                 <div class="month-save">
                                    Save <span>$1.00</span>
                                 </div>
                                 <div class="month-package">
                              +      USD $5.00
                                 </div>
                                 <div class="month-special">
                                 </div>
                              </li>
                              <li>
                                 <div class="month-radio">
                                    <label class="control control--radio">
                                       500 Credits
                                       <input type="radio" name="radio"/>
                                       <div class="control__indicator"></div>
                                    </label>
                                 </div>
                                 <div class="month-save">
                                    Save <span>$3.00</span>
                                 </div>
                                 <div class="month-package">
                                    USD $10.00
                                 </div>
                                 <div class="month-special">
                                    <span class="popular">Popular</span>
                                 </div>
                              </li>
                              <li>
                                 <div class="month-radio">
                                    <label class="control control--radio">
                                       800 Credits
                                       <input type="radio" name="radio"/>
                                       <div class="control__indicator"></div>
                                    </label>
                                 </div>
                                 <div class="month-save">
                                    Save <span>$5.00</span>
                                 </div>
                                 <div class="month-package">
                                    USD $15.00
                                 </div>
                                 <div class="month-special">
                                 </div>
                              </li>
                              <li>
                                 <div class="month-radio">
                                    <label class="control control--radio">
                                       1000 Credits
                                       <input type="radio" name="radio"/>
                                       <div class="control__indicator"></div>
                                    </label>
                                 </div>
                                 <div class="month-save">
                                    Save <span>$4.00</span>
                                 </div>
                                 <div class="month-package">
                                    USD $10.00
                                 </div>
                                 <!-- <div class="month-special">
                                    <span class="value">Best Value</span>
                                    </div> -->
                              </li>
                           </ul>
                        </div>
                     </div>
                   <?php  } ?>
               </div>
               <div id="payment-paypal" class="tab-pane fade payment-content center-align active in">
                  <a href="javascript:void(0)"><img src="images/blue-paypal-btn.png"/></a>
                  <div class="paypal-info">
                     <div class="leftbox">
                        <input type="checkbox" id="test12">
                        <label for="test12">Topup my iwasinqatar credits automatically when my balance falls below 100 credits. To disable auto-topup, please uncheck the box <a href="javascript:void(0)"><i class="zmdi zmdi-help"></i></a></label>
                     </div>
                     <div class="clear"></div>
                     <div class="expandable-holder conditions">
                        <p>Your paypal account will be charged USD $9.99.<span class="expand-link invertsign" onclick="mng_expandable(this,'mainright');"> Services conditions <i class="mdi mdi-menu-right"></i></span>
                        </p>
                        <div class="gray-area expandable-area">
                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed fortuna fortis; Non autem hoc: igitur ne illud quidem. At ego quem huic anteponam non audeo dicere;
                              <br /><br />
                              Nec vero sum nescius esse utilitatem in historia, non modo voluptatem. Nam aliquando posse recte fieri dicunt nulla expectata nec quaesita voluptate. Modo etiam paulum ad dexteram de via declinavi, ut ad Pericli sepulcrum accederem. Universa enim illorum ratione cum tota vestra confligendum puto. Quodsi ipsam honestatem undique pertectam atque absolutam. Quodsi vultum tibi, si incessum fingeres, quo gravior viderere, non esses tui similis; Duo Reges: constructio interrete. Habes, inquam, Cato, formam eorum, de quibus loquor, philosophorum. Dat enim intervalla et relaxat. Quid ei reliquisti, nisi te, quoquo modo loqueretur, intellegere, quid diceret?
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
               <div id="payment-creditcard" class="payment-content center-align">
                  <div class="credit-card">
                     <div class="card-frame">
                        <div class="crow">
                           <label>Name on the card</label>
                           <input type='text' placeholder="NAME ON THE CARD" class="cardname"/>
                        </div>
                        <div class="row">
                           <div class="col m6 s12">
                              <div class="crow">
                                 <label>Card Number</label>
                                 <input type='text' placeholder="0000 0000 0000 0000"/>
                              </div>
                           </div>
                           <div class="col m6 s12">
                              <div class="crow dateinput">
                                 <label>Expiry Date</label>
                                 <div class="row">
                                    <div class="col m6 s6">
                                       <select class="select2" tabindex="-1" >
                                          <option>MM</option>
                                          <option>01</option>
                                          <option>02</option>
                                          <option>03</option>
                                       </select>
                                    </div>
                                    <div class="col m6 s6">
                                       <select class="select2" tabindex="-1" >
                                          <option>YYYY</option>
                                          <option>2017</option>
                                          <option>2016</option>
                                          <option>2015</option>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="crow">
                           <label>CVV/CVC</label>
                           <input type='text' class="cardcvv"/>
                        </div>
                     </div>
                  </div>
                  <input type="image" name="card_submit" id="cardSubmitBtn" value="Pay by card" src="images/paybycard.png" class="paybycard">
                  <!-- <a href="javascript:void(0)" class="btn btn-primary btn-xl">Pay by card</a> -->
               </div>
               <?php if($file != 'ads') { ?>
               <div id="payment-mobile" class="tab-pane fade payment-content center-align">
               </div>
               <?php } ?>
            </div>
         </div>
      </div>
   </div>
</div>