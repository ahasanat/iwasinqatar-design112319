<div id="custom_user_modal" class="modal modalxii_level1" style="z-index: 1100;">
	<div class="content_header"> 
		<button class="close_span waves-effect">
			<i class="mdi mdi-close mdi-20px"></i>
		</button>
		<p class="selected_photo_text">person selected</p>
		<a href="javascript:void(0)" id="customdone" class="done_btn action_btn">Done</a>
	</div>
	<nav class="search_for_tag">
		<div class="nav-wrapper">
		  <form>
		    <div class="input-field">
		      <input type="search" required="" class="search_box">
		        <label class="label-icon" for="search_box">
		          <i class="zmdi zmdi-search"></i>
		        </label>
		      </div>
		  </form>
		</div>
	</nav>
	<div class="person_box">
		<div class="person_detail_container person_detail_div">
			<span class="person_profile">
				<img src="images/people-1.png">
			</span>
			<div class="person_name_container">
				<p class="person_name" id="checkPerson0">Adel Google1</p>
				<p class="user_checkbox" style="z-index:99999;">
					<input type="checkbox" id="filled_for_person_0" data-chkperson_id="0" class="chk_person">
					<label for="filled_for_person_0"></label>
				</p>
			</div>
		</div>
	</div>
</div>