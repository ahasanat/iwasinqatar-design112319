<div id="add-photo-popup" class="modal addphoto_modal custom_md_modal">
   <div class="modal_header">
      <button class="close_btn custom_modal_close_btn close_modal">
      <i class="mdi mdi-close mdi-20px material_close"></i>
      </button>
      <h3>Add photos</h3>
   </div>
   <div class="custom_modal_content modal_content">
      <div class="content-holder nsidepad nbpad">
         <form class="add-album-form">
            <div class="post-photos modal-album">
               <div class="img-row nice-scroll">
                  <div class="img-box">
                     <div class="custom-file addimg-box">
                        <div class="addimg-icon"><span class="zmdi zmdi-plus"></span></div>
                        <input class="upload custom-upload remove-custom-upload" title="Choose a file to upload" required="" data-class=".post-photos .img-row" multiple="true" type="file">
                     </div>
                  </div>
               </div>
            </div>
            <div class="frow stretch-side">
               <div class="btn-holder">
                  <a href="javascript:void(0)" class="post_btn m-0 active_post_btn modal_modal" data-class="addbtn">Add</a>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>