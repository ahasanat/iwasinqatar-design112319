<div id="compose_addcategories" class="modal compose_inner_modal modalxii_level1 modal-categories">
   <div class="content_header">
      <button class="close_span waves-effect">
      <i class="mdi mdi-close mdi-20px material_close"></i>
      </button>
      <p class="categories-heading">Add categories</p>
      <a href="javascript:void(0)" id="chk_person_done" class="done_btn action_btn">Done</a>
   </div>
   <div class="person_box">
      <div class="person_detail_container person_detail_div">
         <p class="user_checkbox" style="z-index:99999;">
            <input type="checkbox" id="filled_for_person_0" data-chkperson_id="0" class="chk_person">
            <label for="filled_for_person_0"></label>
         </p>
         <div class="person_name_container">
            <p class="person_name" id="checkPerson0">category 1</p>
         </div>
      </div>
      <div class="person_detail_container person_detail_div">
         <p class="user_checkbox" style="z-index:99999;">
            <input type="checkbox" id="filled_for_person_0" data-chkperson_id="0" class="chk_person">
            <label for="filled_for_person_0"></label>
         </p>
         <div class="person_name_container">
            <p class="person_name" id="checkPerson0">category 2</p>
         </div>
      </div>
      <div class="person_detail_container person_detail_div">
         <p class="user_checkbox" style="z-index:99999;">
            <input type="checkbox" id="filled_for_person_0" data-chkperson_id="0" class="chk_person">
            <label for="filled_for_person_0"></label>
         </p>
         <div class="person_name_container">
            <p class="person_name" id="checkPerson0">category 3</p>
         </div>
      </div>
   </div>
</div>