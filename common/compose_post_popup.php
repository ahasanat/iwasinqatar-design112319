<!-- compose tool box modal -->
<div id="compose_tool_box" class="modal compose_tool_box post-popup custom_modal main_modal">
   <div class="hidden_header">
      <div class="content_header">
         <button class="close_span cancel_poup waves-effect">
         <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
         </button>
         <p class="modal_header_xs">Write Post</p>
         <a type="button" class="post_btn action_btn post_btn_xs close_modal waves-effect"  onclick="verify()">Post</a>
      </div>
   </div>
   <div class="modal-content">
      <div class="new-post active">
         <div class="top-stuff">
            <!--<div class="side-user">-->
            <div class="postuser-info">
               <span class="img-holder"><img class="circle" src="images/demo-profile.jpg" /></span>
               <div class="desc-holder">
                  <span class="profile_name">Nimish Parekh</span>
                  <label id="tag_person" class="tag_person_new"></label>
                  <div class="public_dropdown_container">
                     <a class="dropdown_text dropdown-button-left normalpostcreateprivacylabel" href="javascript:void(0)" onclick="privacymodal(this)" data-modeltag="normalpostcreateprivacylabel" data-fetch="no" data-label="normalpost">
                        <span id="post_privacy2" class="post_privacy_label">Public</span>
                        <i class="zmdi zmdi-caret-down"></i>
                     </a>
                  </div>
               </div>
            </div>
            <div class="settings-icon">
               <a class="dropdown-button "  href="javascript:void(0)" data-activates="newpost_settings">
               <i class="zmdi zmdi-more"></i>
               </a>
               <ul id="newpost_settings" class="dropdown-content custom_dropdown">
                  <li>
                     <a href="javascript:void(0)">
                     <input type="checkbox" id="toolbox_disable_sharing" />
                     <label for="toolbox_disable_sharing">Disable Sharing</label>
                     </a>
                  </li>
                  <li>
                     <a href="javascript:void(0)">
                     <input type="checkbox" id="toolbox_disable_comments" />
                     <label for="toolbox_disable_comments">Disable Comments</label>
                     </a>
                  </li>
                  <li>
                     <a  onclick="clearPost()">Clear Post</a>
                  </li>
               </ul>
            </div>
         </div>
         <div class="clear"></div>
         <div class="scroll_div">
            <div class="npost-content">
               <div class="post-mcontent">
                  <div class="npost-title title_post_container">									
                     <input type="text" class="title" placeholder="Title of this post">									
                  </div>
                  <div class="clear"></div>
                  <div class="desc">
                     <!--<textarea class="newpost-tt materialize-textarea" placeholder="What's new?"></textarea>-->
                     <textarea id="new_post_comment" placeholder="What's new?" class="materialize-textarea comment_textarea new_post_comment"></textarea>
                  </div>
                  <div class="post-photos">
                     <div class="img-row">
                     </div>
                  </div>
                  <div class="post-tag">
                     <div class="areatitle">With</div>
                     <div class="areadesc">
                        <input type="text" class="ptag" placeholder="Who are you with?"/>
                     </div>
                  </div>
                  <div class="location_parent">
                     <label id="selectedlocation" class="selected_loc"></label>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="modal-footer">
      <div class="new-post active">
         <div class="post-bcontent">
            <div class="footer_icon_container">
               <button class="comment_footer_icon waves-effect" id="compose_uploadphotomodalAction">
               <i class="zmdi zmdi-camera"></i>
               </button>
               <button class="comment_footer_icon waves-effect" id="compose_addpersonAction">
               <i class="zmdi zmdi-account"></i>
               </button>
               <button class="comment_footer_icon waves-effect" data-query="all" onfocus="filderMapLocationModal(this)">
               <i class="zmdi zmdi-pin"></i>
               </button>
               <button class="comment_footer_icon compose_titleAction waves-effect" id="compose_titleAction">
               <img src="images/addtitleBl.png">
               </button>
            </div>
            <div class="public_dropdown_container_xs">
               <a class="dropdown_text dropdown-button-left normalpostcreateprivacylabel" href="javascript:void(0)" onclick="privacymodal(this)" data-modeltag="normalpostcreateprivacylabel" data-fetch="no" data-label="normalpost">
                  <span id="post_privacy2" class="post_privacy_label">Public</span>
                  <i class="zmdi zmdi-caret-down"></i>
               </a>
            </div>
            <div class="post-bholder">
               <div class="post-loader"><img src="images/home-loader.gif"/></div>
               <div class="hidden_xs">
                  <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">cancel</a>
                  <a href="javascript:void(0)" class="btngen-center-align waves-effect btn-flat disabled submit">Post</a>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>