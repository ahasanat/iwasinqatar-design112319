<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" href="images/favicon.ico">
      <title>I was in Qatar</title>
      <link href="css/materialize.css" rel="stylesheet">
      <link href="css/font-awesome.css" rel="stylesheet">
      <link href="css/animate.css" rel="stylesheet"/>
      <link href="css/material-icons.css" rel="stylesheet">
      <link href="css/material-design-iconic-font.css" rel="stylesheet">
      <link href="css/tooltipster.bundle.min.css" rel="stylesheet">
      <link href="css/tooltipster-sideTip-borderless.min.css" rel="stylesheet">
      <link href="css/custom-plugin.css" rel="stylesheet">
      <link href="css/template.css" rel="stylesheet">
      <link href="css/custom-croppie.css" rel="stylesheet">
      <link href="css/master-responsive.css" rel="stylesheet">
      <link href="css/themes.css" rel="stylesheet">
      <link href="css/materialdesignicons.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" media="screen" href="css/all-ie-only.css" />
      <link href="css/justifiedGallery.css" rel="stylesheet" >
      <link href="css/lightgallery.css" rel="stylesheet">
      <link href="css/lg-transitions.css" rel="stylesheet">
      <link href="css/daterangepicker.css" rel="stylesheet">
      <script src="js/jquery.min.js"></script>
      <!--  <script src="js/materialize.js" type="text/javascript" charset="utf-8" ></script> -->
   </head>
   <body class="theme-color">
      <div class="home-page bodydup pageloader">
         <div id="loader-wrapper" class="home_loader">
            <div class="loader-logo">
               <div class="lds-css ng-scope">
                  <div class="lds-rolling lds-rolling100">
                     <div></div>
                  </div>
               </div>
            </div> 
            <!--<div id="loader"></div>-->
            <div class="loader-text">Please wait...</div>
            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
         </div>
         <div class="home-wrapper">
         <header>
            <div class="home-container">
               <div class="mobile-menu topicon">
                  <a href="javascript:void(0)" class="mbl-menuicon"><i class="mdi mdi-menu"></i></a>
               </div>
               <div class="hlogo-holder">
                  <a class="home-logo" href="javascript:void(0)"><img src="images/home-logo.png"></a>                              
               </div>
               <ul class="home-menu">
                  <li><a href="index.php">Home</a></li>
                  <li><a href="discussion.php">Discussion</a></li>
                  <li><a href="photostream.php">Photostream</a></li>
                  <li><a href="blog.php">Blog</a></li>
                  <li><a href="advertisement.php"><i class="zmdi zmdi-plus-square pr-5"></i> Advert</a></li>
               </ul>
               <div class="head-right">
                  <div class="search-part">
                     <a class="homebtn" onclick="flipSectionTo('login');" href="javascript:void(0)">
                     <i class="mdi mdi-lock"></i>
                     <span>Login</span> 
                     </a>
                  </div>
                  <div class="signup-part">
                     <a class="homebtn" onclick="flipSectionTo('login');" href="javascript:void(0)">
                     <i class="mdi mdi-lock"></i>
                     <span>Login</span>
                     </a>
                  </div>
                  <div class="login-part">
                     <a class="homebtn" onclick="flipSectionTo('search');" href="javascript:void(0)">
                     <i class="mdi mdi-close"></i>
                     <span>Close</span>
                     </a>
                  </div>
                  <div class="forgot-part">
                     <a class="homebtn" onclick="flipSectionTo('login');" href="javascript:void(0)">
                     <i class="mdi mdi-lock"></i>
                     <span>Login</span>
                     </a>
                  </div>
               </div>
            </div>
         </header>
         <div class="sidemenu-holder m-hide">
            <div class="sidemenu">
               <a href="javascript:void(0)" class="closemenu waves-effect waves-theme"><i class="mdi mdi-close"></i></a>          
               <div class="side-user">                   
                  Check this out
               </div>
               <div class="sidemenu-ul">
                  <ul class="large-menuicons">
                     <li><a href="index.php"><i class="zmdi zmdi-home"></i> Home</a></li>
                     <li><a href="discussion.php"><i class="zmdi zmdi-account"></i> Discussion</a></li>
                     <li><a href="photostream.php"><i class="zmdi zmdi-image"></i> Photostream</a></li>
                     <li><a href="blog.php"><i class="zmdi zmdi-blogger"></i> Blog</a></li>
                     <li><a href="advertisement.php"><i class="mdi mdi-plus"></i> Advert</a></li>
                  </ul>
               </div>
            </div>
         </div>

         <div class="hcontent-holder banner-section">
            <span class="overlay"></span>
            <div class="home-content">
               <div class="search-part homel-part">
                  <div class="container">
                     <div class="search-box">
                        <div class="box-content">
                           <div class="login-notice">
                              <span class="success-note">Successfully Logged in!</span>
                              <span class="info-note">Fill in the mandatory fields.</span>
                              <span class="error-note">Please Enter Email address and Password</span>
                           </div>
                           <div class="bc-row home-search">
                              <div class="row">
                                 <h3 class="main-header-text white-text expandable-text">Target, one million photos of Qatar by 2022</h3>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="login-part homel-part login-sec">
                  <div class="hidden_header">
                    <div class="content_header">
                      <button class="close_span cancel_poup waves-effect" onclick="flipSectionTo('search');">
                          <i class="mdi mdi-close mdi-20px"></i>
                      </button>
                      <p class="modal_header_xs">Login</p>
                    </div>
                  </div>
                  <div class="container">
                     <div class="homebox login-box animated wow zoomIn" data-wow-duration="1200ms" data-wow-delay="500ms">
                        <div class="sociallink-area">                   
                           <a href="/iwasinqatarcode/frontend/web?r=site/auth&amp;authclient=facebook" class="fb-btn white-text"><span><i class="mdi mdi-facebook"></i></span>Connect with Facebook</a>
                        </div>
                        <div class="sociallink-area">                   
                           <a href="/iwasinqatarcode/frontend/web?r=site/auth&amp;authclient=facebook" class="fb-btn google-connect white-text"><span><i class="mdi mdi-google"></i></span>Connect with Google</a>
                        </div>
                        <div class="sociallink-area">                   
                           <a href="/iwasinqatarcode/frontend/web?r=site/auth&amp;authclient=facebook" class="fb-btn instagram-connect white-text"><span><i class="mdi mdi-instagram"></i></span>Connect with Instagram</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="banner-footer">
               <div class="home-container">
                  <div class="row">
                     <div class="col-lg-12 f-left">
                        <div class="user-count">
                           <p>
                              Upload photos and share your experience
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- <div class="pattern"></div> -->
         </div>
         
         <video autoplay loop class="video-background" muted plays-inline>
           <source src="images/qatartour.mp4" type="video/mp4">
         </video>

         <!-- row2 start -->
         <div class="home-section socially-connected upload-photos">
            <div class="container">
               <div class="section home-row2">
                  <div class="home-title">
                     <h4>Upload Photos</h4>
                     <p>Upload and share your photo to the world</p>
                  </div>
                  <!--Icon Section-->
                  <div class="socials-section socially-connected whitebg">
                     <div class="row">

                        <div class="gallery-content">
                           <div class="lgt-gallery-photo lgt-gallery-justified home-justified-gallery dis-none">
                              <div data-src="images/wgallery1.jpg" class="allow-gallery">
                                   <img class="himg" src="images/wgallery1.jpg"/>   
                                   <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                 <div class="caption">
                                    <div class="left">
                                       <span class="title">Big Bird</span> <br>
                                       <span class="attribution">By Adel Hasanat</span>
                                    </div>
                                    <div class="right icons">
                                       <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                       </a><span class="lcount">7</span>
                                       <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                    </div>
                                 </div>
                              </div>
                              <div data-src="images/wgallery2.jpg" class="allow-gallery">
                                   <img class="himg" src="images/wgallery2.jpg"/>   
                                   <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                 <div class="caption">
                                    <div class="left">
                                       <span class="title">Big Bird</span> <br>
                                       <span class="attribution">By Adel Hasanat</span>
                                    </div>
                                    <div class="right icons">
                                       <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                       </a><span class="lcount">7</span>
                                       <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                    </div>
                                 </div>
                              </div>
                              <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                   <img class="himg" src="images/wgallery3.jpg"/>   
                                   <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                 <div class="caption">
                                    <div class="left">
                                       <span class="title">Big Bird</span> <br>
                                       <span class="attribution">By Adel Hasanat</span>
                                    </div>
                                    <div class="right icons">
                                       <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                       </a><span class="lcount">7</span>
                                       <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                    </div>
                                 </div>
                              </div>
                              <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                   <img class="himg" src="images/wgallery3.jpg"/>   
                                   <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                 <div class="caption">
                                    <div class="left">
                                       <span class="title">Big Bird</span> <br>
                                       <span class="attribution">By Adel Hasanat</span>
                                    </div>
                                    <div class="right icons">
                                       <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                       </a><span class="lcount">7</span>
                                       <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                    </div>
                                 </div>
                              </div>
                              <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                   <img class="himg" src="images/wgallery3.jpg"/>   
                                   <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                 <div class="caption">
                                    <div class="left">
                                       <span class="title">Big Bird</span> <br>
                                       <span class="attribution">By Adel Hasanat</span>
                                    </div>
                                    <div class="right icons">
                                       <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                       </a><span class="lcount">7</span>
                                       <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                    </div>
                                 </div>
                              </div>
                              <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                   <img class="himg" src="images/wgallery3.jpg"/>   
                                   <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                 <div class="caption">
                                    <div class="left">
                                       <span class="title">Big Bird</span> <br>
                                       <span class="attribution">By Adel Hasanat</span>
                                    </div>
                                    <div class="right icons">
                                       <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                       </a><span class="lcount">7</span>
                                       <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                    </div>
                                 </div>
                              </div>
                              <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                   <img class="himg" src="images/wgallery3.jpg"/>   
                                   <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                 <div class="caption">
                                    <div class="left">
                                       <span class="title">Big Bird</span> <br>
                                       <span class="attribution">By Adel Hasanat</span>
                                    </div>
                                    <div class="right icons">
                                       <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                       </a><span class="lcount">7</span>
                                       <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                    </div>
                                 </div>
                              </div>
                              <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                   <img class="himg" src="images/wgallery3.jpg"/>   
                                   <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                 <div class="caption">
                                    <div class="left">
                                       <span class="title">Big Bird</span> <br>
                                       <span class="attribution">By Adel Hasanat</span>
                                    </div>
                                    <div class="right icons">
                                       <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                       </a><span class="lcount">7</span>
                                       <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                    </div>
                                 </div>
                              </div>
                              <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                   <img class="himg" src="images/wgallery3.jpg"/>   
                                   <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                 <div class="caption">
                                    <div class="left">
                                       <span class="title">Big Bird</span> <br>
                                       <span class="attribution">By Adel Hasanat</span>
                                    </div>
                                    <div class="right icons">
                                       <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                       </a><span class="lcount">7</span>
                                       <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                    </div>
                                 </div>
                              </div>
                              <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                   <img class="himg" src="images/wgallery3.jpg"/>   
                                   <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                 <div class="caption">
                                    <div class="left">
                                       <span class="title">Big Bird</span> <br>
                                       <span class="attribution">By Adel Hasanat</span>
                                    </div>
                                    <div class="right icons">
                                       <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                       </a><span class="lcount">7</span>
                                       <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                    </div>
                                 </div>
                              </div>
                              <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                   <img class="himg" src="images/wgallery3.jpg"/>   
                                   <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                 <div class="caption">
                                    <div class="left">
                                       <span class="title">Big Bird</span> <br>
                                       <span class="attribution">By Adel Hasanat</span>
                                    </div>
                                    <div class="right icons">
                                       <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                       </a><span class="lcount">7</span>
                                       <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                    </div>
                                 </div>
                              </div>
                              <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                   <img class="himg" src="images/wgallery3.jpg"/>   
                                   <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                 <div class="caption">
                                    <div class="left">
                                       <span class="title">Big Bird</span> <br>
                                       <span class="attribution">By Adel Hasanat</span>
                                    </div>
                                    <div class="right icons">
                                       <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                       </a><span class="lcount">7</span>
                                       <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                    </div>
                                 </div>
                              </div>
                              <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                   <img class="himg" src="images/wgallery3.jpg"/>   
                                   <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                 <div class="caption">
                                    <div class="left">
                                       <span class="title">Big Bird</span> <br>
                                       <span class="attribution">By Adel Hasanat</span>
                                    </div>
                                    <div class="right icons">
                                       <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                       </a><span class="lcount">7</span>
                                       <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                    </div>
                                 </div>
                              </div>
                              <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                   <img class="himg" src="images/wgallery3.jpg"/>   
                                   <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                 <div class="caption">
                                    <div class="left">
                                       <span class="title">Big Bird</span> <br>
                                       <span class="attribution">By Adel Hasanat</span>
                                    </div>
                                    <div class="right icons">
                                       <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                       </a><span class="lcount">7</span>
                                       <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                    </div>
                                 </div>
                              </div>
                              <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                   <img class="himg" src="images/wgallery3.jpg"/>   
                                   <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                 <div class="caption">
                                    <div class="left">
                                       <span class="title">Big Bird</span> <br>
                                       <span class="attribution">By Adel Hasanat</span>
                                    </div>
                                    <div class="right icons">
                                       <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                       </a><span class="lcount">7</span>
                                       <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                    </div>
                                 </div>
                              </div>
                              <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                   <img class="himg" src="images/wgallery3.jpg"/>   
                                   <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                 <div class="caption">
                                    <div class="left">
                                       <span class="title">Big Bird</span> <br>
                                       <span class="attribution">By Adel Hasanat</span>
                                    </div>
                                    <div class="right icons">
                                       <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                       </a><span class="lcount">7</span>
                                       <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="text-center">
                           <a href="javascript:void(0)" class="btn-custom mb-10 mt-20 white-text">More Photos</a>
                        </div>

                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- row2 end -->

         <div class="qatar-wall">
            <div class="hcontent-holder home-section info-section info1-section qatar-qatar">
               <div class="container">
                  <div class="info-area">
                     <div class="row">
                        <div class="col m6 s12 wow slideInLeft">
                           <div class="home-title">
                              <h4>i Was in Qatar</h4>
                           </div>
                           <p class="para-qatar">
                              I was in Qatar Initiative”. Our goal is to create “Qatar Photo Gallery” with a million photos. We are looking for photos from the beginning of the exploration of Qatar until now. In hope to be listed in 2025 Guinness Book of World Records. The photos collected will be reviewed by an editorial and the best five photos will enter a contest 
                           </p>
                           <a href="javascript:void(0)" class="btn-custom">More</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div class="hcontent-holder home-section info-section info2-section qatar-qatar">
            <div class="container">
               <div class="info-area">
                  <div class="row">
                     <div class="col m6 s12 right qatar">
                        <div class="home-title">
                           <h4>Qatar Blogs</h4>
                        </div>
                        <p class="para-qatar">The entrance to Qatar is a long, winding sandstone canyon known as the Siq (about 2km). There are minor carvings spotted here and there throughout the Siq, but the most impressive sights are the colorful and unusual sandstone patterns in the rock walls. There are also remains of terracotta pipes built into the sides of the canyon that were used in Roman times to carry water.
                           <br /><br />
                           Upon exiting the Siq, visitors can view the jaw-dropping grandeur of the Treasury (al-Khazneh in Arabic). Be sure to note the urn atop the Treasury structure.
                        </p>
                        <a href="javascript:void(0)" class="btn-custom">More Qatar Blogs</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         
         <div class="home-section socially-connected">
            <div class="container">
               <div class="section home-row2">
                  <div class="home-title">
                     <h4>Connection</h4>
                     <p>Learn about your travel destination from other travellers</p>
                  </div>
                  <!--   Icon Section-->
                  <div class="socials-section socially-connected">
                     <div class="row">
                        <div class="col s12 m4 wow animated zoomIn" data-wow-delay="500ms" data-wow-duration="1200ms">
                           <div class="icon-block">
                              <i class="mdi mdi-compass-outline center-block"></i>
                              <div class="descholder">
                                 <h3 class="font-25">Discover</h3>
                                 <h6>What to do</h6>
                                 <p>Find out what to do before you head on your travel destination</p>
                              </div>
                           </div>
                        </div>
                        <div class="col s12 m4 wow animated zoomIn" data-wow-delay="500ms" data-wow-duration="1200ms">
                           <div class="icon-block">
                              <i class="mdi mdi-share-variant center-block"></i>
                              <div class="descholder">
                                 <h3 class="font-25">SHARE</h3>
                                 <h6>YOUR EXPERIENCES</h6>
                                 <p>Share your trip experience and photos with the rest of the travel community</p>
                              </div>
                           </div>
                        </div>
                        <div class="col s12 m4 hidden-sm wow animated zoomIn" data-wow-delay="500ms" data-wow-duration="1200ms">
                           <div class="icon-block">
                              <i class="mdi mdi-account-search center-block"></i>
                              <div class="descholder">
                                 <h3 class="font-25">MEET</h3>
                                 <h6>LIKE-MINDED PEOPLE</h6>
                                 <p>Meet locals or travellers and get recommendations from locals</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div class="hcontent-holder home-section state-section row-icon">
            <div class="container">
               <div class="state-area">
                  <div class="row center">
                     <div class="col m3 s6 wow bounceInUp" data-wow-duration="0.7s" data-wow-delay="0ms">
                        <div class="iconholder">
                           <i class="zmdi zmdi-accounts"></i>
                        </div>
                        <div class="descholder">
                           <h3>62</h3>
                           <p>Travellers</p>
                        </div>
                     </div>
                     <div class="m3 col s6 wow bounceInUp" data-wow-duration="0.7s" data-wow-delay="3ms">
                        <div class="iconholder">
                           <i class="mdi mdi-clipboard"></i>
                        </div>
                        <div class="descholder">
                           <h3>758</h3>
                           <p>Guides</p>
                        </div>
                     </div>
                     <div class="m3 col s6 wow bounceInUp" data-wow-duration="0.7s" data-wow-delay="6ms">
                        <div class="iconholder">
                           <i class="mdi mdi-file-image"></i>
                        </div>
                        <div class="descholder">
                           <h3>357</h3>
                           <p>Photos</p>
                        </div>
                     </div>
                     <div class="m3 col s6 wow bounceInUp" data-wow-duration="0.7s" data-wow-delay="9ms">
                        <div class="iconholder">
                           <i class="zmdi zmdi-check zmdi-hc-lg"></i>
                        </div>
                        <div class="descholder">
                           <h3>29</h3>
                           <p>Locals</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div class="hcontent-holder home-section gray-section tours-page tours dine-local">
            <div class="container mt-10">
               <div class="home-title">
                  <h4>Dine with Locals</h4>
                  <p class="mt-15">Local cooking offered to travellers by local hosts all over the world. Many of us travel because we want to experience local culture, dine with local, eat local food and meet local people. Local hosts share meals and at the same time they share insights to each other's cultures and lives.</p>
               </div>
               <div class="tours-section">
                  <div class="row">
                     <div class="col l4 m6 s12 wow slideInLeft">
                        <div class="tour-box">
                           <span class="imgholder">
                              <img src="images/home-tour1.jpg">
                              <div class="price-tag"><span> €70 </span></div>
                           </span>
                           <span class="descholder">
                              <a href="">
                                 <img src="https://screen-api.eatwith.com/files/478854/-/scale_crop/68x68/center/-/progressive/yes/" alt="">
                              </a>
                              <small class="dine-hosttext">Hosted by <a dir="auto" href="">Tino</a> in Amsterdam</small>
                              <div class="dine-eventtags">
                                 <div class="tag-inner">Dinner</div>
                              </div>
                              <a class="dine-eventtitle" dir="auto" href="">A Seafood Feast with a Venetian Sailor</a>
                              <div class="dine-rating pt-20 center">
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                              </div>
                           </span>
                        </div>
                     </div>
                     <div class="col l4 m6 s12 wow slideInLeft">
                        <div class="tour-box">
                           <span class="imgholder">
                              <img src="images/home-tour1.jpg">
                              <div class="price-tag"><span> €70 </span></div>
                           </span>
                           <span class="descholder">
                              <a href="">
                                 <img src="https://screen-api.eatwith.com/files/478854/-/scale_crop/68x68/center/-/progressive/yes/" alt="">
                              </a>
                              <small class="dine-hosttext">Hosted by <a dir="auto" href="">Tino</a> in Amsterdam</small>
                              <div class="dine-eventtags">
                                 <div class="tag-inner">Dinner</div>
                              </div>
                              <a class="dine-eventtitle" dir="auto" href="">A Seafood Feast with a Venetian Sailor</a>
                              <div class="dine-rating pt-20 center">
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                              </div>
                           </span>
                        </div>
                     </div>
                     <div class="col l4 m6 s12 d-none-sm wow slideInLeft">
                        <div class="tour-box">
                           <span class="imgholder">
                              <img src="images/home-tour1.jpg">
                              <div class="price-tag"><span> €70 </span></div>
                           </span>
                           <span class="descholder">
                              <a href="">
                                 <img src="https://screen-api.eatwith.com/files/478854/-/scale_crop/68x68/center/-/progressive/yes/" alt="">
                              </a>
                              <small class="dine-hosttext">Hosted by <a dir="auto" href="">Tino</a> in Amsterdam</small>
                              <div class="dine-eventtags">
                                 <div class="tag-inner">Dinner</div>
                              </div>
                              <a class="dine-eventtitle" dir="auto" href="">A Seafood Feast with a Venetian Sailor</a>
                              <div class="dine-rating pt-20 center">
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                                 <i class="mdi mdi-star"></i>
                              </div>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div class="hcontent-holder home-section info-section info2-section qatar-qatar stay-withlocal">
            <div class="container">
               <div class="info-area">
                  <div class="row">
                     <div class="col m6 s12 right qatar">
                        <div class="home-title">
                           <h4>Stay with Locals</h4>
                        </div>
                        <p class="para-qatar">Quality and affordability value accommodation option for short or long term stays. Every home has a host present and they do more than just hand over keys. They'll help you settle into life in a new place. stay with locals provide a truly affordable and safe way to stay with locals, get to know and meet locals, learn a new language, cooking and about cultures.
                        <br /><br />
                        Stay with locals will Immerse you in local culture when you are traveling. If you want avoid overcrowded tourist attractions and stay in a way too expensive hotel. then stay with locals is for you!!
                        </p>
                        <a href="javascript:void(0)" class="btn-custom">More stay with Locals Profiles</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div class="china-wall">
            <div class="hcontent-holder home-section info-section info1-section camp-local china-qatar">
               <div class="container">
                  <div class="info-area">
                     <div class="row">
                        <div class="col m6 s12 wow slideInLeft">
                           <div class="home-title">
                              <h4>Camp with Locals</h4>
                           </div>
                           <p class="para-china">Camping season is nearly upon us and you can enjoy some days and nights under the stars with family and friends in the natural wilderness.In addition, you can enjoy a ra
                              nge of activities and approaches to outdoor accommodation such as canoeing, climbing, fishing, and hunting.
                           <br /><br />
                           YOUR NEXT CAMPING ADVENTURE AWAITS YOU.
                           </p>
                           <a href="javascript:void(0)" class="btn-custom white-text">More stay with Locals Profiles</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div class="hcontent-holder home-section gray-section guide local-guide">
            <div class="container mt-10">
               <div class="home-title">
                  <h4>Local Guide</h4>
                  <p class="mt-15">Find a guide to let you see the real beauties of the place you are visiting</p>
               </div>
               <div class="localguide-area pt-20">
                  <div class="row">
                     <div class="col m4 s12 wow slidInLeft" data-wow-duration="2s" data-wow-delay="3ms" >
                        <div class="localguide-box">
                           <div class="imgholder">
                              <img src="images/guide-1.png"/>
                              <div class="overlay">
                                 <span class="licensed-span">Licensed</span>
                              </div>
                           </div>
                           <div class="descholder">
                              <h3>Joseph McGill</h3>
                              <p>Vietnam</p>
                           </div>
                        </div>
                     </div>
                     <div class="col m4 s12 wow  slidInUp" data-wow-duration="2s" data-wow-delay="0ms" >
                        <div class="localguide-box">
                           <div class="imgholder">
                              <img src="images/guide-2.png"/>
                           </div>
                           <div class="descholder">
                              <h3>Mark Doe</h3>
                              <p>London</p>
                           </div>
                        </div>
                     </div>
                     <div class="col m4 s12 hidden-sm wow slidInRight" data-wow-duration="2s" data-wow-delay="3ms" >
                        <div class="localguide-box">
                           <div class="imgholder">
                              <img src="images/guide-3.png"/>
                           </div>
                           <div class="descholder">
                              <h3>John Grainder</h3>
                              <p>South Africa</p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <center>
                  <a href="javascript:void(0)" class="btn-custom mb-10">
                  See more guides that you may hire
                  </a>
               </center>
               </div>
            </div>
         </div>
         
         <div class="hcontent-holder home-section gray-section tours-page tours">
            <div class="container mt-10">
               <div class="home-title">
                  <h4>Qatar Tours</h4>
                  <p class="mt-15">Tours, things to do, sightseeing&nbsp;tours, day trips and more powered by&nbsp;Viator.</p>
               </div>
               <div class="tours-section">
                  <div class="row">
                     <div class="col m4 s12 wow slideInLeft">
                        <div class="tour-box">
                           <span class="imgholder"><img src="images/home-tour1.jpg"></span>
                           <span class="descholder">
                           <span class="head6">Rome, Venice, Florence</span>
                           <span class="head5">2 Day Cappadocia Tour from Istanbul</span>
                           <span class="info">
                           <span class="ratings">
                           <label>45 Reviews</label>
                           <span class="clear"></span>
                           <i class="mdi mdi-star"></i>
                           <i class="mdi mdi-star"></i>
                           <i class="mdi mdi-star"></i>
                           <i class="mdi mdi-star"></i>
                           <i class="mdi mdi-star"></i>
                           </span>
                           <span class="pricing">
                           <span class="currency">From usd</span>
                           <span class="amount">$234.23</span>
                           </span>
                           </span>
                           </span>
                        </div>
                     </div>
                     <div class="col m4 s12 wow slideInUp">
                        <div class="tour-box">
                           <span class="imgholder"><img src="images/home-tour2.jpg"></span>
                           <span class="descholder">
                           <span class="head6">Tours &amp; Sight seeing</span>
                           <span class="head5">Full Day: Classic Istanbul Tour Including Blue Mosque,</span>
                           <span class="info">
                           <span class="ratings">
                           <label>45 Reviews</label>
                           <span class="clear"></span>
                           <i class="mdi mdi-star"></i>
                           <i class="mdi mdi-star"></i>
                           <i class="mdi mdi-star"></i>
                           <i class="mdi mdi-star"></i>
                           <i class="mdi mdi-star"></i>
                           </span>
                           <span class="pricing">
                           <span class="currency">From usd</span>
                           <span class="amount">$234.23</span>
                           </span>
                           </span>
                           </span>
                        </div>
                     </div>
                     <div class="col m4 s12 wow slideInRight">
                        <div class="tour-box">
                           <span class="imgholder"><img src="images/home-tour3.jpg"></span>
                           <span class="descholder">
                           <span class="head6">Cruises, Sailing &amp; Water Tours</span>
                           <span class="head5">Istanbul Bosphorus Cruise with Dinner and Belly-Dancing Show</span>
                           <span class="info">
                           <span class="ratings">
                           <label>45 Reviews</label>
                           <span class="clear"></span>
                           <i class="mdi mdi-star"></i>
                           <i class="mdi mdi-star"></i>
                           <i class="mdi mdi-star"></i>
                           <i class="mdi mdi-star"></i>
                           <i class="mdi mdi-star"></i>
                           </span>
                           <span class="pricing">
                           <span class="currency">From usd</span>
                           <span class="amount">$234.23</span>
                           </span>
                           </span>
                           </span>
                        </div>
                     </div>
                  </div>
                  <a href="javascript:void(0)" class="btn-custom">
                  Checkout more tours and activities
                  </a>
               </div>
            </div>
         </div>
         
         <div class="qatar-siq">
            <div class="hcontent-holder home-section gray-section news-section">
               <div class="container">
                  <div class="feedback-area">
                     <div class="row">
                        <div class="col m4 s12 wow slideInUp">
                           <div class="feedbackbox">
                              <div class="imgholder">
                                 <img src="images/feedback-1.png"/>
                              </div>
                              <div class="descholder">
                                 <p>Travel is all about new experiences. No matter where you're going, Touristlink gives you opportunity to get a real feel of the culture. Meet up with a local for a coffee or beer,</p>
                              </div>
                           </div>
                        </div>
                        <div class="col m4 s12 wow slideInUp">
                           <div class="feedbackbox">
                              <div class="imgholder">
                                 <img src="images/feedback-2.PNG"/>
                              </div>
                              <div class="descholder">
                                 <p>Travel is all about new experiences. No matter where you're going, Touristlink gives you opportunity to get a real feel of the culture. Meet up with a local for a coffee or beer,</p>
                              </div>
                           </div>
                        </div>
                        <div class="col m4 s12 wow slideInUp hidden-xs">
                           <div class="feedbackbox">
                              <div class="imgholder">
                                 <img src="images/feedback-3.PNG"/>
                              </div>
                              <div class="descholder">
                                 <p>Travel is all about new experiences. No matter where you're going, Touristlink gives you opportunity to get a real feel of the culture. Meet up with a local for a coffee or beer,</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="hcontent-holder home-section map-section">
               <iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=qatar%2C%20amman%2C%20qatar&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.bitgeeks.net/embed-google-map/"></a><style>.mapouter{position:relative;text-align:right;height:500px;width:600px;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:600px;}</style>
            </div>
            <footer class="Palsout-footer">
               <div class="footer-cols">
                  <div class="container">
                     <div class="row">
                        <div class="col m6 s12">
                           <h5>Email</h5>
                           <i class="mdi mdi-email white-text"></i>
                           <p>
                              General: <a href="mailto:office@yoursite.com" title="">office@Iwasinqatar.com</a>
                              <br />
                              Support: <a href="mailto:support@example.com" title="">support@Iwasinqatar.com</a>
                           </p>
                        </div>
                        <div class="col m6 s12">
                           <h5 class="">Follow</h5>
                           <i class="mdi mdi-facebook"></i> &nbsp; &nbsp;
                           <i class="mdi mdi-instagram"></i>
                           <p>
                              <a href="www.facebook.com" target="_blank" title="">Find us on Facebook</a>
                              <br />
                              <a href="www.instagram.com" target="_blank" title="">Get us on Instagram</a>
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="copyright">
                  <div class="container">
                     <p>&copy; Iwasinqatar 2019. All rights reserved.</p>
                  </div>
               </div>
            </footer>
         </div>
      </div>
            
      <!-- popups --> 
      <?php include('common/datepicker.php'); ?>

      <div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1 map_modalUniq" style="z-index: 1003; display: none; opacity: 0; transform: scaleX(0.7); top: 8%;">
         <?php include('common/map_modal.php'); ?>
      </div>

      <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"></script>
      <script>
         var wow = new WOW();
         wow.init();
      </script>
      <?php include("script.php"); ?>     
   </body>
</html>


<script>
    if($(window).width() < 767)
    {
      $(".search-part .homebtn").click(function(){
         $("body").addClass("hide_header");
      });
      $(".homes-part .close_span, .homel-part .close_span").click(function(){
         $("body").removeClass("hide_header");
      });      
    } else { 
         $("body").removeClass("hide_header");
    }
   var cropper = $('.cropper');

   if (cropper.length) {
     $.each(cropper, function(i, val) {
       var uploadCrop;

       var readFile = function(input) {
         if (input.files && input.files[i]) {
           var reader = new FileReader();

           reader.onload = function(e) {
             uploadCrop.croppie('bind', {
               url: e.target.result
             });
           };

           reader.readAsDataURL(input.files[i]);
         } else {
           alert("Sorry - you're browser doesn't support the FileReader API");
         }
       };

       uploadCrop = $('.js-cropping').croppie({ // TODO: fix so its selects right element
         viewport: {
           width: 200,
           height: 200
         },
         boundary: {
           width: 375,
           height: 360
         },
         enableOrientation: true
       });

       $('.js-cropper-upload').on('change', function() {
         $('.crop').show(); // TODO: fix so its selects right element
         readFile(this);
       });
       
       $('.js-cropper-rotate--btn').on('click', function(ev) {
         uploadCrop.croppie('rotate', parseInt($(this).data('deg')));
       });

       $('.js-cropper-result--btn').on('click', function(ev) {
         uploadCrop.croppie('result', 'canvas').then(function(resp) {
           popupResult({
             src: resp
           });
         });
       });

       var popupResult = function(result) {
         var html;

         if (result.html) {
           html = result.html;
         }

         if (result.src) {
           html = '<img src="' + result.src + '" />';
         }
         $('.js-cropper-result').show();  
         $('.js-cropper-result').html(html); // TODO: fix so its selects right element
         $('.crop').hide();
         $('.image-upload').show();
       };
     });
   }
   
   
   
   // js to initialize justified gallery
   setTimeout(
     function() 
     {
         $('.home-justified-gallery').justifiedGallery({
            lastRow: 'nojustify',
            rowHeight: 220,
            maxRowHeight: 220,
            margins: 10,
            sizeRangeSuffixes: {
                 lt100: '_t',
                 lt240: '_m',
                 lt320: '_n',
                 lt500: '',
                 lt640: '_z',
                 lt1024: '_b'
            }
         });
         $('.home-justified-gallery').css('opacity', '1');
     }, 8000);
</script>