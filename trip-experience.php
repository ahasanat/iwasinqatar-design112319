<?php include("header.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon">
         <span class="icon-holder ispan">
         <i class="mdi mdi-arrow-up-bold-circle"></i>

         </span>
      </div>
   </div>
</div>
<div class="clear"></div>
<div class="container page_container">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout ipad-mfix hide-addflow">
      <div class="main-content with-lmenu general-page generaldetails-page tripexperience-page main-page split-page">
         <div class="combined-column">
            <div class="content-box">
               <div class="cbox-title nborder">
                  <i class="mdi mdi-file-document"></i>
                  Trip Experience
               </div>
               <div class="right-tabs pagetabs">
                  <ul class="tabs">
                     <li class="tab"><a class="active" href="#tripexperience-all" data-toggle="tab" aria-expanded="true">All</a></li>
                     <li class="tab"><a href="#tripexperience-yours" data-toggle="tab" aria-expanded="false">Yours</a></li>
                     <li class="tab"><a href="#tripexperience-saved" data-toggle="tab" aria-expanded="false">Saved</a></li>
                  </ul>
               </div>
               <div class="cbox-desc">
                  <div class="tab-content view-holder">
                     <div class="tripexperience-details page-details">
                        <div class="row">
                           <div class="tripexperience-summery gdetails-summery">
                              <div class="main-info">
                                 <a href="javascript:void(0)" class="expand-link mbl-filter-icon main-icon" onclick="mbl_mng_drop_searcharea(this,'searcharea1')"><i class="mdi mdi-tune mdi-20px grey-text"></i></a>
                                 <div class="search-area side-area main-search" id="searcharea1">
                                    <a href="javascript:void(0)" class="expand-link" onclick="mng_drop_searcharea(this)">Advanced Search</a>
                                    <div class="expandable-area">
                                       <a href="javascript:void(0)" class="closearea" onclick="mng_drop_searcharea(this)">
                                       <img src="images/cross-icon.png"/>
                                       <span>DONE</span>
                                       </a>
                                       <div class="trip-search">
                                          <h6>Find trip experiences</h6>
                                          <div class="sliding-middle-out anim-area underlined fullwidth">
                                             <input id="autocomplete" name="useraddress" required="" data-query="M" onfocus="filderMapLocationModal(this)" placeholder="Search by city or country" autocomplete="off" type="text" class="fullwidth search_string desktopviewinput">
                                             <input class="form-control mobileviewinput" data-query="M"  onfocus="filderMapLocationModal(this)" id="tripexsearch2" placeholder="Search by city or country" >
                                             <a href="javascript:void(0)" class="searchbtn"><i class="zmdi zmdi-search"></i></a>
                                          </div>
                                       </div>
                                       <div class="trip-listing">
                                          <h5>Checkout Trip Experiences</h5>
                                          <div class="trip-accordion">
                                             <ul class="collapsible" data-collapsible="accordion">
                                                <li>
                                                   <div class="collapsible-header">Europe Trips<i class="mdi mdi-menu-right"></i></div>
                                                   <div class="collapsible-body">
                                                      <div class="trip-acccontent">
                                                         <ul class="trip-sidelist">
                                                            <li><a href="javascript:void(0)">Itali trips <span>( 4 )</span></a></li>
                                                            <li><a href="javascript:void(0)">Germany trips <span>( 2 )</span></a></li>
                                                            <li><a href="javascript:void(0)">Belgium trips <span>( 12 )</span></a></li>
                                                            <li><a href="javascript:void(0)">Norway trips <span>( 3 )</span></a></li>
                                                            <li><a href="javascript:void(0)">Portugal trips <span>( 10 )</span></a></li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li>
                                                   <div class="collapsible-header">Asia Trips<i class="mdi mdi-menu-right"></i></div>
                                                   <div class="collapsible-body">
                                                      <div class="trip-acccontent">
                                                         <ul class="trip-sidelist">
                                                            <li><a href="javascript:void(0)">India trips <span>( 4 )</span></a></li>
                                                            <li><a href="javascript:void(0)">China trips <span>( 2 )</span></a></li>
                                                            <li><a href="javascript:void(0)">Nepal trips <span>( 12 )</span></a></li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li>
                                                   <div class="collapsible-header">Africa trips<i class="mdi mdi-menu-right"></i></div>
                                                   <div class="collapsible-body">
                                                      <div class="trip-acccontent">
                                                         <ul class="trip-sidelist">
                                                            <li><a href="javascript:void(0)">Nigeria trips <span>( 4 )</span></a></li>
                                                            <li><a href="javascript:void(0)">Sudan trips <span>( 2 )</span></a></li>
                                                            <li><a href="javascript:void(0)">Kenya trips <span>( 12 )</span></a></li>
                                                            <li><a href="javascript:void(0)">Egypt trips <span>( 67 )</span></a></li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li>
                                                   <div class="collapsible-header">America trips<i class="mdi mdi-menu-right"></i></div>
                                                   <div class="collapsible-body">
                                                      <div class="trip-acccontent">
                                                         <ul class="trip-sidelist">
                                                            <li><a href="javascript:void(0)">Argentina trips <span>( 4 )</span></a></li>
                                                            <li><a href="javascript:void(0)">Columbia trips <span>( 2 )</span></a></li>
                                                            <li><a href="javascript:void(0)">Canada trips <span>( 12 )</span></a></li>
                                                            <li><a href="javascript:void(0)">Brazil trips <span>( 6 )</span></a></li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="sideboxes">
                                 <?php include('common/recently_joined.php'); ?>
                                 <div class="content-box bshadow">
                                    <div class="cbox-desc">
                                       <div class="side-travad brand-travad">
                                          <div class="travad-maintitle">Best coffee in the world!</div>
                                          <div class="imgholder">
                                             <img src="images/brand-p.jpg">
                                          </div>
                                          <div class="descholder">
                                             <div class="travad-subtitle">We just get new starbucks coffee that is double in caffine that everybody is calling it a boost!</div>
                                             <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn waves-effect waves-light">Explore</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="content-box bshadow">
                                    <div class="side-travad action-travad">
                                       <div class="travad-maintitle">
                                          <span class="iholder"><i class=”mdi mdi-account-group”></i></i></span>
                                          <h6>Heal Well</h6>
                                          <span class="adtext">Sponsored</span>
                                       </div>
                                       <div class="imgholder">
                                          <img src="images/groupad-actionvideo.jpg"/>
                                       </div>
                                       <div class="descholder">
                                          <div class="travad-title">Medical Research Methodolgy</div>
                                          <div class="travad-subtitle">Checkout the new video on our website exploring the latest techniques of medicine research</div>
                                          <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn waves-effect waves-light">Learn More</a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="post-column">
                              <div class="tab-content">
                                 <div class="tab-pane fade main-pane active in" id="tripexperience-all">
                                    <div class="post-list">
                                       <div class="post-holder bshadow">
                                          <div class="post-topbar">
                                             <div class="post-userinfo">
                                                <div class="img-holder">
                                                   <div class="profiletipholder">
                                                      <span class="profile-tooltip">
                                                      <img class="circle" src="images/demo-profile.jpg"/>
                                                      </span>
                                                      <span class="profiletooltip_content slidingpan-holder">
                                                         <div class="profile-tip" style="display:none;">
                                                            <div class="profile-tip-avatar">
                                                               <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                               <div class="sliding-pan location-span">
                                                                  <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                               </div>
                                                            </div>
                                                            <div class="profile-tip-name">
                                                               <a href="javascript:void(0)">Adel Hasanat</a>
                                                            </div>
                                                            <div class="profile-tip-info">
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </span>
                                                   </div>
                                                </div>
                                                <div class="desc-holder">
                                                   <span>By </span><a href="javascript:void(0)">Nimish Parekh</a> <span class="withtext">-- with <a href="javascript:void(0)">Alap Shah</a> and <a href="javascript:void(0)">4 others</a></span>
                                                   <span class="timestamp">2 hrs<span class="glyphicon glyphicon-globe"></span></span>
                                                </div>
                                             </div>
                                             <div class="settings-icon">
                                                <!-- Dropdown Structure -->
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='#' data-activates='post_settings1'>
                                                <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='post_settings1' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a href="javascript:void(0)">Hide experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)" class="savepost-link">Unsave experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Mute notification for this experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Mute connect experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Report experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)" class="composeeditpostAction">Edit experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)" onclick="generateDiscard('dis_delpost')">Delete experience</a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                          <div class="post-content tripexperince-post">
                                             <div class="post-details">
                                                <div class="post-title">
                                                   Visiting Qatar
                                                </div>
                                             </div>
                                             <div class="trip-summery">
                                                <div class="location-info">
                                                   <h5><i class="zmdi zmdi-pin"></i> Qatar</h5>
                                                   <i class="mdi mdi-menu-right"></i>

                                                   <a href="javascript:void(0)" onclick="openViewMap(this)">View on map</a>
                                                </div>
                                             </div>
                                             <div class="map-holder">
                                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3110.3465133386144!2d-9.167423685010494!3d38.77868997958898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd193295d5b45545%3A0x3f9e7b6a5f00e12c!2sPerta!5e0!3m2!1sen!2sin!4v1481089901870" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                             </div>
                                             <div class="post-img-holder">
                                                <div class="lgt-gallery lgt-gallery-photo post-img one-img dis-none">
                                                   <a href="images/post-img.jpg" data-size="1600x1600" data-med="images/post-img.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="pimg-holder himg-box">
                                                   <img class="himg" src="images/post-img.jpg" alt="" />
                                                   </a>
                                                </div>
                                                <a href="javascript:void(0)" class="pinlink"><i class="mdi mdi-nature"></i></a>
                                             </div>
                                             <div class="post-details">
                                                <div class="post-desc">
                                                   <div class="para-section">
                                                      <div class="para">
                                                         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet. Praesent pharetra rutrum eros. In nec nulla id enim faucibus pellentesque quis at dolor. Aenean a cursus quam, et fringilla tellus. Vivamus eu lorem est. Donec non urna sit amet arcu dictum dapibus.</p>
                                                      </div>
                                                      <a href="javascript:void(0)" class="readlink">Read More</a>
                                                   </div>
                                                </div>
                                             </div>
                                             <a class="overlay-link comments-popup popup-modal" href="#postdetail-popup">&nbsp;</a>
                                          </div>
                                          <div class="clear"></div>
                                          <div class="post-data">
                                             <div class="post-actions">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip active" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <i class="zmdi zmdi-thumb-up"></i>
                                                </a><span class="lcount">7</span>
                                                </span>
                                                </span>							
                                                <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a><span class="lcount">4</span>
                                                <a href="javascript:void(0)" class="pa-publish" title="Publish"><i class="zmdi zmdi-upload"></i></a>
                                                <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="lcount">4</span>
                                                <!--<a class="overlay-link comments-popup popup-modal" href="#comments-popup">&nbsp;</a>-->
                                             </div>
                                             <div class="comments-section panel">
                                                <div class="post-more">
                                                   <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-down zmdi-hc-lg"></i></a>
                                                   <span class="total-comments">3 of 7</span>
                                                </div>
                                                <div class="post-comments">
                                                   <div class="pcomments">
                                                      <div class="pcomment-earlier">
                                                         <div class="pcomment-holder">
                                                            <div class="pcomment main-comment">
                                                               <div class="img-holder">
                                                                  <div class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/demo-profile.jpg"/>
                                                                     </span>
                                                                     <span class="profiletooltip_content">
                                                                        <div class="profile-tip" style="display:none;">
                                                                           <div class="slidingpan-holder">
                                                                              <div class="profile-tip-avatar">
                                                                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                                 <div class="sliding-pan location-span">
                                                                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                                 </div>
                                                                              </div>
                                                                              <div class="profile-tip-name">
                                                                                 <a href="javascript:void(0)">Adel Hasanat</a>
                                                                              </div>
                                                                              <div class="profile-tip-info">
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                                 </div>
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                                 </div>
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <div class="normal-mode">
                                                                     <div class="desc">
                                                                        <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="comment-stuff">
                                                                        <div class="more-opt">
                                                                           <span class="likeholder">
                                                                           <span class="like-tooltip">
                                                                           <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                           <i class="zmdi zmdi-thumb-up"></i>
                                                                           </a>
                                                                           </span>
                                                                           </span>	
                                                                           <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                           <!-- Dropdown Trigger -->
                                                                           <a class='dropdown-button more_btn' href="javascript:void(0)" data-activates='drop_down2'>
                                                                           <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                           </a>
                                                                           <!-- Dropdown Structure -->
                                                                           <ul id='drop_down2' class='dropdown-content custom_dropdown'>
                                                                              <li>
                                                                                 <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                              </li>
                                                                              <li>
                                                                                 <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                              </li>
                                                                           </ul>
                                                                        </div>
                                                                        <div class="less-opt">
                                                                           <div class="timestamp">8h</div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="edit-mode">
                                                                     <div class="desc">
                                                                        <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                           <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                        </div>
                                                                        <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="clear"></div>
                                                            <div class="comment-reply-holder comment-addreply">
                                                               <div class="addnew-comment valign-wrapper comment-reply">
                                                                  <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                                  <div class="desc-holder">
                                                                     <div class="sliding-middle-out anim-area tt-holder">
                                                                        <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="pcomment-holder">
                                                            <div class="pcomment main-comment">
                                                               <div class="img-holder">
                                                                  <div class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/demo-profile.jpg"/>
                                                                     </span>
                                                                     <span class="profiletooltip_content">
                                                                        <div class="profile-tip" style="display:none;">
                                                                           <div class="slidingpan-holder">
                                                                              <div class="profile-tip-avatar">
                                                                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                                 <div class="sliding-pan location-span">
                                                                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                                 </div>
                                                                              </div>
                                                                              <div class="profile-tip-name">
                                                                                 <a href="javascript:void(0)">Adel Hasanat</a>
                                                                              </div>
                                                                              <div class="profile-tip-info">
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                                 </div>
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                                 </div>
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <div class="normal-mode">
                                                                     <div class="desc">
                                                                        <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="comment-stuff">
                                                                        <div class="more-opt">
                                                                           <span class="likeholder">
                                                                           <span class="like-tooltip">
                                                                           <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                           <i class="zmdi zmdi-thumb-up"></i>
                                                                           </a>
                                                                           </span>
                                                                           </span>	
                                                                           <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                           <!-- Dropdown Trigger -->
                                                                           <a class='dropdown-button more_btn' href='#' data-activates='drop_down3'>
                                                                           <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                           </a>
                                                                           <!-- Dropdown Structure -->
                                                                           <ul id='drop_down3' class='dropdown-content custom_dropdown'>
                                                                              <li>
                                                                                 <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                              </li>
                                                                              <li>
                                                                                 <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                              </li>
                                                                           </ul>
                                                                        </div>
                                                                        <div class="less-opt">
                                                                           <div class="timestamp">8h</div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="edit-mode">
                                                                     <div class="desc">
                                                                        <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                           <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                        </div>
                                                                        <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="clear"></div>
                                                            <div class="comment-reply-holder comment-addreply">
                                                               <div class="addnew-comment valign-wrapper comment-reply">
                                                                  <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                                  <div class="desc-holder">
                                                                     <div class="sliding-middle-out anim-area tt-holder">
                                                                        <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="pcomment-holder has-comments">
                                                         <div class="pcomment main-comment">
                                                            <div class="img-holder">
                                                               <div class="profiletipholder">
                                                                  <span class="profile-tooltip">
                                                                  <img class="circle" src="images/demo-profile.jpg"/>
                                                                  </span>
                                                                  <span class="profiletooltip_content">
                                                                     <div class="profile-tip" style="display:none;">
                                                                        <div class="slidingpan-holder">
                                                                           <div class="profile-tip-avatar">
                                                                              <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                              <div class="sliding-pan location-span">
                                                                                 <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                              </div>
                                                                           </div>
                                                                           <div class="profile-tip-name">
                                                                              <a href="javascript:void(0)">Adel Hasanat</a>
                                                                           </div>
                                                                           <div class="profile-tip-info">
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                              </div>
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                              </div>
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </span>
                                                               </div>
                                                            </div>
                                                            <div class="desc-holder">
                                                               <div class="normal-mode">
                                                                  <div class="desc">
                                                                     <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                  </div>
                                                                  <div class="comment-stuff">
                                                                     <div class="more-opt">
                                                                        <span class="likeholder">
                                                                        <span class="like-tooltip">
                                                                        <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                        <i class="zmdi zmdi-thumb-up"></i>
                                                                        </a>
                                                                        </span>
                                                                        </span>	
                                                                        <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                        <!-- Dropdown Trigger -->
                                                                        <a class='dropdown-button more_btn' href='#' data-activates='drop_down4'>
                                                                        <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                        </a>
                                                                        <!-- Dropdown Structure -->
                                                                        <ul id='drop_down4' class='dropdown-content custom_dropdown'>
                                                                           <li>
                                                                              <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                           </li>
                                                                           <li>
                                                                              <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                     <div class="less-opt">
                                                                        <div class="timestamp">8h</div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="edit-mode">
                                                                  <div class="desc">
                                                                     <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                        <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                     </div>
                                                                     <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="clear"></div>
                                                         <div class="comment-reply-holder">
                                                            <div class="comments-reply-summery">
                                                               <a href="javascript:void(0)" onclick="openReplies(this)">
                                                               <i class="mdi mdi-share"></i>
                                                               2 Replies													
                                                               </a>
                                                               <i class="mdi mdi-bullseye dot-i"></i>
                                                               Just Now
                                                            </div>
                                                            <div class="comments-reply-details">
                                                               <div class="pcomment comment-reply">
                                                                  <div class="img-holder">
                                                                     <div class="profiletipholder">
                                                                        <span class="profile-tooltip">
                                                                        <img class="circle" src="images/demo-profile.jpg"/>
                                                                        </span>
                                                                        <span class="profiletooltip_content">
                                                                           <div class="profile-tip" style="display:none;">
                                                                              <div class="slidingpan-holder">
                                                                                 <div class="profile-tip-avatar">
                                                                                    <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                                    <div class="sliding-pan location-span">
                                                                                       <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="profile-tip-name">
                                                                                    <a href="javascript:void(0)">Adel Hasanat</a>
                                                                                 </div>
                                                                                 <div class="profile-tip-info">
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                                    </div>
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                                    </div>
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="desc-holder">
                                                                     <div class="normal-mode">
                                                                        <div class="desc">
                                                                           <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                           <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                        </div>
                                                                        <div class="comment-stuff">
                                                                           <div class="more-opt">
                                                                              <span class="likeholder">
                                                                              <span class="like-tooltip">
                                                                              <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                              <i class="zmdi zmdi-thumb-up"></i>
                                                                              </a>
                                                                              </span>
                                                                              </span>	
                                                                              <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                              <!-- Dropdown Trigger -->
                                                                              <a class='dropdown-button more_btn' href='#' data-activates='drop_down5'>
                                                                              <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                              </a>
                                                                              <!-- Dropdown Structure -->
                                                                              <ul id='drop_down5' class='dropdown-content custom_dropdown'>
                                                                                 <li>
                                                                                    <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                                 </li>
                                                                                 <li>
                                                                                    <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                                 </li>
                                                                              </ul>
                                                                           </div>
                                                                           <div class="less-opt">
                                                                              <div class="timestamp">8h</div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="edit-mode">
                                                                        <div class="desc">
                                                                           <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                              <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                           </div>
                                                                           <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="pcomment comment-reply">
                                                                  <div class="img-holder">
                                                                     <div class="profiletipholder">
                                                                        <span class="profile-tooltip">
                                                                        <img class="circle" src="images/demo-profile.jpg"/>
                                                                        </span>
                                                                        <span class="profiletooltip_content">
                                                                           <div class="profile-tip" style="display:none;">
                                                                              <div class="slidingpan-holder">
                                                                                 <div class="profile-tip-avatar">
                                                                                    <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                                    <div class="sliding-pan location-span">
                                                                                       <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="profile-tip-name">
                                                                                    <a href="javascript:void(0)">Adel Hasanat</a>
                                                                                 </div>
                                                                                 <div class="profile-tip-info">
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                                    </div>
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                                    </div>
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="desc-holder">
                                                                     <div class="normal-mode">
                                                                        <div class="desc">
                                                                           <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                           <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                                                        </div>
                                                                        <div class="comment-stuff">
                                                                           <div class="more-opt">
                                                                              <span class="likeholder">
                                                                              <span class="like-tooltip">
                                                                              <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                              <i class="zmdi zmdi-thumb-up"></i>
                                                                              </a>
                                                                              </span>
                                                                              </span>	
                                                                              <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                              <!-- Dropdown Trigger -->
                                                                              <a class='dropdown-button more_btn' href='#' data-activates='drop_down6'>
                                                                              <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                              </a>
                                                                              <!-- Dropdown Structure -->
                                                                              <ul id='drop_down6' class='dropdown-content custom_dropdown'>
                                                                                 <li>
                                                                                    <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                                 </li>
                                                                                 <li>
                                                                                    <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                                 </li>
                                                                              </ul>
                                                                           </div>
                                                                           <div class="less-opt">
                                                                              <div class="timestamp">8h</div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="edit-mode">
                                                                        <div class="desc">
                                                                           <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                              <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                           </div>
                                                                           <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="comment-reply-holder comment-addreply">
                                                            <div class="addnew-comment valign-wrapper comment-reply">
                                                               <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                               <div class="desc-holder">
                                                                  <div class="sliding-middle-out anim-area tt-holder">
                                                                     <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="pcomment-holder">
                                                         <div class="pcomment main-comment">
                                                            <div class="img-holder">
                                                               <div class="profiletipholder">
                                                                  <span class="profile-tooltip">
                                                                  <img class="circle" src="images/demo-profile.jpg"/>
                                                                  </span>
                                                                  <span class="profiletooltip_content">
                                                                     <div class="profile-tip" style="display:none;">
                                                                        <div class="slidingpan-holder">
                                                                           <div class="profile-tip-avatar">
                                                                              <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                              <div class="sliding-pan location-span">
                                                                                 <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                              </div>
                                                                           </div>
                                                                           <div class="profile-tip-name">
                                                                              <a href="javascript:void(0)">Adel Hasanat</a>
                                                                           </div>
                                                                           <div class="profile-tip-info">
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                              </div>
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                              </div>
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </span>
                                                               </div>
                                                            </div>
                                                            <div class="desc-holder">
                                                               <div class="normal-mode">
                                                                  <div class="desc">
                                                                     <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                     <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                                                  </div>
                                                                  <div class="comment-stuff">
                                                                     <div class="more-opt">
                                                                        <span class="likeholder">
                                                                        <span class="like-tooltip">
                                                                        <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                        <i class="zmdi zmdi-thumb-up"></i>			
                                                                        </a>
                                                                        </span>
                                                                        </span>	
                                                                        <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                        <!-- Dropdown Trigger -->
                                                                        <a class='dropdown-button more_btn' href='#' data-activates='drop_down7'>
                                                                        <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                        </a>
                                                                        <!-- Dropdown Structure -->
                                                                        <ul id='drop_down7' class='dropdown-content custom_dropdown'>
                                                                           <li>
                                                                              <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                           </li>
                                                                           <li>
                                                                              <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                     <div class="less-opt">
                                                                        <div class="timestamp">8h</div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="edit-mode">
                                                                  <div class="desc">
                                                                     <div class="sliding-middle-out anim-area tt-holder fullwidth">
                                                                        <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                     </div>
                                                                     <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="clear"></div>
                                                         <div class="comment-reply-holder comment-addreply">
                                                            <div class="addnew-comment valign-wrapper comment-reply">
                                                               <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                               <div class="desc-holder">
                                                                  <div class="sliding-middle-out anim-area tt-holder">
                                                                     <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="addnew-comment valign-wrapper">
                                                      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                      <div class="desc-holder">
                                                         <div class="sliding-middle-out anim-area tt-holder">
                                                            <textarea class="materialize-textarea" id="comment_txt_3" placeholder="Write a comment..."></textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <!--<a class="overlay-link comments-popup popup-modal" href="#comments-popup">&nbsp;</a>-->
                                             </div>
                                          </div>
                                       </div>
                                       <div class="post-holder bshadow">
                                          <div class="post-topbar">
                                             <div class="post-userinfo">
                                                <div class="img-holder">
                                                   <div class="profiletipholder">
                                                      <span class="profile-tooltip">
                                                      <img class="circle" src="images/demo-profile.jpg"/>
                                                      </span>
                                                      <span class="profiletooltip_content slidingpan-holder">
                                                         <div class="profile-tip" style="display:none;">
                                                            <div class="profile-tip-avatar">
                                                               <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                               <div class="sliding-pan location-span">
                                                                  <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                               </div>
                                                            </div>
                                                            <div class="profile-tip-name">
                                                               <a href="javascript:void(0)">Adel Hasanat</a>
                                                            </div>
                                                            <div class="profile-tip-info">
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </span>
                                                   </div>
                                                </div>
                                                <div class="desc-holder">
                                                   <span>By </span><a href="javascript:void(0)">Nimish Parekh</a> <span class="withtext">-- with <a href="javascript:void(0)">Alap Shah</a> and <a href="javascript:void(0)">4 others</a></span>
                                                   <span class="timestamp">2 hrs<span class="glyphicon glyphicon-globe"></span></span>
                                                </div>
                                             </div>
                                             <div class="settings-icon">
                                                <!-- Dropdown Structure -->
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='#' data-activates='post_settings2'>
                                                <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='post_settings2' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a href="javascript:void(0)">Hide experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)" class="savepost-link">Save experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Mute notification for this experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Mute connect experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Report experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)" class="composeeditpostAction">Edit experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)" onclick="generateDiscard('dis_delpost')">Delete experience</a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                          <div class="post-content tripexperince-post">
                                             <div class="post-details">
                                                <div class="post-title">
                                                   Amazing London
                                                </div>
                                             </div>
                                             <div class="trip-summery">
                                                <div class="route-holder">
                                                   <label>Stops :</label>
                                                   <ul class="triproute">
                                                      <li>Edinburgh</li>
                                                      <li>Bath</li>
                                                      <li>Cambridge</li>
                                                   </ul>
                                                </div>
                                                <div class="location-info">
                                                   <h5><i class="zmdi zmdi-pin"></i> London</h5>
                                                   <i class="mdi mdi-menu-right"></i>

                                                   <a href="javascript:void(0)" onclick="openViewMap(this)">View on map</a>
                                                </div>
                                             </div>
                                             <div class="map-holder">
                                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d317718.6932706203!2d-0.3817811797588898!3d51.52830797377821!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d8a00baf21de75%3A0x52963a5addd52a99!2sLondon%2C+UK!5e0!3m2!1sen!2sin!4v1481090949647" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                             </div>
                                             <div class="post-img-holder">
                                                <div class="lgt-gallery lgt-gallery-photo post-img two-img dis-none">
                                                   <a href="images/london1.jpg" data-size="1600x1600" data-med="images/london1.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="pimg-holder himg-box">
                                                   <img class="himg" src="images/london1.jpg" alt="" />
                                                   </a>
                                                   <a href="images/london2.jpg" data-size="1600x1068" data-med="images/london2.jpg" data-med-size="1024x683" data-author="Samuel Rohl" class="pimg-holder vimg-box">
                                                   <img class="himg" src="images/london2.jpg" alt="" />
                                                   </a>
                                                </div>
                                             </div>
                                             <div class="post-details">
                                                <div class="post-desc">
                                                   <div class="para-section">
                                                      <div class="para">
                                                         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet. Praesent pharetra rutrum eros. In nec nulla id enim faucibus pellentesque quis at dolor. Aenean a cursus quam, et fringilla tellus. Vivamus eu lorem est. Donec non urna sit amet arcu dictum dapibus.</p>
                                                      </div>
                                                      <a href="javascript:void(0)" class="readlink">Read More</a>
                                                   </div>
                                                </div>
                                             </div>
                                             <a class="overlay-link comments-popup popup-modal" href="#postdetail-popup">&nbsp;</a>
                                          </div>
                                          <div class="clear"></div>
                                          <div class="post-data">
                                             <div class="post-actions">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip active" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <i class="zmdi zmdi-thumb-up"></i>
                                                </a><span class="lcount">7</span>
                                                </span>
                                                </span>							
                                                <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a><span class="lcount">4</span>
                                                <a href="javascript:void(0)" class="pa-publish" title="Publish"><i class="zmdi zmdi-upload"></i></a>
                                                <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="lcount">4</span>
                                                <!--<a class="overlay-link comments-popup popup-modal" href="#comments-popup">&nbsp;</a>-->
                                             </div>
                                             <div class="comments-section panel">
                                                <div class="post-more">
                                                   <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-down zmdi-hc-lg"></i></a>
                                                   <span class="total-comments">3 of 7</span>
                                                </div>
                                                <div class="post-comments">
                                                   <div class="pcomments">
                                                      <div class="pcomment-earlier">
                                                         <div class="pcomment-holder">
                                                            <div class="pcomment main-comment">
                                                               <div class="img-holder">
                                                                  <div class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/demo-profile.jpg"/>
                                                                     </span>
                                                                     <span class="profiletooltip_content">
                                                                        <div class="profile-tip" style="display:none;">
                                                                           <div class="slidingpan-holder">
                                                                              <div class="profile-tip-avatar">
                                                                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                                 <div class="sliding-pan location-span">
                                                                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                                 </div>
                                                                              </div>
                                                                              <div class="profile-tip-name">
                                                                                 <a href="javascript:void(0)">Adel Hasanat</a>
                                                                              </div>
                                                                              <div class="profile-tip-info">
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                                 </div>
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                                 </div>
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <div class="normal-mode">
                                                                     <div class="desc">
                                                                        <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="comment-stuff">
                                                                        <div class="more-opt">
                                                                           <span class="likeholder">
                                                                           <span class="like-tooltip">
                                                                           <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                           <i class="zmdi zmdi-thumb-up"></i>
                                                                           </a>
                                                                           </span>
                                                                           </span>	
                                                                           <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                           <!-- Dropdown Trigger -->
                                                                           <a class='dropdown-button more_btn' href="javascript:void(0)" data-activates='drop_down2'>
                                                                           <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                           </a>
                                                                           <!-- Dropdown Structure -->
                                                                           <ul id='drop_down2' class='dropdown-content custom_dropdown'>
                                                                              <li>
                                                                                 <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                              </li>
                                                                              <li>
                                                                                 <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                              </li>
                                                                           </ul>
                                                                        </div>
                                                                        <div class="less-opt">
                                                                           <div class="timestamp">8h</div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="edit-mode">
                                                                     <div class="desc">
                                                                        <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                           <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                        </div>
                                                                        <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="clear"></div>
                                                            <div class="comment-reply-holder comment-addreply">
                                                               <div class="addnew-comment valign-wrapper comment-reply">
                                                                  <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                                  <div class="desc-holder">
                                                                     <div class="sliding-middle-out anim-area tt-holder">
                                                                        <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="pcomment-holder">
                                                            <div class="pcomment main-comment">
                                                               <div class="img-holder">
                                                                  <div class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/demo-profile.jpg"/>
                                                                     </span>
                                                                     <span class="profiletooltip_content">
                                                                        <div class="profile-tip" style="display:none;">
                                                                           <div class="slidingpan-holder">
                                                                              <div class="profile-tip-avatar">
                                                                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                                 <div class="sliding-pan location-span">
                                                                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                                 </div>
                                                                              </div>
                                                                              <div class="profile-tip-name">
                                                                                 <a href="javascript:void(0)">Adel Hasanat</a>
                                                                              </div>
                                                                              <div class="profile-tip-info">
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                                 </div>
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                                 </div>
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <div class="normal-mode">
                                                                     <div class="desc">
                                                                        <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="comment-stuff">
                                                                        <div class="more-opt">
                                                                           <span class="likeholder">
                                                                           <span class="like-tooltip">
                                                                           <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                           <i class="zmdi zmdi-thumb-up"></i>
                                                                           </a>
                                                                           </span>
                                                                           </span>	
                                                                           <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                           <!-- Dropdown Trigger -->
                                                                           <a class='dropdown-button more_btn' href='#' data-activates='drop_down3'>
                                                                           <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                           </a>
                                                                           <!-- Dropdown Structure -->
                                                                           <ul id='drop_down3' class='dropdown-content custom_dropdown'>
                                                                              <li>
                                                                                 <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                              </li>
                                                                              <li>
                                                                                 <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                              </li>
                                                                           </ul>
                                                                        </div>
                                                                        <div class="less-opt">
                                                                           <div class="timestamp">8h</div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="edit-mode">
                                                                     <div class="desc">
                                                                        <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                           <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                        </div>
                                                                        <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="clear"></div>
                                                            <div class="comment-reply-holder comment-addreply">
                                                               <div class="addnew-comment valign-wrapper comment-reply">
                                                                  <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                                  <div class="desc-holder">
                                                                     <div class="sliding-middle-out anim-area tt-holder">
                                                                        <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="pcomment-holder has-comments">
                                                         <div class="pcomment main-comment">
                                                            <div class="img-holder">
                                                               <div class="profiletipholder">
                                                                  <span class="profile-tooltip">
                                                                  <img class="circle" src="images/demo-profile.jpg"/>
                                                                  </span>
                                                                  <span class="profiletooltip_content">
                                                                     <div class="profile-tip" style="display:none;">
                                                                        <div class="slidingpan-holder">
                                                                           <div class="profile-tip-avatar">
                                                                              <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                              <div class="sliding-pan location-span">
                                                                                 <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                              </div>
                                                                           </div>
                                                                           <div class="profile-tip-name">
                                                                              <a href="javascript:void(0)">Adel Hasanat</a>
                                                                           </div>
                                                                           <div class="profile-tip-info">
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                              </div>
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                              </div>
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </span>
                                                               </div>
                                                            </div>
                                                            <div class="desc-holder">
                                                               <div class="normal-mode">
                                                                  <div class="desc">
                                                                     <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                  </div>
                                                                  <div class="comment-stuff">
                                                                     <div class="more-opt">
                                                                        <span class="likeholder">
                                                                        <span class="like-tooltip">
                                                                        <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                        <i class="zmdi zmdi-thumb-up"></i>
                                                                        </a>
                                                                        </span>
                                                                        </span>	
                                                                        <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                        <!-- Dropdown Trigger -->
                                                                        <a class='dropdown-button more_btn' href='#' data-activates='drop_down4'>
                                                                        <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                        </a>
                                                                        <!-- Dropdown Structure -->
                                                                        <ul id='drop_down4' class='dropdown-content custom_dropdown'>
                                                                           <li>
                                                                              <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                           </li>
                                                                           <li>
                                                                              <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                     <div class="less-opt">
                                                                        <div class="timestamp">8h</div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="edit-mode">
                                                                  <div class="desc">
                                                                     <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                        <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                     </div>
                                                                     <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="clear"></div>
                                                         <div class="comment-reply-holder">
                                                            <div class="comments-reply-summery">
                                                               <a href="javascript:void(0)" onclick="openReplies(this)">
                                                               <i class="mdi mdi-share"></i>
                                                               2 Replies													
                                                               </a>
                                                               <i class="mdi mdi-bullseye dot-i"></i>
                                                               Just Now
                                                            </div>
                                                            <div class="comments-reply-details">
                                                               <div class="pcomment comment-reply">
                                                                  <div class="img-holder">
                                                                     <div class="profiletipholder">
                                                                        <span class="profile-tooltip">
                                                                        <img class="circle" src="images/demo-profile.jpg"/>
                                                                        </span>
                                                                        <span class="profiletooltip_content">
                                                                           <div class="profile-tip" style="display:none;">
                                                                              <div class="slidingpan-holder">
                                                                                 <div class="profile-tip-avatar">
                                                                                    <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                                    <div class="sliding-pan location-span">
                                                                                       <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="profile-tip-name">
                                                                                    <a href="javascript:void(0)">Adel Hasanat</a>
                                                                                 </div>
                                                                                 <div class="profile-tip-info">
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                                    </div>
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                                    </div>
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="desc-holder">
                                                                     <div class="normal-mode">
                                                                        <div class="desc">
                                                                           <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                           <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                        </div>
                                                                        <div class="comment-stuff">
                                                                           <div class="more-opt">
                                                                              <span class="likeholder">
                                                                              <span class="like-tooltip">
                                                                              <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                              <i class="zmdi zmdi-thumb-up"></i>
                                                                              </a>
                                                                              </span>
                                                                              </span>	
                                                                              <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                              <!-- Dropdown Trigger -->
                                                                              <a class='dropdown-button more_btn' href='#' data-activates='drop_down5'>
                                                                              <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                              </a>
                                                                              <!-- Dropdown Structure -->
                                                                              <ul id='drop_down5' class='dropdown-content custom_dropdown'>
                                                                                 <li>
                                                                                    <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                                 </li>
                                                                                 <li>
                                                                                    <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                                 </li>
                                                                              </ul>
                                                                           </div>
                                                                           <div class="less-opt">
                                                                              <div class="timestamp">8h</div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="edit-mode">
                                                                        <div class="desc">
                                                                           <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                              <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                           </div>
                                                                           <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="pcomment comment-reply">
                                                                  <div class="img-holder">
                                                                     <div class="profiletipholder">
                                                                        <span class="profile-tooltip">
                                                                        <img class="circle" src="images/demo-profile.jpg"/>
                                                                        </span>
                                                                        <span class="profiletooltip_content">
                                                                           <div class="profile-tip" style="display:none;">
                                                                              <div class="slidingpan-holder">
                                                                                 <div class="profile-tip-avatar">
                                                                                    <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                                    <div class="sliding-pan location-span">
                                                                                       <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="profile-tip-name">
                                                                                    <a href="javascript:void(0)">Adel Hasanat</a>
                                                                                 </div>
                                                                                 <div class="profile-tip-info">
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                                    </div>
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                                    </div>
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="desc-holder">
                                                                     <div class="normal-mode">
                                                                        <div class="desc">
                                                                           <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                           <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                                                        </div>
                                                                        <div class="comment-stuff">
                                                                           <div class="more-opt">
                                                                              <span class="likeholder">
                                                                              <span class="like-tooltip">
                                                                              <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                              <i class="zmdi zmdi-thumb-up"></i>
                                                                              </a>
                                                                              </span>
                                                                              </span>	
                                                                              <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                              <!-- Dropdown Trigger -->
                                                                              <a class='dropdown-button more_btn' href='#' data-activates='drop_down6'>
                                                                              <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                              </a>
                                                                              <!-- Dropdown Structure -->
                                                                              <ul id='drop_down6' class='dropdown-content custom_dropdown'>
                                                                                 <li>
                                                                                    <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                                 </li>
                                                                                 <li>
                                                                                    <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                                 </li>
                                                                              </ul>
                                                                           </div>
                                                                           <div class="less-opt">
                                                                              <div class="timestamp">8h</div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="edit-mode">
                                                                        <div class="desc">
                                                                           <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                              <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                           </div>
                                                                           <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="comment-reply-holder comment-addreply">
                                                            <div class="addnew-comment valign-wrapper comment-reply">
                                                               <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                               <div class="desc-holder">
                                                                  <div class="sliding-middle-out anim-area tt-holder">
                                                                     <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="pcomment-holder">
                                                         <div class="pcomment main-comment">
                                                            <div class="img-holder">
                                                               <div class="profiletipholder">
                                                                  <span class="profile-tooltip">
                                                                  <img class="circle" src="images/demo-profile.jpg"/>
                                                                  </span>
                                                                  <span class="profiletooltip_content">
                                                                     <div class="profile-tip" style="display:none;">
                                                                        <div class="slidingpan-holder">
                                                                           <div class="profile-tip-avatar">
                                                                              <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                              <div class="sliding-pan location-span">
                                                                                 <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                              </div>
                                                                           </div>
                                                                           <div class="profile-tip-name">
                                                                              <a href="javascript:void(0)">Adel Hasanat</a>
                                                                           </div>
                                                                           <div class="profile-tip-info">
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                              </div>
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                              </div>
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </span>
                                                               </div>
                                                            </div>
                                                            <div class="desc-holder">
                                                               <div class="normal-mode">
                                                                  <div class="desc">
                                                                     <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                     <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                                                  </div>
                                                                  <div class="comment-stuff">
                                                                     <div class="more-opt">
                                                                        <span class="likeholder">
                                                                        <span class="like-tooltip">
                                                                        <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                        <i class="zmdi zmdi-thumb-up"></i>			
                                                                        </a>
                                                                        </span>
                                                                        </span>	
                                                                        <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                        <!-- Dropdown Trigger -->
                                                                        <a class='dropdown-button more_btn' href='#' data-activates='drop_down7'>
                                                                        <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                        </a>
                                                                        <!-- Dropdown Structure -->
                                                                        <ul id='drop_down7' class='dropdown-content custom_dropdown'>
                                                                           <li>
                                                                              <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                           </li>
                                                                           <li>
                                                                              <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                     <div class="less-opt">
                                                                        <div class="timestamp">8h</div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="edit-mode">
                                                                  <div class="desc">
                                                                     <div class="sliding-middle-out anim-area tt-holder fullwidth">
                                                                        <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                     </div>
                                                                     <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="clear"></div>
                                                         <div class="comment-reply-holder comment-addreply">
                                                            <div class="addnew-comment valign-wrapper comment-reply">
                                                               <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                               <div class="desc-holder">
                                                                  <div class="sliding-middle-out anim-area tt-holder">
                                                                     <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="addnew-comment valign-wrapper">
                                                      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                      <div class="desc-holder">
                                                         <div class="sliding-middle-out anim-area tt-holder">
                                                            <textarea class="materialize-textarea" id="comment_txt_3" placeholder="Write a comment..."></textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="tab-pane fade main-pane" id="tripexperience-yours">
                                    <div class="post-list">
                                       <div class="new-post base-newpost">
                                          <form action="">
                                             <div class="npost-content">
                                                <div class="post-mcontent">
                                                   <i class="mdi mdi-pencil-box-outline main-icon"></i>
                                                   <div class="desc">
                                                      <div class="input-field comments_box">
                                                         <input placeholder="Create trip experience" type="text" class="validate commentmodalAction_form" />
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </form>
                                          <div class="overlay" id="composetoolboxAction"></div>
                                       </div>
                                       <div class="post-holder bshadow">
                                          <div class="post-topbar">
                                             <div class="post-userinfo">
                                                <div class="img-holder">
                                                   <div class="profiletipholder">
                                                      <span class="profile-tooltip">
                                                      <img class="circle" src="images/demo-profile.jpg"/>
                                                      </span>
                                                      <span class="profiletooltip_content slidingpan-holder">
                                                         <div class="profile-tip" style="display:none;">
                                                            <div class="profile-tip-avatar">
                                                               <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                               <div class="sliding-pan location-span">
                                                                  <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                               </div>
                                                            </div>
                                                            <div class="profile-tip-name">
                                                               <a href="javascript:void(0)">Adel Hasanat</a>
                                                            </div>
                                                            <div class="profile-tip-info">
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </span>
                                                   </div>
                                                </div>
                                                <div class="desc-holder">
                                                   <span>By </span><a href="javascript:void(0)">Nimish Parekh</a> <span class="withtext">-- with <a href="javascript:void(0)">Alap Shah</a> and <a href="javascript:void(0)">4 others</a></span>
                                                   <span class="timestamp">2 hrs<span class="glyphicon glyphicon-globe"></span></span>
                                                </div>
                                             </div>
                                             <div class="settings-icon">
                                                <!-- Dropdown Structure -->
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='#' data-activates='post_settings3'>
                                                <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='post_settings3' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a href="javascript:void(0)">Hide experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)" class="savepost-link">Save experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Mute notification for this experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Mute connect experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Report experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)" class="composeeditpostAction">Edit experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)" onclick="generateDiscard('dis_delpost')">Delete experience</a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                          <div class="post-content tripexperince-post">
                                             <div class="post-details">
                                                <div class="post-title">
                                                   Visiting Qatar
                                                </div>
                                             </div>
                                             <div class="trip-summery">
                                                <div class="location-info">
                                                   <h5><i class="zmdi zmdi-pin"></i> Qatar</h5>
                                                   <i class="mdi mdi-menu-right"></i>

                                                   <a href="javascript:void(0)" onclick="openViewMap(this)">View on map</a>
                                                </div>
                                                <div class="views-info">
                                                   <i class=”mdi mdi-eye”></i>45 Views
                                                </div>
                                             </div>
                                             <div class="map-holder">
                                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3110.3465133386144!2d-9.167423685010494!3d38.77868997958898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd193295d5b45545%3A0x3f9e7b6a5f00e12c!2sPerta!5e0!3m2!1sen!2sin!4v1481089901870" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                             </div>
                                             <div class="post-img-holder">
                                                <div class="lgt-gallery lgt-gallery-photo post-img one-img dis-none">
                                                   <a href="images/post-img.jpg" data-size="1600x1600" data-med="images/post-img.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="pimg-holder himg-box">
                                                   <img class="himg" src="images/post-img.jpg" alt="" />
                                                   </a>
                                                </div>
                                                <a href="javascript:void(0)" class="pinlink"><i class="mdi mdi-nature"></i></a>
                                             </div>
                                             <div class="post-details">
                                                <div class="post-desc">
                                                   <div class="para-section">
                                                      <div class="para">
                                                         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet. Praesent pharetra rutrum eros. In nec nulla id enim faucibus pellentesque quis at dolor. Aenean a cursus quam, et fringilla tellus. Vivamus eu lorem est. Donec non urna sit amet arcu dictum dapibus.</p>
                                                      </div>
                                                      <a href="javascript:void(0)" class="readlink">Read More</a>
                                                   </div>
                                                </div>
                                             </div>
                                             <a class="overlay-link comments-popup popup-modal" href="#postdetail-popup">&nbsp;</a>
                                          </div>
                                          <div class="clear"></div>
                                          <div class="post-data">
                                             <div class="post-actions">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip active" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <i class="zmdi zmdi-thumb-up"></i>
                                                </a><span class="lcount">7</span>
                                                </span>
                                                </span>							
                                                <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a><span class="lcount">4</span>
                                                <a href="javascript:void(0)" class="pa-publish" title="Publish"><i class="zmdi zmdi-upload"></i></a>
                                                <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="lcount">4</span>
                                             </div>
                                             <div class="comments-section panel">
                                                <div class="post-more">
                                                   <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-down zmdi-hc-lg"></i></a>
                                                   <span class="total-comments">3 of 7</span>
                                                </div>
                                                <div class="post-comments">
                                                   <div class="pcomments">
                                                      <div class="pcomment-earlier">
                                                         <div class="pcomment-holder">
                                                            <div class="pcomment main-comment">
                                                               <div class="img-holder">
                                                                  <div class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/demo-profile.jpg"/>
                                                                     </span>
                                                                     <span class="profiletooltip_content">
                                                                        <div class="profile-tip" style="display:none;">
                                                                           <div class="slidingpan-holder">
                                                                              <div class="profile-tip-avatar">
                                                                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                                 <div class="sliding-pan location-span">
                                                                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                                 </div>
                                                                              </div>
                                                                              <div class="profile-tip-name">
                                                                                 <a href="javascript:void(0)">Adel Hasanat</a>
                                                                              </div>
                                                                              <div class="profile-tip-info">
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                                 </div>
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                                 </div>
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <div class="normal-mode">
                                                                     <div class="desc">
                                                                        <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="comment-stuff">
                                                                        <div class="more-opt">
                                                                           <span class="likeholder">
                                                                           <span class="like-tooltip">
                                                                           <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                           <i class="zmdi zmdi-thumb-up"></i>
                                                                           </a>
                                                                           </span>
                                                                           </span>	
                                                                           <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                           <!-- Dropdown Trigger -->
                                                                           <a class='dropdown-button more_btn' href="javascript:void(0)" data-activates='drop_down2'>
                                                                           <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                           </a>
                                                                           <!-- Dropdown Structure -->
                                                                           <ul id='drop_down2' class='dropdown-content custom_dropdown'>
                                                                              <li>
                                                                                 <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                              </li>
                                                                              <li>
                                                                                 <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                              </li>
                                                                           </ul>
                                                                        </div>
                                                                        <div class="less-opt">
                                                                           <div class="timestamp">8h</div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="edit-mode">
                                                                     <div class="desc">
                                                                        <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                           <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                        </div>
                                                                        <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="clear"></div>
                                                            <div class="comment-reply-holder comment-addreply">
                                                               <div class="addnew-comment valign-wrapper comment-reply">
                                                                  <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                                  <div class="desc-holder">
                                                                     <div class="sliding-middle-out anim-area tt-holder">
                                                                        <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="pcomment-holder">
                                                            <div class="pcomment main-comment">
                                                               <div class="img-holder">
                                                                  <div class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/demo-profile.jpg"/>
                                                                     </span>
                                                                     <span class="profiletooltip_content">
                                                                        <div class="profile-tip" style="display:none;">
                                                                           <div class="slidingpan-holder">
                                                                              <div class="profile-tip-avatar">
                                                                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                                 <div class="sliding-pan location-span">
                                                                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                                 </div>
                                                                              </div>
                                                                              <div class="profile-tip-name">
                                                                                 <a href="javascript:void(0)">Adel Hasanat</a>
                                                                              </div>
                                                                              <div class="profile-tip-info">
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                                 </div>
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                                 </div>
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <div class="normal-mode">
                                                                     <div class="desc">
                                                                        <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="comment-stuff">
                                                                        <div class="more-opt">
                                                                           <span class="likeholder">
                                                                           <span class="like-tooltip">
                                                                           <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                           <i class="zmdi zmdi-thumb-up"></i>
                                                                           </a>
                                                                           </span>
                                                                           </span>	
                                                                           <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                           <!-- Dropdown Trigger -->
                                                                           <a class='dropdown-button more_btn' href='#' data-activates='drop_down3'>
                                                                           <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                           </a>
                                                                           <!-- Dropdown Structure -->
                                                                           <ul id='drop_down3' class='dropdown-content custom_dropdown'>
                                                                              <li>
                                                                                 <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                              </li>
                                                                              <li>
                                                                                 <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                              </li>
                                                                           </ul>
                                                                        </div>
                                                                        <div class="less-opt">
                                                                           <div class="timestamp">8h</div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="edit-mode">
                                                                     <div class="desc">
                                                                        <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                           <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                        </div>
                                                                        <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="clear"></div>
                                                            <div class="comment-reply-holder comment-addreply">
                                                               <div class="addnew-comment valign-wrapper comment-reply">
                                                                  <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                                  <div class="desc-holder">
                                                                     <div class="sliding-middle-out anim-area tt-holder">
                                                                        <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="pcomment-holder has-comments">
                                                         <div class="pcomment main-comment">
                                                            <div class="img-holder">
                                                               <div class="profiletipholder">
                                                                  <span class="profile-tooltip">
                                                                  <img class="circle" src="images/demo-profile.jpg"/>
                                                                  </span>
                                                                  <span class="profiletooltip_content">
                                                                     <div class="profile-tip" style="display:none;">
                                                                        <div class="slidingpan-holder">
                                                                           <div class="profile-tip-avatar">
                                                                              <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                              <div class="sliding-pan location-span">
                                                                                 <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                              </div>
                                                                           </div>
                                                                           <div class="profile-tip-name">
                                                                              <a href="javascript:void(0)">Adel Hasanat</a>
                                                                           </div>
                                                                           <div class="profile-tip-info">
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                              </div>
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                              </div>
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </span>
                                                               </div>
                                                            </div>
                                                            <div class="desc-holder">
                                                               <div class="normal-mode">
                                                                  <div class="desc">
                                                                     <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                  </div>
                                                                  <div class="comment-stuff">
                                                                     <div class="more-opt">
                                                                        <span class="likeholder">
                                                                        <span class="like-tooltip">
                                                                        <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                        <i class="zmdi zmdi-thumb-up"></i>
                                                                        </a>
                                                                        </span>
                                                                        </span>	
                                                                        <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                        <!-- Dropdown Trigger -->
                                                                        <a class='dropdown-button more_btn' href='#' data-activates='drop_down4'>
                                                                        <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                        </a>
                                                                        <!-- Dropdown Structure -->
                                                                        <ul id='drop_down4' class='dropdown-content custom_dropdown'>
                                                                           <li>
                                                                              <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                           </li>
                                                                           <li>
                                                                              <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                     <div class="less-opt">
                                                                        <div class="timestamp">8h</div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="edit-mode">
                                                                  <div class="desc">
                                                                     <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                        <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                     </div>
                                                                     <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="clear"></div>
                                                         <div class="comment-reply-holder">
                                                            <div class="comments-reply-summery">
                                                               <a href="javascript:void(0)" onclick="openReplies(this)">
                                                               <i class="mdi mdi-share"></i>
                                                               2 Replies													
                                                               </a>
                                                               <i class="mdi mdi-bullseye dot-i"></i>
                                                               Just Now
                                                            </div>
                                                            <div class="comments-reply-details">
                                                               <div class="pcomment comment-reply">
                                                                  <div class="img-holder">
                                                                     <div class="profiletipholder">
                                                                        <span class="profile-tooltip">
                                                                        <img class="circle" src="images/demo-profile.jpg"/>
                                                                        </span>
                                                                        <span class="profiletooltip_content">
                                                                           <div class="profile-tip" style="display:none;">
                                                                              <div class="slidingpan-holder">
                                                                                 <div class="profile-tip-avatar">
                                                                                    <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                                    <div class="sliding-pan location-span">
                                                                                       <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="profile-tip-name">
                                                                                    <a href="javascript:void(0)">Adel Hasanat</a>
                                                                                 </div>
                                                                                 <div class="profile-tip-info">
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                                    </div>
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                                    </div>
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="desc-holder">
                                                                     <div class="normal-mode">
                                                                        <div class="desc">
                                                                           <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                           <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                        </div>
                                                                        <div class="comment-stuff">
                                                                           <div class="more-opt">
                                                                              <span class="likeholder">
                                                                              <span class="like-tooltip">
                                                                              <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                              <i class="zmdi zmdi-thumb-up"></i>
                                                                              </a>
                                                                              </span>
                                                                              </span>	
                                                                              <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                              <!-- Dropdown Trigger -->
                                                                              <a class='dropdown-button more_btn' href='#' data-activates='drop_down5'>
                                                                              <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                              </a>
                                                                              <!-- Dropdown Structure -->
                                                                              <ul id='drop_down5' class='dropdown-content custom_dropdown'>
                                                                                 <li>
                                                                                    <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                                 </li>
                                                                                 <li>
                                                                                    <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                                 </li>
                                                                              </ul>
                                                                           </div>
                                                                           <div class="less-opt">
                                                                              <div class="timestamp">8h</div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="edit-mode">
                                                                        <div class="desc">
                                                                           <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                              <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                           </div>
                                                                           <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="pcomment comment-reply">
                                                                  <div class="img-holder">
                                                                     <div class="profiletipholder">
                                                                        <span class="profile-tooltip">
                                                                        <img class="circle" src="images/demo-profile.jpg"/>
                                                                        </span>
                                                                        <span class="profiletooltip_content">
                                                                           <div class="profile-tip" style="display:none;">
                                                                              <div class="slidingpan-holder">
                                                                                 <div class="profile-tip-avatar">
                                                                                    <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                                    <div class="sliding-pan location-span">
                                                                                       <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="profile-tip-name">
                                                                                    <a href="javascript:void(0)">Adel Hasanat</a>
                                                                                 </div>
                                                                                 <div class="profile-tip-info">
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                                    </div>
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                                    </div>
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="desc-holder">
                                                                     <div class="normal-mode">
                                                                        <div class="desc">
                                                                           <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                           <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                                                        </div>
                                                                        <div class="comment-stuff">
                                                                           <div class="more-opt">
                                                                              <span class="likeholder">
                                                                              <span class="like-tooltip">
                                                                              <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                              <i class="zmdi zmdi-thumb-up"></i>
                                                                              </a>
                                                                              </span>
                                                                              </span>	
                                                                              <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                              <!-- Dropdown Trigger -->
                                                                              <a class='dropdown-button more_btn' href='#' data-activates='drop_down6'>
                                                                              <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                              </a>
                                                                              <!-- Dropdown Structure -->
                                                                              <ul id='drop_down6' class='dropdown-content custom_dropdown'>
                                                                                 <li>
                                                                                    <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                                 </li>
                                                                                 <li>
                                                                                    <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                                 </li>
                                                                              </ul>
                                                                           </div>
                                                                           <div class="less-opt">
                                                                              <div class="timestamp">8h</div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="edit-mode">
                                                                        <div class="desc">
                                                                           <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                              <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                           </div>
                                                                           <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="comment-reply-holder comment-addreply">
                                                            <div class="addnew-comment valign-wrapper comment-reply">
                                                               <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                               <div class="desc-holder">
                                                                  <div class="sliding-middle-out anim-area tt-holder">
                                                                     <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="pcomment-holder">
                                                         <div class="pcomment main-comment">
                                                            <div class="img-holder">
                                                               <div class="profiletipholder">
                                                                  <span class="profile-tooltip">
                                                                  <img class="circle" src="images/demo-profile.jpg"/>
                                                                  </span>
                                                                  <span class="profiletooltip_content">
                                                                     <div class="profile-tip" style="display:none;">
                                                                        <div class="slidingpan-holder">
                                                                           <div class="profile-tip-avatar">
                                                                              <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                              <div class="sliding-pan location-span">
                                                                                 <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                              </div>
                                                                           </div>
                                                                           <div class="profile-tip-name">
                                                                              <a href="javascript:void(0)">Adel Hasanat</a>
                                                                           </div>
                                                                           <div class="profile-tip-info">
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                              </div>
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                              </div>
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </span>
                                                               </div>
                                                            </div>
                                                            <div class="desc-holder">
                                                               <div class="normal-mode">
                                                                  <div class="desc">
                                                                     <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                     <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                                                  </div>
                                                                  <div class="comment-stuff">
                                                                     <div class="more-opt">
                                                                        <span class="likeholder">
                                                                        <span class="like-tooltip">
                                                                        <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                        <i class="zmdi zmdi-thumb-up"></i>			
                                                                        </a>
                                                                        </span>
                                                                        </span>	
                                                                        <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                        <!-- Dropdown Trigger -->
                                                                        <a class='dropdown-button more_btn' href='#' data-activates='drop_down7'>
                                                                        <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                        </a>
                                                                        <!-- Dropdown Structure -->
                                                                        <ul id='drop_down7' class='dropdown-content custom_dropdown'>
                                                                           <li>
                                                                              <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                           </li>
                                                                           <li>
                                                                              <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                     <div class="less-opt">
                                                                        <div class="timestamp">8h</div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="edit-mode">
                                                                  <div class="desc">
                                                                     <div class="sliding-middle-out anim-area tt-holder fullwidth">
                                                                        <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                     </div>
                                                                     <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="clear"></div>
                                                         <div class="comment-reply-holder comment-addreply">
                                                            <div class="addnew-comment valign-wrapper comment-reply">
                                                               <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                               <div class="desc-holder">
                                                                  <div class="sliding-middle-out anim-area tt-holder">
                                                                     <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="addnew-comment valign-wrapper">
                                                      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                      <div class="desc-holder">
                                                         <div class="sliding-middle-out anim-area tt-holder">
                                                            <textarea class="materialize-textarea" id="comment_txt_3" placeholder="Write a comment..."></textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="post-holder bshadow">
                                          <div class="post-topbar">
                                             <div class="post-userinfo">
                                                <div class="img-holder">
                                                   <div class="profiletipholder">
                                                      <span class="profile-tooltip">
                                                      <img class="circle" src="images/demo-profile.jpg"/>
                                                      </span>
                                                      <span class="profiletooltip_content slidingpan-holder">
                                                         <div class="profile-tip" style="display:none;">
                                                            <div class="profile-tip-avatar">
                                                               <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                               <div class="sliding-pan location-span">
                                                                  <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                               </div>
                                                            </div>
                                                            <div class="profile-tip-name">
                                                               <a href="javascript:void(0)">Adel Hasanat</a>
                                                            </div>
                                                            <div class="profile-tip-info">
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </span>
                                                   </div>
                                                </div>
                                                <div class="desc-holder">
                                                   <span>By </span><a href="javascript:void(0)">Nimish Parekh</a> <span class="withtext">-- with <a href="javascript:void(0)">Alap Shah</a> and <a href="javascript:void(0)">4 others</a></span>
                                                   <span class="timestamp">2 hrs<span class="glyphicon glyphicon-globe"></span></span>
                                                </div>
                                             </div>
                                             <div class="settings-icon">
                                                <!-- Dropdown Structure -->
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='#' data-activates='post_settings4'>
                                                <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='post_settings4' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a href="javascript:void(0)">Hide experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)" class="savepost-link">Save experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Mute notification for this experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Mute connect experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Report experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)" class="composeeditpostAction">Edit experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)" onclick="generateDiscard('dis_delpost')">Delete experience</a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                          <div class="post-content tripexperince-post">
                                             <div class="post-details">
                                                <div class="post-title">
                                                   Amazing London
                                                </div>
                                             </div>
                                             <div class="trip-summery">
                                                <div class="route-holder">
                                                   <label>Stops :</label>
                                                   <ul class="triproute">
                                                      <li>Edinburgh</li>
                                                      <li>Bath</li>
                                                      <li>Cambridge</li>
                                                   </ul>
                                                </div>
                                                <div class="location-info">
                                                   <h5><i class="zmdi zmdi-pin"></i> London</h5>
                                                   <i class="mdi mdi-menu-right"></i>

                                                   <a href="javascript:void(0)" onclick="openViewMap(this)">View on map</a>
                                                </div>
                                                <div class="views-info">
                                                   <i class=”mdi mdi-eye”></i>45 Views
                                                </div>
                                             </div>
                                             <div class="map-holder">
                                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d317718.6932706203!2d-0.3817811797588898!3d51.52830797377821!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d8a00baf21de75%3A0x52963a5addd52a99!2sLondon%2C+UK!5e0!3m2!1sen!2sin!4v1481090949647" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                             </div>
                                             <div class="post-img-holder">
                                                <div class="lgt-gallery lgt-gallery-photo post-img two-img dis-none">
                                                   <a href="images/london1.jpg" data-size="1600x1600" data-med="images/london1.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="pimg-holder himg-box">
                                                   <img class="himg" src="images/london1.jpg" alt="" />
                                                   </a>
                                                   <a href="images/london2.jpg" data-size="1600x1068" data-med="images/london2.jpg" data-med-size="1024x683" data-author="Samuel Rohl" class="pimg-holder vimg-box">
                                                   <img class="himg" src="images/london2.jpg" alt="" />
                                                   </a>
                                                </div>
                                             </div>
                                             <div class="post-details">
                                                <div class="post-desc">
                                                   <div class="para-section">
                                                      <div class="para">
                                                         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet. Praesent pharetra rutrum eros. In nec nulla id enim faucibus pellentesque quis at dolor. Aenean a cursus quam, et fringilla tellus. Vivamus eu lorem est. Donec non urna sit amet arcu dictum dapibus.</p>
                                                      </div>
                                                      <a href="javascript:void(0)" class="readlink">Read More</a>
                                                   </div>
                                                </div>
                                             </div>
                                             <a class="overlay-link comments-popup popup-modal" href="#postdetail-popup">&nbsp;</a>
                                          </div>
                                          <div class="clear"></div>
                                          <div class="post-data">
                                             <div class="post-actions">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip active" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <i class="zmdi zmdi-thumb-up"></i>
                                                </a><span class="lcount">7</span>
                                                </span>
                                                </span>							
                                                <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a><span class="lcount">4</span>
                                                <a href="javascript:void(0)" class="pa-publish" title="Publish"><i class="zmdi zmdi-upload"></i></a>
                                                <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="lcount">4</span>
                                             </div>
                                             <div class="comments-section panel">
                                                <div class="post-more">
                                                   <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-down zmdi-hc-lg"></i></a>
                                                   <span class="total-comments">3 of 7</span>
                                                </div>
                                                <div class="post-comments">
                                                   <div class="pcomments">
                                                      <div class="pcomment-earlier">
                                                         <div class="pcomment-holder">
                                                            <div class="pcomment main-comment">
                                                               <div class="img-holder">
                                                                  <div class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/demo-profile.jpg"/>
                                                                     </span>
                                                                     <span class="profiletooltip_content">
                                                                        <div class="profile-tip" style="display:none;">
                                                                           <div class="slidingpan-holder">
                                                                              <div class="profile-tip-avatar">
                                                                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                                 <div class="sliding-pan location-span">
                                                                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                                 </div>
                                                                              </div>
                                                                              <div class="profile-tip-name">
                                                                                 <a href="javascript:void(0)">Adel Hasanat</a>
                                                                              </div>
                                                                              <div class="profile-tip-info">
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                                 </div>
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                                 </div>
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <div class="normal-mode">
                                                                     <div class="desc">
                                                                        <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="comment-stuff">
                                                                        <div class="more-opt">
                                                                           <span class="likeholder">
                                                                           <span class="like-tooltip">
                                                                           <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                           <i class="zmdi zmdi-thumb-up"></i>
                                                                           </a>
                                                                           </span>
                                                                           </span>	
                                                                           <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                           <!-- Dropdown Trigger -->
                                                                           <a class='dropdown-button more_btn' href="javascript:void(0)" data-activates='drop_down2'>
                                                                           <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                           </a>
                                                                           <!-- Dropdown Structure -->
                                                                           <ul id='drop_down2' class='dropdown-content custom_dropdown'>
                                                                              <li>
                                                                                 <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                              </li>
                                                                              <li>
                                                                                 <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                              </li>
                                                                           </ul>
                                                                        </div>
                                                                        <div class="less-opt">
                                                                           <div class="timestamp">8h</div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="edit-mode">
                                                                     <div class="desc">
                                                                        <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                           <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                        </div>
                                                                        <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="clear"></div>
                                                            <div class="comment-reply-holder comment-addreply">
                                                               <div class="addnew-comment valign-wrapper comment-reply">
                                                                  <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                                  <div class="desc-holder">
                                                                     <div class="sliding-middle-out anim-area tt-holder">
                                                                        <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="pcomment-holder">
                                                            <div class="pcomment main-comment">
                                                               <div class="img-holder">
                                                                  <div class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/demo-profile.jpg"/>
                                                                     </span>
                                                                     <span class="profiletooltip_content">
                                                                        <div class="profile-tip" style="display:none;">
                                                                           <div class="slidingpan-holder">
                                                                              <div class="profile-tip-avatar">
                                                                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                                 <div class="sliding-pan location-span">
                                                                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                                 </div>
                                                                              </div>
                                                                              <div class="profile-tip-name">
                                                                                 <a href="javascript:void(0)">Adel Hasanat</a>
                                                                              </div>
                                                                              <div class="profile-tip-info">
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                                 </div>
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                                 </div>
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <div class="normal-mode">
                                                                     <div class="desc">
                                                                        <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="comment-stuff">
                                                                        <div class="more-opt">
                                                                           <span class="likeholder">
                                                                           <span class="like-tooltip">
                                                                           <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                           <i class="zmdi zmdi-thumb-up"></i>
                                                                           </a>
                                                                           </span>
                                                                           </span>	
                                                                           <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                           <!-- Dropdown Trigger -->
                                                                           <a class='dropdown-button more_btn' href='#' data-activates='drop_down3'>
                                                                           <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                           </a>
                                                                           <!-- Dropdown Structure -->
                                                                           <ul id='drop_down3' class='dropdown-content custom_dropdown'>
                                                                              <li>
                                                                                 <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                              </li>
                                                                              <li>
                                                                                 <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                              </li>
                                                                           </ul>
                                                                        </div>
                                                                        <div class="less-opt">
                                                                           <div class="timestamp">8h</div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="edit-mode">
                                                                     <div class="desc">
                                                                        <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                           <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                        </div>
                                                                        <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="clear"></div>
                                                            <div class="comment-reply-holder comment-addreply">
                                                               <div class="addnew-comment valign-wrapper comment-reply">
                                                                  <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                                  <div class="desc-holder">
                                                                     <div class="sliding-middle-out anim-area tt-holder">
                                                                        <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="pcomment-holder has-comments">
                                                         <div class="pcomment main-comment">
                                                            <div class="img-holder">
                                                               <div class="profiletipholder">
                                                                  <span class="profile-tooltip">
                                                                  <img class="circle" src="images/demo-profile.jpg"/>
                                                                  </span>
                                                                  <span class="profiletooltip_content">
                                                                     <div class="profile-tip" style="display:none;">
                                                                        <div class="slidingpan-holder">
                                                                           <div class="profile-tip-avatar">
                                                                              <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                              <div class="sliding-pan location-span">
                                                                                 <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                              </div>
                                                                           </div>
                                                                           <div class="profile-tip-name">
                                                                              <a href="javascript:void(0)">Adel Hasanat</a>
                                                                           </div>
                                                                           <div class="profile-tip-info">
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                              </div>
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                              </div>
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </span>
                                                               </div>
                                                            </div>
                                                            <div class="desc-holder">
                                                               <div class="normal-mode">
                                                                  <div class="desc">
                                                                     <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                  </div>
                                                                  <div class="comment-stuff">
                                                                     <div class="more-opt">
                                                                        <span class="likeholder">
                                                                        <span class="like-tooltip">
                                                                        <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                        <i class="zmdi zmdi-thumb-up"></i>
                                                                        </a>
                                                                        </span>
                                                                        </span>	
                                                                        <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                        <!-- Dropdown Trigger -->
                                                                        <a class='dropdown-button more_btn' href='#' data-activates='drop_down4'>
                                                                        <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                        </a>
                                                                        <!-- Dropdown Structure -->
                                                                        <ul id='drop_down4' class='dropdown-content custom_dropdown'>
                                                                           <li>
                                                                              <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                           </li>
                                                                           <li>
                                                                              <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                     <div class="less-opt">
                                                                        <div class="timestamp">8h</div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="edit-mode">
                                                                  <div class="desc">
                                                                     <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                        <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                     </div>
                                                                     <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="clear"></div>
                                                         <div class="comment-reply-holder">
                                                            <div class="comments-reply-summery">
                                                               <a href="javascript:void(0)" onclick="openReplies(this)">
                                                               <i class="mdi mdi-share"></i>
                                                               2 Replies													
                                                               </a>
                                                               <i class="mdi mdi-bullseye dot-i"></i>
                                                               Just Now
                                                            </div>
                                                            <div class="comments-reply-details">
                                                               <div class="pcomment comment-reply">
                                                                  <div class="img-holder">
                                                                     <div class="profiletipholder">
                                                                        <span class="profile-tooltip">
                                                                        <img class="circle" src="images/demo-profile.jpg"/>
                                                                        </span>
                                                                        <span class="profiletooltip_content">
                                                                           <div class="profile-tip" style="display:none;">
                                                                              <div class="slidingpan-holder">
                                                                                 <div class="profile-tip-avatar">
                                                                                    <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                                    <div class="sliding-pan location-span">
                                                                                       <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="profile-tip-name">
                                                                                    <a href="javascript:void(0)">Adel Hasanat</a>
                                                                                 </div>
                                                                                 <div class="profile-tip-info">
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                                    </div>
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                                    </div>
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="desc-holder">
                                                                     <div class="normal-mode">
                                                                        <div class="desc">
                                                                           <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                           <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                        </div>
                                                                        <div class="comment-stuff">
                                                                           <div class="more-opt">
                                                                              <span class="likeholder">
                                                                              <span class="like-tooltip">
                                                                              <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                              <i class="zmdi zmdi-thumb-up"></i>
                                                                              </a>
                                                                              </span>
                                                                              </span>	
                                                                              <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                              <!-- Dropdown Trigger -->
                                                                              <a class='dropdown-button more_btn' href='#' data-activates='drop_down5'>
                                                                              <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                              </a>
                                                                              <!-- Dropdown Structure -->
                                                                              <ul id='drop_down5' class='dropdown-content custom_dropdown'>
                                                                                 <li>
                                                                                    <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                                 </li>
                                                                                 <li>
                                                                                    <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                                 </li>
                                                                              </ul>
                                                                           </div>
                                                                           <div class="less-opt">
                                                                              <div class="timestamp">8h</div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="edit-mode">
                                                                        <div class="desc">
                                                                           <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                              <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                           </div>
                                                                           <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="pcomment comment-reply">
                                                                  <div class="img-holder">
                                                                     <div class="profiletipholder">
                                                                        <span class="profile-tooltip">
                                                                        <img class="circle" src="images/demo-profile.jpg"/>
                                                                        </span>
                                                                        <span class="profiletooltip_content">
                                                                           <div class="profile-tip" style="display:none;">
                                                                              <div class="slidingpan-holder">
                                                                                 <div class="profile-tip-avatar">
                                                                                    <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                                    <div class="sliding-pan location-span">
                                                                                       <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="profile-tip-name">
                                                                                    <a href="javascript:void(0)">Adel Hasanat</a>
                                                                                 </div>
                                                                                 <div class="profile-tip-info">
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                                    </div>
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                                    </div>
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="desc-holder">
                                                                     <div class="normal-mode">
                                                                        <div class="desc">
                                                                           <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                           <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                                                        </div>
                                                                        <div class="comment-stuff">
                                                                           <div class="more-opt">
                                                                              <span class="likeholder">
                                                                              <span class="like-tooltip">
                                                                              <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                              <i class="zmdi zmdi-thumb-up"></i>
                                                                              </a>
                                                                              </span>
                                                                              </span>	
                                                                              <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                              <!-- Dropdown Trigger -->
                                                                              <a class='dropdown-button more_btn' href='#' data-activates='drop_down6'>
                                                                              <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                              </a>
                                                                              <!-- Dropdown Structure -->
                                                                              <ul id='drop_down6' class='dropdown-content custom_dropdown'>
                                                                                 <li>
                                                                                    <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                                 </li>
                                                                                 <li>
                                                                                    <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                                 </li>
                                                                              </ul>
                                                                           </div>
                                                                           <div class="less-opt">
                                                                              <div class="timestamp">8h</div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="edit-mode">
                                                                        <div class="desc">
                                                                           <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                              <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                           </div>
                                                                           <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="comment-reply-holder comment-addreply">
                                                            <div class="addnew-comment valign-wrapper comment-reply">
                                                               <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                               <div class="desc-holder">
                                                                  <div class="sliding-middle-out anim-area tt-holder">
                                                                     <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="pcomment-holder">
                                                         <div class="pcomment main-comment">
                                                            <div class="img-holder">
                                                               <div class="profiletipholder">
                                                                  <span class="profile-tooltip">
                                                                  <img class="circle" src="images/demo-profile.jpg"/>
                                                                  </span>
                                                                  <span class="profiletooltip_content">
                                                                     <div class="profile-tip" style="display:none;">
                                                                        <div class="slidingpan-holder">
                                                                           <div class="profile-tip-avatar">
                                                                              <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                              <div class="sliding-pan location-span">
                                                                                 <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                              </div>
                                                                           </div>
                                                                           <div class="profile-tip-name">
                                                                              <a href="javascript:void(0)">Adel Hasanat</a>
                                                                           </div>
                                                                           <div class="profile-tip-info">
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                              </div>
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                              </div>
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </span>
                                                               </div>
                                                            </div>
                                                            <div class="desc-holder">
                                                               <div class="normal-mode">
                                                                  <div class="desc">
                                                                     <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                     <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                                                  </div>
                                                                  <div class="comment-stuff">
                                                                     <div class="more-opt">
                                                                        <span class="likeholder">
                                                                        <span class="like-tooltip">
                                                                        <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                        <i class="zmdi zmdi-thumb-up"></i>			
                                                                        </a>
                                                                        </span>
                                                                        </span>	
                                                                        <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                        <!-- Dropdown Trigger -->
                                                                        <a class='dropdown-button more_btn' href='#' data-activates='drop_down7'>
                                                                        <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                        </a>
                                                                        <!-- Dropdown Structure -->
                                                                        <ul id='drop_down7' class='dropdown-content custom_dropdown'>
                                                                           <li>
                                                                              <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                           </li>
                                                                           <li>
                                                                              <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                     <div class="less-opt">
                                                                        <div class="timestamp">8h</div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="edit-mode">
                                                                  <div class="desc">
                                                                     <div class="sliding-middle-out anim-area tt-holder fullwidth">
                                                                        <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                     </div>
                                                                     <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="clear"></div>
                                                         <div class="comment-reply-holder comment-addreply">
                                                            <div class="addnew-comment valign-wrapper comment-reply">
                                                               <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                               <div class="desc-holder">
                                                                  <div class="sliding-middle-out anim-area tt-holder">
                                                                     <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="addnew-comment valign-wrapper">
                                                      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                      <div class="desc-holder">
                                                         <div class="sliding-middle-out anim-area tt-holder">
                                                            <textarea class="materialize-textarea" id="comment_txt_3" placeholder="Write a comment..."></textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <!--<a class="overlay-link comments-popup popup-modal" href="#comments-popup">&nbsp;</a>-->
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="tab-pane fade main-pane active in" id="tripexperience-saved">
                                    <div class="post-list">
                                       <div class="post-holder bshadow">
                                          <div class="post-topbar">
                                             <div class="post-userinfo">
                                                <div class="img-holder">
                                                   <div class="profiletipholder">
                                                      <span class="profile-tooltip">
                                                      <img class="circle" src="images/demo-profile.jpg"/>
                                                      </span>
                                                      <span class="profiletooltip_content slidingpan-holder">
                                                         <div class="profile-tip" style="display:none;">
                                                            <div class="profile-tip-avatar">
                                                               <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                               <div class="sliding-pan location-span">
                                                                  <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                               </div>
                                                            </div>
                                                            <div class="profile-tip-name">
                                                               <a href="javascript:void(0)">Adel Hasanat</a>
                                                            </div>
                                                            <div class="profile-tip-info">
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </span>
                                                   </div>
                                                </div>
                                                <div class="desc-holder">
                                                   <span>By </span><a href="javascript:void(0)">Nimish Parekh</a> <span class="withtext">-- with <a href="javascript:void(0)">Alap Shah</a> and <a href="javascript:void(0)">4 others</a></span>
                                                   <span class="timestamp">2 hrs<span class="glyphicon glyphicon-globe"></span></span>
                                                </div>
                                             </div>
                                             <div class="settings-icon">
                                                <!-- Dropdown Structure -->
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='#' data-activates='post_settings1re'>
                                                <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='post_settings1re' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a href="javascript:void(0)">Hide experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)" class="savepost-link">Unsave experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Mute notification for this experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Mute connect experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Report experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)" class="composeeditpostAction">Edit experience</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)" onclick="generateDiscard('dis_delpost')">Delete experience</a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                          <div class="post-content tripexperince-post">
                                             <div class="post-details">
                                                <div class="post-title">
                                                   Visiting Qatar
                                                </div>
                                             </div>
                                             <div class="trip-summery">
                                                <div class="location-info">
                                                   <h5><i class="zmdi zmdi-pin"></i> Qatar</h5>
                                                   <i class="mdi mdi-menu-right"></i>

                                                   <a href="javascript:void(0)" onclick="openViewMap(this)">View on map</a>
                                                </div>
                                             </div>
                                             <div class="map-holder">
                                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3110.3465133386144!2d-9.167423685010494!3d38.77868997958898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd193295d5b45545%3A0x3f9e7b6a5f00e12c!2sPerta!5e0!3m2!1sen!2sin!4v1481089901870" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                             </div>
                                             <div class="post-img-holder">
                                                <div class="lgt-gallery lgt-gallery-photo post-img one-img dis-none">
                                                   <a href="images/post-img.jpg" data-size="1600x1600" data-med="images/post-img.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="pimg-holder himg-box">
                                                   <img class="himg" src="images/post-img.jpg" alt="" />
                                                   </a>
                                                </div>
                                                <a href="javascript:void(0)" class="pinlink"><i class="mdi mdi-nature"></i></a>
                                             </div>
                                             <div class="post-details">
                                                <div class="post-desc">
                                                   <div class="para-section">
                                                      <div class="para">
                                                         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet. Praesent pharetra rutrum eros. In nec nulla id enim faucibus pellentesque quis at dolor. Aenean a cursus quam, et fringilla tellus. Vivamus eu lorem est. Donec non urna sit amet arcu dictum dapibus.</p>
                                                      </div>
                                                      <a href="javascript:void(0)" class="readlink">Read More</a>
                                                   </div>
                                                </div>
                                             </div>
                                             <a class="overlay-link comments-popup popup-modal" href="#postdetail-popup">&nbsp;</a>
                                          </div>
                                          <div class="clear"></div>
                                          <div class="post-data">
                                             <div class="post-actions">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip active" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <i class="zmdi zmdi-thumb-up"></i>
                                                </a><span class="lcount">7</span>
                                                </span>
                                                </span>							
                                                <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a><span class="lcount">4</span>
                                                <a href="javascript:void(0)" class="pa-publish" title="Publish"><i class="zmdi zmdi-upload"></i></a>
                                                <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="lcount">4</span>
                                             </div>
                                             <div class="comments-section panel">
                                                <div class="post-more">
                                                   <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-down zmdi-hc-lg"></i></a>
                                                   <span class="total-comments">3 of 7</span>
                                                </div>
                                                <div class="post-comments">
                                                   <div class="pcomments">
                                                      <div class="pcomment-earlier">
                                                         <div class="pcomment-holder">
                                                            <div class="pcomment main-comment">
                                                               <div class="img-holder">
                                                                  <div class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/demo-profile.jpg"/>
                                                                     </span>
                                                                     <span class="profiletooltip_content">
                                                                        <div class="profile-tip" style="display:none;">
                                                                           <div class="slidingpan-holder">
                                                                              <div class="profile-tip-avatar">
                                                                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                                 <div class="sliding-pan location-span">
                                                                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                                 </div>
                                                                              </div>
                                                                              <div class="profile-tip-name">
                                                                                 <a href="javascript:void(0)">Adel Hasanat</a>
                                                                              </div>
                                                                              <div class="profile-tip-info">
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                                 </div>
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                                 </div>
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <div class="normal-mode">
                                                                     <div class="desc">
                                                                        <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="comment-stuff">
                                                                        <div class="more-opt">
                                                                           <span class="likeholder">
                                                                           <span class="like-tooltip">
                                                                           <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                           <i class="zmdi zmdi-thumb-up"></i>
                                                                           </a>
                                                                           </span>
                                                                           </span>	
                                                                           <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                           <!-- Dropdown Trigger -->
                                                                           <a class='dropdown-button more_btn' href="javascript:void(0)" data-activates='drop_down2'>
                                                                           <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                           </a>
                                                                           <!-- Dropdown Structure -->
                                                                           <ul id='drop_down2' class='dropdown-content custom_dropdown'>
                                                                              <li>
                                                                                 <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                              </li>
                                                                              <li>
                                                                                 <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                              </li>
                                                                           </ul>
                                                                        </div>
                                                                        <div class="less-opt">
                                                                           <div class="timestamp">8h</div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="edit-mode">
                                                                     <div class="desc">
                                                                        <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                           <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                        </div>
                                                                        <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="clear"></div>
                                                            <div class="comment-reply-holder comment-addreply">
                                                               <div class="addnew-comment valign-wrapper comment-reply">
                                                                  <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                                  <div class="desc-holder">
                                                                     <div class="sliding-middle-out anim-area tt-holder">
                                                                        <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="pcomment-holder">
                                                            <div class="pcomment main-comment">
                                                               <div class="img-holder">
                                                                  <div class="profiletipholder">
                                                                     <span class="profile-tooltip">
                                                                     <img class="circle" src="images/demo-profile.jpg"/>
                                                                     </span>
                                                                     <span class="profiletooltip_content">
                                                                        <div class="profile-tip" style="display:none;">
                                                                           <div class="slidingpan-holder">
                                                                              <div class="profile-tip-avatar">
                                                                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                                 <div class="sliding-pan location-span">
                                                                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                                 </div>
                                                                              </div>
                                                                              <div class="profile-tip-name">
                                                                                 <a href="javascript:void(0)">Adel Hasanat</a>
                                                                              </div>
                                                                              <div class="profile-tip-info">
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                                 </div>
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                                 </div>
                                                                                 <div class="profiletip-icon">
                                                                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                               <div class="desc-holder">
                                                                  <div class="normal-mode">
                                                                     <div class="desc">
                                                                        <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                     </div>
                                                                     <div class="comment-stuff">
                                                                        <div class="more-opt">
                                                                           <span class="likeholder">
                                                                           <span class="like-tooltip">
                                                                           <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                           <i class="zmdi zmdi-thumb-up"></i>
                                                                           </a>
                                                                           </span>
                                                                           </span>	
                                                                           <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                           <!-- Dropdown Trigger -->
                                                                           <a class='dropdown-button more_btn' href='#' data-activates='drop_down3'>
                                                                           <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                           </a>
                                                                           <!-- Dropdown Structure -->
                                                                           <ul id='drop_down3' class='dropdown-content custom_dropdown'>
                                                                              <li>
                                                                                 <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                              </li>
                                                                              <li>
                                                                                 <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                              </li>
                                                                           </ul>
                                                                        </div>
                                                                        <div class="less-opt">
                                                                           <div class="timestamp">8h</div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="edit-mode">
                                                                     <div class="desc">
                                                                        <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                           <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                        </div>
                                                                        <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="clear"></div>
                                                            <div class="comment-reply-holder comment-addreply">
                                                               <div class="addnew-comment valign-wrapper comment-reply">
                                                                  <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                                  <div class="desc-holder">
                                                                     <div class="sliding-middle-out anim-area tt-holder">
                                                                        <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="pcomment-holder has-comments">
                                                         <div class="pcomment main-comment">
                                                            <div class="img-holder">
                                                               <div class="profiletipholder">
                                                                  <span class="profile-tooltip">
                                                                  <img class="circle" src="images/demo-profile.jpg"/>
                                                                  </span>
                                                                  <span class="profiletooltip_content">
                                                                     <div class="profile-tip" style="display:none;">
                                                                        <div class="slidingpan-holder">
                                                                           <div class="profile-tip-avatar">
                                                                              <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                              <div class="sliding-pan location-span">
                                                                                 <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                              </div>
                                                                           </div>
                                                                           <div class="profile-tip-name">
                                                                              <a href="javascript:void(0)">Adel Hasanat</a>
                                                                           </div>
                                                                           <div class="profile-tip-info">
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                              </div>
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                              </div>
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </span>
                                                               </div>
                                                            </div>
                                                            <div class="desc-holder">
                                                               <div class="normal-mode">
                                                                  <div class="desc">
                                                                     <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                  </div>
                                                                  <div class="comment-stuff">
                                                                     <div class="more-opt">
                                                                        <span class="likeholder">
                                                                        <span class="like-tooltip">
                                                                        <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                        <i class="zmdi zmdi-thumb-up"></i>
                                                                        </a>
                                                                        </span>
                                                                        </span>	
                                                                        <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                        <!-- Dropdown Trigger -->
                                                                        <a class='dropdown-button more_btn' href='#' data-activates='drop_down4'>
                                                                        <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                        </a>
                                                                        <!-- Dropdown Structure -->
                                                                        <ul id='drop_down4' class='dropdown-content custom_dropdown'>
                                                                           <li>
                                                                              <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                           </li>
                                                                           <li>
                                                                              <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                     <div class="less-opt">
                                                                        <div class="timestamp">8h</div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="edit-mode">
                                                                  <div class="desc">
                                                                     <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                        <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                     </div>
                                                                     <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="clear"></div>
                                                         <div class="comment-reply-holder">
                                                            <div class="comments-reply-summery">
                                                               <a href="javascript:void(0)" onclick="openReplies(this)">
                                                               <i class="mdi mdi-share"></i>
                                                               2 Replies													
                                                               </a>
                                                               <i class="mdi mdi-bullseye dot-i"></i>
                                                               Just Now
                                                            </div>
                                                            <div class="comments-reply-details">
                                                               <div class="pcomment comment-reply">
                                                                  <div class="img-holder">
                                                                     <div class="profiletipholder">
                                                                        <span class="profile-tooltip">
                                                                        <img class="circle" src="images/demo-profile.jpg"/>
                                                                        </span>
                                                                        <span class="profiletooltip_content">
                                                                           <div class="profile-tip" style="display:none;">
                                                                              <div class="slidingpan-holder">
                                                                                 <div class="profile-tip-avatar">
                                                                                    <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                                    <div class="sliding-pan location-span">
                                                                                       <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="profile-tip-name">
                                                                                    <a href="javascript:void(0)">Adel Hasanat</a>
                                                                                 </div>
                                                                                 <div class="profile-tip-info">
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                                    </div>
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                                    </div>
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="desc-holder">
                                                                     <div class="normal-mode">
                                                                        <div class="desc">
                                                                           <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                           <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                        </div>
                                                                        <div class="comment-stuff">
                                                                           <div class="more-opt">
                                                                              <span class="likeholder">
                                                                              <span class="like-tooltip">
                                                                              <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                              <i class="zmdi zmdi-thumb-up"></i>
                                                                              </a>
                                                                              </span>
                                                                              </span>	
                                                                              <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                              <!-- Dropdown Trigger -->
                                                                              <a class='dropdown-button more_btn' href='#' data-activates='drop_down5'>
                                                                              <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                              </a>
                                                                              <!-- Dropdown Structure -->
                                                                              <ul id='drop_down5' class='dropdown-content custom_dropdown'>
                                                                                 <li>
                                                                                    <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                                 </li>
                                                                                 <li>
                                                                                    <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                                 </li>
                                                                              </ul>
                                                                           </div>
                                                                           <div class="less-opt">
                                                                              <div class="timestamp">8h</div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="edit-mode">
                                                                        <div class="desc">
                                                                           <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                              <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                           </div>
                                                                           <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="pcomment comment-reply">
                                                                  <div class="img-holder">
                                                                     <div class="profiletipholder">
                                                                        <span class="profile-tooltip">
                                                                        <img class="circle" src="images/demo-profile.jpg"/>
                                                                        </span>
                                                                        <span class="profiletooltip_content">
                                                                           <div class="profile-tip" style="display:none;">
                                                                              <div class="slidingpan-holder">
                                                                                 <div class="profile-tip-avatar">
                                                                                    <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                                    <div class="sliding-pan location-span">
                                                                                       <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="profile-tip-name">
                                                                                    <a href="javascript:void(0)">Adel Hasanat</a>
                                                                                 </div>
                                                                                 <div class="profile-tip-info">
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                                    </div>
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                                    </div>
                                                                                    <div class="profiletip-icon">
                                                                                       <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="desc-holder">
                                                                     <div class="normal-mode">
                                                                        <div class="desc">
                                                                           <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                           <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                                                        </div>
                                                                        <div class="comment-stuff">
                                                                           <div class="more-opt">
                                                                              <span class="likeholder">
                                                                              <span class="like-tooltip">
                                                                              <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                              <i class="zmdi zmdi-thumb-up"></i>
                                                                              </a>
                                                                              </span>
                                                                              </span>	
                                                                              <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                              <!-- Dropdown Trigger -->
                                                                              <a class='dropdown-button more_btn' href='#' data-activates='drop_down6'>
                                                                              <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                              </a>
                                                                              <!-- Dropdown Structure -->
                                                                              <ul id='drop_down6' class='dropdown-content custom_dropdown'>
                                                                                 <li>
                                                                                    <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                                 </li>
                                                                                 <li>
                                                                                    <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                                 </li>
                                                                              </ul>
                                                                           </div>
                                                                           <div class="less-opt">
                                                                              <div class="timestamp">8h</div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="edit-mode">
                                                                        <div class="desc">
                                                                           <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                              <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                           </div>
                                                                           <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="comment-reply-holder comment-addreply">
                                                            <div class="addnew-comment valign-wrapper comment-reply">
                                                               <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                               <div class="desc-holder">
                                                                  <div class="sliding-middle-out anim-area tt-holder">
                                                                     <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="pcomment-holder">
                                                         <div class="pcomment main-comment">
                                                            <div class="img-holder">
                                                               <div class="profiletipholder">
                                                                  <span class="profile-tooltip">
                                                                  <img class="circle" src="images/demo-profile.jpg"/>
                                                                  </span>
                                                                  <span class="profiletooltip_content">
                                                                     <div class="profile-tip" style="display:none;">
                                                                        <div class="slidingpan-holder">
                                                                           <div class="profile-tip-avatar">
                                                                              <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                                              <div class="sliding-pan location-span">
                                                                                 <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                              </div>
                                                                           </div>
                                                                           <div class="profile-tip-name">
                                                                              <a href="javascript:void(0)">Adel Hasanat</a>
                                                                           </div>
                                                                           <div class="profile-tip-info">
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                              </div>
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                              </div>
                                                                              <div class="profiletip-icon">
                                                                                 <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </span>
                                                               </div>
                                                            </div>
                                                            <div class="desc-holder">
                                                               <div class="normal-mode">
                                                                  <div class="desc">
                                                                     <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                     <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                                                  </div>
                                                                  <div class="comment-stuff">
                                                                     <div class="more-opt">
                                                                        <span class="likeholder">
                                                                        <span class="like-tooltip">
                                                                        <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                        <i class="zmdi zmdi-thumb-up"></i>			
                                                                        </a>
                                                                        </span>
                                                                        </span>	
                                                                        <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                                        <!-- Dropdown Trigger -->
                                                                        <a class='dropdown-button more_btn' href='#' data-activates='drop_down7'>
                                                                        <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                                        </a>
                                                                        <!-- Dropdown Structure -->
                                                                        <ul id='drop_down7' class='dropdown-content custom_dropdown'>
                                                                           <li>
                                                                              <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                           </li>
                                                                           <li>
                                                                              <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                     <div class="less-opt">
                                                                        <div class="timestamp">8h</div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="edit-mode">
                                                                  <div class="desc">
                                                                     <div class="sliding-middle-out anim-area tt-holder fullwidth">
                                                                        <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                     </div>
                                                                     <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="clear"></div>
                                                         <div class="comment-reply-holder comment-addreply">
                                                            <div class="addnew-comment valign-wrapper comment-reply">
                                                               <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                               <div class="desc-holder">
                                                                  <div class="sliding-middle-out anim-area tt-holder">
                                                                     <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="addnew-comment valign-wrapper">
                                                      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                      <div class="desc-holder">
                                                         <div class="sliding-middle-out anim-area tt-holder">
                                                            <textarea class="materialize-textarea" id="comment_txt_3" placeholder="Write a comment..."></textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php include('common/chat.php'); ?>
      </div>
      <div class="new-post-mobile clear">
         <a class="popup-window composetoolboxAction" href="javascript:void(0)"><i class="mdi mdi-pencil"></i></a>
      </div>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>	 
<?php include('common/compose_post_popup.php'); ?>

<?php  include('common/share_popup.php'); ?>
<?php include('common/editpost_popup.php'); ?>
<?php include('common/discard_popup.php'); ?>

<!--map modal-->
<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">	 
   <?php include('common/map_modal.php'); ?>
</div>
<?php include('common/datepicker.php'); ?>
<?php include("script.php"); ?>
</body>
</html>