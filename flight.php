<?php include("header.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i></span></div>
   </div>
</div>
<div class="clear"></div>
<?php include("common/leftmenu.php"); ?>
<!-- <div class="container page_container"> -->
<div class="fixed-layout">
   <div class="main-content with-lmenu transheader-page tours-page main-page">
      <div class="combined-column wide-open">
         <div class="content-box">
            <div class="banner-section">
               <div class="overlay"></div>
               <h4>Find your ideal flight at lowest price</h4>
               <div class="search-whole hotels-search">
                  <div class="frow searchrow">
                     <div class="row">
                        <div class="col s12 m9 l9 stext-holder">
                           <div class="sliding-middle-out anim-area underlined location fullwidth">
                              <input type="text" placeholder="Enter City i.e. London" class="fullwidth">
                           </div>
                        </div>
                        <div class="col s12 m3 l3 sbtn-holder">
                           <a href="javascript:void(0)" class="btn btn-primary"><i class="zmdi zmdi-search"></i><span>Search</span></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="container valign-wrapper">
               <p class="flow-text mx-auto">Coming Soon!!</p>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- </div> -->
<?php include("common/footer.php"); ?>
</div>	
<?php include("script.php"); ?>
</body>
</html>