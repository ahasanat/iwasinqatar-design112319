<?php include("header.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon">
         <span class="icon-holder ispan">
         <i class="mdi mdi-arrow-up-bold-circle"></i>
         </span>
      </div>
   </div>
</div>
<div class="clear"></div>
<div class="container page_container fulltab">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout ipad-mfix hide-addflow">
      <div class="main-content with-lmenu general-page generaldetails-page commevents-page commevents-details-page main-page">
         <div class="combined-column">
            <div class="content-box">
               <div class="cbox-title nborder">
                  <i class="mdi mdi-calendar"></i>
                  Community Events
                  <a href="community-events.php" class="backbtn"><i class="mdi mdi-menu-left"></i> Back to Community Events</a>
               </div>
               <div class="cbox-desc">
                  <div class="view-holder">
                     <div class="general-details">
                        <div class="gdetails-summery">
                           <div class="main-info">
                              <div class="imgholder">
                                 <img src="images/additem-commevents.png"/>
                                 <div class="back-link">
                                    <a href="community-events.php" class="waves-effect waves-theme"><i class="mdi mdi-arrow-left"></i></a>
                                 </div>
                                 <div class="action-links item_detail_dropdown">
                                    <a href="javascript:void(0)" class="orglink share-it waves-effect waves-theme">
                                    <i class="zmdi zmdi-mail-reply zmdi-hc-lg zmdi-hc-flip-horizontal"></i>
                                    </a>
                                    <div class="settings-icon">
                                       <a class="dropdown-button waves-theme waves-effect" href="javascript:void(0)" data-activates="detail_setting">
                                       <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                       </a>
                                       <ul id="detail_setting" class="dropdown-content custom_dropdown">
                                          <li>
                                             <a href="javascript:void(0)" onclick="openAddItemModal()">Edit Event</a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)" onclick="manageMembers()">Manage Members</a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)" onclick="openMembersList()">Event Attendees</a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)" onclick="PreferenceModel()">Preferences</a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)" onclick="DeleteItem()">Delete Event</a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)" onclick="generateDiscard('dis_unattend')">Leave Event</a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)" onclick="reportAbuseModal()">Report abuse</a>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                              <div class="content-holder dateholder">
                                 <div class="dateinfo">
                                    <span class="month">Nov</span>
                                    <span class="date">17</span>
                                 </div>
                              </div>
                              <div class="content-holder gdetails-moreinfo expandable-holder">
                                 <a href="javascript:void(0)" class="expandable-link invertsign" onclick="mng_expandable(this)"><i class="mdi mdi-chevron-down"></i></a>
                                 <div class="expandable-area">
                                    <h4>International Children Festival</h4>
                                    <div class="member-info">
                                       <span>45 attendees</span>
                                       <span>+</span>
                                       <span>Public</span>
                                    </div>
                                    <div class="tagline">
                                       <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                                    </div>
                                    <div class="action-btns">
                                       <a onclick="commeventsGoing('commevent1',this)">Attend</a>
                                    </div>
                                 </div>
                              </div>
                              <div class="content-holder">
                                 <div class="hori-menus">
                                    <ul class="icon-list tabs tabs-vert">
                                       <li class="tab"><a href="#commevents-about" class="active" data-toggle="tab" aria-expanded="true" onclick="resetDetailTabs(this,'commevents-details','commevents-about')">About <span class="mobile_hide">Event</span><i class="zmdi zmdi-check"></i></a></li>
                                       <li class="tab"><a href="#commevents-discussion" data-toggle="tab" aria-expanded="true" onclick="resetDetailTabs(this,'commevents-details','commevents-discussion')">Discussion<i class="zmdi zmdi-check"></i></a></li>
                                       <li class="tab"><a href="#commevents-maps" data-toggle="tab" aria-expanded="false" onclick="resetDetailTabs(this,'commevents-details','commevents-maps')">Location<i class="zmdi zmdi-check"></i></a></li>
                                       <li class="tab"><a href="#commevents-photos" data-toggle="tab" aria-expanded="true"  onclick="resetDetailTabs(this,'commevents-details','commevents-photos')">Photos<i class="zmdi zmdi-check"></i></a></li>
                                       <li class="tab"><a href="#commevents-invite" data-toggle="tab" aria-expanded="true"  onclick="resetDetailTabs(this,'commevents-details','commevents-invite')">Invite <span class="mobile_hide">People</span><i class="zmdi zmdi-check"></i></a></li>
                                       <li class="tab member-link"><a href="#commevents-attending" class="pl-0" data-toggle="tab" aria-expanded="true"  onclick="resetDetailTabs(this,'commevents-details','commevents-attending')"><span class="mobile_hide">25 people attending</span><span class="show-on-small forcefullyhide">Attendees</span></a></li>
                                    </ul>
                                 </div>
                                 <div class="people">
                                    <div class="plist">
                                       <span><a href="javascript:void(0)"><img src="images/people-1.png"></a></span>
                                       <span><a href="javascript:void(0)"><img src="images/people-2.png"></a></span>
                                       <span><a href="javascript:void(0)"><img src="images/people-3.png"></a></span>
                                       <span><a href="javascript:void(0)"><img src="images/people-1.png"></a></span>
                                       <span><a href="javascript:void(0)"><img src="images/people-2.png"></a></span>
                                       <span><a href="javascript:void(0)"><img src="images/people-3.png"></a></span>
                                       <span><a href="javascript:void(0)"><img src="images/people-1.png"></a></span>
                                       <span><a href="javascript:void(0)"><img src="images/people-2.png"></a></span>
                                       <span><a href="javascript:void(0)"><img src="images/people-3.png"></a></span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="sideboxes">
                              <?php include('common/recently_joined.php'); ?>
                              <div class="content-box bshadow">
                                 <div class="cbox-desc">
                                    <div class="side-travad brand-ad">
                                       <div class="travad-maintitle">Best coffee in the world!</div>
                                       <div class="imgholder">
                                          <img src="images/brand-p.jpg">
                                       </div>
                                       <div class="descholder">
                                          <div class="ad-subtitle">We just get new starbucks coffee that is double in caffine that everybody is calling it a boost!</div>
                                          <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn waves-effect waves-light">Explore</a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="content-box bshadow">
                                 <div class="side-travad action-travad">
                                    <div class="travad-maintitle">
                                       <span class="iholder"><i class=”mdi mdi-account-group”></i></i></span>
                                       <h6>Heal Well</h6>
                                       <span class="adtext">Sponsored</span>
                                    </div>
                                    <div class="imgholder">
                                       <img src="images/groupad-actionvideo.jpg"/>
                                    </div>
                                    <div class="descholder">
                                       <div class="ad-title">Medical Research Methodolgy</div>
                                       <div class="ad-subtitle">Checkout the new video on our website exploring the latest techniques of medicine research</div>
                                       <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn waves-effect waves-light">Learn More</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="post-column">
                           <div class="tab-content">
                              <div class="tab-pane fade main-pane" id="commevents-maps">
                                 <span class="mob-title"><i class="zmdi zmdi-pin"></i>Location</span>
                                 <div class="content-box bshadow inner-content-box">
                                    <div class="cbox-title">
                                       <h5>
                                          <i class="zmdi zmdi-pin"></i>
                                          Location
                                       </h5>
                                    </div>
                                    <div class="cbox-desc">
                                       <div class="map-holder">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="tab-pane fade main-pane" id="commevents-attending">
                                 <span class="mob-title"><i class=”mdi mdi-account-group”></i></i>People attending this event</span>
                                 <div class="content-box bshadow inner-content-box">
                                    <div class="cbox-title">
                                       <h5>
                                          <i class=”mdi mdi-account-group”></i></i>
                                          People attending this event
                                       </h5>
                                    </div>
                                    <div class="cbox-desc">
                                       <div class="likes-summery">
                                          <div class="invite-likes">
                                             <div class="invite-holder">
                                                <div class="list-holder nice-scroll">
                                                   <ul>
                                                      <li>
                                                         <a class="invitelike-connect" href="javascript:void(0)">
                                                            <span class="imgholder"><img src="images/people-1.png"/></span>
                                                            <span class="descholder">
                                                               <h6>Kelly Mark</h6>
                                                            </span>
                                                         </a>
                                                      </li>
                                                      <li>
                                                         <a class="invitelike-connect" href="javascript:void(0)">
                                                            <span class="imgholder"><img src="images/people-2.png"/></span>
                                                            <span class="descholder">
                                                               <h6>John Davior</h6>
                                                            </span>
                                                         </a>
                                                      </li>
                                                      <li>
                                                         <a class="invitelike-connect" href="javascript:void(0)">
                                                            <span class="imgholder"><img src="images/people-2.png"/></span>
                                                            <span class="descholder">
                                                               <h6>Joe Doe</h6>
                                                            </span>
                                                         </a>
                                                      </li>
                                                      <li>
                                                         <a class="invitelike-connect" href="javascript:void(0)">
                                                            <span class="imgholder"><img src="images/people-1.png"/></span>
                                                            <span class="descholder">
                                                               <h6>Kelly Mark</h6>
                                                            </span>
                                                         </a>
                                                      </li>
                                                      <li>
                                                         <a class="invitelike-connect" href="javascript:void(0)">
                                                            <span class="imgholder"><img src="images/people-2.png"/></span>
                                                            <span class="descholder">
                                                               <h6>John Davior</h6>
                                                            </span>
                                                         </a>
                                                      </li>
                                                      <li>
                                                         <a class="invitelike-connect" href="javascript:void(0)">
                                                            <span class="imgholder"><img src="images/people-2.png"/></span>
                                                            <span class="descholder">
                                                               <h6>Joe Doe</h6>
                                                            </span>
                                                         </a>
                                                      </li>
                                                      <li>
                                                         <a class="invitelike-connect" href="javascript:void(0)">
                                                            <span class="imgholder"><img src="images/people-1.png"/></span>
                                                            <span class="descholder">
                                                               <h6>Kelly Mark</h6>
                                                            </span>
                                                         </a>
                                                      </li>
                                                      <li>
                                                         <a class="invitelike-connect" href="javascript:void(0)">
                                                            <span class="imgholder"><img src="images/people-2.png"/></span>
                                                            <span class="descholder">
                                                               <h6>John Davior</h6>
                                                            </span>
                                                         </a>
                                                      </li>
                                                      <li>
                                                         <a class="invitelike-connect" href="javascript:void(0)">
                                                            <span class="imgholder"><img src="images/people-2.png"/></span>
                                                            <span class="descholder">
                                                               <h6>Joe Doe</h6>
                                                            </span>
                                                         </a>
                                                      </li>
                                                      <li>
                                                         <a class="invitelike-connect" href="javascript:void(0)">
                                                            <span class="imgholder"><img src="images/people-1.png"/></span>
                                                            <span class="descholder">
                                                               <h6>Kelly Mark</h6>
                                                            </span>
                                                         </a>
                                                      </li>
                                                      <li>
                                                         <a class="invitelike-connect" href="javascript:void(0)">
                                                            <span class="imgholder"><img src="images/people-2.png"/></span>
                                                            <span class="descholder">
                                                               <h6>John Davior</h6>
                                                            </span>
                                                         </a>
                                                      </li>
                                                      <li>
                                                         <a class="invitelike-connect" href="javascript:void(0)">
                                                            <span class="imgholder"><img src="images/people-2.png"/></span>
                                                            <span class="descholder">
                                                               <h6>Joe Doe</h6>
                                                            </span>
                                                         </a>
                                                      </li>
                                                      <li>
                                                         <a class="invitelike-connect" href="javascript:void(0)">
                                                            <span class="imgholder"><img src="images/people-1.png"/></span>
                                                            <span class="descholder">
                                                               <h6>Kelly Mark</h6>
                                                            </span>
                                                         </a>
                                                      </li>
                                                      <li>
                                                         <a class="invitelike-connect" href="javascript:void(0)">
                                                            <span class="imgholder"><img src="images/people-2.png"/></span>
                                                            <span class="descholder">
                                                               <h6>John Davior</h6>
                                                            </span>
                                                         </a>
                                                      </li>
                                                      <li>
                                                         <a class="invitelike-connect" href="javascript:void(0)">
                                                            <span class="imgholder"><img src="images/people-2.png"/></span>
                                                            <span class="descholder">
                                                               <h6>Joe Doe</h6>
                                                            </span>
                                                         </a>
                                                      </li>
                                                      <li>
                                                         <a class="invitelike-connect" href="javascript:void(0)">
                                                            <span class="imgholder"><img src="images/people-1.png"/></span>
                                                            <span class="descholder">
                                                               <h6>Kelly Mark</h6>
                                                            </span>
                                                         </a>
                                                      </li>
                                                      <li>
                                                         <a class="invitelike-connect" href="javascript:void(0)">
                                                            <span class="imgholder"><img src="images/people-2.png"/></span>
                                                            <span class="descholder">
                                                               <h6>John Davior</h6>
                                                            </span>
                                                         </a>
                                                      </li>
                                                      <li>
                                                         <a class="invitelike-connect" href="javascript:void(0)">
                                                            <span class="imgholder"><img src="images/people-2.png"/></span>
                                                            <span class="descholder">
                                                               <h6>Joe Doe</h6>
                                                            </span>
                                                         </a>
                                                      </li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="tab-pane fade main-pane" id="commevents-photos">
                                 <span class="mob-title"><i class="mdi mdi-file-image"></i>Photos</span>
                                 <div class="content-box bshadow inner-content-box">
                                    <div class="cbox-title">
                                       <h5>
                                          <i class="mdi mdi-file-image"></i>
                                          Photos
                                       </h5>
                                    </div>
                                    <div class="cbox-desc">
                                       <div class="albums-grid images-container">
                                          <div class="row">
                                             <div class="grid-box">
                                                <div class="divrel">																	
                                                   <a href="javascript:void(0)" id="add-photo-photos" class="add-photo">
                                                   <span class="icont">+</span>
                                                   Add New Photo
                                                   </a>
                                                   <input type="file" name="upload" class="hidden_uploader" title="Choose a file to upload" required data-class="#add-photo-popup .post-photos .img-row" multiple/>
                                                </div>
                                             </div>
                                             <div class="grid-box">
                                                <div class="photo-box">
                                                   <div class="imgholder">
                                                      <figure>
                                                         <a href="images/post-img1.jpg" data-size="1600x1600" data-med="images/post-img1.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="himg-box">
                                                         <img class="himg" src="images/post-img1.jpg"/>
                                                         </a>																		
                                                      </figure>
                                                   </div>
                                                   <div class="edit-link">
                                                      <div class="dropdown dropdown-custom ">
                                                         <a class="dropdown-button " href="javascript:void(0)" data-activates="album_setting1">
                                                         <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                                         </a>
                                                         <ul id="album_setting1" class="dropdown-small dropdown-content custom_dropdown ">
                                                            <li><a href="javascript:void(0)">Delete this photo</a></li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                   <div class="descholder">
                                                      <a href="javascript:void(0)" class="namelink"><span>Album Name</span></a>
                                                      <div class="options">
                                                         <a href="javascript:void(0)">Like</a>	
                                                         <div class="info">			
                                                            <a href="javascript:void(0)" class="view-likes" data-id='photo-1' data-section='photos'><span class="glyphicon glyphicon-thumbs-up"></span>56</a>			
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="grid-box">
                                                <div class="photo-box">
                                                   <div class="imgholder">
                                                      <figure>
                                                         <a href="images/album2.png" data-size="1600x1600" data-med="images/album2.png" data-med-size="1024x1024" data-author="Folkert Gorter" class="himg-box">
                                                         <img class="himg" src="images/album2.png"/>
                                                         </a>																			
                                                      </figure>
                                                   </div>
                                                   <div class="edit-link">
                                                      <div class="dropdown dropdown-custom ">
                                                         <a class="dropdown-button " href="javascript:void(0)" data-activates="album_setting2">
                                                         <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                                         </a>
                                                         <ul id="album_setting2" class="dropdown-small dropdown-content custom_dropdown ">
                                                            <li><a href="javascript:void(0)">Delete this photo</a></li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                   <div class="descholder">
                                                      <a href="javascript:void(0)" class="namelink"><span>Album Name</span></a>
                                                      <div class="options">
                                                         <a href="javascript:void(0)">Like</a>	
                                                         <div class="info">																	
                                                            <a href="javascript:void(0)" class="view-likes" data-id='photo-1' data-section='photos'><span class="glyphicon glyphicon-thumbs-up"></span>56</a>			
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="grid-box">
                                                <div class="photo-box">
                                                   <div class="imgholder">
                                                      <figure>
                                                         <a href="images/post-img5.jpg" data-size="1600x1600" data-med="images/post-img5.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="vimg-box">
                                                         <img class="vimg" src="images/post-img5.jpg"/>
                                                         </a>
                                                      </figure>
                                                   </div>
                                                   <div class="edit-link">
                                                      <div class="dropdown dropdown-custom ">
                                                         <a class="dropdown-button " href="javascript:void(0)" data-activates="album_setting3">
                                                         <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                                         </a>
                                                         <ul id="album_setting3" class="dropdown-small dropdown-content custom_dropdown ">
                                                            <li><a href="javascript:void(0)">Delete this photo</a></li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                   <div class="descholder">
                                                      <a href="javascript:void(0)" class="namelink"><span>Album Name</span></a>
                                                      <div class="options">
                                                         <a href="javascript:void(0)">Like</a>	
                                                         <div class="info">																	
                                                            <a href="javascript:void(0)" class="view-likes" data-id='photo-1' data-section='photos'><span class="glyphicon glyphicon-thumbs-up"></span>56</a>			
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="grid-box">
                                                <div class="photo-box">
                                                   <div class="imgholder">
                                                      <figure>
                                                         <a href="images/post-img4.jpg" data-size="1600x1600" data-med="images/post-img4.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="himg-box">
                                                         <img class="himg" src="images/post-img4.jpg"/>
                                                         </a>
                                                      </figure>
                                                   </div>
                                                   <div class="edit-link">
                                                      <div class="dropdown dropdown-custom ">
                                                         <a class="dropdown-button " href="javascript:void(0)" data-activates="album_setting4">
                                                         <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                                         </a>
                                                         <ul id="album_setting4" class="dropdown-small dropdown-content custom_dropdown ">
                                                            <li><a href="javascript:void(0)">Delete this photo</a></li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                   <div class="descholder">
                                                      <a href="javascript:void(0)" class="namelink"><span>Album Name</span></a>
                                                      <div class="options">
                                                         <a href="javascript:void(0)">Like</a>	
                                                         <div class="info">																	
                                                            <a href="javascript:void(0)" class="view-likes" data-id='photo-1' data-section='photos'><span class="glyphicon glyphicon-thumbs-up"></span>56</a>			
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="grid-box">
                                                <div class="photo-box">
                                                   <div class="imgholder">
                                                      <figure>
                                                         <a href="images/album5.png" data-size="1600x1600" data-med="images/album5.png" data-med-size="1024x1024" data-author="Folkert Gorter" class="himg-box">
                                                         <img class="himg" src="images/album5.png"/>
                                                         </a>																			
                                                      </figure>
                                                   </div>
                                                   <div class="edit-link">
                                                      <div class="dropdown dropdown-custom ">
                                                         <a class="dropdown-button " href="javascript:void(0)" data-activates="album_setting5">
                                                         <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                                         </a>
                                                         <ul id="album_setting5" class="dropdown-small dropdown-content custom_dropdown ">
                                                            <li><a href="javascript:void(0)">Delete this photo</a></li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                   <div class="descholder">
                                                      <a href="javascript:void(0)" class="namelink"><span>Album Name</span></a>
                                                      <div class="options">
                                                         <a href="javascript:void(0)">Like</a>	
                                                         <div class="info">																	
                                                            <a href="javascript:void(0)" class="view-likes" data-id='photo-1' data-section='photos'><span class="glyphicon glyphicon-thumbs-up"></span>56</a>			
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="grid-box">
                                                <div class="photo-box">
                                                   <div class="imgholder">
                                                      <figure>
                                                         <a href="images/post-img3.jpg" data-size="1600x1600" data-med="images/post-img3.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="vimg-box">
                                                         <img class="vimg" src="images/post-img3.jpg"/>
                                                         </a>
                                                      </figure>
                                                   </div>
                                                   <div class="edit-link">
                                                      <div class="dropdown dropdown-custom ">
                                                         <a class="dropdown-button " href="javascript:void(0)" data-activates="album_setting6">
                                                         <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                                         </a>
                                                         <ul id="album_setting6" class="dropdown-small dropdown-content custom_dropdown ">
                                                            <li><a href="javascript:void(0)">Delete this photo</a></li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                   <div class="descholder">
                                                      <a href="javascript:void(0)" class="namelink"><span>Album Name</span></a>
                                                      <div class="options">
                                                         <a href="javascript:void(0)">Like</a>	
                                                         <div class="info">																	
                                                            <a href="javascript:void(0)" class="view-likes" data-id='photo-1' data-section='photos'><span class="glyphicon glyphicon-thumbs-up"></span>56</a>			
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="grid-box">
                                                <div class="photo-box">
                                                   <div class="imgholder">
                                                      <figure>
                                                         <a href="images/album7.png" data-size="1600x1600" data-med="images/album7.png" data-med-size="1024x1024" data-author="Folkert Gorter" class="Himg-box">
                                                         <img class="Himg" src="images/album7.png"/>
                                                         </a>
                                                      </figure>
                                                   </div>
                                                   <div class="edit-link">
                                                      <div class="dropdown dropdown-custom ">
                                                         <a class="dropdown-button " href="javascript:void(0)" data-activates="album_setting7">
                                                         <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                                         </a>
                                                         <ul id="album_setting7" class="dropdown-small dropdown-content custom_dropdown ">
                                                            <li><a href="javascript:void(0)">Delete this photo</a></li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                   <div class="descholder">
                                                      <a href="javascript:void(0)" class="namelink"><span>Album Name</span></a>
                                                      <div class="options">
                                                         <a href="javascript:void(0)">Like</a>	
                                                         <div class="info">																	
                                                            <a href="javascript:void(0)" class="view-likes" data-id='photo-1' data-section='photos'><span class="glyphicon glyphicon-thumbs-up"></span>56</a>			
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="tab-pane fade main-pane" id="commevents-invite">
                                 <span class="mob-title"><i class="mdi mdi-bullhorn"></i>Invite Connections</span>
                                 <div class="content-box bshadow inner-content-box">
                                    <div class="cbox-title">
                                       <h5>
                                          <i class="mdi mdi-bullhorn"></i>
                                          Invite Connections
                                       </h5>
                                    </div>
                                    <div class="cbox-desc">
                                       <div class="likes-summery">
                                          <div class="connect-likes">
                                             <h5><a href="javascript:void(0)">6 Connections</a> are attending International Children Festival</h5>
                                             <ul>
                                                <li><a href="javascript:void(0)"><img src="images/people-1.png"/></a></li>
                                                <li><a href="javascript:void(0)"><img src="images/people-2.png"/></a></li>
                                                <li><a href="javascript:void(0)"><img src="images/people-3.png"/></a></li>
                                                <li><a href="javascript:void(0)"><img src="images/people-1.png"/></a></li>
                                                <li><a href="javascript:void(0)"><img src="images/people-2.png"/></a></li>
                                                <li><a href="javascript:void(0)"><img src="images/people-3.png"/></a></li>
                                             </ul>
                                          </div>
                                          <div class="invite-likes">
                                             <p>Invite your connections to attend this event<a href="javascript:void(0)">See All</a></p>
                                             <div class="invite-holder">
                                                <form>
                                                   <div class="tholder">
                                                      <div class="sliding-middle-out anim-area underlined">
                                                         <input type="text" placeholder="Search connect's name"/>
                                                         <a href="javascript:void(0)"><img src="images/cross-icon.png"/></a>
                                                      </div>
                                                   </div>
                                                </form>
                                                <div class="list-holder">
                                                   <ul>
                                                      <li>
                                                         <div class="invitelike-connect">
                                                            <div class="imgholder"><img src="images/people-1.png"/></div>
                                                            <div class="descholder">
                                                               <h6>Kelly Mark</h6>
                                                               <div class="btn-holder">
                                                                  <a href="javascript:void(0)" class="btn-invite">Invite</a>
                                                                  <a href="javascript:void(0)" class="btn-invite-close"><i class="mdi mdi-close"></i></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </li>
                                                      <li>
                                                         <div class="invitelike-connect">
                                                            <div class="imgholder"><img src="images/people-2.png"/></div>
                                                            <div class="descholder">
                                                               <h6>John Davior</h6>
                                                               <div class="btn-holder">
                                                                  <a href="javascript:void(0)" class="btn-invite">Invite</a>
                                                                  <a href="javascript:void(0)" class="btn-invite-close"><i class="mdi mdi-close"></i></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </li>
                                                      <li>
                                                         <div class="invitelike-connect">
                                                            <div class="imgholder"><img src="images/people-2.png"/></div>
                                                            <div class="descholder">
                                                               <h6>Joe Doe</h6>
                                                               <div class="btn-holder">
                                                                  <a href="javascript:void(0)" class="btn-invite">Invite</a>
                                                                  <a href="javascript:void(0)" class="btn-invite-close"><i class="mdi mdi-close"></i></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </li>
                                                      <li>
                                                         <div class="invitelike-connect">
                                                            <div class="imgholder"><img src="images/people-3.png"/></div>
                                                            <div class="descholder">
                                                               <h6>Kelly Mark</h6>
                                                               <div class="btn-holder">
                                                                  <a href="javascript:void(0)" class="btn-invite">Invite</a>
                                                                  <a href="javascript:void(0)" class="btn-invite-close"><i class="mdi mdi-close"></i></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </li>
                                                      <li>
                                                         <div class="invitelike-connect">
                                                            <div class="imgholder"><img src="images/people-2.png"/></div>
                                                            <div class="descholder">
                                                               <h6>John Davior</h6>
                                                               <div class="btn-holder">
                                                                  <a href="javascript:void(0)" class="btn-invite">Invite</a>
                                                                  <a href="javascript:void(0)" class="btn-invite-close"><i class="mdi mdi-close"></i></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="tab-pane fade main-pane" id="commevents-discussion">
                                 <span class="mob-title"><i class="mdi mdi-newspaper"></i>Discussion</span>
                                 <?php include('common/new_post.php'); ?> 
                              </div>
                              <div class="tab-pane fade main-pane active in" id="commevents-about">
                                 <span class="mob-title"><i class="mdi mdi-information"></i>About Event</span>
                                 <div class="content-box bshadow inner-content-box">
                                    <div class="event-info">
                                       <div class="cbox-title">
                                          <h5>
                                             <i class="mdi mdi-information"></i>
                                             About Event
                                          </h5>
                                       </div>
                                       <div class="cbox-desc">
                                          <ul class="section-ul">
                                             <li>
                                                <div class="eventinfo">
                                                   <div class="dateinfo">
                                                      <span class="month">Nov</span>
                                                      <span class="date">17</span>
                                                   </div>
                                                   <div class="descinfo">
                                                      <h4>International Children Festival</h4>
                                                      <p><span>Public</span> : Organized by <a href="javascript:void(0)">Adel Hasanat</a></p>
                                                   </div>
                                                </div>
                                                <div class="clear"></div>
                                                <ul class="eventinfo-ul">
                                                   <li>
                                                      <div class="eventinfo-row">
                                                         <span class="iconholder"><i class="mdi mdi-clock-outline"></i></span>
                                                         <span class="descholder">
                                                            <h5>Today at 1 PM - 4 PM</h5>
                                                            <p>Starts in about 1 hour</p>
                                                         </span>
                                                      </div>
                                                   </li>
                                                   <li>
                                                      <div class="eventinfo-row">
                                                         <span class="iconholder"><i class="zmdi zmdi-pin"></i></span>
                                                         <span class="descholder">
                                                            <h5>Qatar Univeristy</h5>
                                                            <p>Amman, Qatar</p>
                                                            <a href="javascript:void(0)" onclick="showEventMap(this)">Show map</a>
                                                         </span>
                                                         <div class="mapholder">
                                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3110.3465133386144!2d-9.167423685010494!3d38.77868997958898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd193295d5b45545%3A0x3f9e7b6a5f00e12c!2sPerta!5e0!3m2!1sen!2sin!4v1481089901870" width="600" height="450" frameborder="0"  allowfullscreen></iframe>
                                                         </div>
                                                      </div>
                                                   </li>
                                                   <li>
                                                      <div class="eventinfo-row">
                                                         <span class="iconholder"><i class="mdi mdi-ticket"></i></span>
                                                         <span class="descholder">
                                                            <h5>Ticket Information</h5>
                                                            <p>docs.google.com</p>
                                                            <a href="javascript:void(0)">Find Tickets</a>
                                                         </span>
                                                      </div>
                                                   </li>
                                                   <li>
                                                      <div class="eventinfo-row">
                                                         <span class="iconholder"><i class="mdi mdi-car"></i></span>
                                                         <span class="descholder">
                                                            <h5>Parking Information</h5>
                                                            <p>docs.google.com</p>
                                                            <a href="javascript:void(0)">Find Tickets</a>
                                                         </span>
                                                      </div>
                                                   </li>
                                                   <li>
                                                      <div class="eventinfo-row">
                                                         <span class="iconholder"><i class="mdi mdi-link"></i></span>
                                                         <span class="descholder">
                                                            <h5>Website</h5>
                                                            <p>www.google.com</p>
                                                         </span>
                                                      </div>
                                                   </li>
                                                </ul>
                                             </li>
                                             <li>
                                                <div class="row">
                                                   <div class="col-sm-12 col-xs-12">
                                                      <label>Event's Organizer</label>
                                                   </div>
                                                   <div class="col-sm-12 col-xs-12">
                                                      <ul class="member-ul">
                                                         <li>
                                                            <div class="member-box">
                                                               <a href="javascript:void(0)">
                                                               <img class="circle" src="images/demo-profile.jpg"/>
                                                               <span>Adel Hasanat</span>
                                                               </a>
                                                            </div>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="content-box bshadow inner-content-box">
                                    <div class="event-info">
                                       <div class="cbox-title">
                                          <h5>																	
                                             Details
                                          </h5>
                                       </div>
                                       <div class="post-desc white-box">
                                          <div class="para-section">
                                             <div class="para">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.
                                                   <br><br>
                                                   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.
                                                </p>
                                             </div>
                                             <a href="javascript:void(0)" class="readlink">Read More</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php include('common/chat.php'); ?>				
         <div class="new-post-mobile clear">
            <a class="popup-window composetoolboxAction" href="javascript:void(0)"><i class="mdi mdi-pencil"></i></a>
         </div>
      </div>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>	
<?php  include('common/preference_popup.php'); ?>
<!--collection followers modal -->
<div id="collection_followers" class="modal item_members">
   <div class="modal_header">
      <button class="close_btn custom_modal_close_btn close_modal waves-effect">
      <i class="mdi mdi-close mdi-20px"></i>
      </button>
      <h3>Collection followers</h3>
   </div>
   <div class="custom_modal_content modal_content">
      <div class="follower-container">
         <ul>
            <li>
               <div class="follwer_profile">
                  <img class="circle" src="images/demo-profile.jpg" />
               </div>
               <div class="follwer_name">
                  <span>User Name</span>
               </div>
            </li>
            <li>
               <div class="follwer_profile">
                  <img class="circle" src="images/demo-profile.jpg" />
               </div>
               <div class="follwer_name">
                  <span>User Name</span>
               </div>
            </li>
            <li>
               <div class="follwer_profile">
                  <img class="circle" src="images/demo-profile.jpg" />
               </div>
               <div class="follwer_name">
                  <span>User Name</span>
               </div>
            </li>
            <li>
               <div class="follwer_profile">
                  <img class="circle" src="images/demo-profile.jpg" />
               </div>
               <div class="follwer_name">
                  <span>User Name</span>
               </div>
            </li>
            <li>
               <div class="follwer_profile">
                  <img class="circle" src="images/demo-profile.jpg" />
               </div>
               <div class="follwer_name">
                  <span>User Name</span>
               </div>
            </li>
         </ul>
      </div>
   </div>
</div>
<!--map modal-->
<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">
   <?php include('common/map_modal.php'); ?>
</div>
<?php include('common/uploadphoto_popup.php'); ?>
<?php include('common/addperson_popup.php'); ?>
<?php include('common/reportpost-popup.php'); ?>
<?php include('common/compose_post_popup.php'); ?>
<?php include('common/comment_popup.php'); ?>
<?php include('common/postopen_popup.php'); ?>

<?php  include('common/share_popup.php'); ?>

<?php include('common/editpost_popup.php'); ?>

<?php include('common/upload_img_box.php'); ?>
<!--add event modal-->
<div id="add_event_modal" class="modal add-item-popup custom_md_modal">
   <div class="modal_content_container">
      <div class="modal_content_child modal-content">
         <div class="popup-title">
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>	
            <h3>Create Event</h3>
            <a type="button" class="item_done crop_done hidden_close_span close_modal waves-effect" href="javascript:void(0)" >Done</a>
         </div>
         <div class="main-pcontent">
            <form class="add-item-form">
               <div class="frow frowfull">
                  <div class="crop-holder" id="image-cropper">
                     <div class="cropit-preview"></div>
                     <div class="main-img">
                        <img src="images/additem-commevents.png">
                     </div>
                     <div class="main-img1">
                        <img id="imageid" draggable="false"/>
                     </div>
                     <div class="btnupload custom_up_load" id="upload_img_action">
                        <div class="fileUpload">
                           <i class="zmdi zmdi-camera zmdi-hc-lg"></i>
                           <input type="file" name="filupload" id="crop-file" class="upload cropit-image-input" />
                        </div>
                     </div>
                     <a  href="javascript:void(0)" class="btn btn-save image_save_btn image_save dis-none">
                     <span class="zmdi zmdi-check"></span>
                     </a>
                     <a id="removeimg" href="javascript:void(0)" class="collection_image_trash image_trash">
                     <i class="mdi mdi-close mdi-20px"></i>
                     </a>
                  </div>
               </div>
               <div class="sidepad">
                  <div class="frow">
                     <input id="event_title" type="text" class="validate item_title" placeholder="Event title" />
                  </div>
                  <div class="frow">
                     <textarea id="Collection_tagline" class="materialize-textarea mb0 md_textarea item_tagline" placeholder="Event tagline"></textarea>
                     <span class="char-limit">0/80</span>								
                  </div>
                  <div class="frow">
                     <input type="text" placeholder="Address" data-query="all" onfocus="filderMapLocationModal(this)" class="materialize-textarea md_textarea item_address" autocomplete="off"/>
                  </div>
                  <div class="frow">
                     <div class="row">
                        <div class="col s6">
                           <input type="text" data-toggle="datepicker" class="datepickerinput" data-query="all" placeholder="Date" readonly/>
                        </div>
                        <div class="col s6">
                           <input type="text" class="timepicker" placeholder="Time"/>
                        </div>
                     </div>
                  </div>
                  <div class="frow">
                     <textarea type="text" placeholder="Tell people more about the event" class="materialize-textarea md_textarea item_about"></textarea>
                  </div>
                  <div class="frow security-area">
                     <label>Privacy</label>
                     <div class="right">
                        <a class="dropdown_text dropdown-button" href="javascript:void(0)" data-activates="collection_privacy">
                        <span>Public</span>
                        <i class="mdi mdi-menu-down"></i>
                        </a>
                        <ul id="collection_privacy" class="dropdown-content  new_drop_colle">
                           <li> <a href="javascript:void(0)">Private</a> </li>
                           <li> <a href="javascript:void(0)">Connections</a> </li>
                           <li class="customli_modal"> <a href="javascript:void(0)">Custom</a> </li>
                           <li> <a href="javascript:void(0)">Public</a> </li>
                        </ul>
                     </div>
                  </div>
                  <div class="frow">
                     <div class="expandable-holder">
                        <a href="javascript:void(0)" class="expand-link invertsign" onclick="mng_expandable(this)">Advanced Option <i class="mdi mdi-menu-down"></i></a>
                        <div class="expandable-area">
                           <div class="frow">
                              <input type="text" placeholder="Website URL (optional)" class="materialize-textarea md_textarea item_website"/>
                           </div>
                           <div class="frow">
                              <input type="text" placeholder="Ticket seller URL (optional)" class="materialize-textarea md_textarea item_ticket"/>
                           </div>
                           <div class="frow">
                              <input type="text" placeholder="Youtube URL (optional)" class="materialize-textarea md_textarea item_youtube"/>
                           </div>
                           <div class="frow">
                              <input type="text" placeholder="Transit and parking information (optional)" class="materialize-textarea md_textarea item_parking"/>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="valign-wrapper additem_modal_footer modal-footer">
      <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">Cancel</a>
      <a href="javascript:void(0)" class="btngen-center-align waves-effect" data-class="addbtn">Create</a>
   </div>
</div>
<div id="managemembers-popup" class="modal manage-modal managemembers-popup">
   <div class="modal_header">
      <button class="close_btn custom_modal_close_btn close_modal waves-effect">
      <i class="mdi mdi-close mdi-20px"></i>
      </button>
      <h3>Manage Members</h3>
   </div>
   <div class="custom_modal_content modal_content">
      <div class="main-pcontent spadding">
         <ul class="tabs">
            <li class="tab"><a href="#member-request" data-toggle="tab" aria-expanded="false">Member Requested</a></li>
            <li class="tab"><a href="#member-all" class="active" data-toggle="tab" aria-expanded="true">All Members</a></li>
         </ul>
         <div class="tab-content">
            <div id="member-request" class="tab-pane fade">
               <!--<h5>Member requests</h5>-->
               <ul class="manage-members">
                  <li>
                     <div class="member-li">
                        <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                        <div class="descholder">
                           <span class="head6">User Name</span>
                           <div class="settings">
                              <a href="javascript:void(0)" class="btn btn-primary btn-sm waves-effect waves-light">Accept</a>
                              <a href="javascript:void(0)" class="btn btn-primary btn-sm waves-effect waves-light">Reject</a>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li>
                     <div class="member-li">
                        <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                        <div class="descholder">
                           <span class="head6">User Name</span>
                           <div class="settings">
                              <a href="javascript:void(0)" class="btn btn-primary btn-sm waves-effect waves-light">Accept</a>
                              <a href="javascript:void(0)" class="btn btn-primary btn-sm waves-effect waves-light">Reject</a>
                           </div>
                        </div>
                     </div>
                  </li>
               </ul>
            </div>
            <div id="member-all" class="tab-pane fade active">
               <!--<h5>All Members</h5>-->
               <ul class="manage-members">
                  <li>
                     <div class="member-li">
                        <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                        <div class="descholder">
                           <span class="head6">User Name</span>
                           <div class="settings">
                              <span class="status">Admin</span>
                              <div class="right">
                                 <a class="dropdown-button " href="javascript:void(0)" data-activates="mm_settings1">
                                 <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                 </a>
                                 <ul id="mm_settings1" class="dropdown-content custom_dropdown">
                                    <li><a href="javascript:void(0)">Option</a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li>
                     <div class="member-li">
                        <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                        <div class="descholder">
                           <span class="head6">User Name</span>
                           <div class="settings">
                              <span class="status">Member</span>
                              <div class="right">
                                 <a class="dropdown-button " href="javascript:void(0)" data-activates="mm_settings2">
                                 <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                 </a>
                                 <ul id="mm_settings2" class="dropdown-content custom_dropdown">
                                    <li><a href="javascript:void(0)">Remove member</a></li>
                                    <li><a href="javascript:void(0)">Ban member</a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li>
                     <div class="member-li">
                        <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                        <div class="descholder">
                           <span class="head6">User Name</span>
                           <div class="settings">
                              <span class="status">Member</span>
                              <div class="right">
                                 <a class="dropdown-button " href="javascript:void(0)" data-activates="mm_settings3">
                                 <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                 </a>
                                 <ul id="mm_settings3" class="dropdown-content custom_dropdown">
                                    <li><a href="javascript:void(0)">Remove member</a></li>
                                    <li><a href="javascript:void(0)">Ban member</a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li>
                     <div class="member-li">
                        <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                        <div class="descholder">
                           <span class="head6">User Name</span>
                           <div class="settings">
                              <span class="status">Admin</span>
                              <div class="right">
                                 <a class="dropdown-button " href="javascript:void(0)" data-activates="mm_settings1">
                                 <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                 </a>
                                 <ul id="mm_settings1" class="dropdown-content custom_dropdown">
                                    <li><a href="javascript:void(0)">Option</a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li>
                     <div class="member-li">
                        <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                        <div class="descholder">
                           <span class="head6">User Name</span>
                           <div class="settings">
                              <span class="status">Member</span>
                              <div class="right">
                                 <a class="dropdown-button " href="javascript:void(0)" data-activates="mm_settings2">
                                 <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                 </a>
                                 <ul id="mm_settings2" class="dropdown-content custom_dropdown">
                                    <li><a href="javascript:void(0)">Remove member</a></li>
                                    <li><a href="javascript:void(0)">Ban member</a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li>
                     <div class="member-li">
                        <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                        <div class="descholder">
                           <span class="head6">User Name</span>
                           <div class="settings">
                              <span class="status">Member</span>
                              <div class="right">
                                 <a class="dropdown-button " href="javascript:void(0)" data-activates="mm_settings3">
                                 <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                 </a>
                                 <ul id="mm_settings3" class="dropdown-content custom_dropdown">
                                    <li><a href="javascript:void(0)">Remove member</a></li>
                                    <li><a href="javascript:void(0)">Ban member</a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li>
                     <div class="member-li">
                        <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                        <div class="descholder">
                           <span class="head6">User Name</span>
                           <div class="settings">
                              <span class="status">Admin</span>
                              <div class="right">
                                 <a class="dropdown-button " href="javascript:void(0)" data-activates="mm_settings1">
                                 <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                 </a>
                                 <ul id="mm_settings1" class="dropdown-content custom_dropdown">
                                    <li><a href="javascript:void(0)">Option</a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li>
                     <div class="member-li">
                        <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                        <div class="descholder">
                           <span class="head6">User Name</span>
                           <div class="settings">
                              <span class="status">Member</span>
                              <div class="right">
                                 <a class="dropdown-button " href="javascript:void(0)" data-activates="mm_settings2">
                                 <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                 </a>
                                 <ul id="mm_settings2" class="dropdown-content custom_dropdown">
                                    <li><a href="javascript:void(0)">Remove member</a></li>
                                    <li><a href="javascript:void(0)">Ban member</a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li>
                     <div class="member-li">
                        <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                        <div class="descholder">
                           <span class="head6">User Name</span>
                           <div class="settings">
                              <span class="status">Member</span>
                              <div class="right">
                                 <a class="dropdown-button " href="javascript:void(0)" data-activates="mm_settings3">
                                 <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                 </a>
                                 <ul id="mm_settings3" class="dropdown-content custom_dropdown">
                                    <li><a href="javascript:void(0)">Remove member</a></li>
                                    <li><a href="javascript:void(0)">Ban member</a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li>
                     <div class="member-li">
                        <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                        <div class="descholder">
                           <span class="head6">User Name</span>
                           <div class="settings">
                              <span class="status">Admin</span>
                              <div class="right">
                                 <a class="dropdown-button " href="javascript:void(0)" data-activates="mm_settings1">
                                 <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                 </a>
                                 <ul id="mm_settings1" class="dropdown-content custom_dropdown">
                                    <li><a href="javascript:void(0)">Option</a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li>
                     <div class="member-li">
                        <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                        <div class="descholder">
                           <span class="head6">User Name</span>
                           <div class="settings">
                              <span class="status">Member</span>
                              <div class="right">
                                 <a class="dropdown-button " href="javascript:void(0)" data-activates="mm_settings2">
                                 <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                 </a>
                                 <ul id="mm_settings2" class="dropdown-content custom_dropdown">
                                    <li><a href="javascript:void(0)">Remove member</a></li>
                                    <li><a href="javascript:void(0)">Ban member</a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li>
                     <div class="member-li">
                        <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                        <div class="descholder">
                           <span class="head6">User Name</span>
                           <div class="settings">
                              <span class="status">Member</span>
                              <div class="right">
                                 <a class="dropdown-button " href="javascript:void(0)" data-activates="mm_settings3">
                                 <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                 </a>
                                 <ul id="mm_settings3" class="dropdown-content custom_dropdown">
                                    <li><a href="javascript:void(0)">Remove member</a></li>
                                    <li><a href="javascript:void(0)">Ban member</a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="community-event-datepicker">
   <?php include('common/datepicker.php'); ?>
</div>
<?php include('common/custom_modal.php'); ?>

<?php include('common/add_photo_popup.php'); ?>
<?php include('common/discard_popup.php'); ?>

<?php include("script.php"); ?>		
</body>
</html>